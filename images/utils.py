import fnmatch
from difflib import SequenceMatcher
from typing import List, Dict

import numpy as np
from PIL import Image, ImageEnhance, ImageOps


def enhance(img: Image.Image, fn_name: str, factor: float) -> Image.Image:
    """

    Args:
        img: A Pillow Image object (PIL.Image.Image)
        fn_name:
        factor:

    Returns:
        img: A Pillow Image object (PIL.Image.Image)
    """

    if fn_name == 'enhance_contrast':
        enhancer = ImageEnhance.Contrast(img)
        img = enhancer.enhance(factor)

    elif fn_name == 'enhance_brightness':
        enhancer = ImageEnhance.Brightness(img)
        img = enhancer.enhance(factor)

    elif fn_name == 'enhance_sharpness':
        enhancer = ImageEnhance.Sharpness(img)
        img = enhancer.enhance(factor)

    elif fn_name == 'enhance_color':
        enhancer = ImageEnhance.Color(img)
        img = enhancer.enhance(factor)

    elif fn_name == 'autocontrast':
        img = ImageOps.autocontrast(img, cutoff=0.3, ignore=None)

    elif fn_name == 'equalize':
        img = ImageOps.equalize(img, mask=None)

    return img


def resize(img: Image.Image, resize_ratio: float) -> Image.Image:
    """

    Args:
        img: A Pillow Image object (PIL.Image.Image)
        resize_ratio:

    Returns:
        img: A Pillow Image object (PIL.Image.Image)
    """
    return img.resize((int(img.size[0]*resize_ratio), int(img.size[1]*resize_ratio)), resample=Image.LANCZOS)


def crop(img: Image.Image, x_px: List[float], y_px: List[float], width_crop: int, height_crop: int) -> Image.Image:
    """

    Args:
        img: A Pillow Image object (PIL.Image.Image)
        x_px:
        y_px:
        width_crop:
        height_crop:

    Returns:
        img: A Pillow Image object (PIL.Image.Image)
    """
    width_img, height_img = img.size

    left = int(np.max([x_px - width_crop / 2, 0]))
    top = int(np.max([y_px - height_crop / 2, 0]))
    right = round(left + width_crop)
    bottom = round(top + height_crop)

    if right > width_img:
        right = width_img
        left = right - width_crop

    if bottom > height_img:
        bottom = height_img
        top = bottom - height_crop

    return img.crop((left, top, right, bottom))


def stitch_images(imgs: List[Image.Image], nb_cam: int) -> Image.Image:
    """

    Args:
        imgs: List of Pillow Image objects (PIL.Image.Image)
        nb_cam:

    Returns:
        img: A Pillow Image object (PIL.Image.Image)
    """
    img = imgs[1]
    for camn in range(2, nb_cam + 1):
        img2 = imgs[camn]

        dst = Image.new('P', (img.width + img2.width, img.height))
        dst.paste(img, (0, 0))
        dst.paste(img2, (img.width, 0))
        img = dst.copy()

    return img


def save_tiff(img: Image.Image, save_path: str):
    """

    Args:
        img: A Pillow Image object (PIL.Image.Image)
        save_path:
    """
    array = np.uint8(np.array(img) / 256)
    Image.fromarray(array).save(save_path, compression='lzw')


def find_common_substrings(str_list: List[str], keep_shifted_matched: bool = True, keep_only_counts_higher_than: int = None,
                           return_nun_matches_instead_occurrences: bool = False, ) -> Dict[str, int]:
    """

    Args:
        str_list:
        keep_shifted_matched:
        keep_only_counts_higher_than:
        return_nun_matches_instead_occurrences:

    Returns:
        common_substrings:
        counts:
    """

    common_substrings_dict = {}
    for i in range(0, len(str_list)):

        for j in range(i+1, len(str_list)):

            string1 = str_list[i]
            string2 = str_list[j]

            match = SequenceMatcher(None, string1, string2).find_longest_match(0, len(string1), 0, len(string2))

            if not keep_shifted_matched and match.a != match.b: continue

            matching_substring = string1[match.a:match.a+match.size]

            if matching_substring not in common_substrings_dict:
                common_substrings_dict[matching_substring] = 1
            else:
                common_substrings_dict[matching_substring] += 1

    if keep_only_counts_higher_than is not None:
        common_substrings_dict = {key: common_substrings_dict[key] for key in common_substrings_dict.keys()
                                  if common_substrings_dict[key] >= keep_only_counts_higher_than}

    if not return_nun_matches_instead_occurrences:
        for key in common_substrings_dict.keys():
            common_substrings_dict[key] = sum(key in s for s in str_list)

    # Sort in descending order
    common_substrings_dict = dict(sorted(common_substrings_dict.items(), key=lambda item: item[1], reverse=True))

    common_substrings, counts = list(common_substrings_dict.keys()), list(common_substrings_dict.values())

    return common_substrings, counts


def find_common_substrings_with_one_separator(str_list: List[str], nb_str: int = 1) -> Dict[str, int]:
    """ Will find the most common substring with separator (*, so non shifted matches of two longest substring),
        between the first n string and all the other strings in the given list

    Args:
        str_list: List of strings, usually file names
        (e.g. ['cam1_202205040001-obj1.tif', 'cam1_202205040002-obj1.tif', 'cam1_202205040003-obj1.tif', ...]
                -> then most common substrings with separator is 'cam1_20220504000*-obj1.tif')
        nb_str: the number of str that will be compared to all the other str

    Return:
        common_substrings:
        counts:
    """

    assert 1 <= nb_str <= len(str_list)

    common_substrings_dict = {}
    for i in range(0, nb_str):
        for j in range(i+1, len(str_list)):

            string1 = str_list[i]
            string2 = str_list[j]

            matches = SequenceMatcher(None, string1, string2).get_matching_blocks()

            if len(matches)-1 < 2: continue  # If there is less than two common substrings
            if matches[0].a != matches[0].b and matches[1].a != matches[1].b: continue  # If the matches are shifted

            matching_substring = string1[matches[0].a:matches[0].a+matches[0].size] + '*' + string1[matches[-2].a:matches[-2].a+matches[-2].size]

            if matching_substring not in common_substrings_dict:
                common_substrings_dict[matching_substring] = 1
            else:
                common_substrings_dict[matching_substring] += 1

    for key in common_substrings_dict.keys():
        common_substrings_dict[key] = len(fnmatch.filter(str_list, key))

    # Sort in descending order
    common_substrings_dict = dict(sorted(common_substrings_dict.items(), key=lambda item: item[1], reverse=True))

    common_substrings, counts = list(common_substrings_dict.keys()), list(common_substrings_dict.values())

    return common_substrings, counts


def find_uncommon_substrings(str_list: List[str], common_substring) -> List[str]:
    """

    Args:
        str_list:
        common_substring:

    Returns:
        uncommon_substrings:
    """

    ind_star = common_substring.find('*')
    uncommon_substrings = [s[ind_star:ind_star-len(common_substring)+1] for s in str_list]

    return uncommon_substrings


def find_file_numbers(str_list: List[str], common_substring) -> List[int]:
    """

    Args:
        str_list:
        common_substring:

    Returns:
        file_numbers:
    """

    ind_star = common_substring.find('*')
    file_numbers = [int(s[ind_star:s.rfind('.')]) for s in str_list]

    return file_numbers

