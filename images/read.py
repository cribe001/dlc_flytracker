
import numpy as np
from PIL import Image

from typing import List


def read_image(path: str, bit_shift: str = 'full', custom_range: List[int] = []) -> Image.Image:
    """

    Args:
        path:
        bit_shift:
        custom_range:

    Returns:
        img: A Pillow Image object (PIL.Image.Image)
    """

    img = Image.open(path)

    if img.mode is not 'L':
        img_16bit = np.asarray(img)

        if type(img_16bit.max()) is not np.uint16:
            img = Image.fromarray(np.zeros(img.size, dtype=np.uint8)).convert('L')
            return img  # Return black images

        if str(bit_shift) == 'full':  # use the full 12bit
            if img_16bit.max() > 1024.0:
                # For cam that have a different range of values
                img_8bit = ((img_16bit - 512.0) / (1024.0 / 255.0)).astype(np.uint8)

            else:
                img_8bit = (img_16bit / (1024.0 / 255.0)).astype(np.uint8)

        elif str(bit_shift) == 'high':  # use the high 8bit
            img_16bit[img_16bit > 308.0] = 308.0
            img_8bit = (img_16bit / (308.0 / 255.0)).astype(np.uint8)

        elif str(bit_shift) == 'low':  # use the low 8bit
            img_8bit = ((img_16bit - 154.0) / (1024.0 / 255.0)).astype(np.uint8)

        elif str(bit_shift) == 'minmax':  # map the range to 0 - 255
            img_8bit = ((img_16bit - img_16bit.min()) / (img_16bit.ptp() / 255.0)).astype(np.uint8)

        elif str(bit_shift) == 'custom':  # map a custom range
            img_8bit = ((img_16bit - custom_range[0]) / ((custom_range[1] - custom_range[0]) / 255.0)).astype(np.uint8)

        else:
            raise ValueError('bit_shift ({0}) is incorrect'.format(bit_shift))

        img = Image.fromarray(img_8bit).convert('L')
        # img.show()

    return img


if __name__ == '__main__':

    path = '/media/user/MosquitoEscape_Photron2/Photron2/cam2_20200216_112753/cam2_20200216_1127532380.tif'

    img = read_image(path)

    print(img)
    print(img.size)


