import os
import re
from typing import List, Tuple

import numpy as np

from images.utils import find_common_substrings_with_one_separator, find_file_numbers


__author__ = "C.J. Voesenek and A. Cribellier"
__maintainer__ = "A. Cribellier"
__email__ = "antoine.cribellier@wur.nl"


class ImageSequence:
    """Sequence of images on disk.

    This object can be indexed like an array (i.e. sequence[0]), retrieving
    the nth image from the sequence.
    """

    def __init__(self, names: List[str] = None, path: str = None) -> None:
        """Initialises an ImageSequence object.

        Args:
            names: Names of the images in the sequence
            path: The path of the directory with the images
        """

        assert names is not None

        self._names = names  # type: List[str]
        self._names.sort()

        self._path = path  # type: str

        # Will try to get image numbers from their names
        common_substrings, counts = find_common_substrings_with_one_separator(self._names)  # Takes too long to check for all images
        most_common_substring, num_occurrences = common_substrings[0], counts[0]

        if num_occurrences == len(self._names):
            # uncommon_substrings = find_uncommon_substrings(self._names, most_common_substring)
            # self._numbers = [int(s) for s in uncommon_substrings]  # type: List[int]
            self._numbers = find_file_numbers(self._names, most_common_substring)  # type: List[int]
            assert len(self._numbers) == len(self._names)

            ind_sort = np.argsort(self._numbers)
            self._numbers = [self._numbers[i] for i in ind_sort]
            self._names = [self._names[i] for i in ind_sort]

        else:
            self._numbers = list(range(1, len(self._names)+1))  # type: List[int]

    @property
    def nb_images(self) -> int:
        return len(self._names)

    @property
    def nb_xi(self) -> int:
        return self[0].nb_xi

    @property
    def nb_eta(self) -> int:
        return self[0].nb_eta

    @property
    def shape(self) -> Tuple[int, int]:
        return self[0].shape

    def get_name(self, index: int) -> str:
        """Retrieves the name of an image from the sequence.

        Args:
            index: The index of the image.

        Returns:
            The name of the image at the specified index.
        """
        return self._names[index]

    def get_names(self) -> List[str]:
        """Retrieves the names of all the images of the sequence.

        Returns:
            The names of the images
        """
        return self._names

    def get_names_from_numbers(self, numbers: List[str]) -> List[int]:
        """ Retrieves the names of images from their numbers. Assumes that the list of numbers is sorted.

        Args:
            numbers: The numbers of images.

        Returns:
            The names of the image for the specified numbers.
        """

        nb_numbers = len(numbers)

        index = 0
        names = []
        for _index, _number in enumerate(self._numbers):
            while index < nb_numbers and _number == numbers[index]:
                names.append(self.get_name(_index))
                index += 1

        assert len(numbers) == nb_numbers

        return names

    def get_number(self, index: int) -> int:
        """Retrieves the number of an image from the sequence.

        Args:
            index: The index of the image.

        Returns:
            The number of the image at the specified index.
        """
        return self._numbers[index]

    def get_numbers(self) -> List[int]:
        """Retrieves the numbers of all the images of the sequence.

        Returns:
            The numbers of the images (e.g. frame number)
        """
        return self._numbers

    def get_numbers_from_names(self, names: List[str]) -> List[int]:
        """Retrieves the number of images from their names. Assumes that the list of names is sorted.

        Args:
            names: The names of images.

        Returns:
            The number of the image for the specified names.
        """

        nb_names = len(names)

        index = 0
        numbers = []
        for _index, _name in enumerate(self._names):
            while index < nb_names and os.path.splitext(_name)[0] in names[index]:
                numbers.append(self.get_number(_index))
                index += 1

        assert len(numbers) == nb_names

        return numbers

    def get_path(self) -> List[str]:
        """Retrieves the path of the directory where the images of the sequence are.

        Returns:
            The path of the directory with the images.
        """
        return self._path

    def delete(self, index) -> None:
        """ Deletes an image from the sequence.
        """

        self._names.pop(index)

    @staticmethod
    def from_directory(directory: str, expr: str = r"\d+.(tif|png|jpg|bmp)",
                       identifiers: List[int] = None) -> "ImageSequence":
        """Gathers image files from directory.

        This function sets the files attribute of the instance to a list of
        files from the specified directory, matching the regular expression
        and optionally the image identifiers. If no image identifiers are
        specified, the list is in alphabetical order. If image identifiers
        are specified, the output is sorted in the same order

        Args:
            directory: The path to a directory containing images.
            expr: Regular expression pattern matching image files.
            identifiers: The identifiers of the images that should be loaded
                from this sequence.

        Raises:
            IOError: Could not match images to all specified identifiers.
            IOError: No image files matching {expr} found in directory
                {directory}.
        """
        contents = os.listdir(directory)
        pattern = re.compile(expr)

        files = []
        file_names = []
        id_list = []
        for file_name in contents:
            match = re.search(pattern, file_name)
            if match:
                # Check whether this file is in the (optionally specified) list
                # of identifiers
                if identifiers:
                    id_cur = int(match.group(0))
                    do_append = id_cur in identifiers
                    if do_append:
                        id_list.append(id_cur)
                else:
                    do_append = True

                if do_append:
                    files.append(os.path.join(directory, file_name))
                    file_names.append(os.path.basename(os.path.normpath(file_name)))

        # If a list of identifiers was specified, check whether we found them all, then sort the list by identifier
        if identifiers is not None:
            if len(identifiers) != len(files):
                raise IOError("Could not match images to all specified identifiers.")
            files.sort(key=dict(zip(files, id_list)).get)

        else:
            files.sort()

        if len(files) == 0:
            raise IOError("No image files matching \"" + expr + "\" found in directory \"" + directory + "\".")

        return ImageSequence(file_names, directory)
