import os
from math import gcd
from typing import Dict, List, Optional, Tuple

import numpy as np

from images import ImageSequence

__author__ = "C.J. Voesenek and A. Cribellier"
__maintainer__ = "A. Cribellier"
__email__ = "antoine.cribellier@wur.nl"


class MultiImageSequence:
    """A combination of multiple synchronised image sequences (a.k.a a recording).

    Attributes:
        names: List of names of the sequences.
        sequences: A dictionary of image sequences, with the names as keys.
        nb_sequences: The number of image sequences in this object.

        index: A dictionary of lists with the indices into an image sequence
            of each overall time step. When images are not defined at a time
            instant, it contains None.

        frame_rate_total: The overall frame rate of this multi-camera sequence
        frame_rates: A dictionary of the frame rates of every sequence,
            with the identifiers as keys.

        time: A list of times at every step.
        nb_time: The number of time steps in this sequence.
    """

    def __init__(self, sequences: Dict[str, ImageSequence],
                 frame_rates: Dict[str, float] or int,
                 shifts: Dict[str, int] = None,
                 paths: List[str] = None) -> None:
        """Initialises a MultiImageSequence.

        Args:
            sequences: A dictionary of image sequences, with the identifiers
                as keys.
            frame_rates: A dictionary of the frame rates of every sequence,
                with the identifiers as keys or an int if the frame_rate is the same for all.
            shifts: A dictionary of shifts in number of frames of the start of
                each sequence with respect to a common time point, with the
                identifiers as keys. This is used when cameras are running at a
                different frame rate, but the video sequences do not start at
                the exact same time instance (as could happen when e.g.,
                and end trigger is used). For example, a camera running at
                half the frame rate of another camera may have a shift of one
                frame w.r.t. the other.
            paths: List of paths to directories containing each with a recording sequence

        Raises:
            ValueError: Specify all image sequences as an ImageSequence object.
        """

        if type(frame_rates) is int:
            frame_rates = {identifier: frame_rates for identifier in self.names}

        # Check whether the dictionaries have matching identifiers.
        valid_keys = True
        for name in sequences.keys():
            valid_keys = valid_keys and name in frame_rates
            if shifts is not None:
                valid_keys = valid_keys and name in shifts

        if not valid_keys:
            raise ValueError("The dictionaries of image sequences, "
                             "frame rates, and (optionally) shifts should "
                             "all have the same keys")

        # Check whether all sequences are ImageSequence objects
        for key in sequences.keys():
            if not isinstance(sequences[key], ImageSequence):
                raise ValueError("Specify all image sequences as an "
                                 "ImageSequence object.")

        self.sequences = sequences  # type: Dict[str, ImageSequence]

        self.frame_rate_total = None  # type: float
        self.frame_rates = frame_rates  # type: Dict[str, float]
        self.index = None  # type: Dict[str, List[int]]
        self.time = None  # type: np.ndarray

        if not shifts:
            shifts = dict()
            for name in self.names:
                shifts[name] = 0
        self.shifts = shifts  # type: Dict[str, int]

        if paths is not None:
            assert len(paths) == len(self.names)
        self._paths = paths  # type: List[str]

        self._calculate_time()

    @property
    def names(self) -> List[str]:
        return list(self.sequences.keys())

    @property
    def nb_sequences(self) -> int:
        return len(self.sequences)

    @property
    def nb_time(self) -> int:
        return self.time.size

    def _calculate_time(self) -> None:
        """Calculates a time vector and the sequence indices in it."""

        # Find the greatest common divisor of all frame rate combinations. We
        # progress through the list, updating the overall frame rate every
        # time using the greatest common divisor (gcd()).
        self.frame_rate_total = self.frame_rates[self.names[0]]
        for name in self.names:
            div = gcd(self.frame_rate_total, self.frame_rates[name])
            self.frame_rate_total = div * (self.frame_rate_total //
                                           div * self.frame_rates[name] // div)

        # Calculate step size with respect to the overall frame rate. Also
        # adjust the shift to be in the overall time vector.
        step = dict(zip(self.names, [0] * self.nb_sequences))
        nb_t = dict(zip(self.names, [0] * self.nb_sequences))
        for name in self.names:
            step[name] = self.frame_rate_total // self.frame_rates[name]
            nb_t[name] = self.sequences[name].nb_images * step[name]

            self.shifts[name] = self.shifts[name] * step[name]

        # Calculate the overall time vector - find the sequence with the
        # longest total time
        dt = 1 / self.frame_rate_total
        for name in self.names:
            nb_t[name] = self.sequences[name].nb_images * step[name] + self.shifts[name]
        nb_t = max(nb_t.values())
        it = range(nb_t)
        self.time = np.asarray([i * dt for i in it])

        # Calculate indices into every sequence of all time steps.
        self.index = dict.fromkeys(self.names)
        for name in self.names:
            index_cur = [None] * nb_t
            for frame in range(self.sequences[name].nb_images):
                ind = step[name] * frame + self.shifts[name]
                index_cur[ind] = frame

            self.index[name] = index_cur

    def get(self, name: any) -> ImageSequence:
        """Retrieves an image sequence from its name.

        Args:
            name: The name (key) of the image sequence to load.

        Returns:
            The image sequence of the specified name (key).
        """
        return self.sequences[name]

    # def get_name(self, index: int) -> str:
    #     """Retrieves the name (key) of the image sequences of the specified index.
    #
    #     Args:
    #         index: The index of the image sequence to get the name of.
    #
    #     Returns:
    #         The name (key) of the image sequence of the specified index.
    #     """
    #     return self.names[index]
    #
    # def get_names(self) -> List[str]:
    #     """Retrieves the names (keys) of all image sequences.
    #
    #     Returns:
    #         The names (keys) of all the image sequences
    #     """
    #     return self.names

    def get_path(self, name) -> str:
        """Retrieves the path of the directory where the image sequence from the specified index are.

        Args:
            name: The name (key) of the image sequence to load.

        Returns:
            The path of the directory with image sequence for the specified index
        """

        return self._paths[self.names.index(name)]

    def get_paths(self) -> List[str]:
        """ Retrieves the paths of the directories where the images of the sequences are.

        Returns:
            The paths of the directories with the image sequences
        """
        return self._paths

    @staticmethod
    def from_directory(paths: List[str], frame_rates: List[float] or int, names: List[any] = None,
                       shifts: List[int] = None, expr: str = r"\d+.(tif|png|jpg|bmp)") -> "MultiImageSequence":
        """ Gathers recording files from directory.

        This function sets the files attribute of the instance to a list of
        subdirectories from the specified directory.
        In each of these subdirectories, it matches the regular expression
        and optionally the recording names. If no recording names are
        specified, the list is in alphabetical order. If recording names
        are specified, the output is sorted in the same order

        Args:
            paths: List of paths to directories containing each with a recording sequence.
            frame_rates: A dictionary of the frame rates of every sequence,
                with the identifiers as keys or an int if the frame_rate is the same for all.
            names: A List of names for the sequences. If None, the names of the folder will be used
            shifts: A dictionary of shifts in number of frames of the start of
                each sequence with respect to a common time point, with the
                identifiers as keys. This is used when cameras are running at a
                different frame rate, but the video sequences do not start at
                the exact same time instance (as could happen when e.g.,
                and end trigger is used). For example, a camera running at
                half the frame rate of another camera may have a shift of one
                frame w.r.t. the other.
            expr: Regular expression pattern matching image files.
        """

        if names is None:
            # names = [os.path.basename(os.path.normpath(directory)) for directory in paths]
            names = ['seq{0}'.format(i+1) for i, directory in enumerate(paths)]

        assert len(names) == len(set(names))  # All names are unique

        if type(frame_rates) in [int, np.int_]:
            frame_rates = {name: int(frame_rates) for name in names}

        elif type(frame_rates) in [list, np.ndarray]:
            assert len(frame_rates) == len(paths)
            frame_rates = {file_name: int(frame_rates[i]) for i, file_name in enumerate(names)}

        else:
            assert type(frame_rates) is dict and list(frame_rates.keys()) == names

        if shifts is not None:
            assert len(shifts) == len(paths)
            shifts = {file_name: shifts[i] for i, file_name in enumerate(names)}

        sequences = {}
        for i, directory in enumerate(paths):
            sequences[names[i]] = ImageSequence.from_directory(directory, expr=expr)

        return MultiImageSequence(sequences, frame_rates, shifts, paths)

