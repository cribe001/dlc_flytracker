import subprocess


def get_version():
    """
    Attempts to retrieve the current version of the tracker.

    Returns:
        If successful: the hash of the currently checked out commit.
        If not successful: 'unknown'
    """

    try:
        result = subprocess.check_output(['git', 'rev-parse', 'HEAD'])
        return result.decode().strip()

    except:
        # Any error in this command will prevent us from retrieving a version.
        return 'unknown'
