import yaml, os, copy
from abc import ABC, abstractmethod
from dataclasses import dataclass, field

import textwrap
from typing import Any, Dict, Optional, Tuple, List


def create_yaml_indent():
    return ' ' * 2


def default_field(obj):
    return field(default_factory=lambda: copy.copy(obj))


class BaseSettings(ABC):

    @staticmethod
    @abstractmethod
    def from_dict(raw):
        pass

    def to_dict(self):
        """ Returns the dataclass as a dictionary.

        Returns:
            A dictionary that contains all the class variables
        """

        return self.__dict__

    def to_yaml_str(self):
        """Represents the settings as a YAML string.

        Args:
            A YAML string with the settings.
        """

        return yaml.safe_dump(self.__dict__)

    def to_yaml(self, path):
        """Write settings to a YAML file.

        Args:
            path: Path of the configuration file.
        """

        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))

        with open(path, 'w') as file:
            file.write(self.to_yaml_str())


@dataclass
class BatchSettings(BaseSettings):
    """
    Class for keeping track of the main settings of batch processing.
    """

    nb_cam: int = None

    image_sizes: Dict[any, Tuple[int, int]] = None
    frame_rates: Dict[any, int] = None

    first_frame: int = None
    last_frame: int = None
    missing_frames: Dict[any, int] = None

    recording_names_to_process: List[str] = None
    recording_paths: Dict[any, str] = None
    save_path: str = None
    dlt_path: str = None

    type_image: str = "grey"

    match_recordings_by_date: bool = True
    recording_matching_settings: Dict[str, any] = default_field({'match_recordings_by': 'date', 'expr': r"\d{8}_\d{6}",
                                                                 'format_date_str': '%Y%m%d_%H%M%S', 'threshold_s': 25})
    # recording_matching_settings: Dict[str, any] = default_field({'match_recordings_by': 'name', 'expr': r"\d+.(tif|png|jpg|bmp)",
    #                                                              'threshold_ratio_diff': 0.8, 'threshold_nb_diff_char': 3})

    move_to_delete: bool = False
    delete_previous: bool = True

    @staticmethod
    def from_dict(raw):
        """Creates a batch processing settings from a dictionary.

        Args:
            raw: Dictionary to use.

        Returns:
            batch processing settings initialised from the dictionary.
        """

        return BatchSettings(**raw)


@dataclass
class ProtocolStepSettings(BaseSettings):
    """Settings for a step of an image processing protocol (???).

    Attributes:
        num:
        fn: Function to apply (???).
        kwargs: Arguments to pass to the function (???).
    """

    num: int = 1
    fn: Optional[str] = None
    kwargs: Dict[str, Any] = field(default_factory=dict)

    @staticmethod
    def from_dict(raw):
        """Creates image processing protocol step settings from a dictionary.

        Args:
            raw: Dictionary to use.

        Returns:
            Image processing protocol step settings from the dictionary.
        """
        return ProtocolStepSettings(**raw)

    def to_yaml_str(self):
        """Represents the settings as a YAML string.

        Args:
            A YAML string with the settings.
        """
        result = ''
        if self.fn is not None:
            result += f'fn: {self.fn}\n'
        if len(self.kwargs) > 0:
            result += f'kwargs:\n'
            result += textwrap.indent(yaml.safe_dump(self.kwargs, default_flow_style=False), create_yaml_indent())
        return result

    def to_yaml(self, path):
        """Write settings to a YAML file.

        Args:
            path: Path of the configuration file.
        """

        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))

        with open(path, 'w') as file:
            file.write(self.to_yaml_str())


class ProtocolSettings(BaseSettings):
    """Definition of the image processing protocol (???).

    Attributes:
        steps: Image processing (???) steps in the protocol.
    """

    def __init__(self):
        """Initialises an empty image processing protocol (???).
        """
        self.steps = []

    @property
    def fns(self) -> List[int]:
        return [step.fn for step in self.steps]

    @property
    def nums(self) -> List[int]:
        return [step.num for step in self.steps]

    def add_step(self, fn, kwargs):
        """ Add a step to the list of steps in the image processing protocol

        Args:
            fn: Function to apply (???).
            kwargs: Arguments to pass to the function (???).
        """

        if self.nums:
            num = self.nums[-1] + 1
        else:
            num = 1

        self.steps.append(ProtocolStepSettings(num=num, fn=fn, kwargs=kwargs))

        assert self.nums == list(range(1, max(self.nums) + 1)), \
            'Protocol nums should be unique integer in ascending order and with unity increment'

    def get(self, num: int) -> ProtocolStepSettings:
        """

        Args:
            num:

        Returns:
            The step at the specified name.
        """
        return self.steps[self.nums.index(num)]

    @staticmethod
    def from_dict(raw):
        """Creates an image processing protocol (???) from a dictionary.

        Args:
            raw: Dictionary to use; may be None.

        Returns:
            Image processing protocol initialised from the dictionary.
        """
        settings = ProtocolSettings()

        for num in raw.keys():
            step_dict = raw[num] if raw[num] is not None else {}
            assert type(step_dict) is dict

            step_dict['num'] = num
            settings.steps.append(ProtocolStepSettings.from_dict(step_dict))

        return settings

    def to_dict(self):
        """ Returns the dataclass as a dictionary.

        Returns:
            protocol_dict: A dictionary that contains all the class variables
        """

        protocol_dict = {}
        for num in self.nums:
            protocol_dict[num] = self.get(num).to_dict()

        return protocol_dict

    def to_yaml_str(self):
        """Represents the settings as a YAML string.

        Args:
            A YAML string with the settings.
        """
        result = ''
        for step in self.steps:
            result += f'{step.num}:\n'
            result += textwrap.indent(step.to_yaml_str(), create_yaml_indent())
        return result

    def to_yaml(self, path):
        """Write settings to a YAML file.

        Args:
            path: Path of the configuration file.
        """

        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))

        with open(path, 'w') as file:
            file.write(self.to_yaml_str())


class Settings(BaseSettings):
    """Settings for the DLC fly tracker.

    Attributes:
        batch: Settings for the batch processing
        protocol: Definition of the image processing protocol (???)
    """

    def __init__(self):
        """Initialises DLC fly tracker settings.

        Uses default values for all settings.
        """
        self.batch = BatchSettings()
        self.protocol = ProtocolSettings()

    @staticmethod
    def from_dict(raw_dict):
        """Creates a DLC fly tracker settings from a dictionary.

        Values not specified in the dictionary will receive default values (if
        possible).

        Args:
            raw_dict: Dictionary to use.

        Returns:
            DLC fly tracker settings initialised from the dictionary.
        """
        # The constructor initialises defaults
        settings = Settings()

        # Overwrite defaults with values from the dictionary.
        if 'batch' in raw_dict:
            settings.batch = BatchSettings.from_dict(raw_dict['batch'])

        if 'protocol' in raw_dict:
            settings.protocol = ProtocolSettings.from_dict(raw_dict['protocol'])

        return settings

    @staticmethod
    def from_dicts(batch_dict, protocol_dict):
        """Creates a DLC fly tracker settings from a dictionary.

        Values not specified in the dictionary will receive default values (if
        possible).

        Args:
            batch_dict: Batch dictionary to use.
            protocol_dict: Protocol dictionary to use.

        Returns:
            DLC fly tracker settings initialised from the dictionary.
        """

        return Settings.from_dict({'batch': batch_dict, 'protocol': protocol_dict})

    @staticmethod
    def from_yaml(path):
        """Reads DLC fly tracker configuration from a YAML file.

        Args:
            path: Configuration file to read.

        Returns:
            DLC fly tracker configuration as a Settings object.
        """
        with open(path, 'r') as file:
            input = yaml.safe_load(file)
        return Settings.from_dict(input)

    def to_yaml_str(self):
        """Represents the settings as a YAML string.

        Args:
            A YAML string with the settings.
        """
        result = ''

        indent_prefix = create_yaml_indent()

        result += 'batch:\n'
        result += textwrap.indent(self.batch.to_yaml_str(), indent_prefix)

        result += 'protocol:\n'
        result += textwrap.indent(self.protocol.to_yaml_str(), indent_prefix)

        return result

    def to_yaml(self, path):
        """Write settings to a YAML file.

        Args:
            path: Path of the configuration file.
        """

        if not os.path.exists(os.path.dirname(path)):
            os.makedirs(os.path.dirname(path))

        with open(path, 'w') as file:
            file.write(self.to_yaml_str())

