import textwrap

from .version import get_version


def print_header(settings):
    print('+' + '-' * 78 + '+')
    print('| ' + 'DeepLabCut fly tracker'.ljust(77) + '|')
    print('+' + '-' * 78 + '+')
    print()

    version = get_version()
    print(f"Version: {version}")
    print()

    config_yaml = settings.to_yaml_str().rstrip()
    print("Configuration:")
    print(textwrap.indent(config_yaml, ' ' * 4))
