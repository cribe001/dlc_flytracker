import copy
import numpy as np

from typing import Dict, List

from skeleton_fitter.modules.limbs import insect_wings_flat
from skeleton_fitter.modules.bodies import insect_body_slim


class AnimalSettings:
    """ Class for keeping track of the settings of a fly skeleton.

    An animal skeleton consist of multiple modules (one body + various number of pairs of limbs (right and left))
    for example: a fly consist of one body: insect_body_slim,
                        and multiple limbs: one pair of insect_wings_flat (+ three pairs of insect_legs)
    """

    body_module_name = 'insect_body_slim'  # type: str
    limb_module_names = ['insect_wings_flat']  # type: List[str]
    limb_names = ['wings']  # type: str

    # Alternatively, we could use a skeleton with legs
    # limb_module_names = ['insect_wings_flat', 'insect_legs', 'insect_legs', 'insect_legs']  # type: List[str]
    # limb_names = ['wings', 'front_legs', 'middle_legs', 'back_legs']  # type: str

    assert len(limb_module_names) == len(limb_names)

    @staticmethod
    def from_dict(dictionary):
        animal_sets = AnimalSettings()
        for key, value in dictionary.items():
            setattr(animal_sets, key, value)

        return animal_sets


def cost_function(param_values: List[int], param_names: List[str], init_skeleton3d: Dict[str, any],
                  init_params: Dict[str, any], raw_skeleton: Dict[str, any], options: Dict[str, any]) -> float:
    """
    Cost function (sum of residuals) to fit body or wings skeleton (in init_skeleton3d) to points (in raw_skeleton)

    Args:
        param_values: ndarray, shape (n,) with values of the parameters that will be varied
        param_names: list of names of all parameters that will be optimized, shape (n,)
        init_skeleton3d: dict() with initial 3d coordinates of skeleton joints
        init_params: dict() with initial values of angles and lengths used to fit init_skeleton3d to raw_skeleton
        raw_skeleton: dict() with raw 2d or 3d coordinates of skeleton joints (will try to fit init_skeleton3d to these)
        options: dict with various optional parameters

    Returns:
        sum_res: Sum of residuals when comparing init_skeleton3d (after transformed with param_values) to raw_skeleton
    """
    new_params = copy.deepcopy(init_params)
    for i, param_name in enumerate(param_names):
        new_params[param_name] = param_values[i]

    # Build, rotate skeleton + Estimate sum of residuals
    if 'body' in options['fit_method']:
        body_skeleton3d = copy.deepcopy(init_skeleton3d)
        body_skeleton3d = insect_body_slim.build_skeleton3d(body_skeleton3d, new_params)

        body_skeleton3d = insect_body_slim.rotate_skeleton3d(body_skeleton3d, new_params)
        body_skeleton3d = insect_body_slim.translate_skeleton3d(body_skeleton3d, new_params)

        if '3d' in options['fit_method']:
            residuals = insect_body_slim.residuals3d(body_skeleton3d, raw_skeleton, options['default_residual'])

        elif '2d' in options['fit_method']:
            residuals = insect_body_slim.residuals2d(body_skeleton3d, raw_skeleton, options['default_residual'], options['dlt_coefs'])

    elif 'limb' in options['fit_method']:
        wing_skeleton3d = copy.deepcopy(init_skeleton3d)
        wing_skeleton3d = insect_wings_flat.build_skeleton3d(wing_skeleton3d, new_params)
        wing_skeleton3d = insect_wings_flat.rotate_and_translate_skeleton3d(wing_skeleton3d, new_params, options['side'])

        if '3d' in options['fit_method']:
            residuals = insect_wings_flat.residuals3d(wing_skeleton3d, raw_skeleton, options['default_residual'])
        elif '2d' in options['fit_method']:
            residuals = insect_wings_flat.residuals2d(wing_skeleton3d, raw_skeleton, options['default_residual'], options['dlt_coefs'])
    else:
        print('WARN: {0} is not a valid method!'.format(options['fit_method']))

    if options['opt_method'] in ['leastsq', 'least_squares']:
        return residuals
    else:
        return np.sum(residuals ** 2)


# TODO check if following is useless
# def replace_outliers(data, m=2.):
#     data = np.array(data)
#
#     d = np.abs(data - np.median(data))
#     mdev = np.median(d)
#     s = d/mdev if mdev else 0.
#
#     in_out = np.where(s < m)
#     for i in in_out: data[i] = np.nan
#     return data

