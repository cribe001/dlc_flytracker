import copy
import numpy as np

from angles import normalize

from scipy.optimize import minimize, least_squares, differential_evolution, leastsq, brute

from skeleton_fitter.utils import check_params_ranges


def optimise(animal_name, init_skeleton3d, init_params, raw_skeleton, param_names, fit_method, opt_method, bounds_dict, dlt_coefs=[]):
    """

    Args:
        animal_name: name of the .py file in skeleton_fitter.cost_function folder
                     that contains the cost_function that should be evaluated, string (e.g. "fly")
        init_skeleton3d:
        init_params:
        raw_skeleton:
        param_names: names of the parameters to optimize
        fit_method:
        opt_method: optimization method ('nelder-mead', 'powell', 'least_squares', 'leastsq')
        bounds_dict:
        dlt_coefs: nb_cam*12 array with the DLT coefficients of all camera (only for 2d case)

    Returns:
        res: results from optimization
    """

    cost_function = getattr(__import__("skeleton_fitter.animals." + animal_name, fromlist=["cost_function"]), "cost_function")

    solver_options = {'disp': False}
    options = {'fit_method': fit_method, 'opt_method': opt_method, 'default_residual': 0.005}

    if 'right' in options['fit_method']:
        options['side'] = 'right'
    elif 'left' in options['fit_method']:
        options['side'] = 'left'

    if '2d' in fit_method:
        options['dlt_coefs'] = dlt_coefs
        options['default_residual'] = 5.0  # in pixels

    args_cost = tuple((param_names, init_skeleton3d, init_params, raw_skeleton, options))

    bounds = []
    check_params_ranges(init_params, bounds_dict, option='doNothing')
    for param_name in param_names:
        bounds.append(bounds_dict[param_name])
    bounds = tuple(bounds)

    init_param_values = []
    for param_name in param_names:
        init_param_values.append(init_params[param_name])

    # Optimize parameters values to minimize cost function (fit init_skeleton3d to raw_skeleton)
    if opt_method in ['nelder-mead']:
        res = minimize(cost_function, init_param_values, method=opt_method, args=args_cost, options=solver_options)

    elif opt_method in ['powell']:  # with bounds
        res = minimize(cost_function, init_param_values, method=opt_method, args=args_cost, options=solver_options, bounds=bounds)

    elif opt_method == 'least_squares':
        verbose = 1 if solver_options['disp'] else 0

        bounds_min, bounds_max = [], []
        for bound in bounds:
            bounds_min.append(bound[0])
            bounds_max.append(bound[1])

        # possible methods = [‘trf’, ‘dogbox’, ‘lm’]
        res = least_squares(cost_function, init_param_values, method='trf', args=args_cost,
                            bounds=(bounds_min, bounds_max), verbose=verbose)

    elif opt_method == 'leastsq':  # should be same as 'least_squares' with 'lm' (Levenberg-Marquardt)
        res_leastsq, cov_x, infodict, mesgstr, ier = \
            leastsq(cost_function, init_param_values, args=args_cost, full_output=True)

        res = infodict
        res['x'] = res_leastsq
        res['message'] = mesgstr

    elif opt_method == 'diff_evol':
        res = differential_evolution(cost_function, bounds, args=args_cost)

    elif opt_method == 'brute':
        nb_bins = 180

        ranges = []
        for cur_bound in bounds:
            ranges.append((slice(cur_bound[0], cur_bound[1], nb_bins)))

        res_brute = brute(cost_function, tuple(ranges), args=args_cost)
        res = {'x': res_brute}

    else:
        print('WARN: {0} is not a valid method!'.format(opt_method))
        return {}

    # Normalize angles
    final_params = copy.deepcopy(init_params)
    for i, param_name in enumerate(param_names):
        if '_a' in param_name:
            res['x'][i] = normalize(res['x'][i], -180, 180)
        final_params[param_name] = res['x'][i]

    return res


def optim_fit_body_params(animal_name, body_skeleton3d_init, body_skeleton, body_params_init, param_names, fit_method, opt_method,
                          body_bounds, return_nan=False, dlt_coefs=[]):
    """

    Args:
        animal_name: name of the .py file in skeleton_fitter.cost_function folder
                     that contains the cost_function that should be evaluated, string (e.g. "fly")
        body_skeleton3d_init:
        body_skeleton:
        body_params_init:
        param_names: names of the parameters to optimize
        fit_method:
        opt_method: optimization method (nelder-mead, powell, least_squares, leastsq)
        body_bounds:
        return_nan: Will only return np.nan if True (True or False)
        dlt_coefs: nb_cam*12 array with the DLT coefficients of all camera (only for 2d case)

    Returns:
        body_param_ests:
        rmse:
        nb_iterations:
    """

    body_param_ests = copy.deepcopy(body_params_init)

    is_any_param_nan = np.array([np.isnan(body_params_init[label]) for label in body_params_init.keys()]).any()
    if return_nan or is_any_param_nan:
        for param_name in param_names:
            body_param_ests[param_name] = np.nan

        return body_param_ests, np.nan, 0

    if '3d' in fit_method:
        res = optimise(animal_name, body_skeleton3d_init, body_params_init, body_skeleton, param_names,
                       fit_method, opt_method, body_bounds)

    elif '2d' in fit_method:
        res = optimise(animal_name, body_skeleton3d_init, body_params_init, body_skeleton, param_names,
                       fit_method, opt_method, body_bounds, dlt_coefs=dlt_coefs)

    if opt_method == 'leastsq':
        rmse = np.sqrt(sum(res['fvec'] ** 2)) / len(res['fvec'])

    elif opt_method == 'least_squares':
        rmse = np.sqrt(sum(res['fun'] ** 2)) / len(res['fun'])

    elif 'fun' in res.keys():
        rmse = np.sqrt(res['fun']) / len(body_skeleton3d_init.keys())

    for j, param_name in enumerate(param_names):
        body_param_ests[param_name] = res['x'][j]

    nb_iterations = res['nfev']

    return body_param_ests, rmse, nb_iterations


def optim_fit_limbs_params(animal_name,  limbs_skeleton3d_init, limbs_skeleton, limbs_params_init, param_names,
                           fit_method, opt_method, limb_bounds, return_nan=[False, False], dlt_coefs=[]):
    """

    Args:
        animal_name: name of the .py file in skeleton_fitter.cost_function folder
                     that contains the cost_function that should be evaluated, string (e.g. "fly")
        limbs_skeleton3d_init:
        limbs_skeleton:
        limbs_params_init:
        param_names: names of the parameters to optimize
        fit_method:
        opt_method: optimization method (nelder-mead, powell, least_squares, leastsq)
        limb_bounds:
        return_nan: Will only return np.nan if True (by default = [False, False])
        dlt_coefs: nb_cam*12 array with the DLT coefficients of all camera (only for 2d case)

    Returns:
        body_param_ests:
        rmse:
        nb_iterations:
    """

    limbs_params_ests = copy.deepcopy(limbs_params_init)

    rmse, nb_iterations = {}, {}
    for i, side in enumerate(limbs_skeleton3d_init.keys()):

        is_any_param_nan = np.array([np.isnan(limbs_params_init[side][label]) for label in limbs_params_init[side].keys()]).any()
        if return_nan[i] or is_any_param_nan:
            for param_name in param_names:
                limbs_params_ests[side][param_name] = np.nan

            rmse[side], nb_iterations[side] = np.nan, 0
            continue

        if '3d' in fit_method:
            res = optimise(animal_name, limbs_skeleton3d_init[side], limbs_params_init[side], limbs_skeleton[side],
                           param_names, '{0}_{1}'.format(side, fit_method), opt_method, limb_bounds)
        elif '2d' in fit_method:
            res = optimise(animal_name, limbs_skeleton3d_init[side], limbs_params_init[side], limbs_skeleton[side],
                           param_names, '{0}_{1}'.format(side, fit_method), opt_method, limb_bounds, dlt_coefs=dlt_coefs)

        if opt_method == 'leastsq':
            rmse[side] = np.sqrt(sum(res['fvec'] ** 2)) / len(res['fvec'])

        elif opt_method == 'least_squares':
            rmse[side] = np.sqrt(sum(res['fun'] ** 2)) / len(res['fun'])

        elif 'fun' in res.keys():
            rmse[side] = np.sqrt(res['fun']) / len(limbs_skeleton3d_init[side].keys())

        for j, param_name in enumerate(param_names):
            limbs_params_ests[side][param_name] = res['x'][j]

        nb_iterations[side] = res['nfev']

    return limbs_params_ests, rmse, nb_iterations
