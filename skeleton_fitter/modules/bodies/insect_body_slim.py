import copy, yaml
import numpy as np

from dataclasses import dataclass

from scipy.spatial.transform import Rotation

from typing import Dict, List

from skeleton_fitter.utils import reproject_skeleton3d_to2d, length_from_vector
from camera.utils import get_rotation_angle_btw_vectors


def init_params(init_dict: Dict[str, any]) -> Dict[str, float]:
    """
        Load initial kinematic parameters

    Args:
        init_dict: Dictionary with initial values of the skeleton coordinates/parameters/bounds

    Returns:
        body_params:
    """

    param_names = ['default_pitch_a', 'yaw_a', 'pitch_a', 'roll_a',
                   'ratio_com_torso', 'ratio_proboscis_head', 'ratio_body',
                   'proboscis_torso_a', 'torso_abdomen_a', 'proboscis_l', 'torso_l', 'torso_r', 'abdomen_l',
                   'x_com', 'y_com', 'z_com']

    body_params = dict()
    for param_name in param_names:
        body_params[param_name] = init_dict['init_params']['body'][param_name]

    return body_params


def init_bounds(init_dict: Dict[str, any]) -> Dict[str, float]:
    """
        Load initial bounds for kinematic parameters

    Args:
        init_dict: Dictionary with initial values of the skeleton coordinates/parameters/bounds

    Returns:
        body_bounds:
    """

    param_names = ['default_pitch_a', 'yaw_a', 'pitch_a', 'roll_a',
                   'ratio_com_torso', 'ratio_proboscis_head', 'ratio_body',
                   'proboscis_torso_a', 'torso_abdomen_a', 'proboscis_l', 'torso_l', 'torso_r', 'abdomen_l',
                   'x_com', 'y_com', 'z_com']

    body_bounds = dict()
    for param_name in param_names:
        body_bounds[param_name] = tuple(init_dict['init_bounds']['body'][param_name])

    return body_bounds


def init_skeleton3d(init_dict: Dict[str, any], body_params: Dict[str, float]) -> Dict[str, List[float]]:
    """
        Initialize body skeleton

    Args:
        init_dict: Dictionary with initial values of the skeleton coordinates/parameters/bounds
        body_params:

    Returns:
        body_skeleton3d:
    """

    # Initialise a body with 3 segments (proboscis-head, torso, abdomen) along x axis
    labels = ['proboscis_tip', 'proboscis_head_joint', 'head_torso_joint',
              'right_wing_hinge', 'left_wing_hinge', 'torso_abdomen_joint', 'abdomen_middle', 'abdomen_tip']

    body_skeleton3d = dict()
    for label in labels:
        body_skeleton3d[label] = np.asarray(init_dict['init_skeleton3d']['body'][label])

    body_skeleton3d = build_skeleton3d(body_skeleton3d, body_params)

    return body_skeleton3d


def build_skeleton3d(init_body_skeleton3d: Dict[str, List[float]], body_params: Dict[str, float]) -> Dict[str, List[float]]:
    """

    Args:
        init_body_skeleton3d:
        body_params:

    Returns:
        body_skeleton3d:
    """

    body_skeleton3d = copy.deepcopy(init_body_skeleton3d)

    body_params2 = copy.deepcopy(body_params)
    for label in ['torso_r', 'torso_l', 'abdomen_l', 'proboscis_l']:
        body_params2[label] = body_params2[label] * body_params2['ratio_body']

    # Make sure origin is at the center of mass
    origin = body_params2['ratio_com_torso'] * (body_skeleton3d['head_torso_joint'] - body_skeleton3d['torso_abdomen_joint']) \
             + body_skeleton3d['torso_abdomen_joint']

    for label in body_skeleton3d.keys():
        body_skeleton3d[label] = body_skeleton3d[label] - origin

    # Set hinges next to the origin (center of mass)
    body_skeleton3d['right_wing_hinge'][0], body_skeleton3d['left_wing_hinge'][0] = 0.0, 0.0

    # Scaling
    torso_l = body_skeleton3d['head_torso_joint'][0] - body_skeleton3d['torso_abdomen_joint'][0]
    torso_r = np.sqrt(body_skeleton3d['right_wing_hinge'][1] ** 2 + body_skeleton3d['left_wing_hinge'][1] ** 2)
    for label in ['head_torso_joint', 'torso_abdomen_joint', 'right_wing_hinge', 'left_wing_hinge']:
        body_skeleton3d[label] = np.array([body_skeleton3d[label][0] * body_params2['torso_l'] / torso_l,
                                           body_skeleton3d[label][1] * body_params2['torso_r'] / torso_r,
                                           body_skeleton3d[label][2] * body_params2['torso_r'] / torso_r])

    body_skeleton3d['proboscis_tip'][0] = body_skeleton3d['head_torso_joint'][0] + body_params2['proboscis_l']
    body_skeleton3d['proboscis_head_joint'][0] = \
        body_skeleton3d['head_torso_joint'][0] + body_params2['proboscis_l'] * body_params2['ratio_proboscis_head']
    body_skeleton3d['abdomen_middle'][0] = body_skeleton3d['torso_abdomen_joint'][0] - body_params2['abdomen_l'] / 2
    body_skeleton3d['abdomen_tip'][0] = body_skeleton3d['torso_abdomen_joint'][0] - body_params2['abdomen_l']

    # Curving
    body_skeleton3d['proboscis_tip'][0] = body_skeleton3d['head_torso_joint'][0] \
        + body_params2['proboscis_l'] * np.cos(np.deg2rad(180.0 - body_params2['proboscis_torso_a']))
    body_skeleton3d['proboscis_head_joint'][0] = body_skeleton3d['head_torso_joint'][0] \
        + body_params2['ratio_proboscis_head'] * body_params2['proboscis_l'] * np.cos(np.deg2rad(180.0 - body_params2['proboscis_torso_a']))
    body_skeleton3d['abdomen_middle'][0] = body_skeleton3d['torso_abdomen_joint'][0] \
        - body_params2['abdomen_l'] / 2 * np.cos(np.deg2rad(180.0 - body_params2['torso_abdomen_a']))
    body_skeleton3d['abdomen_tip'][0] = body_skeleton3d['torso_abdomen_joint'][0] \
        - body_params2['abdomen_l'] * np.cos(np.deg2rad(180.0 - body_params2['torso_abdomen_a']))

    body_skeleton3d['proboscis_tip'][2] = - body_params2['proboscis_l'] * np.sin(np.deg2rad(180.0 - body_params2['proboscis_torso_a']))
    body_skeleton3d['proboscis_head_joint'][2] = \
        - body_params2['ratio_proboscis_head'] * body_params2['proboscis_l'] * np.sin(np.deg2rad(180.0 - body_params2['proboscis_torso_a']))
    body_skeleton3d['abdomen_middle'][2] = - body_params2['abdomen_l'] / 2 * np.sin(np.deg2rad(180.0 - body_params2['torso_abdomen_a']))
    body_skeleton3d['abdomen_tip'][2] = - body_params2['abdomen_l'] * np.sin(np.deg2rad(180.0 - body_params2['torso_abdomen_a']))

    return body_skeleton3d


def rotate_skeleton3d(body_skeleton3d: Dict[str, List[float]], body_params: Dict[str, float]) -> Dict[str, List[float]]:
    """

    Args:
        body_skeleton3d:
        body_params: dict with the initial values of the wing parameters (e.g. roll)

    Returns:
        body_skeleton3d_world:
    """
    # check_body_params_ranges(body_params)

    # Rotation (extrinsic rotations ('xyz') = Rz(yaw_a) @ Ry(pitch_a) @ Rx(roll_a))
    # This is the same as using intrinsic rotation ('ZYX')
    # TODO use quaternions?
    r_stroke2body = Rotation.from_euler('y', - body_params['default_pitch_a'], degrees=True)
    r_world2stroke = Rotation.from_euler('xyz', [- body_params['roll_a'], - body_params['pitch_a'], - body_params['yaw_a']], degrees=True)

    body_skeleton3d_world = copy.deepcopy(body_skeleton3d)
    for label in body_skeleton3d_world.keys():
        body_skeleton3d_world[label] = r_world2stroke.apply(r_stroke2body.apply(body_skeleton3d_world[label]))

    return body_skeleton3d_world


def translate_skeleton3d(body_skeleton3d: Dict[str, List[float]], body_params: Dict[str, float]) -> Dict[str, List[float]]:
    """

    Args:
        body_skeleton3d:
        body_params: dict with the initial values of the wing parameters (e.g. roll)

    Returns:
        body_skeleton3d_world:
    """
    # check_body_params_ranges(body_params)

    # Translating
    center_of_mass_coords_world = np.array([body_params['x_com'], body_params['y_com'], body_params['z_com']])

    body_skeleton3d_world = copy.deepcopy(body_skeleton3d)
    for label in body_skeleton3d_world.keys():
        body_skeleton3d_world[label] = body_skeleton3d_world[label] + center_of_mass_coords_world

    return body_skeleton3d_world


def estimate_params_from_skeleton3d(body_skeleton3d_world: Dict[str, List[float]],
                                    init_body_params: Dict[str, float]) -> Dict[str, float]:
    """
    Estimate parameters of the transformations/rotations needed to get body_skeleton3d_world

    Args:
        body_skeleton3d_world: dict with the 3d coordinates of the body skeleton
        init_body_params: dict with the initial values of the body parameters (initial transformation/rotation)

    Returns:
        body_params: dict with estimated body parameters
    """
    # Only works if all angles < 90.0 and > - 90.0° (except body_params['proboscis_torso_a'] and body_params['torso_abdomen_a'])

    body_params = copy.deepcopy(init_body_params)
    body_skeleton3d_world2 = copy.deepcopy(body_skeleton3d_world)

    head_head_vec = body_skeleton3d_world2['head_torso_joint'] - body_skeleton3d_world2['proboscis_head_joint']
    head_proboscis_vec = body_skeleton3d_world2['head_torso_joint'] - body_skeleton3d_world2['proboscis_tip']
    body_params['ratio_proboscis_head'] = np.sqrt(sum(head_head_vec ** 2)) / np.sqrt(sum(head_proboscis_vec ** 2))

    abdomen_head_vec = body_skeleton3d_world2['torso_abdomen_joint'] - body_skeleton3d_world2['head_torso_joint']
    abdomen_right_hinge_vec = body_skeleton3d_world2['torso_abdomen_joint'] - body_skeleton3d_world2['right_wing_hinge']
    abdomen_left_hinge_vec = body_skeleton3d_world2['torso_abdomen_joint'] - body_skeleton3d_world2['left_wing_hinge']
    abdomen_com_vec = \
        ((np.dot(abdomen_right_hinge_vec, abdomen_head_vec)/np.sqrt(sum(abdomen_head_vec ** 2)) ** 2) * abdomen_head_vec
        + (np.dot(abdomen_left_hinge_vec, abdomen_head_vec) / np.sqrt(sum(abdomen_head_vec ** 2)) ** 2) * abdomen_head_vec)/2

    body_params['ratio_com_torso'] = np.sqrt(sum(abdomen_com_vec ** 2)) / np.sqrt(sum(abdomen_head_vec ** 2))

    [body_params['x_com'], body_params['y_com'], body_params['z_com']] = \
        body_params['ratio_com_torso'] * (body_skeleton3d_world2['head_torso_joint']
                                          - body_skeleton3d_world2['torso_abdomen_joint']) \
        + body_skeleton3d_world2['torso_abdomen_joint']

    for label in body_skeleton3d_world2.keys():
        body_skeleton3d_world2[label] = \
            body_skeleton3d_world2[label] - [body_params['x_com'], body_params['y_com'], body_params['z_com']]

    # Vectors along the proboscis, torso,  abdomen and hinges
    proboscis_v = body_skeleton3d_world2['proboscis_tip'] - body_skeleton3d_world2['head_torso_joint']
    torso_v = body_skeleton3d_world2['head_torso_joint'] - body_skeleton3d_world2['torso_abdomen_joint']
    abdomen_v = body_skeleton3d_world2['torso_abdomen_joint'] - body_skeleton3d_world2['abdomen_tip']
    hinges_v = body_skeleton3d_world2['left_wing_hinge'] - body_skeleton3d_world2['right_wing_hinge']

    torso_v_projxy, torso_v_projxz = torso_v.copy(), torso_v.copy()
    torso_v_projxy[2], torso_v_projxz[1] = 0.0, 0.0

    # Angles (extrinsic rotations ('xyz') = Rz(yaw_a) @ Ry(pitch_a) @ Rx(roll_a))
    # This is the same as using intrinsic rotation ('ZYX')
    body_params['yaw_a'] = - np.sign(torso_v[1]) * get_rotation_angle_btw_vectors(torso_v_projxy, [1., 0., 0.])

    r_stroke2body = Rotation.from_euler('y', - body_params['default_pitch_a'], degrees=True)
    r_world2yaw = Rotation.from_euler('z', - body_params['yaw_a'], degrees=True)

    for label in ['pitch_a', 'roll_a', 'proboscis_torso_a', 'torso_abdomen_a']: body_params[label] = np.nan

    if not any(np.isnan(torso_v)):
        # TODO: Replace .inv by transpose? (orthogonal matrix)
        body_params['pitch_a'] = \
            get_rotation_angle_btw_vectors(r_stroke2body.inv().apply(r_world2yaw.inv().apply(torso_v)), [1., 0., 0.])

        if not any(np.isnan(hinges_v)):

            r_world2yaw_pitch = Rotation.from_euler('yz', [- body_params['pitch_a'], - body_params['yaw_a']], degrees=True)
            body_params['roll_a'] = - get_rotation_angle_btw_vectors(r_stroke2body.inv().apply(r_world2yaw_pitch.inv().apply(hinges_v)), [0., 1., 0.])

            r_world2stroke = Rotation.from_euler('xyz', [- body_params['roll_a'], - body_params['pitch_a'], - body_params['yaw_a']], degrees=True)

            body_params['proboscis_torso_a'] = \
                180.0 - np.abs(get_rotation_angle_btw_vectors(r_stroke2body.inv().apply(r_world2stroke.inv().apply(proboscis_v)),
                                                               r_stroke2body.inv().apply(r_world2stroke.inv().apply(torso_v))))
            body_params['torso_abdomen_a'] = \
                180.0 - np.abs(get_rotation_angle_btw_vectors(r_stroke2body.inv().apply(r_world2stroke.inv().apply(torso_v)),
                                                               r_stroke2body.inv().apply(r_world2stroke.inv().apply(abdomen_v))))

    # Round parameters
    round_dec = 4
    for label in ['yaw_a', 'pitch_a', 'roll_a', 'proboscis_torso_a', 'torso_abdomen_a']:
        body_params[label] = np.round(body_params[label], round_dec)

    # Lengths
    body_params['proboscis_l'] = np.round(length_from_vector(proboscis_v), 9)
    body_params['torso_l'] = np.round(length_from_vector(torso_v), 9)
    body_params['torso_r'] = np.round(length_from_vector(hinges_v) * np.sqrt(2) / 2, 9)
    body_params['abdomen_l'] = np.round(length_from_vector(abdomen_v), 9)

    # check_body_params_ranges(body_params)

    return body_params


def get_copy_params_zero(body_params: Dict[str, float], label_to_zero=['yaw_a', 'pitch_a', 'roll_a']) -> Dict[str, float]:
    """

    Args:
        body_params:
        label_to_zero:

    Returns:
        new_body_params:
    """
    new_body_params = copy.deepcopy(body_params)
    for label in new_body_params.keys():
        if label in label_to_zero:
            new_body_params[label] = 0.0

    return new_body_params


def residuals2d(body_skeleton3d: Dict[str, List[float]], body_skeleton2d: Dict[str, List[float]],
                default_residual: float, dlt_coefs: List[List[float]]) -> List[float]:
    """
    Compute residuals when comparing body_skeleton3d and body_skeleton2d

    Args:
        body_skeleton3d: dict with 3d coordinates of the body skeleton ({'head': [0, 0, 0]})
        body_skeleton2d: dict with 2d coordinates of the body skeleton ({'head': [0, 0, 0]})
        default_residual: float
        dlt_coefs:

    Returns:
        residuals: distances between body_skeleton3d and body_skeleton2d
    """
    nb_cam = len(dlt_coefs)

    body_skeleton2d_from3d = reproject_skeleton3d_to2d(body_skeleton3d, dlt_coefs)

    residuals = np.zeros((len(body_skeleton3d.keys()), nb_cam))
    for i, label in enumerate(body_skeleton3d.keys()):
        for j, camn in enumerate(range(1, nb_cam + 1)):

            residuals[i][j] = np.sqrt(np.sum((body_skeleton2d[label][camn][0:2] - body_skeleton2d_from3d[label][camn][0:2]) ** 2))

            # weight on the likelihood? body_skeleton2d[label][camn][-1]
            if np.isnan(residuals[i][j]):
                residuals[i][j] = default_residual

    return residuals.flatten()


def residuals3d(body_skeleton3d_1: Dict[str, List[float]], body_skeleton3d_2: Dict[str, List[float]],
                default_residual: float) -> List[float]:
    """
    Compute residuals when comparing body_skeleton3d_1 and body_skeleton3d_2

    Args:
        body_skeleton3d_1: dict with 3d coordinates of the body skeleton ({'head': [0, 0, 0]})
        body_skeleton3d_2: dict with 3d coordinates of the body skeleton ({'head': [0, 0, 0]})
        default_residual: float

    Returns:
        residuals: distances between body_skeleton3d_1 and body_skeleton3d_2
    """

    residuals = np.zeros(len(body_skeleton3d_1.keys()))
    for i, label in enumerate(body_skeleton3d_1.keys()):
        residuals[i] = np.sqrt(np.sum((body_skeleton3d_2[label] - body_skeleton3d_1[label]) ** 2))

        if np.isnan(residuals[i]):
            residuals[i] = default_residual

    return residuals


@dataclass
class BodyModuleSettings:
    """
    Class for keeping track of the settings of the insect_body_slim module.
    """

    def __init__(self, init_yaml_path: str) -> None:
        """

        Args:
            init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
        """

        with open(init_yaml_path, 'r') as file:
            init_dict = yaml.safe_load(file)

        self.params_init = init_params(init_dict)  # type: Dict[str, Dict[str, any]]
        self.bounds_init = init_bounds(init_dict)  # type: Dict[str, Dict[str, any]]
        self.skeleton3d_init = init_skeleton3d(init_dict, self.params_init)  # type: Dict[str, Dict[str, any]]

        self.param_names = list(self.params_init.keys())  # type: List[str]
        self.labels = list(self.skeleton3d_init.keys())  # type: List[str]

        self.param_names_to_keep_cst = ['proboscis_l', 'torso_l', 'torso_r', 'abdomen_l',
                                        'proboscis_torso_a', 'torso_abdomen_a',
                                        'ratio_com_torso', 'ratio_proboscis_head']  # type: List[str]

        self.threshold_likelihood = 0.9  # type: float
        self.threshold_nb_pts = 4  # type: int

        assert 0.0 < self.threshold_likelihood <= 1.0
        assert 1 < self.threshold_nb_pts <= len(self.labels)

    @staticmethod
    def from_dict(init_yaml_path: str, dictionary: Dict[str, any]):
        """

        Args:
            init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
            dictionary: Dict that contains values of class attributes that will be updated
        """

        module_sets = BodyModuleSettings(init_yaml_path)
        for key, value in dictionary.items():
            setattr(module_sets, key, value)

        return module_sets

