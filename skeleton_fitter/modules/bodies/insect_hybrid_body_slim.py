import yaml

import numpy as np
import matplotlib.pyplot as plt

from dataclasses import dataclass

from typing import Dict, List

from utils.settings import BaseSettings

from skeleton_fitter.utils import set_axes3d_equal
import skeleton_fitter.modules.bodies.insect_body_slim as body

# Need these imports to be able to use these functions directly from these hybrid module (without duplicating them here)
from skeleton_fitter.modules.bodies.insect_body_slim import rotate_skeleton3d, translate_skeleton3d, \
    estimate_params_from_skeleton3d, get_copy_params_zero, residuals2d, residuals3d


def init_params(init_dict: Dict[str, any]) -> Dict[str, float]:
    """
        Load initial kinematic parameters

    Args:
        init_dict: Dictionary with initial values of the skeleton coordinates/parameters/bounds

    Returns:
        hybrid_params:
    """

    param_names = ['default_pitch_a', 'yaw_a', 'pitch_a', 'roll_a',
                   'ratio_com_torso', 'ratio_proboscis_head', 'ratio_body',
                   'proboscis_torso_a', 'torso_abdomen_a', 'proboscis_l', 'torso_l', 'torso_r', 'abdomen_l',
                   'x_com', 'y_com', 'z_com',
                   'wbaverage_span', 'wbaverage_deviation_a']

    hybrid_params = dict()
    for param_name in param_names:
        hybrid_params[param_name] = init_dict['init_params']['body'][param_name]

    return hybrid_params


def init_bounds(init_dict: Dict[str, any]) -> Dict[str, float]:
    """
        Load initial bounds for kinematic parameters

    Args:
        init_dict: Dictionary with initial values of the skeleton coordinates/parameters/bounds

    Returns:
        hybrid_bounds:
    """

    param_names = ['default_pitch_a', 'yaw_a', 'pitch_a', 'roll_a',
                   'ratio_com_torso', 'ratio_proboscis_head', 'ratio_body',
                   'proboscis_torso_a', 'torso_abdomen_a', 'proboscis_l', 'torso_l', 'torso_r', 'abdomen_l',
                   'x_com', 'y_com', 'z_com',
                   'wbaverage_span', 'wbaverage_deviation_a']

    hybrid_bounds = dict()
    for param_name in param_names:
        hybrid_bounds[param_name] = tuple(init_dict['init_bounds']['body'][param_name])

    return hybrid_bounds


def init_skeleton3d(init_dict: Dict[str, any], hybrid_params: Dict[str, float]) -> Dict[str, List[float]]:
    """

    Args:
        init_dict: Dictionary with initial values of the skeleton coordinates/parameters/bounds
        hybrid_params:

    Returns:
        hybrid_skeleton3d:
    """

    hybrid_skeleton3d = body.init_skeleton3d(init_dict, hybrid_params)
    hybrid_skeleton3d['right_wing_tip'] = \
        hybrid_skeleton3d['right_wing_hinge'] \
        + np.array([0.0, - hybrid_params['wbaverage_span'] * np.cos(np.deg2rad(hybrid_params['wbaverage_deviation_a'])),
                         hybrid_params['wbaverage_span'] * np.sin(np.deg2rad(hybrid_params['wbaverage_deviation_a']))])

    hybrid_skeleton3d['left_wing_tip'] = \
        hybrid_skeleton3d['left_wing_hinge'] \
        + np.array([0.0, hybrid_params['wbaverage_span'] * np.cos(np.deg2rad(hybrid_params['wbaverage_deviation_a'])),
                         hybrid_params['wbaverage_span'] * np.sin(np.deg2rad(hybrid_params['wbaverage_deviation_a']))])

    return hybrid_skeleton3d


def build_skeleton3d(init_hybrid_skeleton3d: Dict[str, List[float]],
                     hybrid_params: Dict[str, float]) -> Dict[str, List[float]]:
    """

    Args:
        init_hybrid_skeleton3d:
        hybrid_params:

    Returns:
        hybrid_skeleton3d:
    """

    hybrid_skeleton3d = body.build_skeleton3d(init_hybrid_skeleton3d, hybrid_params)
    hybrid_skeleton3d['right_wing_tip'] = \
        hybrid_skeleton3d['right_wing_hinge'] \
        + np.array([0.0, - hybrid_params['wbaverage_span'] * np.cos(np.deg2rad(hybrid_params['wbaverage_deviation_a'])),
                    hybrid_params['wbaverage_span'] * np.sin(np.deg2rad(hybrid_params['wbaverage_deviation_a']))])

    hybrid_skeleton3d['left_wing_tip'] = \
        hybrid_skeleton3d['left_wing_hinge'] \
        + np.array([0.0, hybrid_params['wbaverage_span'] * np.cos(np.deg2rad(hybrid_params['wbaverage_deviation_a'])),
                    hybrid_params['wbaverage_span'] * np.sin(np.deg2rad(hybrid_params['wbaverage_deviation_a']))])

    return hybrid_skeleton3d


def plot(body_skeleton3d: Dict[str, List[float]], wings_skeleton3d: Dict[str, List[float]]) -> None:
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for label in body_skeleton3d.keys():
        ax.scatter(body_skeleton3d[label][0], body_skeleton3d[label][1], body_skeleton3d[label][2], marker='*')

    for side in ['right', 'left']:
        for label in wings_skeleton3d[side].keys():
            ax.scatter(wings_skeleton3d[side][label][0], wings_skeleton3d[side][label][1], wings_skeleton3d[side][label][2],
                       marker='+')

    set_axes3d_equal(ax)
    ax.set_xlabel('x [m]')
    ax.set_ylabel('y [m]')
    ax.set_zlabel('z [m]')
    plt.show()


@dataclass
class BodyModuleSettings:
    """
    Class for keeping track of the settings of the insect_hybrid_body_slim module.

    """

    def __init__(self, init_yaml_path: str) -> None:
        """

        Args:
            init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
        """

        with open(init_yaml_path, 'r') as file:
            init_dict = yaml.safe_load(file)

        self.params_init = init_params(init_dict)  # type: Dict[str, Dict[str, any]]
        self.bounds_init = init_bounds(init_dict)  # type: Dict[str, Dict[str, any]]
        self.skeleton3d_init = init_skeleton3d(init_dict, self.params_init)  # type: Dict[str, Dict[str, any]]

        self.param_names = list(self.params_init.keys())  # type: List[str]
        self.labels = list(self.skeleton3d_init.keys())  # type: List[str]

        self.param_names_to_keep_cst = ['proboscis_l', 'torso_l', 'torso_r', 'abdomen_l',
                                        'proboscis_torso_a', 'torso_abdomen_a',
                                        'ratio_com_torso', 'ratio_proboscis_head']  # type: List[str]

        self.threshold_likelihood = 0.9  # type: float
        self.threshold_nb_pts = 4  # type: int

        assert 0.0 < self.threshold_likelihood <= 1.0
        assert 1 < self.threshold_nb_pts <= len(self.labels)

    @staticmethod
    def from_dict(init_yaml_path: str, dictionary: Dict[str, any]):
        """

        Args:
            init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
            dictionary: Dict that contains values of class attributes that will be updated
        """

        module_sets = BodyModuleSettings(init_yaml_path)
        for key, value in dictionary.items():
            setattr(module_sets, key, value)

        return module_sets

