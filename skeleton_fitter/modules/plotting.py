from typing import Dict, List

import matplotlib.pyplot as plt

from skeleton_fitter.utils import set_axes3d_equal


def plot_body(body_skeleton3d: Dict[str, List[float]]) -> None:
    """

    Args:
        body_skeleton3d:
    """
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for label in body_skeleton3d.keys():
        ax.scatter(body_skeleton3d[label][0], body_skeleton3d[label][1], body_skeleton3d[label][2], marker='*')

    set_axes3d_equal(ax)
    ax.set_xlabel('x [m]')
    ax.set_ylabel('y [m]')
    ax.set_zlabel('z [m]')
    plt.show()


def plot_limb(wing_skeleton3d: Dict[str, List[float]]) -> None:
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for label in wing_skeleton3d.keys():
        ax.scatter(wing_skeleton3d[label][0],
                   wing_skeleton3d[label][1],
                   wing_skeleton3d[label][2], marker='+')

    set_axes3d_equal(ax)
    ax.set_xlabel('x [m]')
    ax.set_ylabel('y [m]')
    ax.set_zlabel('z [m]')
    plt.show()


def plot_limbs(wings_skeleton3d: Dict[str, List[float]]) -> None:
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for side in ['right', 'left']:
        for label in wings_skeleton3d[side].keys():
            ax.scatter(wings_skeleton3d[side][label][0],
                       wings_skeleton3d[side][label][1],
                       wings_skeleton3d[side][label][2], marker='+')

    set_axes3d_equal(ax)
    ax.set_xlabel('x [m]')
    ax.set_ylabel('y [m]')
    ax.set_zlabel('z [m]')
    plt.show()


def plot_body_and_limbs(body_skeleton3d: Dict[str, List[float]], wings_skeleton3d: Dict[str, List[float]]) -> None:
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for label in body_skeleton3d.keys():
        ax.scatter(body_skeleton3d[label][0],
                   body_skeleton3d[label][1],
                   body_skeleton3d[label][2], marker='*')

    for side in ['right', 'left']:
        for label in wings_skeleton3d[side].keys():
            ax.scatter(wings_skeleton3d[side][label][0],
                       wings_skeleton3d[side][label][1],
                       wings_skeleton3d[side][label][2], marker='+')

    set_axes3d_equal(ax)
    ax.set_xlabel('x [m]')
    ax.set_ylabel('y [m]')
    ax.set_zlabel('z [m]')
    plt.show()
