import copy, yaml
import numpy as np

from dataclasses import dataclass


from scipy.spatial.transform import Rotation

from typing import Dict, List

from skeleton_fitter.modules.bodies.insect_body_slim import BodyModuleSettings

from skeleton_fitter.utils import reproject_skeleton3d_to2d, length_from_vector
from camera.utils import get_rotation_angle_btw_vectors


def init_params(init_dict: Dict[str, any], body_params: Dict[str, float]) -> Dict[str, float]:
    """

    Args:
        init_dict: Dictionary with initial values of the skeleton coordinates/parameters/bounds
        body_params:

    Returns:
        wings_params:
    """

    param_names_to_load_from_yaml = ['stroke_a', 'deviation_a', 'rotation_a', 'span', 'chord', 'ratio_wing']

    wings_params = {'right': copy.deepcopy(body_params), 'left': copy.deepcopy(body_params)}
    for side in ['right', 'left']:
        for param_name in param_names_to_load_from_yaml:
            wings_params[side][param_name] = init_dict['init_params']['limbs']['wing'][param_name]

        # TODO! Add the possibility to estimate wings' curvatures
        # wings_params[side]['span_curvature'] = 0.0  # curvature = 1/arc_length
        # wings_params[side]['chord_curvature'] = 0.0

        wings_params[side]['aspect_ratio'] = wings_params[side]['span'] / wings_params[side]['chord']

        wings_params[side]['x_hinge'] = body_params['x_com']
        if side == 'right': wings_params[side]['y_hinge'] = body_params['y_com'] - np.sqrt(2) * body_params['torso_r']
        elif side == 'left': wings_params[side]['y_hinge'] = body_params['y_com'] + np.sqrt(2) * body_params['torso_r']
        wings_params[side]['z_hinge'] = body_params['z_com'] + np.sqrt(2) * body_params['torso_r']

    return wings_params


def init_bounds(init_dict: Dict[str, any], body_bounds: Dict[str, float]) -> Dict[str, float]:
    """

    Args:
        init_dict: Dictionary with initial values of the skeleton coordinates/parameters/bounds
        body_bounds:

    Returns:
        wing_bounds:
    """

    param_names = ['stroke_a', 'deviation_a', 'rotation_a', 'span', 'chord', 'ratio_wing', 'aspect_ratio',
                   'x_hinge', 'y_hinge', 'z_hinge']

    wing_bounds = copy.deepcopy(body_bounds)
    for param_name in param_names:
        wing_bounds[param_name] = tuple(init_dict['init_bounds']['limbs']['wing'][param_name])

    # TODO! Add the possibility to estimate wings' curvatures
    # wing_bounds['span_curvature'] = (0.0, 1.0)
    # wing_bounds['chord_curvature'] = (0.0, 1.0)

    return wing_bounds


def init_skeleton3d(init_dict: Dict[str, any], wings_params: Dict[str, float]) -> Dict[str, List[float]]:
    """

    Args:
        init_dict: Dictionary with initial values of the skeleton coordinates/parameters/bounds
        wings_params:

    Returns:
        wings_skeleton3d:
    """

    labels = ['hinge', 'leading_edge_q1', 'leading_edge_q2', 'leading_edge_q3', 'tip', 'trailing_edge_q3',
              'trailing_edge_q2', 'trailing_edge_q1']

    wings_skeleton3d = {}
    for side in ['left', 'right']:

        wings_skeleton3d[side] = {}
        for label in labels:
            wings_skeleton3d[side][label] = np.asarray(init_dict['init_skeleton3d']['limbs']['wing'][label])

            if side == 'right':  # the default wing is the left wing, the right need to be mirrored along th y axis
                wings_skeleton3d[side][label][1] = -wings_skeleton3d[side][label][1]

        wings_skeleton3d[side] = build_skeleton3d(wings_skeleton3d[side], wings_params[side])

    return wings_skeleton3d


def build_skeleton3d(init_wing_skeleton3d: Dict[str, List[float]], wing_params: Dict[str, float]) -> Dict[str, List[float]]:
    """

    Args:
        init_wing_skeleton3d:
        wing_params:

    Returns:
        wing_skeleton3d:
    """
    wing_skeleton3d = copy.deepcopy(init_wing_skeleton3d)

    # Scaling
    x_scaling_factor = wing_params['chord'] / np.abs(wing_skeleton3d['leading_edge_q2'][0] - wing_skeleton3d['trailing_edge_q2'][0])
    y_scaling_factor = wing_params['span'] / np.abs(wing_skeleton3d['tip'][1] - wing_skeleton3d['hinge'][1])

    for label in wing_skeleton3d.keys():
        wing_skeleton3d[label][0] = x_scaling_factor * wing_skeleton3d[label][0] * wing_params['ratio_wing']
        wing_skeleton3d[label][1] = y_scaling_factor * wing_skeleton3d[label][1] * wing_params['ratio_wing']

    # # Curving
    # ????

    # Make move wing to hinge (from wing_params)
    hinge_coords_world = np.array([wing_params['x_hinge'], wing_params['y_hinge'], wing_params['z_hinge']])
    translation_vector = hinge_coords_world - wing_skeleton3d['hinge']
    for label in wing_skeleton3d.keys():
        wing_skeleton3d[label] = wing_skeleton3d[label] + translation_vector

    return wing_skeleton3d


def rotate_and_translate_skeleton3d(wing_skeleton3d: Dict[str, List[float]], wing_params: Dict[str, float], side: str) -> Dict[str, List[float]]:
    """

    Args:
        wing_skeleton3d: (in body reference frame)
        wing_params: dict with the initial values of the wing parameters (e.g. stroke_a)
        side: 'left' or 'right'

    Returns:
        wing_skeleton3d_world:
    """
    # check_body_params_ranges(body_params)
    # check_wings_params_ranges(wings_params)

    # TODO make rotate_and_translate_skeleton3d about only rotating (already the case?)?

    # Rotating and translating
    # (extrinsic rotations ('xyz') = Rz(yaw_a) @ Ry(pitch_a) @ Rx(roll_a))
    # This is the same as using intrinsic rotation ('ZYX')
    # TODO use quaternions?
    r_world2stroke = Rotation.from_euler('xyz', [wing_params['roll_a'], - wing_params['pitch_a'],- wing_params['yaw_a']], degrees=True)

    if side == 'right':
        r_stroke2wing = Rotation.from_euler('yxz', [- wing_params['rotation_a'],- wing_params['deviation_a'],
                                                    wing_params['stroke_a']], degrees=True)
    elif side == 'left':
        r_stroke2wing = Rotation.from_euler('yxz', [- wing_params['rotation_a'], wing_params['deviation_a'],
                                                    - wing_params['stroke_a']], degrees=True)

    hinge_coords_init = wing_skeleton3d['hinge']
    hinge_coords_world = np.array([wing_params['x_hinge'], wing_params['y_hinge'], wing_params['z_hinge']])
    center_of_mass_coords_world = np.array([wing_params['x_com'], wing_params['y_com'], wing_params['z_com']])

    # hinge_coords_stroke = r_world2stroke.inv().apply(r_stroke2body.inv().apply(hinge_coords_world - center_of_mass_coords_world))
    hinge_coords_stroke = r_world2stroke.inv().apply(hinge_coords_world - center_of_mass_coords_world)

    wing_skeleton3d_world = copy.deepcopy(wing_skeleton3d)
    for label in wing_skeleton3d_world.keys():
        wing_skeleton3d_world[label] = wing_skeleton3d_world[label] - hinge_coords_init  # Set origin to hinge
        wing_skeleton3d_world[label] = r_stroke2wing.apply(wing_skeleton3d_world[label])  # Rotate wing into stroke plane

        # Translate and rotate wing to body reference frame (origin at center of mass)
        wing_skeleton3d_world[label] = wing_skeleton3d_world[label] + hinge_coords_stroke

        wing_skeleton3d_world[label] = r_world2stroke.apply(wing_skeleton3d_world[label])  # Rotate into world ref. plane
        wing_skeleton3d_world[label] = wing_skeleton3d_world[label] + center_of_mass_coords_world  # Translate to center of mass in world ref. plane

    return wing_skeleton3d_world


def translate_skeleton3d(wing_skeleton3d: Dict[str, List[float]], wing_params: Dict[str, float]) -> Dict[str, List[float]]:
    """

    Args:
        wing_skeleton3d:
        wing_params: dict with the initial values of the wing parameters (e.g. stroke_a)
        hinge_coords_world:

    Returns:
        wing_skeleton3d_world:
    """
    # check_body_params_ranges(body_params)

    # Translating
    hinge_coords_world = np.array([wing_params['x_hinge'], wing_params['y_hinge'], wing_params['z_hinge']])

    wing_skeleton3d_world = copy.deepcopy(wing_skeleton3d)
    for label in wing_skeleton3d_world.keys():
        wing_skeleton3d_world[label] = wing_skeleton3d_world[label] + hinge_coords_world

    return wing_skeleton3d_world


def estimate_params_from_skeleton3d(wing_skeleton3d_world: Dict[str, List[float]], body_params: Dict[str, float],
                                    init_wing_params: Dict[str, float], side: str) -> Dict[str, float]:
    """
    Estimate parameters of the transformations/rotations needed to get wing_skeleton3d_world

    Args:
        wing_skeleton3d_world: dict with the 3d coordinates of the wing skeleton
        body_params: previously estimated parameters for the body
        init_wing_params: dict with the initial values of the wing parameters (initial transformation/rotation)
        side: 'left' or 'right'

    Returns:
        wing_params: dict with estimated wing parameters
    """
    # Only works if all wings angles < 90.0 and > - 90.0°

    wing_params = copy.deepcopy(init_wing_params)
    wing_skeleton_stroke = copy.deepcopy(wing_skeleton3d_world)
    for label in body_params.keys():
        wing_params[label] = body_params[label]

    [wing_params['x_hinge'], wing_params['y_hinge'], wing_params['z_hinge']] = wing_skeleton_stroke['hinge']
    for label in wing_skeleton_stroke.keys():
        wing_skeleton_stroke[label] = wing_skeleton_stroke[label] \
            - [wing_params['x_hinge'], wing_params['y_hinge'], wing_params['z_hinge']]

    # Rotation from world ref. frame to stroke ref. frame
    # (extrinsic rotations ('xyz') = Rz(yaw_a) @ Ry(pitch_a) @ Rx(roll_a))
    # This is the same as using intrinsic rotation ('ZYX')
    # TODO use quaternions?
    r_stroke2body = Rotation.from_euler('y', + body_params['default_pitch_a'], degrees=True)
    r_world2stroke = Rotation.from_euler('xyz', [- body_params['roll_a'], - body_params['pitch_a'], - body_params['yaw_a']], degrees=True)

    for label in wing_skeleton_stroke.keys():
        if not any(np.isnan(wing_skeleton_stroke[label])):
            wing_skeleton_stroke[label] = r_world2stroke.inv().apply(wing_skeleton_stroke[label])
            wing_skeleton_stroke[label] = r_stroke2body.inv().apply(wing_skeleton_stroke[label])

    # Vectors along the span and chord
    span_v = wing_skeleton_stroke['tip'] - wing_skeleton_stroke['hinge']
    chord_v = wing_skeleton_stroke['leading_edge_q2'] - wing_skeleton_stroke['trailing_edge_q2']

    span_v_projxy = span_v.copy()
    span_v_projxy[2] = 0.0

    for label in ['stroke_a', 'deviation_a', 'rotation_a']: wing_params[label] = np.nan

    if not any(np.isnan(span_v)) and not any(np.isnan(span_v_projxy)):
        # Angles (extrinsic rotations ('xyz') = Rz(yaw_a) @ Ry(pitch_a) @ Rx(roll_a))
        # This is the same as using intrinsic rotation ('ZYX')
        # TODO use quaternions?
        if side == 'right':
            wing_params['stroke_a'] = np.sign(span_v_projxy[0]) * get_rotation_angle_btw_vectors(span_v_projxy, [0., -1., 0.])
            span_v_inv_stroke = Rotation.from_euler('z', wing_params['stroke_a'], degrees=True).inv().apply(span_v)
            wing_params['deviation_a'] = - get_rotation_angle_btw_vectors(span_v_inv_stroke, [0., -1., 0.])

            # Rotation from world ref. frame to ref. frame before rotation
            r_stroke2rotation = Rotation.from_euler('xz', [- wing_params['deviation_a'], wing_params['stroke_a']], degrees=True)

        elif side == 'left':
            wing_params['stroke_a'] = np.sign(span_v_projxy[0]) * get_rotation_angle_btw_vectors(span_v_projxy, [0., 1., 0.])
            span_v_inv_stroke = Rotation.from_euler('z', - wing_params['stroke_a'], degrees=True).inv().apply(span_v)
            wing_params['deviation_a'] = get_rotation_angle_btw_vectors(span_v_inv_stroke, [0., 1., 0.])

            # Rotation from world ref. frame to ref. frame before rotation
            r_stroke2rotation = Rotation.from_euler('xz', [- wing_params['deviation_a'], - wing_params['stroke_a']], degrees=True)

        if not any(np.isnan(chord_v)):
            wing_params['rotation_a'] = \
                np.sign(chord_v[2]) * get_rotation_angle_btw_vectors(r_stroke2rotation.inv().apply(chord_v), [1., 0., 0.])

    # Round parameters
    round_dec = 4
    for label in ['stroke_a', 'deviation_a', 'rotation_a']:
        wing_params[label] = np.round(wing_params[label], round_dec)

    # check_body_params_ranges(body_params)
    # check_wings_params_ranges(wings_params)

    # Lengths
    wing_params['span'] = np.round(length_from_vector(span_v), 9)
    wing_params['chord'] = np.round(length_from_vector(chord_v), 9)
    wing_params['aspect_ratio'] = wing_params['span'] / wing_params['chord']

    # # Curvatures
    # wing_params['span_curvature'], wing_params['chord_curvature']

    return wing_params


def residuals2d(wing_skeleton3d: Dict[str, List[float]], wing_skeleton2d: Dict[str, List[float]],
                default_residual: float, dlt_coefs: List[List[float]]) -> List[float]:
    """
    Compute residuals when comparing wing_skeleton3d to wing_skeleton2d

    Args:
        wing_skeleton3d:
        wing_skeleton2d:
        default_residual:
        dlt_coefs:

    Returns:
        residuals: distances between wing_skeleton3d and wing_skeleton2d
    """
    nb_cam = len(dlt_coefs)

    wing_skeleton2d_from3d = reproject_skeleton3d_to2d(wing_skeleton3d, dlt_coefs)

    residuals = np.zeros((len(wing_skeleton3d.keys()), nb_cam))
    for i, label in enumerate(wing_skeleton3d.keys()):
        for j, camn in enumerate(range(1, nb_cam + 1)):
            residuals[i][j] = np.sqrt(np.sum((wing_skeleton2d[label][camn][0:2] - wing_skeleton2d_from3d[label][camn][0:2]) ** 2))

            # weight on the likelihood? wing_skeleton2d[label][camn][-1]
            if np.isnan(residuals[i][j]):
                residuals[i][j] = default_residual

    return residuals.flatten()


def residuals3d(wing_skeleton3d_1: Dict[str, List[float]], wing_skeleton3d_2: Dict[str, List[float]], default_residual: float) -> List[float]:
    """
    Compute residuals when comparing wing_skeleton3d_1 to wing_skeleton3d_2

    Args:
        wing_skeleton3d_1:
        wing_skeleton3d_2:
        default_residual:

    Returns:
        residuals: distances between wing_skeleton3d_1 and wing_skeleton3d_2
    """
    residuals = np.zeros(len(wing_skeleton3d_1.keys()))
    for i, label in enumerate(wing_skeleton3d_1.keys()):
        residuals[i] = np.sqrt(np.sum((wing_skeleton3d_2[label] - wing_skeleton3d_1[label]) ** 2))

        if np.isnan(residuals[i]):
            residuals[i] = default_residual

    return residuals


def postprocessing_estimate_limbs_params(wings_params: Dict[str, float], wings_sets):
    """
    Animal specific postprocessing function: Here will keep aspect ratio constant if specified in wings_sets

    Args:
        wings_params:
        wings_sets:

    Returns:
        wings_params:

    """

    if wings_sets.keep_init_aspect_ratio:
        for side in ['right', 'left']:
            span_v = wings_sets.skeleton3d_init[side]['tip'] - wings_sets.skeleton3d_init[side]['hinge']
            chord_v = wings_sets.skeleton3d_init[side]['leading_edge_q2'] - wings_sets.skeleton3d_init[side]['trailing_edge_q2']

            init_aspect_ratio = np.round(length_from_vector(span_v), 9) / np.round(length_from_vector(chord_v), 9)
            init_aspect_ratio = init_aspect_ratio * np.ones(np.shape(wings_params[side]['span']))

            wings_params[side]['aspect_ratio'] = init_aspect_ratio
            wings_params[side]['chord'] = wings_params[side]['span'] / init_aspect_ratio

    return wings_params


def get_copy_multi_params_zero(wings_params: Dict[str, float],
                               label_to_zero=['stroke_a', 'deviation_a', 'rotation_a']) -> Dict[str, float]:
    """

    Args:
        wings_params:
        label_to_zero:

    Returns:
        new_wings_params:
    """
    new_wings_params = copy.deepcopy(wings_params)
    for side in ['right', 'left']:
        for label in new_wings_params[side].keys():
            if label in label_to_zero:
                new_wings_params[side][label] = 0.0

    return new_wings_params


@dataclass
class LimbsModuleSettings:
    """
    Class for keeping track of the settings of the insect_wings_flat module.
    """

    def __init__(self, init_yaml_path: str) -> None:
        """

        Args:
            init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
        """

        self.name = 'wing'  # type: str
        self.sides = ['left', 'right']  # type: List[str]

        self.body_sets = BodyModuleSettings(init_yaml_path)  # type: classmethod

        with open(init_yaml_path, 'r') as file:
            init_dict = yaml.safe_load(file)

        self.params_init = init_params(init_dict, self.body_sets.params_init)   # type: Dict[str, Dict[str, any]]
        self.bounds_init = init_bounds(init_dict, self.body_sets.bounds_init)  # type: Dict[str, Dict[str, any]]
        self.skeleton3d_init = init_skeleton3d(init_dict, self.params_init)  # type: Dict[str, Dict[str, any]]

        self.param_names = list(self.params_init[self.sides[0]].keys())  # type: List[str]
        self.labels = list(self.skeleton3d_init[self.sides[0]].keys())  # type: List[str]

        self.param_names_to_keep_cst = ['span', 'chord', 'aspect_ratio']  # type: List[str]

        self.threshold_likelihood = 0.9  # type: float
        self.threshold_nb_pts = 4  # type: int

        self.keep_init_aspect_ratio = False  # type: bool

        assert 0.0 < self.threshold_likelihood <= 1.0
        assert 1 < self.threshold_nb_pts

    @staticmethod
    def from_dict(init_yaml_path: str, dictionary: Dict[str, any]):
        """

        Args:
            init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
            dictionary: Dict that contains values of class attributes that will be updated
        """

        module_sets = LimbsModuleSettings(init_yaml_path)
        for key, value in dictionary.items():
            setattr(module_sets, key, value)

        return module_sets
