import yaml
import numpy as np
import pandas as pd

from dataclasses import dataclass

from scipy import stats

from typing import Dict, List

from skeleton_fitter.modules.bodies.insect_body_slim import BodyModuleSettings

from skeleton_fitter.utils import reproject_skeleton3d_to2d, length_from_vector

# Need these imports to be able to use these functions directly from insect_wings_flat (without duplicating them here)
from skeleton_fitter.modules.limbs.insect_wings_flat import init_params, init_bounds, build_skeleton3d, \
    rotate_and_translate_skeleton3d, translate_skeleton3d, estimate_params_from_skeleton3d, \
    postprocessing_estimate_limbs_params

# TODO remove commented code?
# def init_wings_skeleton_geometry3d(wings_params):
#     """
#
#     Returns:
#         wings_skeleton3d:
#         wings_geometry3d:
#     """
#     sides = ['right', 'left']
#     labels = ['hinge', 'leading_edge_q1', 'leading_edge_q2', 'leading_edge_q3', 'tip', 'trailing_edge_q3',
#               'trailing_edge_q2', 'trailing_edge_q1']
#
#     # Initialise and get right order
#     wings_skeleton3d, wings_geometry3d = {}, {}
#     for side in sides:
#         wings_skeleton3d[side] = {}
#         for label in labels:
#             wings_skeleton3d[side][label] = np.array([0.0, 0.0, 0.0])
#
#     # Initialise wings skeleton with 8 points around circle in xy plan (hinge at [0, 0, 0], tip at [+/- span, 0, 0])
#     wings_skeleton3d['left']['tip'] = np.array([0.0, 1, 0.0])
#     wings_skeleton3d['right']['tip'] = np.array([0.0, -1.0, 0.0])
#
#     for side in sides:
#         wings_geometry3d[side]['coords'] = np.array([0.0, 0.0, 0.0])
#         wings_geometry3d[side]['index_prev_labels'] = np.array([0])
#
#     nb_parts = 3
#     for side in sides:
#         signe = +1 if side == 'left' else -1
#         radius = np.abs(wings_skeleton3d[side]['tip'][1] / 2)
#
#         # Divide the span into nb_parts
#         for n in range(1, nb_parts + 1):
#             # Build a circle (will be an ellipse after being rescaled)
#             angle = np.pi / (nb_parts + 1) * n
#
#             wings_skeleton3d[side]['leading_edge_q' + str(n)] = \
#                 np.array([radius * np.sin(angle), radius * (np.cos(angle) + signe), 0.0])
#             wings_skeleton3d[side]['trailing_edge_q' + str(n)] = \
#                 np.array([- radius * np.sin(angle), radius * (np.cos(angle) + signe), 0.0])
#
#         angle = np.linspace(0, 2 * np.pi, 10 * len(wings_skeleton3d[side].keys()))
#         wings_geometry3d[side]['coords'] = \
#             np.array([radius * np.sin(angle), radius * (np.cos(angle) + signe), np.zeros((len(angle),))]).T
#
#         wings_geometry3d[side]['index_prev_labels'] = list(range(0, len(wings_skeleton3d[side].keys())))
#         wings_geometry3d[side]['index_prev_labels'] = wings_geometry3d[side]['index_prev_labels'] * int(
#             len(angle) / len(wings_skeleton3d[side].keys()))
#         wings_geometry3d[side]['index_prev_labels'] = np.sort(wings_geometry3d[side]['index_prev_labels'])
#
#         wings_skeleton3d[side] = build_skeleton3d(wings_geometry3d[side], wings_params[side])
#         # wings_geometry3d[side] = insect_wings_flat_geo.build_skeleton3d(wings_geometry3d[side], wings_params[side])
#
#     return wings_skeleton3d, wings_geometry3d


def init_skeleton3d(init_dict: Dict[str, any], wings_params: Dict[str, float]) -> Dict[str, List[float]]:
    """

    Args:
        init_dict: Dictionary with initial values of the skeleton coordinates/parameters/bounds
        wings_params:

    Returns:
        wings_geometry3d:
    """

    labels = ['hinge', 'leading_edge_q1', 'leading_edge_q2', 'leading_edge_q3', 'tip', 'trailing_edge_q3',
              'trailing_edge_q2', 'trailing_edge_q1']

    wings_geometry3d = dict()
    for side in ['left', 'right']:
        geo_labels = list(init_dict['init_skeleton3d']['limbs']['wing'].keys())
        assert all(label in geo_labels for label in labels)
        assert len(geo_labels) > len(labels)

        wings_geometry3d[side] = {}
        for label in geo_labels:
            wings_geometry3d[side][label] = np.asarray(init_dict['init_skeleton3d']['limbs']['wing'][label])

            if side == 'right':  # the default wing is the left wing, the right need to be mirrored along th y axis
                wings_geometry3d[side][label][1] = -wings_geometry3d[side][label][1]

        wings_geometry3d[side] = build_skeleton3d(wings_geometry3d[side], wings_params[side])

    return wings_geometry3d


def residuals2d(wing_geometry3d: Dict[str, List[float]], wing_skeleton2d: Dict[str, List[float]], default_residual: float, dlt_coefs: List[List[float]]) -> List[float]:
    """
    Compute residuals when comparing wing_geometry3d to wing_skeleton2d

    Args:
        wing_geometry3d:
        wing_skeleton2d:
        default_residual:
        dlt_coefs:

    Returns:
        residuals: distances between wing_geometry3d and wing_skeleton2d
    """
    nb_cam = len(dlt_coefs)
    labels = ['hinge', 'leading_edge_q1', 'leading_edge_q2', 'leading_edge_q3', 'tip', 'trailing_edge_q3',
              'trailing_edge_q2', 'trailing_edge_q1']  # to get the right indices in wing_geometry3d['index_prev_labels']

    wing_geometry2d_from3d = reproject_skeleton3d_to2d(wing_geometry3d, dlt_coefs)
    geo_labels = list(wing_geometry2d_from3d.keys())

    residuals = np.zeros((len(wing_skeleton2d.keys()), nb_cam))
    for i, label in enumerate(labels):
        for j, camn in enumerate(range(1, nb_cam + 1)):
            # TODO! add weight on the likelihood? wing_skeleton2d[label][camn][-1]

            index_wing_segment = [ind for ind, geo_label in enumerate(geo_labels) if label in geo_label]
            wing_segment = [wing_geometry2d_from3d[geo_label][camn][0:2] for geo_label in [geo_labels[ind] for ind in index_wing_segment]]

            len_segment = len(wing_segment)
            distance_to_wing_segment = [np.sqrt(np.sum((wing_skeleton2d[label][camn][0:2] - s) ** 2)) for s in wing_segment]

            variance = len_segment*10
            # weight = np.zeros((len_segment,))
            weight = stats.norm.pdf(range(0, len_segment), len_segment / 2, np.sqrt(variance))
            weight = (1.0 - weight / np.max(weight)) * 10.0 + 1.0

            residuals[i][j] = np.min(distance_to_wing_segment * weight)

            if np.isnan(residuals[i][j]):
                residuals[i][j] = default_residual

    return residuals.flatten()


def residuals3d(wing_geometry3d: Dict[str, List[float]], wing_skeleton3d: Dict[str, List[float]], default_residual: float) -> List[float]:
    """
    Compute residuals when comparing wing_geometry3d to wing_skeleton3d

    Args:
        wing_geometry3d:
        wing_skeleton3d:
        default_residual:

    Returns:
        residuals: distances between wing_geometry3d and wing_skeleton3d
    """
    labels = ['hinge', 'leading_edge_q1', 'leading_edge_q2', 'leading_edge_q3', 'tip', 'trailing_edge_q3',
              'trailing_edge_q2', 'trailing_edge_q1']  # to get the right indices in wing_geometry3d['index_prev_labels']

    geo_labels = list(wing_geometry3d.keys())

    residuals = np.zeros(len(wing_skeleton3d.keys()))
    for i, label in enumerate(labels):
        index_wing_segment = [ind for ind, geo_label in enumerate(geo_labels) if label in geo_label]
        wing_segment = [wing_geometry3d[geo_label] for geo_label in [geo_labels[ind] for ind in index_wing_segment]]

        len_segment = len(wing_segment)
        distance_to_wing_segment = [np.sqrt(np.sum((wing_skeleton3d[label] - s) ** 2)) for s in wing_segment]

        variance = len_segment*10
        # weight = np.zeros((len_segment,))
        weight = stats.norm.pdf(range(0, len_segment), len_segment / 2, np.sqrt(variance))
        weight = (1.0 - weight / np.max(weight)) * 10.0 + 1.0

        residuals[i] = np.min(distance_to_wing_segment * weight)

        if np.isnan(residuals[i]):
            residuals[i] = default_residual

    return residuals


@dataclass
class LimbsModuleSettings:
    """
    Class for keeping track of the settings of the insect_wings_flat_geo module.
    """

    def __init__(self, init_yaml_path: str) -> None:
        """

        Args:
            init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
        """

        self.name = 'wing'  # type: str
        self.sides = ['left', 'right']  # type: List[str]

        self.body_sets = BodyModuleSettings(init_yaml_path)  # type: classmethod

        #   yaml_path: Path of a yaml file that contains the skeleton parameters for an animal (e.g. mosquito)

        with open(init_yaml_path, 'r') as file:
            init_dict = yaml.safe_load(file)

        self.params_init = init_params(init_dict, self.body_sets.params_init)   # type: Dict[str, Dict[str, any]]
        self.bounds_init = init_bounds(init_dict, self.body_sets.bounds_init)  # type: Dict[str, Dict[str, any]]
        self.skeleton3d_init = init_skeleton3d(init_dict, self.params_init)  # type: Dict[str, Dict[str, any]]

        self.param_names = list(self.params_init[self.sides[0]].keys())  # type: List[str]
        self.labels = list(self.skeleton3d_init[self.sides[0]].keys())  # type: List[str]

        self.param_names_to_keep_cst = ['span', 'chord', 'aspect_ratio']  # type: List[str]

        self.threshold_likelihood = 0.9  # type: float
        self.threshold_nb_pts = 4  # type: int

        self.keep_init_aspect_ratio = True  # type: bool

        assert 0.0 < self.threshold_likelihood <= 1.0
        assert 1 < self.threshold_nb_pts

    @staticmethod
    def from_dict(init_yaml_path: str, dictionary: Dict[str, any]):
        """

        Args:
            init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
            dictionary: Dict that contains values of class attributes that will be updated
        """

        module_sets = LimbsModuleSettings(init_yaml_path)
        for key, value in dictionary.items():
            setattr(module_sets, key, value)

        return module_sets

