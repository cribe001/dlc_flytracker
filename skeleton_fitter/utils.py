import numpy as np

from camera import calib


def reproject_skeleton3d_to2d(skeleton3d, dlt_coefs):
    """

    Args:
        skeleton3d:
        dlt_coefs:

    Returns:
        skeleton2d_from3d:
    """

    nb_cam = len(dlt_coefs)

    skeleton2d_from3d = {}
    for i, label in enumerate(skeleton3d.keys()):
        skeleton2d_from3d[label] = {1: []}
        for j, camn in enumerate(range(1, nb_cam + 1)):
            uv = calib.find2d(1, dlt_coefs[j], np.array([skeleton3d[label]]))
            skeleton2d_from3d[label][camn] = np.array([uv[0][0], uv[0][1], 1.0])

    return skeleton2d_from3d


def length_from_vector(vector):
    return np.sqrt(vector[0] ** 2 + vector[1] ** 2 + vector[2] ** 2)


def set_axes3d_equal(ax):
    x_limits, y_limits, z_limits = ax.get_xlim3d(), ax.get_ylim3d(), ax.get_zlim3d()

    x_range, x_middle = abs(x_limits[1] - x_limits[0]), np.nanmean(x_limits)
    y_range, y_middle = abs(y_limits[1] - y_limits[0]), np.nanmean(y_limits)
    z_range, z_middle = abs(z_limits[1] - z_limits[0]), np.nanmean(z_limits)

    plot_radius = 0.5 * max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])


def check_params_ranges(params, bounds, option='raiseError'):
    for label in params.keys():
        if not np.isnan(params[label]) and not bounds[label][0] <= params[label] <= bounds[label][1]:
            if option is 'raiseError':
                raise ValueError("{0} out of range ({2} <= {1} <= {3})".format(label, params[label], bounds[label][0], bounds[label][1]))
            elif option is 'printWarning':
                print("WARN: {0} out of range ({2} <= {1} <= {3})".format(label, params[label], bounds[label][0], bounds[label][1]))
            elif option is 'change2nan':
                params[label] = np.nan
            elif option is 'doNothing':
                continue


def distance_btw_pts(pt1, pt2):
    return length_from_vector(pt2 - pt1)


def load_body_params_from_csv(path, param_names):
    body_params = {}
    params_csv = np.genfromtxt(path, delimiter=',', skip_header=0, names=True)

    for body_param_name in param_names:
        body_params[body_param_name] = params_csv[body_param_name]

    return body_params


def load_limbs_params_from_csv(path, body_param_names, limb_labels):
    limbs_params = {}
    params_csv = np.genfromtxt(path, delimiter=',', skip_header=0, names=True)

    for side in ['right', 'left']:
        limbs_params[side] = {}
        for limb_param_name in limb_labels:
            if limb_param_name in body_param_names:
                limbs_params[side][limb_param_name] = params_csv[limb_param_name]
            else:
                limbs_params[side][limb_param_name] = params_csv[limb_param_name + '_' + side]

    return limbs_params