import os, yaml, glob
import cv2
import numpy as np
from PIL import Image
from collections import defaultdict

from typing import List

from dataclasses import dataclass

from utils.settings import BaseSettings


@dataclass
class BackgroundSubtractorSettings(BaseSettings):
    """
    Class for keeping track of the settings of BackgroundSubtractorSettings.
    """
    type: str = 'KNN'  # 'KNN' or 'MOG2'
    varThreshold: int = 12
    dist2Threshold: int = 50
    shadow_threshold: int = 1

    @staticmethod
    def from_dict(raw):
        """Creates a 3D reconstructor settings from a dictionary.

        Args:
            raw: Dictionary to use.

        Returns:
            Background subtractor settings initialised from the dictionary.
        """
        return BackgroundSubtractorSettings(**raw)


@dataclass
class BlobDetectorSettings(BaseSettings):
    """
    Class for keeping track of the settings of BlobDetectorSettings.
    """
    threshold_min: int = 1
    threshold_max: int = 255
    
    area_min: int = 200
    area_max: int = 10000
    
    inertia_ratio_min: float = 0.01
    inertia_ratio_max: float = 1.0

    @staticmethod
    def from_dict(raw):
        """Creates a 3D reconstructor settings from a dictionary.

        Args:
            raw: Dictionary to use.

        Returns:
            Blob detector settings initialised from the dictionary.
        """
        return BlobDetectorSettings(**raw)


class Tracker2D:
    """
    Class to track 2d coordinates of blobs in images
    """
    def __init__(self, images, frames):
        """

        Args:
            images: List of images (numpy arrays)
            frames: List of frame numbers (int)
        """

        self.show_plot = False

        self.images = images  # type: List[np.array()]
        self.frames = frames  # type: List[int]

        self.points = defaultdict(list)

        self.get_blob_detector(BlobDetectorSettings())
        self.get_back_subtractor(BackgroundSubtractorSettings())

    @staticmethod
    def load_images_from_path(image_path: str, image_format: str = 'tif') -> "Tracker2D":
        """ Initialize an instance of Tracker2D from a path that contains a sequence of images

        Args:
            image_path: the path where the images are
            image_format: the image format (e.g. 'tif', 'png', 'jpg')

        Return:
            instance of Tracker2D
        """

        all_image_paths = glob.glob(os.path.join(image_path, '*.{0}'.format(image_format)))

        image_names = [os.path.basename(image_path) for image_path in all_image_paths]
        frames = [int(image_name[20:-len('.{0}'.format(image_format))]) for image_name in image_names]

        if len(image_names) == 0:
            raise Exception("No images were found in {0} with the extension .{1}".format(image_path, image_format))

        return Tracker2D.load_images(image_names, image_path, frames)

    @staticmethod
    def load_images(image_names: List[str], path: str, frames: List[int] = None) -> "Tracker2D":
        """ Initialize an instance of Tracker2D from a list of images and their path

        Args:
            image_names: a List of image names
            path: The path where the images are
            frames: a list of int with the frame number corresponding to the images

        Return:
            instance of Tracker2D
        """

        assert len(image_names) == len(frames)

        images = [cv2.imread(os.path.join(path, image_name), cv2.IMREAD_GRAYSCALE) for image_name in image_names]

        assert not all([img is None for img in images])

        if frames is None:
            frames = list(range(1, len(images) +1))

        else:
            frames = frames

        in_sort = np.argsort(frames)
        images = [images[in_s] for in_s in in_sort]
        frames = [frames[in_s] for in_s in in_sort]

        return Tracker2D(images, frames)

    def do_tracking(self, ind_img_for_background: List[int] = None) -> None:
        """ Do 2d tracking on the loaded list of images (background-subtraction and then blob detection).

        Args:
            ind_img_for_background: Index of images used for the background
        """

        assert len(self.images) != 0, "Images need to be loaded first"

        if ind_img_for_background is None: ind_img_for_background = range(0, len(self.images))

        [self.back_subtractor.apply(self.images[i], learningRate=-1) for i in ind_img_for_background]

        self.background = self.back_subtractor.getBackgroundImage()
        self.background = cv2.medianBlur(self.background, 5)  # Add median filter to images

        self.points = defaultdict(list)

        self.foreground_masks = self.images.copy()
        for i, image in enumerate(self.images):
            self.foreground_masks[i] = self.back_subtractor.apply(image, learningRate=-1)
            self.foreground_masks[i] = cv2.medianBlur(self.foreground_masks[i], 3)  # Add median filter to images

            blobs = self.blob_detector.detect((255 - self.foreground_masks[i]))  # Detect blobs

            x_blob, y_blob = [], []
            if len(blobs) != 0:
                for blob in blobs:
                    x, y = blob.pt  # (width, height) with zero on top-left corner
                    x_blob.append(x), y_blob.append(y)

                    self.points['x'].append(x)
                    self.points['y'].append(y)
                    self.points['frame'].append(self.frames[i])
                    self.points['area'].append(blob.size ** 2)

            # contours, _ = cv2.findContours(self.foreground_masks[i], cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2:]
            # for contour in contours:
            #     approx = cv2.approxPolyDP(contour, 0.005 * cv2.arcLength(contour, True), True)
            #     contour_area = cv2.contourArea(contour)

    def gen_stroboscopic_image(self, radius: int, shape='square', step=1, flip_time=False, background_color=-1) -> None:
        """

        Args:
            radius:
            shape:
            step:
            flip_time:
            background_color:
        """

        if len(self.images) == 0:
            print("WARN: you need to load images first")
            return

        if 'frame' not in self.points.keys() or len(self.points['frame']) == 0:
            print("WARN: you need to track or load 2d points first")
            return

        [self.back_subtractor.apply(image, learningRate=-1) for image in self.images[:-6]]

        if flip_time:  # Will start analysing last frame
            images, frames = np.flip(self.images, axis=0), np.flip(self.frames)
        else:
            images, frames = self.images, self.frames

        self.background = self.back_subtractor.getBackgroundImage()
        self.background = cv2.medianBlur(self.background, 5)  # Add median filter to images

        if 'strip' in shape: height, width = images[0].shape

        if 0 <= background_color <= 1:
            self.strobe_img = np.ones(images[0].shape, np.uint8) * 255 * background_color
        else:
            self.strobe_img = self.background.copy()

        self.foreground_masks = images.copy()
        for i, image in enumerate(images):
            if not i % step == 0: continue

            self.foreground_masks[i] = self.back_subtractor.apply(image, learningRate=0)
            self.foreground_masks[i] = cv2.medianBlur(self.foreground_masks[i], 3)  # Add median filter to images

            in_frame = np.where(frames[i] == self.points['frame'])[0]

            if len(in_frame) == 0: continue
            elif len(in_frame) > 1: in_frame = in_frame[0]

            if np.isnan(self.points['x'][in_frame]): continue
            x, y = int(self.points['x'][in_frame]), int(self.points['y'][in_frame])

            new_mask = np.zeros(image.shape, np.uint8)
            if 'rectangle' in shape:
                new_mask = cv2.rectangle(new_mask, (x - radius, y - radius), (x + radius, y + radius), (255), -1)

            elif 'circle' in shape:
                new_mask = cv2.circle(new_mask, (x, y), radius, (255), -1)
                
            elif 'strip' in shape:
                if 'height' in shape:
                    new_mask = cv2.rectangle(new_mask, (x - radius, 0), (x + radius, height), (255), -1)
                if 'width' in shape:
                    new_mask = cv2.rectangle(new_mask, (0, y - radius), (width, y + radius), (255), -1)
            else:
                print('WARN: cannot recognize shape (', shape, ')!!')

            self.foreground_masks[i] = np.multiply(self.foreground_masks[i] / 255, new_mask /255)
            self.strobe_img = image * self.foreground_masks[i] + self.strobe_img * (1 - self.foreground_masks[i])

        self.strobe_img = self.strobe_img.astype(np.uint8)

    def get_back_subtractor(self, back_subtractor_settings: BackgroundSubtractorSettings()) -> None:
        """

        Args:
            back_subtractor_settings:
        """

        if back_subtractor_settings.type == 'MOG2':
            self.back_subtractor = cv2.createBackgroundSubtractorMOG2(history=-1, varThreshold=back_subtractor_settings.varThreshold, detectShadows=True)

        elif back_subtractor_settings.type == 'KNN':
            self.back_subtractor = cv2.createBackgroundSubtractorKNN(history=-1, dist2Threshold=back_subtractor_settings.dist2Threshold, detectShadows=True)
            self.back_subtractor.setShadowThreshold(back_subtractor_settings.shadow_threshold)  # 0.5 by default

    def get_blob_detector(self, blob_detector_settings: BlobDetectorSettings()) -> None:
        """

        Args:
            blob_detector_settings:
        """

        # Setup SimpleBlobDetector parameters.
        params = cv2.SimpleBlobDetector_Params()

        params.minThreshold = blob_detector_settings.threshold_min
        params.maxThreshold = blob_detector_settings.threshold_max

        params.filterByArea = True
        params.minArea = blob_detector_settings.area_min
        params.maxArea = blob_detector_settings.area_max

        params.filterByInertia = True
        params.minInertiaRatio = blob_detector_settings.inertia_ratio_min
        params.maxInertiaRatio = blob_detector_settings.inertia_ratio_max

        params.filterByCircularity = False
        params.filterByConvexity = False

        # Create a self.blob_detector with the parameters
        ver = cv2.__version__.split('.')
        if int(ver[0]) < 3:
            self.blob_detector = cv2.SimpleBlobDetector(params)
        else:
            self.blob_detector = cv2.SimpleBlobDetector_create(params)

    def save_csv(self, save_name: str, save_path: str) -> None:
        """
        Save 2d coordinates in .csv file

        Args:
            save_name:
            save_path:
        """

        # if os.path.exists(os.path.join(save_path, save_name + '-2d_points.csv')):
        #     os.remove(os.path.join(save_path, save_name + '-2d_points.csv'))
        np.savetxt(os.path.join(save_path, save_name + '-2d_points.csv'),
                   np.c_[self.points['frame'], self.points['x'], self.points['y']], delimiter=',', header='frame,x_px,y_px')

    def save_stroboscopic_image(self, save_name: str, save_path: str) -> None:
        """

        Args:
            save_name:
            save_path:
        """

        Image.fromarray(self.strobe_img).save(os.path.join(save_path, save_name + '-strobe.tif'))


if __name__ == '__main__':
    import glob

    image_format = 'tif'
    # rec_name = 'cam1_20200303_030117'
    # image_path = os.path.join('/media/user/MosquitoEscape_Photron1/Photron1/_Process/_DownSized/', rec_name)
    nb_cam = 3
    rec_names = {1: 'cam1_20200303_030117', 2: 'cam2_20200303_030120', 3: 'cam3_20200303_030120'}
    image_paths = {1: '/media/user/MosquitoEscape_Photron1/Photron1/_Process/_DownSized/',
                   2: '/media/user/MosquitoEscape_Photron2/Photron2/_Process/_DownSized/',
                   3: '/media/user/MosquitoEscape_Photron3/Photron3/_Process/_DownSized/'}

    for camn in range(1, nb_cam + 1):
        print(os.path.join(image_paths[camn], rec_names[camn]))

        all_image_paths = glob.glob(os.path.join(image_paths[camn], '*.{0}'.format(image_format)))
        image_names = [os.path.basename(image_path) for image_path in all_image_paths]
        frames = [int(image_name[20:-len('.{0}'.format(image_format))]) for image_name in image_names]

        tracker = Tracker2D.load_images(image_names, image_paths[camn], frames)
        tracker.do_tracking()

        tracker.save_csv(rec_names[camn], os.path.join(image_paths[camn], rec_names[camn]))


