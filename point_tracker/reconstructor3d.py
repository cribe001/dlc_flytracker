import os, yaml

from typing import List, Tuple

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from dataclasses import dataclass

from utils.settings import BaseSettings

import camera.calib as calib


@dataclass
class Reconstructor3DSettings(BaseSettings):
    """
    Class for keeping track of the settings of Reconstructor3D.
    """

    with_missing: bool = False  # Whether to try to reconstruct 3d obj with less than nb_cam number of 2d points

    threshold_mean_repro_err: float = 5.0
    threshold_nb_camera_views: int = 2  # Minimum number of camera views that will be used for the 3d reconstruction (when with_missing = True)
    threshold_distance_tracks: float = 0.02  # max distance (in meters) between two successive 3d points to set as a track
    threshold_delay_tracks: int = 10  # max delay (in frames) between two successive 3d points to set as a track
    threshold_length_tracks: int = 5  # minimum length of a track to be kept

    image_ratio: float = 1.0  # ratio between size of the images used for calibration over the analyzed images (if changed).

    @staticmethod
    def from_dict(raw):
        """Creates a 3D reconstructor settings from a dictionary.

        Args:
            raw: Dictionary to use.

        Returns:
            3D reconstructor settings initialised from the dictionary.
        """

        return Reconstructor3DSettings(**raw)


class Reconstructor3D:
    """
    Class used to reconstruct 3d coordinates of object from 2d coordinates on images
    """
    def __init__(self, dlt_coefs: List[List[float]]):
        """

        Args:
            dlt_coefs: DLT coefficients (11*nb_cam)
        """

        assert len(dlt_coefs[0]) == 11

        self.dlt_coefs = dlt_coefs
        self.nb_cam = len(self.dlt_coefs)

        self.settings = Reconstructor3DSettings()
        assert self.settings.threshold_nb_camera_views >= 2

        self.pts_dict = {}
        self.objs_dict = {}

    @staticmethod
    def read_dlt_coefs(path: str) -> "Reconstructor3D":
        """
        Read DLT coefficients from .csv file

        Args:
            path: path of the .csv file with DLT (11) coefficients (along rows) for all camera (along column)

        Returns
            Reconstructor3D
        """

        dlt_coefs = calib.read_dlt_coefs(path)

        return Reconstructor3D(dlt_coefs)

    def recon_objs(self, xs_pts: List[int], ys_pts: List[int], frames_coord: List[int], sort_by_rmse: bool = True) -> Tuple[any]:
        """
        Reconstruct 3d coordinates of objects from 2d coordinates of points for all frames
        args are vectors of vectors (e.g. np.array([[451, 123], [78], [147, 65, 78, 79]])) of size nb_cam*nb_obj
        frames should look like xs_pts (e.g. [[1, 2], [1], [1, 1, 2, 5]])

        Args:
            xs_pts: x coordinates in pixel of all pts for all frames for each camera
            ys_pts: y coordinates in pixel of all pts for all frames for each camera
            frames_coord: corresponding frames of all pts for each camera (can have duplicates (several pts per frames))

            sort_by_rmse: Whether to sort the results by rmse (lower error first)

        Returns:
            xs_objs: x coordinates of all reconstructed objects for all frames
            ys_objs: y coordinates of all reconstructed objects for all frames
            zs_objs: z coordinates of all reconstructed objects for all frames
            frames_objs: corresponding frames for all reconstructed objects
            index_match_pts: index of the matched 2d pts used to reconstruct 3d coordinates for all frames (will be None if the cam wasn't used for this match)
            rmses_objs: minimized rmse for all objects for all frames
        """

        self.pts_dict = {}
        all_frames = np.array([])
        for i, camn in enumerate(range(1, self.nb_cam + 1)):
            self.pts_dict[camn] = {'x': xs_pts[i], 'y': ys_pts[i], 'frame': frames_coord[i]}

            all_frames = np.append(all_frames, self.pts_dict[camn]['frame'])

        self.pts_dict['frame'] = np.sort(np.unique(all_frames))

        for i, camn in enumerate(range(1, self.nb_cam + 1)):
            self.pts_dict[camn]['index_frames'] = []
            for frame in self.pts_dict['frame']:
                index_frame = np.squeeze(np.argwhere(frame == self.pts_dict[camn]['frame']))

                if index_frame.size == 1:
                    self.pts_dict[camn]['index_frames'].append([int(index_frame)])
                else:
                    self.pts_dict[camn]['index_frames'].append(list(index_frame))

        xs_objs, ys_objs, zs_objs, frames_objs = [], [], [], []
        indexes_pts, rmses_objs, xs_px_objs, ys_px_objs = [], [], [], []
        for i, frame in enumerate(self.pts_dict['frame']):
            frame = int(frame)

            xs_pt, ys_pt = [], []
            for camn in range(1, self.nb_cam + 1):
                indexes_frame = self.pts_dict[camn]['index_frames'][i]

                xs = np.asarray([self.pts_dict[camn]['x'][index] for index in indexes_frame])
                ys = np.asarray([self.pts_dict[camn]['y'][index] for index in indexes_frame])

                xs_pt.append(xs), ys_pt.append(ys)

            xs_pt, ys_pt = np.asarray(xs_pt, dtype=object), np.asarray(ys_pt, dtype=object)

            if xs_pt.size == 0:
                continue
            xs_obj, ys_obj, zs_obj, index_match_pts, rmses_obj = self.match_pts_and_recon_obj(xs_pt, ys_pt)
            if xs_obj.size == 0:
                continue

            index_pts = - np.ones(np.array(index_match_pts).shape)
            if np.array(index_match_pts).size:
                for j, row in enumerate(index_match_pts):
                    for k, cell in enumerate(row):
                        indexes_frame = self.pts_dict[k+1]['index_frames'][i]

                        if cell == None:
                            index_pts[j][k] = None
                        else:
                            index_pts[j][k] = indexes_frame[cell]

            if sort_by_rmse:
                # Sort obj by rmse (start with obj with the minimum rmse)
                in_sort = np.argsort(rmses_obj)
                if in_sort.size:
                    xs_obj, ys_obj, zs_obj = [xs_obj[in_s] for in_s in in_sort], [ys_obj[in_s] for in_s in in_sort], [zs_obj[in_s] for in_s in in_sort]
                    index_pts, rmses_obj = [np.array(index_pts[in_s]) for in_s in in_sort], [rmses_obj[in_s] for in_s in in_sort]
            else:
                len_x = len(xs_obj)
                xs_obj, ys_obj, zs_obj = [xs_obj[i] for i in range(0, len_x)], [ys_obj[i] for i in range(0, len_x)], [zs_obj[i] for i in range(0, len_x)]
                index_pts, rmses_obj = [np.array(index_pts[i]) for i in range(0, len_x)], [rmses_obj[i] for i in range(0, len_x)]

            xs_objs.append(xs_obj), ys_objs.append(ys_obj), zs_objs.append(zs_obj)
            frames_objs.append(frame), rmses_objs.append(rmses_obj), indexes_pts.append(index_pts)
            xs_px_objs.append(xs_pt), ys_px_objs.append(ys_pt)

        self.objs_dict['x'], self.objs_dict['y'], self.objs_dict['z'] = xs_objs, ys_objs, zs_objs
        self.objs_dict['frame'], self.objs_dict['rmse'], self.objs_dict['index_pts'] = frames_objs, rmses_objs, indexes_pts
        self.objs_dict['x_px'], self.objs_dict['y_px'] = xs_px_objs, ys_px_objs

        return xs_objs, ys_objs, zs_objs, frames_objs, indexes_pts, rmses_objs

    def match_pts_and_recon_obj(self, xs_pts, ys_pts):
        """
        Match 2d pts together (by minimizing rmse) and reconstruct 3d coordinates of objects

        Args:
            xs_pts: x coordinates in pixel of all pts for all camera
            ys_pts: y coordinates in pixel of all pts for all camera

        Returns:
            xs_obj: x coordinates for all objects (3d reconstructed and matched to minimized rmse)
            ys_obj: y coordinates for all objects
            zs_obj: z coordinates for all objects
            index_match_pts: index of the matched 2d pts used to reconstruct 3d coordinates
            rmses_obj: minimized rmse for all objects
        """

        if self.settings.with_missing:
            # Make matrix with indexes of cameras and with None when camera is missing (nan values in xs_px)
            # (add a row of missing cam to look at combination involving less than the max number of cameras)
            indexes_frames = [np.append(np.arange(0, np.array(xs).size), None) for xs in xs_pts]
            for i in range(0, len(xs_pts)):
                for j in range(0, len(xs_pts[i])):
                    if np.isnan(xs_pts[i][j]):
                        indexes_frames[i][j] = None
        else:
            indexes_frames = [[i if not np.isnan(x) else None for i, x in enumerate(xs)] for xs in xs_pts]

        combi_indexes_frames = np.array(np.meshgrid(*indexes_frames)).reshape(self.nb_cam, -1).T

        # Remove combination(s) with less camera than minimum (2 or more)
        for num_combi in reversed(range(0, len(combi_indexes_frames))):
            index_match = list(combi_indexes_frames[num_combi])

            count_missing = sum([s == None for s in index_match])
            if (self.nb_cam - count_missing) < self.settings.threshold_nb_camera_views:
                combi_indexes_frames = np.delete(combi_indexes_frames, num_combi, 0)

        index_match_pts, rmses_obj = np.array([]), np.array([])
        xs_obj, ys_obj, zs_obj = np.array([]), np.array([]), np.array([])
        for num_combi in range(0, len(combi_indexes_frames)):
            index_match = list(combi_indexes_frames[num_combi])
            count_missing = sum([s == None for s in index_match])

            cur_Ls = self.dlt_coefs
            pts_cams = np.array([])
            for i in range(0, self.nb_cam):
                if index_match[i] == None:
                    cur_Ls = np.delete(cur_Ls, i, 0)

                else:
                    pts_cam = np.array([xs_pts[i][index_match[i]], ys_pts[i][index_match[i]]]).T
                    pts_cams = np.vstack([pts_cams, pts_cam]) if pts_cams.size > 0 else pts_cam

            obj_coord = calib.recon3d(3, self.nb_cam - count_missing, cur_Ls, pts_cams*self.settings.image_ratio)

            repro_pts_cams = calib.find2d(self.nb_cam, self.dlt_coefs, np.array([obj_coord]))
            repro_pts_cams = repro_pts_cams/self.settings.image_ratio

            repro_pts_cams_wo_missing = \
                np.array([repro_pts_cams[j] for j, _ in enumerate(range(1, self.nb_cam + 1)) if index_match[j] != None])

            # To correct the error reduction when we use fewer points than the max number of cams
            correction_factor = np.sqrt(1 + count_missing) if count_missing > 0 else 1
            mean_repro_err = np.mean(np.sqrt(np.sum((repro_pts_cams_wo_missing - pts_cams) ** 2, 1))) * correction_factor

            if mean_repro_err < self.settings.threshold_mean_repro_err:
                # Fill tracking results
                xs_obj = np.append(xs_obj, obj_coord[0])
                ys_obj = np.append(ys_obj, obj_coord[1])
                zs_obj = np.append(zs_obj, obj_coord[2])
                rmses_obj = np.append(rmses_obj, mean_repro_err)

                index_match_pts = np.vstack([index_match_pts, index_match]) if index_match_pts.size else np.array([index_match])

                # TODO! prevent using same coordinate for next association

        return xs_obj, ys_obj, zs_obj, index_match_pts, rmses_obj

    def recon_tracks(self, xs_objs, ys_objs, zs_objs, frames_objs, indexes_pts):
        """
        Reconstruct 3d tracks from 3d coordinates of objects for all frames
        args are vectors of vectors (e.g. np.array([[451, 123], [78], [147, 65, 78, 79]])) of size nb_cam*nb_obj

        Args:
            xs_objs: x coordinates of all reconstructed objects for all frames
            ys_objs: y coordinates of all reconstructed objects for all frames
            zs_objs: z coordinates of all reconstructed objects for all frames
            frames_objs: corresponding frames for all reconstructed objects (sorted and unique)
            indexes_pts: index of the matched 2d pts used to reconstruct 3d coordinates for all frames

        Returns:
            tracks_dict: dictionary with tracks of all objects (e.g. tracks_dict[nb_obj] = {'x': [], ...})
        """

        self.tracks_dict = {'obj': {}, 'obj_names': []}
        if type(frames_objs) is not list:
            raise ValueError('frames_objs need to be a list')

        elif not np.array_equal(frames_objs, np.sort(np.unique(frames_objs))):
            raise ValueError('frames_objs must be sorted and unique')

        elif len(frames_objs) <= 1:
            self.tracks_dict = {'obj': {}, 'obj_names': []}
            return self.tracks_dict

        nb_objs_live = []

        min_frame_diff = np.min(np.array(frames_objs[1:]) - np.array(frames_objs[:-1]))

        for i, frame in enumerate(frames_objs):
            if len(nb_objs_live) == 0:  # to initialize first track
                for j in range(0, len(xs_objs[i])):
                    nb_objs_live.append(j + 1)
                    self.tracks_dict['obj'][nb_objs_live[-1]] = {'x': [xs_objs[i][j]], 'y': [ys_objs[i][j]],
                            'z': [zs_objs[i][j]], 'frame': [frame], 'index_pts': [indexes_pts[i][j]]}

                continue

            # TODO! use kalman filter to predict position and improve tracking of multiple objects
            #  (compute min btw estimated position and real position)
            # TODO! find the best match between all points and all objs, currently find best match for an obj,
            #  then find best match for next obj with all points except the previously matched point
            for j in range(0, len(xs_objs[i])):
                distances = []
                for nb_obj_live in nb_objs_live:
                    if self.tracks_dict['obj'][nb_obj_live]['frame'][-1] == frame:  # to avoid using same frame twice
                        distances.append(np.nan)
                    else:
                        distances.append(np.sqrt((self.tracks_dict['obj'][nb_obj_live]['x'][-1] - xs_objs[i][j]) ** 2
                                               + (self.tracks_dict['obj'][nb_obj_live]['y'][-1] - ys_objs[i][j]) ** 2
                                               + (self.tracks_dict['obj'][nb_obj_live]['z'][-1] - zs_objs[i][j]) ** 2))

                # if distance between two successive object is small, then they are stitched together in one track
                if not np.isnan(distances).all() and np.nanmin(distances) < self.settings.threshold_distance_tracks:
                    nb_obj_distance_min = nb_objs_live[np.nanargmin(distances)]

                    self.tracks_dict['obj'][nb_obj_distance_min]['x'].append(xs_objs[i][j])
                    self.tracks_dict['obj'][nb_obj_distance_min]['y'].append(ys_objs[i][j])
                    self.tracks_dict['obj'][nb_obj_distance_min]['z'].append(zs_objs[i][j])
                    self.tracks_dict['obj'][nb_obj_distance_min]['frame'].append(frame)
                    self.tracks_dict['obj'][nb_obj_distance_min]['index_pts'] = \
                        np.vstack([self.tracks_dict['obj'][nb_obj_distance_min]['index_pts'], indexes_pts[i][j]])

                else:  # start a new tracks
                    nb_objs_live.append(nb_objs_live[-1] + 1)
                    self.tracks_dict['obj'][nb_objs_live[-1]] = {'x': [xs_objs[i][j]], 'y': [ys_objs[i][j]],
                            'z': [zs_objs[i][j]], 'frame': [frame], 'index_pts': [indexes_pts[i][j]]}

                # remove obj from nb_objs_live if too old (more than a given number of frames)
                for k, nb_obj_live in enumerate(nb_objs_live):
                    frame_diff = frame - self.tracks_dict['obj'][nb_obj_live]['frame'][-1]
                    if frame_diff > (self.settings.threshold_delay_tracks * min_frame_diff):
                        del nb_objs_live[k]

        # Get rid of tracks that are too short and add nan to other
        for nb_obj in list(self.tracks_dict['obj'].keys()):
            if len(self.tracks_dict['obj'][nb_obj]['x']) <= self.settings.threshold_length_tracks:
                del self.tracks_dict['obj'][nb_obj]
            else:
                self.tracks_dict['obj_names'].append('obj{0}'.format(nb_obj))

                frames_obj = self.tracks_dict['obj'][nb_obj]['frame']
                frames_obj = np.arange(np.min(frames_obj), np.max(frames_obj), min_frame_diff)
                xs_objs, ys_objs, zs_objs, index_pts = [], [], [], []
                for i, frame in enumerate(frames_obj):
                    in_frame = np.squeeze(np.where(frame == self.tracks_dict['obj'][nb_obj]['frame']))

                    if in_frame.shape == (0,):
                        xs_objs.append(np.nan), ys_objs.append(np.nan), zs_objs.append(np.nan)
                        index_pts.append([None, None, None])
                    else:
                        xs_objs.append(self.tracks_dict['obj'][nb_obj]['x'][in_frame])
                        ys_objs.append(self.tracks_dict['obj'][nb_obj]['y'][in_frame])
                        zs_objs.append(self.tracks_dict['obj'][nb_obj]['z'][in_frame])
                        index_pts.append(self.tracks_dict['obj'][nb_obj]['index_pts'][in_frame])

                self.tracks_dict['obj'][nb_obj]['x'] = xs_objs
                self.tracks_dict['obj'][nb_obj]['y'] = ys_objs
                self.tracks_dict['obj'][nb_obj]['z'] = zs_objs
                self.tracks_dict['obj'][nb_obj]['frame'] = frames_obj
                self.tracks_dict['obj'][nb_obj]['index_pts'] = index_pts

        print(">>> {0} track(s) have been kept (out of {1} reconstructed tracks)"
              .format(len(self.tracks_dict['obj_names']), len(nb_objs_live)))

        return self.tracks_dict

    def get_2d_reprojection(self, xs_obj: List[float], ys_obj: List[float], zs_obj: List[float]) -> Tuple[List[int], List[int]]:
        """
        Get 2d reprojection from reconstruct 3d coordinates

        Args:
            xs_obj: x coordinates for all objects (3d reconstructed)
            ys_obj: y coordinates for all objects
            zs_obj: z coordinates for all objects

        Returns:
            xs_pts: x coordinates in pixel reprojection from 3d coordinates
            ys_pts: y coordinates in pixel
        """

        xs_pts, ys_pts = [[] for i in range(0, self.nb_cam)], [[] for i in range(0, self.nb_cam)]
        for i in range(0, len(xs_obj)):
            #if len(xs_obj[i]) == 0: continue

            obj_coord = np.array([[xs_obj[i], ys_obj[i], zs_obj[i]]])

            for j, camn in enumerate(range(1, self.nb_cam + 1)):
                repro_pts_cam = calib.find2d(1, self.dlt_coefs[j], obj_coord).T
                repro_pts_cam = repro_pts_cam/self.settings.image_ratio

                xs_pts[j].append(repro_pts_cam[0][0])
                ys_pts[j].append(repro_pts_cam[1][0])

        xs_pts = np.array(xs_pts)/self.settings.image_ratio
        ys_pts = np.array(ys_pts)/self.settings.image_ratio

        return xs_pts, ys_pts

    def save_points_csv(self, names: List[str], paths: List[str], save_type='all_points') -> None:
        """
        Save 3d points in .csv files

        Args:
            names: names of .csv files
            paths: paths where to save .csv files
            save_type: 'all_frames' (save one point per frame) or 'all_points' (save all points)
        """

        if save_type == 'all_frames':
            # Save best 3d reconstructed points (min rsme) for each frame (if no point for a frame, will save nan)
            xs_objs, ys_objs, zs_objs, frames_objs = [], [], [], []
            for i, frame in enumerate(self.pts_dict['frame']):
                ins_frame = np.where(self.objs_dict['frame'] == frame)[0]
                if len(ins_frame) >= 1 and len(self.objs_dict['x'][ins_frame[0]]) > 0:
                    in_frame = ins_frame[0]
                    xs_objs.append(self.objs_dict['x'][in_frame][0])
                    ys_objs.append(self.objs_dict['y'][in_frame][0])
                    zs_objs.append(self.objs_dict['z'][in_frame][0])
                    frames_objs.append(frame)

                else:
                    xs_objs.append(np.nan)
                    ys_objs.append(np.nan)
                    zs_objs.append(np.nan)
                    frames_objs.append(frame)

            if not np.array_equal(frames_objs, np.sort(np.unique(frames_objs))):
                raise ValueError('frames_objs must be sorted and unique')

        elif save_type == 'all_points':
            # Save all 3d reconstructed points (might not have a point for each frame)
            frames_objs = [np.ones(np.array(self.objs_dict['x'][i]).shape) * frame for i, frame in enumerate(self.pts_dict['frame'])]
            xs_objs = np.concatenate(self.objs_dict['x'])
            ys_objs = np.concatenate(self.objs_dict['y'])
            zs_objs = np.concatenate(self.objs_dict['z'])
            frames_objs = np.concatenate(frames_objs)

        else:
            raise ValueError("'save_type should be 'all_points' or 'all_frames'")

        for i in range(0, len(paths)):
            np.savetxt(os.path.join(paths[i], names[i] + '-3d_points.csv'), np.c_[frames_objs, xs_objs, ys_objs, zs_objs],
                       delimiter=',', header='frame,x,y,z')

    def save_tracks_csv(self, names: List[str], paths: List[str]) -> None:
        """
        Save 3d tracks in .csv files (one by track)

        Args:
            names: names of .csv files
            paths: paths where to save .csv files
        """

        for nb_obj in list(self.tracks_dict['obj'].keys()):

            obj_coords = np.array([])
            for i in range(0, len(self.tracks_dict['obj'][nb_obj]['x'])):
                obj_coord = np.array([self.tracks_dict['obj'][nb_obj]['x'][i],
                                      self.tracks_dict['obj'][nb_obj]['y'][i],
                                      self.tracks_dict['obj'][nb_obj]['z'][i]])
                obj_coords = np.vstack([obj_coords, obj_coord]) if obj_coords.size else obj_coord

            for i, camn in enumerate(range(1, self.nb_cam + 1)):
                repro_pts_cams = calib.find2d(1, self.dlt_coefs[i], np.array(obj_coords)).T
                repro_pts_cams = repro_pts_cams/self.settings.image_ratio

                if not np.array_equal(self.tracks_dict['obj'][nb_obj]['frame'],
                                      np.sort(np.unique(self.tracks_dict['obj'][nb_obj]['frame']))):
                    raise ValueError('frames_objs must be sorted and unique')

                np.savetxt(os.path.join(paths[i], names[i] + '-obj{0}-2d_points.csv'.format(nb_obj)),
                           np.c_[self.tracks_dict['obj'][nb_obj]['frame'], repro_pts_cams[0], repro_pts_cams[1]],
                           delimiter=',', header='frame,x_px,y_px')
                np.savetxt(os.path.join(paths[i], names[i] + '-obj{0}-3d_track.csv'.format(nb_obj)),
                           np.c_[self.tracks_dict['obj'][nb_obj]['frame'], self.tracks_dict['obj'][nb_obj]['x'],
                                 self.tracks_dict['obj'][nb_obj]['y'], self.tracks_dict['obj'][nb_obj]['z']],
                           delimiter=',', header='frame,x,y,z')


if __name__ == '__main__':

    path = os.path.join(os.getcwd(), '../data/mosquito_escapes/')
    image_paths = {1: os.path.join(path, 'cam1'), 2: os.path.join(path, 'cam2'), 3: os.path.join(path, 'cam3')}
    rec_names = {1: 'cam1_20200303_030117', 2: 'cam2_20200303_030120', 3: 'cam3_20200303_030120'}

    coord_calib_csv_path = os.path.join(os.getcwd(), '../data/mosquito_escapes/calib/xyz_calibration_device.csv')
    pts_calib_csv_path = os.path.join(os.getcwd(),  '../data/mosquito_escapes/calib/20200306_xypts.csv')

    dlt_coefs, _ = calib.gen_dlt_coefs_from_paths(coord_calib_csv_path, pts_calib_csv_path, from_matlab=True, img_height=1024)
    calib.save_dlt_coefs(dlt_coefs, os.path.join(os.getcwd(),
                                                 '../data/mosquito_escapes/calib/20200306_DLTcoefs-py.csv'))

    nb_cam = len(rec_names.keys())

    reconstructor = Reconstructor3D(dlt_coefs)

    # From .csv file, fill xs_pose, ys_pose and frames_pose (2d coordinates of blobs)
    pts_csv = {}
    for camn in range(1, nb_cam + 1):
        print(os.path.join(image_paths[camn], rec_names[camn], rec_names[camn] + '-2d_points.csv'))
        pts_csv[camn] = np.genfromtxt(os.path.join(image_paths[camn], rec_names[camn], rec_names[camn] + '-2d_points.csv'),
                                      delimiter=',', skip_header=0, names=True)

    xs_coord = np.array([pts_csv[camn]['x_px'] for camn in range(1, nb_cam + 1)])
    ys_coord = np.array([pts_csv[camn]['y_px'] for camn in range(1, nb_cam + 1)])
    frames_coord = np.array([pts_csv[camn]['frame'] for camn in range(1, nb_cam + 1)])

    xs_pose, ys_pose, zs_pose, frames_pose, indexes_pts, _, = reconstructor.recon_objs(xs_coord, ys_coord, frames_coord)
    tracks_dict = reconstructor.recon_tracks(xs_pose, ys_pose, zs_pose, frames_pose, indexes_pts)

    save_names = [rec_names[camn] for camn in range(1, nb_cam + 1)]
    save_paths = [os.path.join(image_paths[camn], rec_names[camn]) for camn in range(1, nb_cam + 1)]
    reconstructor.save_tracks_csv(save_names, save_paths)

    fig = plt.figure()
    ax = Axes3D(fig)
    for obj_num in tracks_dict['obj'].keys():
        ax.scatter(tracks_dict['obj'][obj_num]['x'], tracks_dict['obj'][obj_num]['y'], tracks_dict['obj'][obj_num]['z'])
    # ax.scatter(xs_pose, ys_pose, zs_pose)
    ax.set_xlabel('x [m]')
    ax.set_ylabel('y [m]')
    ax.set_zlabel('z [m]')
    plt.show()

