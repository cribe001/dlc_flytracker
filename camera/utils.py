from typing import List, Any

import numpy as np
from pyquaternion import Quaternion
from scipy.spatial.transform import Rotation


def normalize(vect: List[float]) -> List[float]:
    norm = np.linalg.norm(vect)

    if norm == 0:
       return vect

    return vect / norm


def get_rotation_btw_vectors(vec1: np.ndarray, vec2: np.ndarray = np.array([0., 0., 1.])) -> Rotation:
    """ Get rotation matrix between two vectors using scipy.spatial.transform.Rotation.

    Args:
        vec1: vector #1 (e.g. observed in initial frame) (N, 3)
        vec2: vector #2 (e.g. observed in another frame) (N, 3)

    Returns:
        R: Instance of scipy.transform.Rotation
    """

    vec1, vec2 = np.asarray(vec1), np.asarray(vec2)

    if len(vec1.shape) == 1: vec1 = np.array([vec1])
    if len(vec2.shape) == 1: vec2 = np.array([vec2])

    assert vec1.shape == (1, 3) and vec2.shape == (1, 3)
    assert not any(np.isnan(vec1)[0]) and not any(np.isnan(vec2)[0])

    decimal = 6  # to avoid problems when trying to align vectors with some coordinates close to zero
    vec1 = np.round(normalize(vec1), decimal)
    vec2 = np.round(normalize(vec2), decimal)

    R, _ = Rotation.align_vectors(vec2, vec1)

    return R


def get_rotation_angle_btw_vectors(vec1: np.ndarray, vec2: np.ndarray = np.array([0., 0., 1.]),
                                   in_degrees: bool = True) -> np.ndarray:
    """ Get rotation angle between two vectors.

    Args:
        vec1: vector #1 (e.g. observed in initial frame) (N, 3)
        vec2: vector #2 (e.g. observed in another frame) (N, 3)
        in_degrees: Whether to give angles in degree. Will be in radians if false. True by default.

    Returns:
        Angle between the two vectors.
    """
        
    unit_vector_1 = vec1 / np.linalg.norm(vec1)
    unit_vector_2 = vec2 / np.linalg.norm(vec2)

    angle = np.arccos(np.clip(np.dot(unit_vector_1, unit_vector_2), -1.0, 1.0))
    if in_degrees: angle = np.rad2deg(angle)

    minor = np.linalg.det(np.stack((unit_vector_1[-2:], unit_vector_2[-2:])))

    if minor == 0:
        sign = 1
    else:
        sign = -np.sign(minor)

    return sign * angle


def get_quat_from_rotation(rotation: Any, in_degrees: bool = True) -> Quaternion:
    """
    Will return a quaternion from various way of defining the orientation

    Args:
         rotation: Rotation needed to change the initial orientation of the object [0, 0, 1] to its final orientation.
            quaternion: Preferred way of defining the rotation.
                Instance of pyquaternion.Quaternion.
                 OR
                List of quaternion components [q0, q1, q2, q3]
             OR
            Instance of scipy.spatial.transform.Rotation
             OR
            Rotation matrix [3*3] OR Transformation matrix [4x4]
             OR
            Euler angles (extrinsic) [alpha, beta, gamma] around X,Y,Z axes (degrees).

        in_degrees: Whether the given angles are in degree. Will use radians if false. True by default.

    Return:
        quat: One instance of pyquaternion.Quaternion
    """

    if isinstance(rotation, Quaternion):
        quat = rotation

    elif isinstance(rotation, Rotation):
        quat = Quaternion(quat0123_to_1230(rotation.as_quat()))

    elif type(rotation) in [list, np.ndarray]:
        rotation = np.asarray(rotation)

        if len(rotation.shape) == 2:
            quat = Quaternion(matrix=rotation)

        elif len(rotation.shape) == 1:
            if len(rotation) == 3:
                quat1230 = Rotation.from_euler('xyz', rotation, degrees=in_degrees).as_quat()
                quat = Quaternion(quat0123_to_1230(quat1230))

            elif len(rotation) == 4:
                quat = Quaternion(rotation)

    else:
        raise 'Please use an orientation that is either a quaternion, rotation matrix or Euler angles'

    return quat


def quat0123_to_1230(quat: np.ndarray) -> np.ndarray:
    """ Convert quaternion format from [q0, q1, q2, q3] to [q1, q2, q3, q0].

    Args:
        quat: quaternion elements [q0, q1, q2, q3]

    Return:
        quat: quaternion elements [q1, q2, q3, q0]
    """

    quat = np.asarray(quat)
    assert len(quat) == 4

    return np.append(quat[1:], quat[0])
