"""
I (Antoine Cribellier) found this code at https://www.mail-archive.com/floatcanvas@mithis.com/msg00513.html
(author: Marcos Duarte)
I made various modification to clarify things and added many functions (read_dlt_coefs, save_dlt_coefs, gen_dlt_coefs, ...)
Also the functions are now working with 8 or 11 DLT coefficients instead of 9 or 12 before
(the last one was always 1, because there is only 8 or 11 independent components)

------------------------------------------------------------------------------------------------------------------------
Camera calibration and point reconstruction based on direct linear transformation (DLT).

The fundamental problem here is to find a mathematical relationship between the 
 coordinates  of a 3D point and its projection onto the images plane. The DLT
 (a linear approximation to this problem) is derived from modelling the object
 and its projection on the images plane as a pinhole camera situation.
In simplistic terms, using the pinhole camera model, it can be found by similar
 triangles the following relation between the images coordinates (u,v) and the 3D
 point (X,Y,Z):
   [ u ]   [ L1  L2  L3  L4 ] [ X ]
   [ v ] = [ L5  L6  L7  L8 ] [ Y ]
   [ 1 ]   [ L9 L10 L11 L12 ] [ Z ]
                              [ 1 ]
The matrix dlt_coef is kwnown as the camera matrix or camera projection matrix. For a
 2D point (X,Y), the last column of the matrix doesn't exist. In fact, the L12 
 term (or L9 for 2D DLT) is not independent from the other parameters and then
 there are only 11 (or 8 for 2D DLT) independent parameters in the DLT to be 
 determined.

DLT is typically used in two steps: 1. camera calibration and 2. object (point)
 reconstruction.
The camera calibration step consists in digitizing points with known coordiantes
 in the real space.
At least 4 points are necessary for the calibration of a plane (2D DLT) and at 
 least 6 points for the calibration of a volume (3D DLT). For the 2D DLT, at least
 one view of the object (points) must be entered. For the 3D DLT, at least 2 
 different views of the object (points) must be entered.
These coordinates (from the object and images(s)) are inputed to the calib
 algorithm which  estimates the camera parameters (8 for 2D DLT and 11 for 3D DLT).
With these camera parameters and with the camera(s) at the same position of the
 calibration step,  we now can reconstruct the real position of any point inside 
 the calibrated space (area for 2D DLT and volume for the 3D DLT) from the point 
 position(s) viewed by the same fixed camera(s).

This code can perform 2D or 3D DLT with any number of views (cameras).
For 3D DLT, at least two views (cameras) are necessary.

There are more accurate (but more complex) algorithms for camera calibration that
 also consider lens distortion. For example, OpenCV and Tsai softwares have been
 ported to Python. However, DLT is classic, simple, and effective (fast) for 
 most applications.

About DLT, see: http://kwon3d.com/theory/dlt/html

This code is based on different implementations and teaching material on DLT found in the internet.
"""
from typing import List, Tuple, Any

import numpy as np
from scipy.optimize import minimize

from pyquaternion import Quaternion
from matplotlib import pyplot as plt

from camera import utils


def estim_dlt_coef(xyz: List[List[float]], uv: List[List[float]], nb_dim: int = 3) -> Tuple[List[float], List[float]]:
    """ Estimate Direct Linear Transformation (DLT) coefficients using known object points and their images points.

    This code performs 2D or 3D DLT camera calibration with any number of views (cameras).
    For 3D DLT, at least two views (cameras) are necessary.

    The coordinates (x,y,z and u,v) are given as columns and the different points as rows.
    For the 2D DLT (object planar space), only the first 2 columns (x and y) are used.
    There must be at least 6 calibration points for the 3D DLT and 4 for the 2D 

    Args:
        xyz: the coordinates in the object 3D or 2D space of the calibration points (3*N).
        uv: the coordinates in the images 2D space of these calibration points (2*N).
        nb_dim: the number of dimensions of the object space: 3 for 3D DLT and 2 for 2D

    Returns:
        dlt_coef: List of 8 or 11 DLT coefficients of one camera (8*1 or 11*1)
        repro_err: error of the DLT (mean residual of the DLT transformation in camera coordinates units (e.g. pixels)).
    """

    # Convert all variables to numpy array:
    xyz = np.asarray(xyz)
    uv = np.asarray(uv)

    # Number of points:
    nb_pts = xyz.shape[0]

    # Check the parameters:
    if uv.shape[0] != nb_pts:
        raise ValueError('xyz (%d points) and uv (%d points) have different number of points.' % (nb_pts, uv.shape[0]))

    if (nb_dim == 2 and xyz.shape[1] != 2) or (nb_dim == 3 and xyz.shape[1] != 3):
        raise ValueError(
            'Incorrect number of coordinates (%d) for %dD DLT (it should be %d).' % (xyz.shape[1], nb_dim, nb_dim))

    if nb_dim == 3 and nb_pts < 6 or nb_dim == 2 and nb_pts < 4:
        raise ValueError(
            '%dD DLT requires at least %d calibration points. Only %d points were entered.' % (
                nb_dim, 2 * nb_dim, nb_pts))

    # Normalize the data to improve the DLT quality (DLT is dependent of the system of coordinates).
    # This is relevant when there is a considerable perspective distortion.
    # Normalization: mean position at origin and mean distance equals to 1 at each direction.
    Txyz, xyzn = normalization(nb_dim, xyz)
    Tuv, uvn = normalization(2, uv)

    A = []
    if nb_dim == 2:  # 2D DLT
        for i in range(nb_pts):
            x, y = xyzn[i, 0], xyzn[i, 1]
            u, v = uvn[i, 0], uvn[i, 1]
            A.append([x, y, 1, 0, 0, 0, -u * x, -u * y, -u])
            A.append([0, 0, 0, x, y, 1, -v * x, -v * y, -v])

    elif nb_dim == 3:  # 3D DLT
        for i in range(nb_pts):
            x, y, z = xyzn[i, 0], xyzn[i, 1], xyzn[i, 2]
            u, v = uvn[i, 0], uvn[i, 1]
            A.append([x, y, z, 1, 0, 0, 0, 0, -u * x, -u * y, -u * z, -u])
            A.append([0, 0, 0, 0, x, y, z, 1, -v * x, -v * y, -v * z, -v])

    # Convert A to array
    A = np.asarray(A)

    # Find the 11 (or 8 for 2D DLT) parameters:
    U, S, Vh = np.linalg.svd(A)
    # TODO Implement modified DLT method (http://www.kwon3d.com/theory/dlt/mdlt.html)
    
    # The parameters are in the last line of Vh and normalize them:
    dlt_coef = Vh[-1, :] / Vh[-1, -1]
    assert dlt_coef[-1] == 1

    # Camera projection matrix:
    H = dlt_coef.reshape(3, nb_dim + 1)

    # De-normalization:
    H = np.dot(np.dot(np.linalg.pinv(Tuv), H), Txyz)
    H = H / H[-1, -1]
    dlt_coef = H.flatten('C')  # was dlt_coef = H.flatten(0)

    dlt_coef = dlt_coef[:-1]  # to remove last coefficient (should always be 1.0) as it's not independent of the others

    # Mean error of the DLT (mean residual of the DLT transformation in units of camera coordinates):
    repro_err = reprojection_error(dlt_coef, xyz, uv)

    return dlt_coef, repro_err


def estim_mdlt_coef(xyz: List[List[float]], uv: List[List[float]], show_plot: bool = False) -> Tuple[List[float], List[float]]:
    """ Estimate modified Direct Linear Transformation (MDLT) coefficients using known object points and their images points.

    This code performs 3D modified DLT camera calibration with any number of views (min. 2 cameras) (Hatze, 1988).

    When estimating DLC coefficients (an overdetermined linear system), SVD solution usually contain a rotation about
    non-orthogonal axes, thus resulting in a small error and coefficients that might correspond to non-orthogonal matrix.
    Hatze's modified DLT method (MDLT) fix this problem by introducing a non-linear constraint of the coefficients.

    This is an implementation of Ty-Hedrick matlab code: https://github.com/tlhedrick/dltdv/blob/master/DLTcal5.m
    More about MDLT here: http://www.kwon3d.com/theory/dlt/mdlt.html

    Hatze, H. (1988) High-precision three-dimensional photogrammetric calibration and object space reconstruction
    using a modified DLT-approach. J Biomechanics 21, 553-538.
    ____________________________________________________________________________________________________________________
    The coordinates (x,y,z and u,v) are given as columns and the different points as rows.
    There must be at least 6 calibration points for the 3D DLT.

    Args:
        xyz: the coordinates in the object 3D space of the calibration points (3*N).
        uv: the coordinates in the images 2D space of these calibration points (2*N).
        show_plot: Whether to plot reprojection error over iterations

    Returns:
        dlt_coef: List of DLT coefficients of one camera (11*1) 
        repro_err: error of the DLT (mean residual of the DLT transformation in camera coordinates units (e.g. pixels)).
    """

    # Start with standard DLT coefficients
    dlt_coef_init, _ = estim_dlt_coef(xyz, uv, 3)

    if show_plot: options = {'return_all': True}
    else: options = {}

    # Minimize repro_err using a Nelder-Mead simplex algorithm
    result = minimize(objective_mdlt, dlt_coef_init[1:], args=(xyz, uv), method='Nelder-Mead', options=options)
    dlt_coef = gen_mdlt_coef_with_constraint(result.x)
    repro_err = reprojection_error(dlt_coef, xyz, uv)

    if repro_err > 0.1:  # If repro error near or above 1 pixel, then Powell method might give better results
        result_P = minimize(objective_mdlt, dlt_coef_init[1:], args=(xyz, uv), method='Powell', options=options)
        dlt_coef_P = gen_mdlt_coef_with_constraint(result_P.x)
        repro_err_P = reprojection_error(dlt_coef_P, xyz, uv)

        if repro_err > repro_err_P:
            dlt_coef = dlt_coef_P
            repro_err = repro_err_P

    if show_plot:
        ortho_errs, repro_errs = np.zeros(len(result.allvecs)), np.zeros(len(result.allvecs))
        for it, dlt_coef10 in enumerate(result.allvecs):
            dlt_coef = gen_mdlt_coef_with_constraint(dlt_coef10)
            repro_errs[it] = reprojection_error(dlt_coef, xyz, uv)
            #ortho_errs[it] = utils.is_orthogonal(rotation_from_dlt_coef(dlt_coef), return_error=True)

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(repro_errs, marker='+', color='g')
        ax.set_xlabel('# iteration')
        ax.set_ylabel('reprojection error')
        ax.set_yscale('log')

        # fig2 = plt.figure()
        # ax2 = fig2.add_subplot(111)
        # ax2.plot(ortho_errs, marker='+', color='g')
        # ax2.set_xlabel('# iteration')
        # ax2.set_ylabel('orthogonality error')
        # ax2.set_yscale('log')

        plt.show()

    return dlt_coef, repro_err


def objective_mdlt(dlt_coef10: List[float], *args) -> float:
    """ Objective function for the estim_mdlt_coef function.

    The idea is to compute the 1st DLT parameter from the other 10 and the non-linear constraint, then
    reconstruct the ideal XY camera points from the 11 DLT parameters and the calibration xyz,
    then measure the error as the difference between the ideal XY points and the digitized xy points

    Args:
        dlt_coef10: last 10 dlt coefficients (out of 11) of the calibration (10*1)
        xyz: the coordinates in the object 3D space of the calibration points (3*N).
        uv: the coordinates in the images 2D space of these calibration points (2*N).

    Returns:
        repro_err: error of the DLT (mean residual of the DLT transformation in camera coordinates units (e.g. pixels)).
    """

    xyz, uv = args

    dlt_coef = gen_mdlt_coef_with_constraint(dlt_coef10)
    repro_err = reprojection_error(dlt_coef, xyz, uv)

    return repro_err


def gen_mdlt_coef_with_constraint(dlt_coef10: List[float]) -> List[float]:
    """ Estimate missing first DLT coefficient from the last 10 coefficients using the modified DLT constraint.

    The core non-linear constraint for modified direct linear transformation (DLT)
    (C1*C5 + C2*C6 + C3*C7)*(C9**2 + C10**2 + C11**2) = (C1*C9 + C2*C10 + C3*C11)*(C5*C9 + C6*C10 + C7*C11)

    Args:
        dlt_coef10: last 10 DLT coefficients (out of 11) use to perform the 3d calibration (10*1)

    Returns:
        dlt_coef: List of DLT coefficients of one camera (11*1)
    """

    dlt_coef = np.concatenate(([0.0], dlt_coef10))  # Inititalize the list of 11 DLT parameters

    # Fill dlt_coef[0] from the non-linear constraint
    dlt_coef[0] = (- dlt_coef[1] * dlt_coef[5] * dlt_coef[8] ** 2 - dlt_coef[1] * dlt_coef[5] * dlt_coef[10] ** 2
                   - dlt_coef[2] * dlt_coef[6] * dlt_coef[8] ** 2 - dlt_coef[2] * dlt_coef[6] * dlt_coef[9] ** 2
                   + dlt_coef[1] * dlt_coef[9] * dlt_coef[4] * dlt_coef[8] + dlt_coef[1] * dlt_coef[9] * dlt_coef[6] *
                   dlt_coef[10]
                   + dlt_coef[2] * dlt_coef[10] * dlt_coef[4] * dlt_coef[8] + dlt_coef[2] * dlt_coef[10] * dlt_coef[5] *
                   dlt_coef[9]) \
                  / (dlt_coef[4] * dlt_coef[9] ** 2 + dlt_coef[4] * dlt_coef[10] ** 2
                     - dlt_coef[8] * dlt_coef[5] * dlt_coef[9] - dlt_coef[8] * dlt_coef[6] * dlt_coef[10])

    return dlt_coef


def reprojection_error(dlt_coef: List[float], xyz: List[List[float]], uv: List[List[float]]) -> float:
    """ Estimate reprojection error.

    First, compute the 2d coordinates from the 3d points using the new calibration.
    Then compare these new coordinates to the initial 2d coordinates that were use to generate the calibration.

    Args:
        dlt_coef: List of 8 or 11 DLT coefficients of one camera (8*1 or 11*1)
        xyz: the coordinates in the object 3D or 2D space of the calibration points (3*N).
        uv: the coordinates in the images 2D space of these calibration points (2*N).

    Returns:
        repro_err: error of the DLT (mean residual of the DLT transformation in camera coordinates units (e.g. pixels)).
    """

    xyz = np.asarray(xyz)

    # x = np.asarray([xyz[i][0] for i in range(0, len(xyz))])
    # y = np.asarray([xyz[i][1] for i in range(0, len(xyz))])
    # z = np.asarray([xyz[i][2] for i in range(0, len(xyz))])
    # uv2 = [(x*dlt_coef[0] + y*dlt_coef[1] + z*dlt_coef[2] + dlt_coef[3])/(x*dlt_coef[8] + y*dlt_coef[9] + z*dlt_coef[10]+1),
    #        (x*dlt_coef[4] + y*dlt_coef[5] + z*dlt_coef[6] + dlt_coef[7])/(x*dlt_coef[8] + y*dlt_coef[9] + z*dlt_coef[10]+1)]
    # OR

    if len(dlt_coef) == 8:
        uv2 = np.dot(np.reshape(np.append(dlt_coef, 1.0), [3, 3]), np.concatenate((xyz.T, np.ones((1, xyz.shape[0])))))

    elif len(dlt_coef) == 11:
        uv2 = np.dot(np.reshape(np.append(dlt_coef, 1.0), [3, 4]), np.concatenate((xyz.T, np.ones((1, xyz.shape[0])))))

    uv2 = uv2 / uv2[2, :]
    uv2 = uv2[0:2, :].T

    # Mean error of the DLT (mean residual of the DLT transformation in units of camera coordinates):
    repro_err = np.sqrt(np.mean(np.sum((uv2 - uv) ** 2, 1)))

    return repro_err


def find2d(nb_cam: int, dlt_coefs: List[List[float]], xyz: List[List[float]]) -> List[List[float]]:
    """ Find 2D reprojection of object point to images point based on the DLT parameters.

    Args:
        nb_cam: number of cameras (views) used.
        dlt_coefs: List with DLT coefficients (nb_cam*8 or nb_cam*11) of multiple cameras.
        xyz: coordinates in the object 3D or 2D space of the calibration points (3*N).

    Returns:
        uv: coordinates in the images 2D space of these calibration points.
            The coordinates (x,y,z and u,v) are given as columns and the different points as rows.
            For the 2D DLT (object planar space), only the first 2 columns (x and y) are used.
            There must be at least 6 calibration points for the 3D DLT and 4 for the 2D
    """

    # TODO flip dlt_coef to have to use format (8*nb_cam or 11*nb_cam)? Or flip the other place to be (nb_cam*8 or nb_cam*11)?

    dlt_coefs = np.asarray(dlt_coefs)  # Convert dlt_coefs to array:
    xyz = np.asarray(xyz)

    if nb_cam == 1:
        dlt_coefs = np.append(dlt_coefs, 1.0)  # to allow reshaping to 3*4 matrix
        H = dlt_coefs.reshape((3, 4))

        uv = np.dot(H, np.concatenate((xyz.T, np.ones((1, xyz.shape[0])))))
        uv = uv / uv[2, :]
        uvs = uv[0:2, :].T

    else:
        uvs = np.array([])
        for i in range(nb_cam):
            dlt_coef = dlt_coefs[i, :]
            dlt_coef = np.append(dlt_coef, 1.0)  # to allow reshaping to 3*4 matrix

            H = dlt_coef.reshape((3, 4))

            uv = np.dot(H, np.concatenate((xyz.T, np.ones((1, xyz.shape[0])))))
            uv = uv / uv[2, :]
            uv = uv[0:2, :].T

            uvs = np.vstack([uvs, uv]) if uvs.size else uv

    return uvs


def recon3d(nb_dim: int, nb_cam: int, dlt_coefs: List[List[float]], uvs: List[List[float]]) -> List[List[float]]:
    """ Reconstruction of object point from images point(s) based on the DLT parameters.

    This code performs 2D or 3D DLT point reconstruction with any number of views (cameras).
    For 3D DLT, at least two views (cameras) are necessary.

    Args:
        nb_dim: number of dimensions of the object space: 3 for 3D DLT and 2 for 2D
        nb_cam: number of cameras (views) used.
        dlt_coefs: List with DLT coefficients (nb_cam*8 or nb_cam*11) of multiple cameras.
        uvs: coordinates of the point in the images 2D space of each camera
        The coordinates of the point are given as columns and the different views as rows (2*N).

    Returns:
        xyz: point coordinates in space (3*N)
    """

    # TODO flip dlt_coef to have to use format (8*nb_cam or 11*nb_cam)? Or flip the other place to be (nb_cam*8 or nb_cam*11)?

    dlt_coefs = np.asarray(dlt_coefs)  # Convert dlt_coefs to array:

    # Check the parameters:
    if np.isnan(uvs).any():
        raise ValueError("uvs shouldn't contain NaN!")

    if dlt_coefs.ndim == 1 and nb_cam != 1:
        raise ValueError(
            'Number of views (%d) and number of sets of camera calibration parameters (1) are different.' % (nb_cam))

    if dlt_coefs.ndim > 1 and nb_cam != dlt_coefs.shape[0]:
        raise ValueError(
            'Number of views (%d) and number of sets of camera calibration parameters (%d) are different.' % (
                nb_cam, dlt_coefs.shape[0]))

    if nb_dim == 3 and dlt_coefs.ndim == 1:
        raise ValueError('At least two sets of camera calibration parameters are needed for 3D point reconstruction.')

    if nb_cam == 1:  # 2D and 1 camera (view), the simplest (and fastest) case
        dlt_coefs = np.append(dlt_coefs, 1.0)  # to allow reshaping to 3*3 matrices

        # One could calculate inv(H) and input that to the code to speed up things if needed.
        # (If there is only 1 camera, this transformation is all Floatcanvas2 might need)
        Hinv = np.linalg.inv(dlt_coefs.reshape(3, 3))

        # Point coordinates in space:
        xyz = np.dot(Hinv, [uvs[0], uvs[1], 1])
        xyz = xyz[0:2] / xyz[2]

    else:
        M = []
        for i in range(nb_cam):
            dlt_coef = dlt_coefs[i, :]
            dlt_coef = np.append(dlt_coef, 1.0)

            u, v = uvs[i][0], uvs[i][1]  # this indexing works for both list and numpy array
            if nb_dim == 2:
                M.append([dlt_coef[0] - u * dlt_coef[6], dlt_coef[1] - u * dlt_coef[7], dlt_coef[2] - u * dlt_coef[8]])
                M.append([dlt_coef[3] - v * dlt_coef[6], dlt_coef[4] - v * dlt_coef[7], dlt_coef[5] - v * dlt_coef[8]])

            elif nb_dim == 3:
                M.append([dlt_coef[0] - u * dlt_coef[8], dlt_coef[1] - u * dlt_coef[9], dlt_coef[2] - u * dlt_coef[10],
                          dlt_coef[3] - u * dlt_coef[11]])
                M.append([dlt_coef[4] - v * dlt_coef[8], dlt_coef[5] - v * dlt_coef[9], dlt_coef[6] - v * dlt_coef[10],
                          dlt_coef[7] - v * dlt_coef[11]])

        # Find the xyz coordinates:
        U, S, Vh = np.linalg.svd(np.asarray(M))

        # Point coordinates in space:
        xyz = Vh[-1, 0:-1] / Vh[-1, -1]

    return xyz


def normalization(nb_dim: int, x: List[float]) -> Tuple[List[List[float]], List[float]]:
    """ Normalization of coordinates (centroid to the origin and mean distance of sqrt(2 or 3).

    Args:
        nb_dim: number of dimensions (2 for 2D; 3 for 3D)
        x: data to be normalized (directions at different columns and points at rows)

    Returns:
        Tr: the transformation matrix (translation plus scaling)
        x: the transformed data
    """

    x = np.asarray(x)
    m, s = np.mean(x, 0), np.std(x)
    if nb_dim == 2:
        Tr = np.array([[s, 0, m[0]], [0, s, m[1]], [0, 0, 1]])
    else:
        Tr = np.array([[s, 0, 0, m[0]], [0, s, 0, m[1]], [0, 0, s, m[2]], [0, 0, 0, 1]])

    Tr = np.linalg.inv(Tr)
    x = np.dot(Tr, np.concatenate((x.T, np.ones((1, x.shape[0])))))
    x = x[0:nb_dim, :].T

    return Tr, x


def intrinsic_from_dlt_coef(dlt_coef: List[float]) -> Tuple[float, float, float, float]:
    """ Compute intrinsic camera parameters from dlt coefficients

    Args:
        dlt_coef: List of DLT coefficients of one camera (11*1) 

    Returns:
        xp: x coordinate of camera principal point (pixels)
        yp: y coordinate of camera principal point (pixels)
        fx: camera focal length along x (pixels)
        fy: camera focal length along y (pixels)
    """

    assert len(dlt_coef) == 11

    # Coordinates of principal points (if a camera sensor is N*M pixels, xp ~ N/2 and yp ~ M/2)
    xp = (dlt_coef[0] * dlt_coef[8] + dlt_coef[1] * dlt_coef[9] + dlt_coef[2] * dlt_coef[10]) \
         / (dlt_coef[8] ** 2 + dlt_coef[9] ** 2 + dlt_coef[10] ** 2)
    yp = (dlt_coef[4] * dlt_coef[8] + dlt_coef[5] * dlt_coef[9] + dlt_coef[6] * dlt_coef[10]) \
         / (dlt_coef[8] ** 2 + dlt_coef[9] ** 2 + dlt_coef[10] ** 2)

    # Focal lengths (or principal distances), from Ty Hedrick matlab code (focal lengths in pixels)
    # fx and fy should be equal for a camera with square pixels
    D = 1 / (dlt_coef[8] ** 2 + dlt_coef[9] ** 2 + dlt_coef[10] ** 2)
    fx = np.sqrt(((xp * dlt_coef[8] - dlt_coef[0]) ** 2 +
                  (xp * dlt_coef[9] - dlt_coef[1]) ** 2 +
                  (xp * dlt_coef[10] - dlt_coef[2]) ** 2) * D)
    fy = np.sqrt(((yp * dlt_coef[8] - dlt_coef[4]) ** 2 +
                  (yp * dlt_coef[9] - dlt_coef[5]) ** 2 +
                  (yp * dlt_coef[10] - dlt_coef[6]) ** 2) * D)

    return xp, yp, fx, fy


def intrinsic_from_extrinsic_and_coordinates(coord: List[float], quat: Quaternion,
                                             xyz: List[List[float]], uv: List[List[float]],
                                             show_plot: bool = False) -> Tuple[float, float, float, float]:
    """ Compute (optimize) intrinsic camera parameters from extrinsic and using known object points and their images points.

    Args:
        coord: [Xc, Yc, Zc]: Camera coordinates (m).
        quat: Instance of pyquaternion.Quaternion, describe the rotation needed to change the initial orientation
              of the camera [0, 0, 1] to its current orientation.
        xyz: the coordinates in the object 3D or 2D space of the calibration points (3*N).
        uv: the coordinates in the images 2D space of these calibration points (2*N).

        show_plot: Whether to plot reprojection error over iterations

    Returns:
        xp: x coordinate of camera principal point (pixels)
        yp: y coordinate of camera principal point (pixels)
        fx: camera focal length along x (pixels)
        fy: camera focal length along y (pixels)
    """

    dlt_coef_init, _ = estim_mdlt_coef(xyz, uv)
    intrinsic_init = list(intrinsic_from_dlt_coef(dlt_coef_init))

    if show_plot:
        options = {'return_all': True}
    else:
        options = {}

    # Minimize repro_err using a Nelder-Mead simplex algorithm
    result = minimize(objective_dlt_with_fixed_extrinsic, intrinsic_init, args=(coord, quat, xyz, uv), method='Nelder-Mead', options=options)
    xp, yp, fx, fy = result.x[0], result.x[1], result.x[2], result.x[3]
    dlt_coef = dlt_coef_from_intrinsic_extrinsic(xp, yp, fx, fy, coord, quat)
    repro_err = reprojection_error(dlt_coef, xyz, uv)

    # if repro_err > 0.1:  # If repro error near or above 1 pixel, then Powell method might give better results
    #     result_P = minimize(objective_mdlt_fixed_extrinsic, intrinsic_init, args=(coord, quat, xyz, uv), method='Powell', options=options)
    #     xp_P, yp_P, fx_P, fy_P = result.x[0], result.x[1], result.x[2], result.x[3]
    #     dlt_coef_P = dlt_coef_from_intrinsic_extrinsic(xp_P, yp_P, fx_P, fy_P, coord, quat)
    #     repro_err_P = reprojection_error(dlt_coef_P, xyz, uv)
    #
    #     if repro_err > repro_err_P:
    #         xp, yp, fx, fy = xp_P, yp_P, fx_P, fy_P
    #         repro_err = repro_err_P

    if show_plot:
        ortho_errs, repro_errs = np.zeros(len(result.allvecs)), np.zeros(len(result.allvecs))
        for it, res in enumerate(result.allvecs):
            xp, yp, fx, fy = res[0], res[1], res[2], res[3]
            dlt_coef = dlt_coef_from_intrinsic_extrinsic(xp, yp, fx, fy, coord, quat)
            repro_errs[it] = reprojection_error(dlt_coef, xyz, uv)
            # ortho_errs[it] = utils.is_orthogonal(rotation_from_dlt_coef(dlt_coef), return_error=True)

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(repro_errs, marker='+', color='g')
        ax.set_xlabel('# iteration')
        ax.set_ylabel('reprojection error')
        ax.set_yscale('log')
        plt.show()

    #dlt_coef, repro_err = estim_mdlt_coef(xyz, uv, show_plot) # To get orthogonal rotation matrix?
    #xp, yp, fx, fy = intrinsic_from_dlt_coef(dlt_coef)

    return xp, yp, fx, fy, repro_err


def objective_dlt_with_fixed_extrinsic(intrinsic: List[float], *args) -> Tuple[List[float], float]:
    """ Objective function for the intrinsic_from_extrinsic_and_coordinates function.

        Args:
            intrinsic:
                xp: x coordinate of camera principal point (pixels)
                yp: y coordinate of camera principal point (pixels)
                fx: camera focal length along x (pixels)
                fy: camera focal length along y (pixels)

            coord: [Xc, Yc, Zc]: Camera positions (m)
            quat: Rotation quaternion needed to change the initial orientation of the camera [0, 0, 1] to its current orientation.
            xyz: the coordinates in the object 3D space of the calibration points (3*N).
            uv: the coordinates in the images 2D space of these calibration points (2*N).

        Returns:
            repro_err: error of the DLT (mean residual of the DLT transformation in camera coordinates units (e.g. pixels)).
        """

    xp, yp, fx, fy, = intrinsic[0], intrinsic[1], intrinsic[2], intrinsic[3]
    coord, quat, xyz, uv = args

    dlt_coef = dlt_coef_from_intrinsic_extrinsic(xp, yp, fx, fy, coord, quat)
    repro_err = reprojection_error(dlt_coef, xyz, uv)

    return repro_err


def extrinsic_from_dlt_coef(dlt_coef: List[float],
                            intrinsic: Tuple[float, float, float, float] = None) -> Tuple[List[float], List[float]]:
    """ Compute extrinsic camera parameters from dlt coefficients

    Args:
        dlt_coef: List of DLT coefficients of one camera (11*1)

        intrinsic: Tuple that contains intrinsic parameters (to avoid having to recompute them)
            xp: x coordinate of camera principal point (pixels)
            yp: y coordinate of camera principal point (pixels)
            fx: camera focal length along x (pixels)
            fy: camera focal length along y (pixels)

    Returns:
        coord: [Xc, Yc, Zc]: Camera coordinates (m).
        quat: Instance of pyquaternion.Quaternion, describe the rotation needed to change the initial orientation
              of the camera [0, 0, 1] to its current orientation.
    """

    assert len(dlt_coef) == 11

    # Camera position (X, Y, Z coordinates)
    coords = coords_from_dlt_coef(dlt_coef)

    # Camera orientation (angles around X, Y, Z) from Ty Hedrick matlab code
    quat = quat_from_dlt_coef(dlt_coef, intrinsic)

    return coords, quat


def extrinsic_from_intrinsic_and_coordinates(xp: float, yp: float, fx: float, fy: float,
                                             xyz: List[List[float]], uv: List[List[float]],
                                             show_plot: bool = False) -> Tuple[List[float], List[float]]:
    """ Compute (optimize) extrinsic camera parameters from intrinsic and using known object points and their images points.

    Args:
        xp: x coordinate of camera principal point (pixels)
        yp: y coordinate of camera principal point (pixels)
        fx: camera focal length along x (pixels)
        fy: camera focal length along y (pixels)
        xyz: the coordinates in the object 3D or 2D space of the calibration points (3*N).
        uv: the coordinates in the images 2D space of these calibration points (2*N).

        show_plot: Whether to plot reprojection error over iterations

    Returns:
        coord: [Xc, Yc, Zc]: Camera coordinates (m).
        quat: Instance of pyquaternion.Quaternion, describe the rotation needed to change the initial orientation
              of the camera [0, 0, 1] to its current orientation.
    """

    dlt_coef_init, _ = estim_mdlt_coef(xyz, uv)
    intrinsic_init = intrinsic_from_dlt_coef(dlt_coef_init)
    coord_init, quat_init = extrinsic_from_dlt_coef(dlt_coef_init, intrinsic_init)

    extrinsic_init = np.concatenate((np.array(coord_init), quat_init.elements))

    if show_plot:
        options = {'return_all': True}
    else:
        options = {}

    # Minimize repro_err using a Nelder-Mead simplex algorithm
    result = minimize(objective_dlt_with_fixed_intrinsic, extrinsic_init, args=(xp, yp, fx, fy, xyz, uv), method='Nelder-Mead', options=options)
    coord, quat_elements = result.x[0:3], result.x[3:]
    quat = Quaternion(quat_elements[0], quat_elements[1], quat_elements[2], quat_elements[3])
    dlt_coef = dlt_coef_from_intrinsic_extrinsic(xp, yp, fx, fy, coord, quat)
    repro_err = reprojection_error(dlt_coef, xyz, uv)

    # if repro_err > 0.1:  # If repro error near or above 1 pixel, then Powell method might give better results
    #     result_P = minimize(objective_mdlt_fixed_intrinsic, extrinsic_init, args=(xp, yp, fx, fy, xyz, uv), method='Powell', options=options)
    #     coord_P, quat_elements = result.x[0:3], result.x[3:]
    #     quat_P = Quaternion(quat_elements[0], quat_elements[1], quat_elements[2], quat_elements[3])
    #     dlt_coef_P = dlt_coef_from_intrinsic_extrinsic(xp, yp, fx, fy, coord_P, quat_P)
    #     repro_err_P = reprojection_error(dlt_coef_P, xyz, uv)
    #
    #     if repro_err > repro_err_P:
    #         coord, quat = coord_P, quat_P
    #         repro_err = repro_err_P

    if show_plot:
        ortho_errs, repro_errs = np.zeros(len(result.allvecs)), np.zeros(len(result.allvecs))
        for it, res in enumerate(result.allvecs):
            coord, quat_elements = res[0:3], res[3:]
            quat = Quaternion(quat_elements[0], quat_elements[1], quat_elements[2], quat_elements[3])
            dlt_coef = dlt_coef_from_intrinsic_extrinsic(xp, yp, fx, fy, coord, quat)
            repro_errs[it] = reprojection_error(dlt_coef, xyz, uv)
            # ortho_errs[it] = utils.is_orthogonal(rotation_from_dlt_coef(dlt_coef), return_error=True)

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(repro_errs, marker='+', color='g')
        ax.set_xlabel('# iteration')
        ax.set_ylabel('reprojection error')
        ax.set_yscale('log')
        plt.show()

    #dlt_coef, repro_err = estim_mdlt_coef(xyz, uv, show_plot) # To get orthogonal rotation matrix?
    #coord, quat = extrinsic_from_dlt_coef(dlt_coef, (xp, yp, fx, fy))

    return coord, quat, repro_err


def objective_dlt_with_fixed_intrinsic(extrinsic: List[float], *args) -> Tuple[List[float], float]:
    """ Objective function for the extrinsic_from_intrinsic_and_coordinates function.

        Args:
            extrinsic:
                coord: [Xc, Yc, Zc]: Camera positions (m)
                quat: Rotation quaternion needed to change the initial orientation of the camera [0, 0, 1] to its current orientation.

            xp: x coordinate of camera principal point (pixels)
            yp: y coordinate of camera principal point (pixels)
            fx: camera focal length along x (pixels)
            fy: camera focal length along y (pixels)
            xyz: the coordinates in the object 3D space of the calibration points (3*N).
            uv: the coordinates in the images 2D space of these calibration points (2*N).

        Returns:
            repro_err: error of the DLT (mean residual of the DLT transformation in camera coordinates units (e.g. pixels)).
        """

    xp, yp, fx, fy, xyz, uv = args
    coord, quat_elements = extrinsic[0:3], extrinsic[3:]
    quat = Quaternion(quat_elements[0], quat_elements[1], quat_elements[2], quat_elements[3])

    dlt_coef = dlt_coef_from_intrinsic_extrinsic(xp, yp, fx, fy, coord, quat)
    repro_err = reprojection_error(dlt_coef, xyz, uv)

    return repro_err


def coords_from_dlt_coef(dlt_coef: List[float]) -> List[float]:
    """ Compute camera coordinates (m) from dlt coefficients

    Args:
        dlt_coef: List of DLT coefficients of one camera (11*1)

    Returns:
        coord: [Xc, Yc, Zc]: Camera coordinates (m).
    """
    coords = np.dot(np.linalg.inv([[dlt_coef[0], dlt_coef[1], dlt_coef[2]],
                                   [dlt_coef[4], dlt_coef[5], dlt_coef[6]],
                                   [dlt_coef[8], dlt_coef[9], dlt_coef[10]]]),
                    np.array([[-dlt_coef[3], -dlt_coef[7], -1]]).T)

    coords = coords.T[0]

    return coords


def quat_from_dlt_coef(dlt_coef: List[float],
                       intrinsic: Tuple[float, float, float, float] = None) -> List[float]:
    """ Compute rotation quaternion of camera from dlt coefficients

    Args:
        dlt_coef: List of DLT coefficients of one camera (11*1)

        intrinsic: Tuple that contains intrinsic parameters (to avoid having to recompute them)
            xp: x coordinate of camera principal point (pixels)
            yp: y coordinate of camera principal point (pixels)
            fx: camera focal length along x (pixels)
            fy: camera focal length along y (pixels)

    Returns:
        quat: Instance of pyquaternion.Quaternion, describe the rotation needed to change the initial orientation
              of the camera [0, 0, 1] to its current orientation.
    """

    T = rotation_from_dlt_coef(dlt_coef, intrinsic)
    # ortho_error = utils.is_orthogonal(T, return_error=True)  # If the rotation matrix is not orthogonal, use estim_mdlt_coef instead of estim_dlt_coef

    return Quaternion(matrix=T)


def rotation_from_dlt_coef(dlt_coef: List[float],
                           intrinsic: Tuple[float, float, float, float] = None) -> List[float]:
    """ Compute rotation matrix of camera from dlt coefficients

    Args:
        dlt_coef: List of DLT coefficients of one camera (11*1)

        intrinsic: Tuple that contains intrinsic parameters (to avoid having to recompute them)
            xp: x coordinate of camera principal point (pixels)
            yp: y coordinate of camera principal point (pixels)
            fx: camera focal length along x (pixels)
            fy: camera focal length along y (pixels)

    Returns:
        T: Rotation matrix needed to change the initial orientation of the camera [0, 0, 1] to its current orientation.
    """

    if intrinsic is not None:
        xp, yp, fx, fy = intrinsic
    else:
        xp, yp, fx, fy = intrinsic_from_dlt_coef(dlt_coef)

    D = 1 / (dlt_coef[8] ** 2 + dlt_coef[9] ** 2 + dlt_coef[10] ** 2)
    T3 = np.dot(np.sqrt(D),
                [[(xp * dlt_coef[8] - dlt_coef[0]) / fx, (xp * dlt_coef[9] - dlt_coef[1]) / fx,
                  (xp * dlt_coef[10] - dlt_coef[2]) / fx],
                 [(yp * dlt_coef[8] - dlt_coef[4]) / fy, (yp * dlt_coef[9] - dlt_coef[5]) / fy,
                  (yp * dlt_coef[10] - dlt_coef[6]) / fy],
                 [dlt_coef[8], dlt_coef[9], dlt_coef[10]]])

    dT3 = np.linalg.det(T3)

    if dT3 < 0:
        T3 = -1 * T3

    T = np.linalg.inv(T3)

    return T


def dlt_coef_from_intrinsic_extrinsic(xp: float, yp: float, fx: float, fy: float,
                                      coords: List[float], rotation: Any, in_degrees: bool = True) -> List[float]:
    """ Generate DLT coefficients from the intrinsic and extrinsic parameters of a camera.

    Args:
        xp: x coordinate of camera principal point (pixels)
        yp: y coordinate of camera principal point (pixels)
        fx: camera focal length along x (pixels)
        fy: camera focal length along y (pixels)
        coords: [Xc, Yc, Zc]: Camera positions (m)
        rotation: Rotation needed to change the initial orientation of the camera [0, 0, 1] to its current orientation.
            quaternion: Preferred way of defining the rotation.
                Instance of pyquaternion.Quaternion.
                 OR
                List of quaternion components [q0, q1, q2, q3]
             OR
            Instance of scipy.spatial.transform.Rotation
             OR
            Rotation matrix [3*3] OR Transformation matrix [4x4]
             OR
            Euler angles (extrinsic) [alpha, beta, gamma] around X,Y,Z axes (degrees).

        in_degrees: Whether the given angles are in degree. Will use radians if false.

    Returns:
        dlt_coef: List of DLT coefficients of one camera (11*1) 

    """

    assert any(coords != [0, 0, 0]), "No camera should be on the origin for the following method to work. " \
                                     "If not, you will need to implement a whole system offset for ALL cameras. " \
                                     "For more details check: biomech.web.unc.edu/dlt-to-from-intrinsic-extrinsic/"

    if coords[2] == 0.0:  # The last coordinate can't be equal to 0.0, so will make it equal to a very small number
        coords = coords.astype(float)
        coords[2] = np.min(coords) * 10e-8

    # Get quaternion from multiple possible rotation
    quat = utils.get_quat_from_rotation(rotation, in_degrees=in_degrees)

    # Calibration matrix
    K = np.array([[-fx,  0.0, xp],  # Use minus sign in front of fx and fy to inverse image of object
                  [0.0, -fy,  yp],
                  [0.0, 0.0, 1.0]])

    m = np.array([[1, 0, 0, 0],
                  [0, 1, 0, 0],
                  [0, 0, 1, 0]])

    # Rotation matrix (inverse of rotation needed to go from initial orientation [0, 0, 1] to current orientation)
    R_inv = quat.inverse.rotation_matrix

    # Translation btw world coordinate system and the camera
    tx, ty, tz = R_inv @ - np.array(coords)

    # Projection matrix
    RT_inv = np.array([[R_inv[0, 0], R_inv[0, 1], R_inv[0, 2], tx],
                       [R_inv[1, 0], R_inv[1, 1], R_inv[1, 2], ty],
                       [R_inv[2, 0], R_inv[2, 1], R_inv[2, 2], tz],
                       [0,           0,           0,           1]])

    # TODO implement inverse whole system offset of the dlt_coef if one camera is at the origin (e.g. dlt_coef == np.nan)
    # if any(dlt_coef == np.nan):
    #     # Create a whole system offset
    #     eR = np.eye(3)  # external rotation
    #     eT = [10, 10, 10]  # external translation
    #
    #     eRT = np.array([[eR[0, 0], eR[0, 1], eR[0, 2], eT[0]],
    #                     [eR[1, 0], eR[1, 1], eR[1, 2], eT[1]],
    #                     [eR[2, 0], eR[2, 1], eR[2, 2], eT[2]],
    #                     [0,        0,        0,        1]])
    #
    #     H = np.dot(np.dot(np.dot(K, m), RT_inv), eRT)

    H = np.dot(np.dot(K, m), RT_inv)
    H = H / H[-1, -1]

    dlt_coef = H.flatten('C')
    dlt_coef = dlt_coef[:-1]

    return dlt_coef


def read_dlt_coefs(csv_path: str) -> List[List[float]]:
    """ Read DLT coefficients from .csv file

    Args:
        csv_path: path of the .csv file where the DLT (11) coefficients (along rows) for all camera (along column) will be written

    Returns:
        dlt_coefs: List with DLT coefficients (11 * nb_cam) of multiple cameras.
    """

    dlt_csv = np.genfromtxt(csv_path, delimiter=',', skip_header=0, names=True)
    assert len(dlt_csv) == 11

    nb_cam = len(dlt_csv[0])
    dlt_csv.dtype.names = ['cam{0}'.format(camn) for camn in range(1, nb_cam + 1)]

    dlt_coefs = np.zeros((nb_cam, 11))
    for i, camn in enumerate(range(1, nb_cam + 1)):
        dlt_coefs[i] = np.array(dlt_csv['cam{0}'.format(camn)])

    return dlt_coefs


def save_dlt_coefs(dlt_coefs: List[List[float]], csv_path: str) -> None:
    """ Save DLT coefficients in .csv file

    Args:
        dlt_coefs: List with DLT coefficients (11 * nb_cam) of multiple cameras.
        csv_path: path of the .csv file with DLT (11) coefficients (along rows) for all camera (along column)
    """

    assert len(dlt_coefs[0]) == 11

    nb_cam = len(dlt_coefs)
    header = ['cam{0}'.format(camn) for camn in range(1, nb_cam + 1)]
    header = ','.join(header)

    np.savetxt(csv_path, np.c_[dlt_coefs].T, delimiter=',', header=header)


def gen_dlt_coef(obj_coord: List[List[float]], img_pts: List[List[float]]) -> Tuple[List[float], List[float]]:
    """ Generate DLT coefficients for one camera

    Args:
        obj_coord: known 3d coordinates of the objects (size: nb_obj*3)
        img_pts: 2d coordinates for all objects for one camera (size: nb_obj*2)

    Returns:
        dlt_coef: List of DLT coefficients of one camera (11*1) 
        mean_repro_err: mean reprojcetion error in pixel (or Root-mean-square deviation)
    """

    dlt_coef, repro_err = estim_dlt_coef(obj_coord, img_pts, 3)

    return dlt_coef, repro_err


def gen_dlt_coefs(obj_coord: List[List[float]], img_pts: List[List[float]], plot_error_analysis: bool = False,
                  point_ids: List[List[int]] = None) -> Tuple[List[List[float]], List[List[float]]]:
    """ Generate DLT coefficients for multiple cameras

    Args:
        obj_coord: known 3d coordinates of the objects (size: nb_obj*3)
        img_pts: 2d coordinates for all objects for multiple cameras (size: nb_cam*nb_obj*3)
        plot_error_analysis: Whether to do an error analysis
        (check if not using each 2d point will impact overall calibration error)
        point_ids: Identification numbers of the points that where kept for the calibration

    Returns:
        dlt_coefs: List of DLT coefficient (11*nb_cam) for all cameras
        mean_repro_err: mean reprojection errors in pixel (or Root-mean-square deviation) for all cameras
    """

    if plot_error_analysis: repro_errs_analysis = {}

    nb_cam = len(obj_coord)

    dlt_coefs = np.zeros((nb_cam, 11))
    repro_errs = np.zeros((nb_cam))
    for i, camn in enumerate(range(1, nb_cam + 1)):

        dlt_coefs[i], repro_errs[i] = gen_dlt_coef(obj_coord[i], img_pts[i])
        print('>> The reprojection error of cam{0} is {1} pixels'.format(camn, repro_errs[i]))

        if plot_error_analysis:
            repro_errs_analysis[camn] = np.zeros(len(img_pts[i].T[0]))

            for ind_pt in range(len(img_pts[i].T[0])):
                img_pts_calib_wo_pt = np.array([[s for j, s in enumerate(img_pts[i].T[0]) if j is not ind_pt],
                                                [s for j, s in enumerate(img_pts[i].T[1]) if j is not ind_pt]]).T
                obj_coord_calib_wo_pt = np.array([[s for j, s in enumerate(obj_coord[i].T[0]) if j is not ind_pt],
                                                  [s for j, s in enumerate(obj_coord[i].T[1]) if j is not ind_pt],
                                                  [s for j, s in enumerate(obj_coord[i].T[2]) if j is not ind_pt]]).T

                _, repro_errs_analysis[camn][ind_pt] = gen_dlt_coef(obj_coord_calib_wo_pt, img_pts_calib_wo_pt)

            fig = plt.figure('Error analysis (cam{0})'.format(camn))
            ax = fig.add_subplot(111)
            ax.scatter(list(range(1, len(repro_errs_analysis[camn]) + 1)), repro_errs_analysis[camn])
            if point_ids != None:
                for i in range(0, len(repro_errs_analysis[camn])):
                    plt.text(i+1, repro_errs_analysis[camn][i], point_ids[camn-1][i])

            ax.set_xlabel('x [m]')
            ax.set_ylabel('y [m]')
            plt.show()

    return dlt_coefs, repro_errs


def gen_dlt_coefs_from_paths(xyz_path: str, xy_path: str, points_to_remove: List[int] = [],
                             plot_error_analysis: bool = False, correction_factor: float = None,
                             from_matlab: bool = False, img_height: int = None,
                             show_plot: bool = False) -> Tuple[List[List[float]], List[List[float]]]:
    """ Generate DLT coefficients for multiple cameras from the paths of csv files with known 3d coordinates of
    the objects and 2d coordinates for all objects for multiple cameras

    Args:
        xyz_path: Path of a csv file containing known 3d coordinates of the objects (size: nb_obj*3)
        xy_path: Path of a csv file containing 2d coordinates for all objects for multiple cameras (size: nb_cam*nb_obj*3)
        points_to_remove: List of indexes of points that need to be ignored for the computation of the DLT coefficients
        plot_error_analysis: Whether to do an error analysis
        (check if not using each 2d point will impact overall calibration error)

        correction_factor: Factor to multiply 3d coordinates (eg 0.001 to convert millimeters to meters)
        from_matlab: Whether to convert 2d coordinates to the coordinate system used here
        img_height: Height of the images of all cameras (assume the same for all),
                    only needed when converting 2d coordinates from matlab

        show_plot:

    Returns:
        dlt_coefs: List of DLT coefficient (11*nb_cam) for all cameras
        mean_repro_err: mean reprojection errors in pixel (or Root-mean-square deviation) for all cameras
    """

    coord_calib_csv = np.genfromtxt(xyz_path, delimiter=',', skip_header=0, names=True)
    pts_calib_csv = np.genfromtxt(xy_path, delimiter=',', skip_header=0, names=True)

    coord_calib = {'x': np.array([s for i, s in enumerate(coord_calib_csv['x']) if i + 1 not in points_to_remove]),
                   'y': np.array([s for i, s in enumerate(coord_calib_csv['y']) if i + 1 not in points_to_remove]),
                   'z': np.array([s for i, s in enumerate(coord_calib_csv['z']) if i + 1 not in points_to_remove])}

    if correction_factor is not None:
        coord_calib['x'] = coord_calib['x'] * correction_factor
        coord_calib['y'] = coord_calib['y'] * correction_factor
        coord_calib['z'] = coord_calib['z'] * correction_factor

    nb_cam = int(len(pts_calib_csv[0]) / 2)

    img_pts = [[]] * nb_cam
    obj_coord = [[]] * nb_cam
    point_ids = [np.array([i+1 for i, s in enumerate(coord_calib_csv['x']) if i + 1 not in points_to_remove])] * nb_cam
    for i, camn in enumerate(range(1, nb_cam + 1)):
        x_pts = np.array(
            [s for i, s in enumerate(pts_calib_csv['cam{0}_X'.format(camn)]) if i + 1 not in points_to_remove])
        y_pts = np.array(
            [s for i, s in enumerate(pts_calib_csv['cam{0}_Y'.format(camn)]) if i + 1 not in points_to_remove])

        if from_matlab:  # Will transform coordinate (from Matlab) system using img_size
            if img_height is None:
                raise ValueError('Need image height in order to convert coordinate system from matlab')

            assert img_height >= np.nanmax(y_pts)

            img_pts[i] = np.array([x_pts, img_height - y_pts], np.float32).T

        else:
            img_pts[i] = np.array([x_pts, y_pts], np.float32).T

        index = ~np.isnan(img_pts[i]).any(axis=1)  # Remove nan
        img_pts[i] = img_pts[i][index]
        point_ids[i] = point_ids[i][index]

        obj_coord[i] = np.array([coord_calib['x'][index], coord_calib['y'][index], coord_calib['z'][index]]).T

        if show_plot:
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for j, _ in enumerate(img_pts[i]):
                ax.scatter(img_pts[i][j][0], img_pts[i][j][1])
                plt.text(img_pts[i][j][0], img_pts[i][j][1], str(point_ids[i][j]))

            ax.set_xlabel('x [m]')
            ax.set_ylabel('y [m]')
            plt.show()

    return gen_dlt_coefs(obj_coord, img_pts, plot_error_analysis=plot_error_analysis, point_ids=point_ids)
