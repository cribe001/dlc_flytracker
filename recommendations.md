# git workflow

## Weird merges from `git pull`
There's a few merge commits with I guess come from `git pull` with local changes on `main`; it used to default to `git fetch && git merge`. This weirdness can be prevented by setting

    `git config --global pull.ff only`

which only allows fast-forward merging when using `git pull`. If you did work on the main branch in two different places, pushed to `main` at one place, then try to `git pull` in the other, it will refuse. An example method to fix this would be:
- Make a temporary branch on the current *local* main:
    - `git switch main`
    - `git branch main-temp`
- Force the main branch to be on the remote's main state
    - `git checkout origin/main`
    - `git branch -f main`
- If it's only a few commits: cherry-pick the commits from `main-temp` onto the new `main`
    - `git log main-temp` to show the commit log for `main-temp` (including commit hashes)
    - For each commit, copy the commit hash and `git cherry-pick <commit hash>`
- If it's a bunch of commits, rebase them onto master
    - `git switch main-temp`
    - `git log` -> find the first commit you want to keep, then copy the hash of the commit *before* that
    - `git rebase --onto main <commit hash before the first commit you want to keep>`
    - Fast-forward merge the rebased `main-temp` onto `main`:
        - `git switch main`
        - `git merge main-temp`
- Delete the temporary branch
    - `git branch -D main-temp`

However, in general, prevent this by just pushing often! *Especially* if you change machines often. Just push whenever you finished something, and when you stopped working (even if it is just a temporary WIP commit).

To reset such a temporary WIP commit, use:
- `git reset --soft HEAD^`
- `git reset`

## General workflow
I'm going to assume here that you'll be working mostly solo at this stage. If you start really collaborating in the future, an issues/pull request workflow is probably a good idea.

- Keep `main` consistent, do most of your work on branches, unless it's a tiny fix; never force push to `main`!
- Push often! Especially if you work on multiple machines
- Clean up your branches before you merge them
    - But don't spend too much time cleaning up at this stage
    - Useful: learn how to use interactive rebases (`git rebase -i`) to edit branch history (but **never** use it on `main`)
- Tag versions of the software that you used for e.g. articles with `git tag <tagname>` for maximum reproducibility.

# Code and architecture

## Main application
The configuration YAML-files are a great idea! As part of moving to a single `main.py`, perhaps you can include all the configuration in it, e.g. also the paths to the images. Making sure there is no configuration in the script makes using the application more convenient and much more reproducible. If you make sure that your program outputs its version (e.g. a git hash -- for example with `git rev-parse HEAD`) and its full configuration, you can always reproduce the data if you have the log (and the original data in the same path, of course...).

## Class architecture

### "Global" state in large classes
`Reconstructor3D` and `Tracker2D` are fairly large classes that seem to do more than one thing. For the things they do, they modify state in the object in a way that might not be obvious to the unsuspecting reader. Perhaps it would be possible to break it up into smaller, more focused classes.

In other cases, you could consider storing relevant data in a simple `dataclass`, and applying (top-level) functions to these data, instead of having these functions in a class. This makes the data flow a bit more clear to an outsider, especially if you have small dataclasses with tightly connected data.

A good rule of thumb of when something should be a class, is that there should be a [class invariant](https://en.wikipedia.org/wiki/Class_invariant) to preserve. That is, by interacting with the object only through its methods, some relation between its data members is preserved. If this is not necessary, you're usually better off either subdividing the class further, or perhaps just grouping the data and processing it with functions.

# Documentation
Very nice that many functions have (consistent) docstrings!

It would be nice to have some context of the software in README.md: a general description of what this software does and why it exists.

A short description of the architecture of the software would be very helpful too.

# Priorities

2. Testing
4. Moving configuration to YAML
6. Miscellaneous: documentation

Recommended workflow:

Prerequisites:
- `master` is protected (i.e. cannot be pushed to directly)

Workflow:
- Make an issue (fill in at least the title)
- Assign issue to yourself (or someone else)
- Create pull request for the issue
- Create branch for the issue
- Do work!
- Make pull request
- Ask someone else to review it (optionally)
- Merge it
- Yay
