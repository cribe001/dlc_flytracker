import os, re, copy

from typing import List, Dict

from difflib import SequenceMatcher
from datetime import datetime

import numpy as np


def match_folders_by_name(folder_names: List[List[str]],
                          threshold_ratio_diff: float = None,
                          threshold_nb_diff_char: int = None) -> List[List[str]]:
    """
    Matches list of folder names together by checking how similar their folder names are and associate them together
    The best matches will be found by comparing to the smaller list of folders to avoid matching one to two folders.

    Args:
        folder_names: List[list[]] of folder names
        threshold_ratio_diff: Will check the similarity percentage (between 0 and 1)
        threshold_nb_diff_char: Will check how many characters are different between the file names.

    Returns:
        matched_folder_names: List of lists (# of recordings * # of cameras) containing the matched folder names.

    Raises:
        ValueError: If both threshold type are given instead of only one
        ValueError: If threshold_nb_diff_char has to be an integer
        TypeError: thresholds have to be either a positive int or a float between 0.0 and 1.0.
    """

    if threshold_ratio_diff is not None and threshold_nb_diff_char is not None:
        raise ValueError("Only one threshold has to be procured")

    elif threshold_ratio_diff is None and threshold_nb_diff_char is None:
        threshold_ratio_diff = 0.8

    if threshold_ratio_diff is not None:
        assert type(threshold_ratio_diff) is float
        assert 0.0 <= threshold_ratio_diff <= 1.0

    elif threshold_nb_diff_char is not None:
        if type(threshold_nb_diff_char) is float and threshold_nb_diff_char.is_integer():
            threshold_nb_diff_char = int(threshold_nb_diff_char)

        elif type(threshold_nb_diff_char) is not int:
            raise ValueError("threshold_nb_diff_char should be an integer")

        assert threshold_nb_diff_char >= 0

    else:
        raise TypeError('Threshàold has to be either a positive int or a float between 0.0 and 1.0')

    nb_directories = len(folder_names)
    nb_folders = [len(x) for x in folder_names]
    i_min = np.argmin(nb_folders)

    matched_folder_names, ratios = [[]] * len(folder_names), [[]] * len(folder_names)
    is_similar_enough = [[]] * len(folder_names)
    for i in range(nb_directories):

        if i == i_min:
            matched_folder_names[i] = folder_names[i_min]
            ratios[i] = [1.0] * len(folder_names[i_min])

        else:
            matches = [max([(SequenceMatcher(a=f1, b=f2).ratio(), f2) for f2 in folder_names[i]]) for f1 in folder_names[i_min]]

            matched_folder_names[i] = [folder_name for ratio, folder_name in matches]
            ratios[i] = [ratio for ratio, folder_name in matches]

        if threshold_ratio_diff is not None:
            is_similar_enough[i] = [ratio >= threshold_ratio_diff for ratio in ratios[i]]

        elif threshold_nb_diff_char is not None:
            nb_char_diffs = [round((1-ratio)*len(folder_names[i_min][j])) for j, ratio in enumerate(ratios[i])]
            is_similar_enough[i] = [nb_char_diff <= threshold_nb_diff_char for nb_char_diff in nb_char_diffs]

    matched_folder_names = [list(x) for x in zip(*matched_folder_names)]
    is_similar_enough = [list(x) for x in zip(*is_similar_enough)]

    matched_folder_names = [x for i, x in enumerate(matched_folder_names) if all(is_similar_enough[i])]

    return matched_folder_names


def match_recordings_by_name(paths: List[str],
                             threshold_ratio_diff: float = None,
                             threshold_nb_diff_char: int = None,
                             keep_only_folder_with_images: bool = True) -> List[List[str]]:
    """
    Matches the recordings together (minimum 2) by checking how similar their folder names are and associate them together
    The best matches will be found by comparing to the smaller list of folders to avoid matching one to two other folders.

    Args:
        paths: List of directory paths (one per camera) each containing folders (to be matched) with recorded images
        threshold_ratio_diff: Will check the similarity percentage (between 0 and 1)
        threshold_nb_diff_char: Will check how many characters are different between the file names.

    Returns:
        matched_folder_names: List of lists (# of recordings * # of cameras) containing the matched folder names.
    """

    folder_names = get_list_of_folder_names(paths, keep_only_folder_with_images=keep_only_folder_with_images)

    if threshold_nb_diff_char is not None:
        return match_folders_by_name(folder_names, threshold_nb_diff_char=threshold_nb_diff_char)

    else:
        return match_folders_by_name(folder_names, threshold_ratio_diff=threshold_ratio_diff)


def match_folders_by_date(folder_names: List[str], expr: str = r"\d{8}_\d{6}", format_date_str: str ='%Y%m%d_%H%M%S',
                          threshold_s: int = 30) -> List[List[str]]:
    """
    Matches list of folder names together by checking how similar their folder names are and associate them together
    The best matches will be found by comparing to the smaller list of folders to avoid matching one to two folders.

    Args:
        folder_names: List[list[]] of folder names
        expr: Regular expression pattern matching recording folders.
        format_date_str: format of the date and time in the folder names.
        threshold_s: Will check if the date in the folder names differ from less than threshold in second
        
    Returns:
        matched_folder_names: List of lists (# of recordings * # of cameras) containing the matched folder names.

    Raises:
        TypeError: threshold_s has to be a positive int or a float between 0.0 and 1.0.
    """

    if type(threshold_s) is float and threshold_s.is_integer():
        threshold_s = int(threshold_s)
    
    if not type(threshold_s) is int and threshold_s >= 0:
        raise TypeError('Threshold has to be either a positive int or a float between 0.0 and 1.0')

    pattern = re.compile(expr)

    dates = [[]] * len(folder_names)
    for i in range(len(folder_names)):

        new_folder_names, new_dates = [], []
        for j in range(len(folder_names[i])):
            match = re.search(pattern, folder_names[i][j])

            if match:
                new_folder_names.append(folder_names[i][j])
                new_dates.append(datetime.strptime(match.group(), format_date_str))

        folder_names[i] = new_folder_names
        dates[i] = new_dates

    nb_folders = [len(x) for x in folder_names]
    i_min = np.argmin(nb_folders)

    matched_folder_names, diff_time_s = [[]] * len(folder_names), [[]] * len(folder_names)
    is_similar_enough = [[]] * len(folder_names)
    for i in range(len(folder_names)):

        if i == i_min:
            matched_folder_names[i] = folder_names[i_min]
            diff_time_s[i] = [0] * len(folder_names[i_min])

        else:
            matches = [min([(abs((d1 - d2).total_seconds()), folder_names[i][j])
                            for j, d2 in enumerate(dates[i])]) for d1 in dates[i_min]]
            matched_folder_names[i] = [folder_name for diff_time_s, folder_name in matches]
            diff_time_s[i] = [diff_s for diff_s, folder_name in matches]

        is_similar_enough[i] = [x <= threshold_s for x in diff_time_s[i]]

    matched_folder_names = [list(x) for x in zip(*matched_folder_names)]
    is_similar_enough = [list(x) for x in zip(*is_similar_enough)]

    matched_folder_names = [x for i, x in enumerate(matched_folder_names) if all(is_similar_enough[i])]

    return matched_folder_names


def match_recordings_by_date(paths: List[str],
                             expr: str = r"\d{8}_\d{6}",
                             format_date_str: str ='%Y%m%d_%H%M%S', threshold_s: int = 30,
                             keep_only_folder_with_images: bool = True) -> List[List[str]]:
    """
    Matches the recordings together (minimum 2) by checking how similar the date in their folder names are
    and associate them together. The best matches will be found by comparing to the smaller list of folders
    to avoid matching one to two other folders.

    Args:
        paths: List of directory paths (one per camera) each containing folders (to be matched) with recorded images
        expr: Regular expression pattern matching recording folders.
        format_date_str: format of the date and time in the folder names.
        threshold_s: Will check if the date in the folder names differ from less than threshold in second
        keep_only_folder_with_images:

    Returns:
        matched_folder_names: List of lists (# of recordings * # of cameras) containing the matched folder names.
    """

    folder_names = get_list_of_folder_names(paths, keep_only_folder_with_images=keep_only_folder_with_images)

    return match_folders_by_date(folder_names, expr, format_date_str, threshold_s)


def get_unmatched_folders(folder_names: List[List[str]], matched_folder_names: List[List[str]]) -> List[List[str]]:
    """

    Args:
        folder_names:
        matched_folder_names:

    Returns:
        unmatched_folder_names
    """

    if len(folder_names) == len(matched_folder_names[0]):
        matched_folder_names = [list(x) for x in zip(*matched_folder_names)]

    unmatched_folder_names = [[]] * len(folder_names)
    for i in range(len(folder_names)):

        unmatched_folder_names[i] = [folder_names[i][j] for j in range(len(folder_names[i]))
                                     if folder_names[i][j] not in matched_folder_names[i]]

    return unmatched_folder_names


def get_unmatched_recordings(paths: List[str], matched_folder_names: List[List[str]],
                             keep_only_folder_with_images: bool = True) -> List[List[str]]:
    """

    Args:
        paths:
        matched_folder_names:
        keep_only_folder_with_images:

    Returns:
        unmatched_folder_names:
    """

    folder_names = get_list_of_folder_names(paths, keep_only_folder_with_images=keep_only_folder_with_images)

    unmatched_folder_names = get_unmatched_folders(folder_names, matched_folder_names)

    return unmatched_folder_names


def get_list_of_folder_names(paths: List[str], keep_only_folder_with_images: bool = True) -> List[List[str]]:
    """

    Args:
        paths:
        keep_only_folder_with_images:

    Returns:
        folder_names:
    """

    folder_names = [[]] * len(paths)
    for ind_cam, directory in enumerate(paths):
        folders = os.listdir(directory)
        folders = sorted(folders, key=lambda i: os.path.splitext(os.path.basename(i)))

        folder_names[ind_cam] = [os.path.basename(os.path.normpath(folder)) for folder in folders]

        assert len(folder_names) > 0, 'ERROR: No recording folders found in directory: {0}'.format(directory)

        if keep_only_folder_with_images:

            ind_folder_to_remove = []
            for ind_folder, folder_name in enumerate(folder_names[ind_cam]):
                if os.path.isdir(os.path.join(directory, folder_name)) and \
                        not does_folder_contains_images(os.path.join(directory, folder_name)):
                    ind_folder_to_remove.append(ind_folder)

            if ind_folder_to_remove:
                [folder_names[ind_cam].pop(i) for i in ind_folder_to_remove[::-1]]

    return folder_names


def get_image_names_in_directory(directory: str, expr: str = r"\d+.(tif|png|jpg|bmp)") -> List[str]:
    """

        Args:
            directory: The path to a directory containing images.
            expr: Regular expression pattern matching image files.
    """

    if not os.path.exists(directory): return []

    contents = os.listdir(directory)
    pattern = re.compile(expr)

    image_names = []
    for file_name in contents:
        match = re.search(pattern, file_name)
        if match:
            image_names.append(os.path.basename(os.path.normpath(file_name)))

    return sorted(image_names)


def does_folder_contains_images(directory: str, expr: str = r"\d+.(tif|png|jpg|bmp)") -> bool:
    """

        Args:
            directory: The path to a directory containing images.
            expr: Regular expression pattern matching image files.
    """

    image_names = get_image_names_in_directory(directory, expr)

    return len(image_names) != 0


def convert_kwargs_to_yaml(kwargs: Dict[any, any]) -> Dict[any, any]:
    """

    Args:
        kwargs:

    Returns:
        new_kwargs:
    """

    new_kwargs = copy.deepcopy(kwargs)
    for key in kwargs.keys():
        if hasattr(new_kwargs[key], '__dict__'):
            new_kwargs[key] = new_kwargs[key].__dict__

        elif type(new_kwargs[key]) is dict:
            new_kwargs = convert_kwargs_to_yaml(new_kwargs)

    return new_kwargs


def find_char(string, character):
    """

    Args:
        string:
        character:

    Returns:
        indices of the character in the string

    """
    return [i for i, letter in enumerate(string) if letter == character]

