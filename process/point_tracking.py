import os, glob, copy
import time

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import cv2
from PIL import Image

from typing import Dict, List, Tuple

from point_tracker.reconstructor3d import Reconstructor3D
from point_tracker.tracker2d import Tracker2D, BlobDetectorSettings, BackgroundSubtractorSettings
from point_tracker.reconstructor3d import Reconstructor3DSettings

from images.read import read_image


def manual_2d_tracking(image_names: List[str], image_path: str, frames: List[int]) -> Dict[str, List[float or int]]:
    """

    Args:
        image_names:
        image_path:
        frames:

    Return:
        coords:

    """

    def click_event(event, x, y, flags, params):
        if event == cv2.EVENT_LBUTTONDOWN:
            img2 = copy.copy(img)
            cv2.circle(img2, (x, y), radius=0, color=(0, 255, 0), thickness=8)
            cv2.imshow(image_name, img2)

            x_px[index], y_px[index] = x, y

    x_px, y_px = np.ones((len(frames),)) * np.nan, np.ones((len(frames),)) * np.nan
    for index, image_name in enumerate(image_names):
        img = cv2.cvtColor(np.array(read_image(os.path.join(image_path, image_name))), cv2.COLOR_RGB2BGR)

        text_str = 'Use mouse to track object (right click), and validate with any key.\n'\
                   'You can cancel last point using ESC.'

        for i, line in enumerate(text_str.split('\n')):
            textsize = cv2.getTextSize(line, cv2.FONT_HERSHEY_SIMPLEX, 0.8, 2)[0]
            x_text, y_text = int((img.shape[1] - textsize[0]) / 2), int((img.shape[0]) / 15) + textsize[1]*2*i
            cv2.putText(img, line, (x_text, y_text), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255), 2)

        cv2.imshow(image_name, img)
        cv2.setMouseCallback(image_name, click_event)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    assert len(frames) == len(x_px)

    return {'frame': frames, 'x_px': x_px, 'y_px': y_px}


def manual_3d_tracking(image_names: Dict[int, List[str]], image_paths: Dict[int, str], frames:  Dict[int, List[int]],
                       dlt_path: str, save_names: Dict[any, str], save_paths: Dict[any, str], reconstructor_settings: Reconstructor3DSettings,
                       index_to_track: List[int] = [0, -1], show_plot: bool = False) -> None:
    """

    Args:
        image_names:
        image_paths:
        frames:
        dlt_path:
        save_names:
        save_paths:
        reconstructor_settings:

        index_to_track:
        show_plot:
    """

    nb_cam = len(image_names)

    pts_csv = {}
    min_delay_frame = np.zeros((nb_cam,))

    commom_frames = list(set.intersection(*[set(frames[camn]) for camn in range(1, nb_cam + 1)]))
    frames_to_track = [commom_frames[index] for index in index_to_track]

    for camn in range(1, nb_cam + 1):
        image_names_to_track = [image_names[camn][index] for index in index_to_track]
        pts_csv[camn] = manual_2d_tracking(image_names_to_track, image_paths[camn], frames_to_track)
        min_delay_frame[camn - 1] = min(np.array(frames_to_track[1:]) - np.array(frames_to_track[0:-1]))

    xs_pose, ys_pose, zs_pose, frames_pose, indexes_pose, _ = \
        reconstruct_3d_points(pts_csv, dlt_path, save_names, save_paths,
                              reconstructor_settings=reconstructor_settings)

    nb_frames = len(frames_to_track)
    indexes = np.zeros((nb_frames, 3))
    x, y, z = np.zeros((nb_frames,)), np.zeros((nb_frames,)), np.zeros((nb_frames,))
    for i, frame in enumerate(frames_to_track):
        index_frame = frames_pose.index(frame)
        if len(xs_pose[index_frame]) > 0:
            x[i], y[i], z[i] = xs_pose[index_frame][0], ys_pose[index_frame][0], zs_pose[index_frame][0]
            indexes[i] = indexes_pose[index_frame][0]

    tracks_dict = {'obj': {1: {'x': x, 'y': y, 'z': z, 'frame': frames_to_track, 'index_pts': indexes}}}
    save_3d_track(tracks_dict, dlt_path, save_names, save_paths, show_plot=show_plot)


def track_2d_objects(image_names: List[str], image_path: str, save_name: str, save_path: str,
                     back_subtractor_settings: BackgroundSubtractorSettings = BackgroundSubtractorSettings,
                     blob_detector_settings: BlobDetectorSettings = BlobDetectorSettings,
                     frames: List[int] = None, show_plot: bool = False) -> None:
    """

    Args:
        image_names:
        image_path:
        save_name:
        save_path:
        back_subtractor_settings:
        blob_detector_settings:
        frames:
        show_plot:
    """

    if frames is None: frames = list(range(1, len(image_names) +1))

    tracker = Tracker2D.load_images(image_names, image_path, frames)

    tracker.get_back_subtractor(back_subtractor_settings)
    tracker.get_blob_detector(blob_detector_settings)

    tracker.do_tracking()

    if show_plot:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        plt.imshow(Image.open(os.path.join(image_path, image_names[0])))
        ax.scatter(tracker.points['x'], tracker.points['y'])
        ax.set_xlabel('x [m]')
        ax.set_ylabel('y [m]')
        plt.show()

    tracker.save_csv(save_name, save_path)


def interp_2d_coords(frames: List[int], csv_name: str, csv_path: str, save_path: str, image_size: Tuple[int, int],
                     show_plot: bool = False) -> Dict[str, List[float or int]]:
    """

    Args:
        frames:
        csv_name:
        csv_path:
        save_path:
        image_size:
        show_plot:

    Returns:
        obj_dict:
    """
    width_img, height_img = image_size

    old_obj_dict = np.genfromtxt(os.path.join(csv_path, csv_name), delimiter=',', skip_header=0, names=True)

    is_not_nan = ~np.isnan(old_obj_dict['x_px']) & ~np.isnan(old_obj_dict['y_px'])

    frames = np.array(frames)
    x_px = np.interp(frames, old_obj_dict['frame'][is_not_nan], old_obj_dict['x_px'][is_not_nan])
    y_px = np.interp(frames, old_obj_dict['frame'][is_not_nan], old_obj_dict['y_px'][is_not_nan])

    for i, _ in enumerate(frames):
        if x_px[i] > width_img: x_px[i] = width_img
        if y_px[i] > height_img: y_px[i] = height_img
        if x_px[i] < 0: x_px[i] = 0
        if y_px[i] < 0: y_px[i] = 0

    np.savetxt(os.path.join(save_path, csv_name), np.c_[frames, x_px, y_px], delimiter=',', header='frame,x_px,y_px')
    obj_dict = {'frame': frames, 'x_px': x_px, 'y_px': y_px}

    if show_plot:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.scatter(obj_dict['x_px'], obj_dict['y_px'], marker='*')
        ax.scatter(old_obj_dict['x_px'], old_obj_dict['y_px'], marker='x')
        ax.set_xlabel('x [pixels]')
        ax.set_ylabel('y [pixels]')
        plt.xlim(0, width_img)
        plt.ylim(height_img, 0)

        fig2 = plt.figure()
        ax2 = fig2.add_subplot(111)
        ax2.scatter(obj_dict['frame'], obj_dict['x_px'], marker='*', label='x')
        ax2.scatter(obj_dict['frame'], obj_dict['y_px'], marker='*', label='y')
        ax2.set_xlabel('frame [-]')
        ax2.set_ylabel('coordinates [pixels]')
        ax2.legend()
        plt.show()

    return obj_dict


def load_2d_coords(frames: List[int], csv_path: str, save_path: str, image_size: Tuple[int, int],
                   from_tracks: bool = False, show_plot: bool = False) -> Dict[any, str]:
    """


    Args:
        frames:
        csv_path:
        save_path:
        image_size:

        from_tracks:
        show_plot:

    Return:
        coords_objs:
    """

    if from_tracks:
        csv_names = glob.glob(os.path.join(csv_path, '*-obj*-2d_points.csv'))
        csv_names = [os.path.basename(os.path.normpath(csv_name)) for csv_name in csv_names]

        obj_names = [csv_name[csv_name.rfind('-obj') + 1:csv_name.find('-2d_points.csv')] for csv_name in csv_names]

        coords_objs = {}
        for i, csv_name in enumerate(csv_names):
            if len(obj_names) == 0:
                coords_objs[obj_names[i]] = {}

            else:
                coords_objs[obj_names[i]] = \
                    interp_2d_coords(frames, csv_name, csv_path, save_path, image_size, show_plot)

    else:
        csv_names = glob.glob(os.path.join(csv_path, '*-2d_points.csv'))
        csv_names = [os.path.basename(os.path.normpath(csv_name)) for csv_name in csv_names]
        csv_names = [csv_name for csv_name in csv_names if 'obj' not in csv_name]

        assert len(csv_names) >= 1, "There should be one *-2d_points.csv in the csv_path"
        assert len(csv_names) <= 1, "There should be only one *-2d_points.csv in the csv_path"

        coords_objs = {'obj0': interp_2d_coords(frames, csv_names[0], csv_path, save_path, image_size, show_plot)}

    return coords_objs


def save_2d_tracks_from_paths(csv_paths: Dict[str, str], save_names: Dict[str, str], save_paths: Dict[str, str]) -> None:
    """

    Args:
        csv_paths:
        save_names:
        save_paths:
    """

    for obj_name in csv_paths.keys():
        pts_csv = {}
        for camn in csv_paths[obj_name].keys():
            pts_csv[camn] = np.genfromtxt(os.path.join(csv_paths[obj_name][camn]),
                                          delimiter=',', skip_header=0, names=True)

        x_px = np.array([pts_csv[camn]['x_px'] for camn in csv_paths[obj_name].keys()])
        y_px = np.array([pts_csv[camn]['y_px'] for camn in csv_paths[obj_name].keys()])
        frames = np.array([pts_csv[camn]['frame'] for camn in csv_paths[obj_name].keys()])

        if not np.array_equal(frames, np.sort(np.unique(frames))):
            raise ValueError('frames_objs must be sorted and unique')

        if not os.path.exists(save_paths[camn]): os.makedirs(save_paths[camn])

        for i, camn in enumerate(csv_paths[obj_name].keys()):
            np.savetxt(os.path.join(save_paths[camn], save_names[camn] + '-{0}-2d_points.csv'.format(obj_name)),
                       np.c_[frames, x_px, y_px], delimiter=',', header='frame,x_px,y_px')


def save_2d_tracks_from_dict(tracks_dict: Dict[str, Dict[int, List[float]]],
                             save_names: Dict[str, str], save_paths: Dict[str, str]) -> None:
    """

    Args:
        tracks_dict:
        save_names:
        save_paths:
    """

    for obj_name in tracks_dict.keys():
        for i, camn in enumerate(tracks_dict[obj_name].keys()):
            x_px = np.array(tracks_dict[obj_name][camn]['x_px'])
            y_px = np.array(tracks_dict[obj_name][camn]['y_px'])
            frames = np.array(tracks_dict[obj_name][camn]['frames'])

            if not np.array_equal(frames, np.sort(np.unique(frames))):
                raise ValueError('frames_objs must be sorted and unique')

            if not os.path.exists(save_paths[camn]): os.makedirs(save_paths[camn])

            np.savetxt(os.path.join(save_paths[camn], save_names[camn] + '-{0}-2d_points.csv'.format(obj_name)),
                       np.c_[frames, x_px, y_px], delimiter=',', header='frame,x_px,y_px')


def reconstruct_3d_points(pts_dict: Dict[any, Dict[str, List[float]]], dlt_path: str, save_names: Dict[any, str],
                          save_paths: Dict[any, str], reconstructor_settings: Reconstructor3DSettings,
                          save_csv: bool = True) -> Tuple[List[float], List[float], List[float],
                                                          List[int], List[int], List[float]]:
    """

    Args:
        pts_dict:
        dlt_path:
        save_names:
        save_paths:
        reconstructor_settings:
        save_csv:

    Returns:
        xs_pose:
        ys_pose:
        zs_pose:
        frames_pose:
        indexes:
        rmses:
    """

    reconstructor = Reconstructor3D.read_dlt_coefs(dlt_path)
    reconstructor.settings = reconstructor_settings

    nb_cam = reconstructor.nb_cam

    xs_coord = np.array([pts_dict[camn]['x_px'] for camn in range(1, nb_cam + 1)])
    ys_coord = np.array([pts_dict[camn]['y_px'] for camn in range(1, nb_cam + 1)])
    frames_coord = np.array([pts_dict[camn]['frame'] for camn in range(1, nb_cam + 1)])

    xs_pose, ys_pose, zs_pose, frames_pose, indexes, rmses, = \
        reconstructor.recon_objs(xs_coord, ys_coord, frames_coord)

    for camn in range(1, nb_cam + 1):
        if not os.path.exists(save_paths[camn]): os.makedirs(save_paths[camn])

    save_names_list = [save_names[camn] for camn in range(1, nb_cam + 1)]
    save_paths_list = [save_paths[camn] for camn in range(1, nb_cam + 1)]
    if save_csv: reconstructor.save_points_csv(save_names_list, save_paths_list, save_type='all_frames')

    return xs_pose, ys_pose, zs_pose, frames_pose, indexes, rmses


def save_3d_track(tracks_dict: Dict[str, Dict[int, Dict[str, List[float]]]], dlt_path: str,
                  save_names: Dict[any, str], save_paths: Dict[any, str], show_plot: bool = False) -> None:
    """

    Args:
        tracks_dict:
        dlt_coefs:
        save_names:
        save_paths:
        show_plot:
    """

    reconstructor = Reconstructor3D.read_dlt_coefs(dlt_path)
    nb_cam = reconstructor.nb_cam

    reconstructor.tracks_dict = tracks_dict

    for camn in range(1, nb_cam + 1):
        if not os.path.exists(save_paths[camn]): os.makedirs(save_paths[camn])

    save_names_list = [save_names[camn] for camn in range(1, nb_cam + 1)]
    save_paths_list = [save_paths[camn] for camn in range(1, nb_cam + 1)]
    reconstructor.save_tracks_csv(save_names_list, save_paths_list)

    if show_plot:
        fig = plt.figure()
        ax = Axes3D(fig)
        for nb_obj in tracks_dict['obj'].keys():
            ax.scatter(tracks_dict['obj'][nb_obj]['x'], tracks_dict['obj'][nb_obj]['y'],
                       tracks_dict['obj'][nb_obj]['z'], marker='o')
        ax.set_xlabel('x [m]')
        ax.set_ylabel('y [m]')
        ax.set_zlabel('z [m]')
        plt.show()


def reconstruct_3d_tracks(pts_dict: Dict[any, Dict[str, List[float]]], dlt_path: str, save_names: Dict[any, str],
                          save_paths: Dict[any, str], reconstructor_settings: Reconstructor3DSettings,
                          show_plot: bool = False, save_csv: bool = True) -> Dict[str, Dict[int, Dict[str, List[float]]]]:
    """

    Args:
        pts_dict:
        dlt_path:
        save_names:
        save_paths:
        reconstructor_settings:
        show_plot:
        save_csv:

    Returns:
        tracks_dict:
    """

    reconstructor = Reconstructor3D.read_dlt_coefs(dlt_path)
    reconstructor.settings = reconstructor_settings
    nb_cam = reconstructor.nb_cam

    xs_coord = np.asarray([np.asarray(pts_dict[camn]['x_px']) for camn in range(1, nb_cam + 1)], dtype=object)
    ys_coord = np.asarray([np.asarray(pts_dict[camn]['y_px']) for camn in range(1, nb_cam + 1)], dtype=object)
    frames_coord = np.asarray([np.asarray(pts_dict[camn]['frame']) for camn in range(1, nb_cam + 1)], dtype=object)

    xs_pose, ys_pose, zs_pose, frames_pose, indexes_pts, _, = \
        reconstructor.recon_objs(xs_coord, ys_coord, frames_coord)

    tracks_dict = reconstructor.recon_tracks(xs_pose, ys_pose, zs_pose, frames_pose, indexes_pts)

    if not xs_pose or not tracks_dict['obj']: return

    if save_csv:
        save_names_list = [save_names[camn] for camn in range(1, nb_cam + 1)]
        save_paths_list = [save_paths[camn] for camn in range(1, nb_cam + 1)]
        reconstructor.save_tracks_csv(save_names_list, save_paths_list)

    if show_plot:
        xs_pose, ys_pose, zs_pose = np.concatenate(xs_pose), np.concatenate(ys_pose), np.concatenate(zs_pose)

        fig = plt.figure()
        ax = Axes3D(fig)
        ax.scatter(xs_pose, ys_pose, zs_pose, marker='+')
        for nb_obj in tracks_dict['obj'].keys():
            ax.scatter(tracks_dict['obj'][nb_obj]['x'], tracks_dict['obj'][nb_obj]['y'],
                       tracks_dict['obj'][nb_obj]['z'], marker='o')
        ax.set_xlabel('x [m]')
        ax.set_ylabel('y [m]')
        ax.set_zlabel('z [m]')
        plt.show()

    return tracks_dict
