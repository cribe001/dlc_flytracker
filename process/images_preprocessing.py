import os
import cv2

import numpy as np
from PIL import Image

from typing import Dict, List

import images.utils as img_utils
from images.read import read_image


def crop_image(img: Image.Image, coords: Dict[str, List[float]], height: int, width: int) -> Image.Image:
    """

    Args:
        img: A Pillow Image object (PIL.Image.Image)
        coords:
        height:
        width:

    Returns:
        img: A Pillow Image object (PIL.Image.Image)
    """

    x_pxs, y_pxs = np.array([]), np.array([])
    for x_name in [s for s in coords.keys() if 'x_' in s]:
        y_name = x_name.replace('x_', 'y_')

        x_pxs = np.append(x_pxs, coords[x_name])
        y_pxs = np.append(y_pxs, coords[y_name])

    assert x_pxs.size and y_pxs.size

    return img_utils.crop(img, x_pxs, y_pxs, height, width)


def crop_all_images(coords_objs: Dict[str, Dict[str, List[float]]], image_names: Dict[any, List[str]], image_path: str,
                    image_save_path: str, height: int, width: int) -> None:
    """

    Args:
        coords_objs:
        image_names:
        image_path:
        image_save_path:
        height:
        width:
    """

    obj_names = coords_objs.keys()

    for ind_img, image_name in enumerate(image_names):

        img = read_image(os.path.join(image_path, image_name))

        img_cropped = []
        for obj_name in obj_names:
            coords_obj = {key: coords_objs[obj_name][key][ind_img] for key in coords_objs[obj_name].keys()}
            
            img_cropped.append(crop_image(img, coords_obj, height, width))

            image_save_name = image_name[:image_name.rfind('.')] + '-' + obj_name + image_name[image_name.rfind('.'):]
            img_cropped[-1].save(os.path.join(image_save_path, image_save_name))
            img_cropped[-1].close()

        img.close()


def stitch_all_images(image_names: Dict[any, List[str]], image_paths: Dict[any, str], frames: List[int],
                      image_save_paths: Dict[any, str], main_camn: int) -> None:
    """

    Args:
        image_names:
        image_paths:
        frames:
        image_save_paths:
        main_camn:
    """

    nb_cam = len(image_names.keys())

    frames_set = set()
    for camn in range(1, nb_cam + 1):
        frames_set = frames_set & set(frames[camn]) if len(frames_set) > 0 else set(frames[camn])

    frames_set = np.sort(list(frames_set))

    index_i = {}
    for camn in range(1, nb_cam + 1):
        index_i[camn] = [frames[camn].index(frame) for frame in frames_set]

    if not os.path.exists(image_save_paths[main_camn]): os.makedirs(image_save_paths[main_camn])

    for i, _ in enumerate(index_i[main_camn]):
        imgs = {}
        for camn in range(1, nb_cam + 1):
            imgs[camn] = Image.open(os.path.join(image_paths[camn], image_names[camn][index_i[camn][i]]))

        img = img_utils.stitch_images(imgs, nb_cam)

        if image_names[main_camn][index_i[main_camn][i]].find('.jpg') != -1 and img.mode != 'RGB': img = img.convert('RGB')

        img.save(os.path.join(image_save_paths[main_camn], image_names[main_camn][index_i[main_camn][i]]), compression='lzw')


def stitch_images_multiprocessing(img: Image.Image, image_names: Dict[any, List[str]], image_rec_paths: Dict[any, str],
                                  frames: List[int], image_save_paths, obj_names: List[str], multi_processes_num: int,
                                  multi_processes_nums: List[int], main_camn: int) -> Image.Image:
    """

    Args:
        img: A Pillow Image object (PIL.Image.Image)
        image_names:
        image_rec_paths:
        frames:
        image_save_paths:
        obj_names:
        multi_processes_num:
        multi_processes_nums:
        main_camn:

    Returns:
        img: A Pillow Image object (PIL.Image.Image)
    """

    nb_cam = len(image_names.keys())

    frames_set = set()
    for camn in range(1, nb_cam + 1):
        frames_set = frames_set & set(frames[camn]) if len(frames_set) > 0 else set(frames[camn])

    frames_set = np.sort(list(frames_set))

    index_i = {}
    for camn in range(1, nb_cam + 1):
        index_i[camn] = [frames[camn].index(frame) for frame in frames_set]

    for obj_name in obj_names:
        # if obj_name == 'obj0' and 'from_fn_name' in kwargs.keys(): break

        for camn in range(1, nb_cam + 1):
            if obj_name not in img[camn].keys():
                img[camn][obj_name] = {}

        for i, _ in enumerate(index_i[main_camn]):
            imgs = {}
            for camn in range(1, nb_cam + 1):
                if i not in img[camn][obj_name].keys():
                    img[camn][obj_name][i] = read_image(os.path.join(image_rec_paths[camn], image_names[camn][i]))

                imgs[camn] = img[camn][obj_name][i]
                img[camn][obj_name][i] = []

            img[main_camn][obj_name][i] = img_utils.stitch_images(imgs, nb_cam)

        if multi_processes_num == multi_processes_nums[-1]:
            for i, image_name in enumerate(image_names[camn]):
                if 'obj0' == obj_name:
                    image_save_name = image_name
                else:
                    image_save_name = image_name[:image_name.rfind('.')] + '-' + obj_name + image_name[image_name.rfind('.'):]

                if image_name.find('.jpg') != -1 and img[main_camn][obj_name][i].mode != 'RGB':
                    img[main_camn][obj_name][i] = img[main_camn][obj_name][i].convert('RGB')

                img[main_camn][obj_name][i].save(os.path.join(image_save_paths[main_camn], image_save_name))
                img[main_camn][obj_name][i].close()

    return img


def save_avi(all_image_names: List[str], image_path: str, rec_name: str, save_path: str, frame_rate: int = 24,
             step_frame: int = 1, lossless: bool = False) -> None:
    """

    Args:
        all_image_names:
        image_path:
        rec_name:
        save_path:
        frame_rate:
        step_frame:
        lossless:
    """

    obj_names = np.unique([s[s.rfind('-obj')+1:s.rfind('.')] for s in all_image_names])

    for obj_name in obj_names:
        image_names = sorted([i for i in all_image_names if obj_name in i], key=lambda s: s[len(rec_name):s.rfind('.')])
        save_name = rec_name + '-' + obj_name + '.avi'

        img = cv2.imread(os.path.join(image_path, image_names[0]), cv2.IMREAD_UNCHANGED)
        is_rgb = len(img.shape) == 3
        if is_rgb:  height_img, width_img, _ = img.shape
        else: height_img, width_img = img.shape

        # commun: 'MP42', 'XVID' or lossless: 'HFYU'
        codec = cv2.VideoWriter_fourcc(*'HFYU') if lossless else cv2.VideoWriter_fourcc(*'MP42')
        writer = cv2.VideoWriter(os.path.join(save_path, save_name), codec, frame_rate, (width_img, height_img,))

        for i in np.arange(0, len(image_names), step_frame):
            img_16bit = cv2.imread(os.path.join(image_path, image_names[i]), cv2.IMREAD_UNCHANGED)
            if is_rgb: writer.write(img_16bit)
            else: writer.write(cv2.cvtColor(img_16bit, cv2.COLOR_GRAY2BGR))
            #cv2.imshow(obj_name, img_16bit)
            #cv2.waitKey(1)

        writer.release()

