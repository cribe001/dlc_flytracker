import copy, glob, os, time
from functools import partial
from multiprocessing import Pool

from typing import Dict, List

import numpy as np
from PIL import Image, ImageDraw
from matplotlib import pyplot as plt
from scipy import optimize, stats

from importlib import import_module

from camera import calib
from images.read import read_image
from process.smoothing_filtering import filter_interp_params, filter_interp

from skeleton_fitter.optimiser import optim_fit_body_params, optim_fit_limbs_params
from skeleton_fitter.utils import reproject_skeleton3d_to2d, load_body_params_from_csv, load_limbs_params_from_csv


def fit_all_skeleton(animal_name: str, fit_method, opt_method, skeleton2d_paths: List[str], skeleton3d_path: str,
                     init_yaml_path: str, dlt_path: str, save_path: str, frame_rate: int, show_plot: bool = False,
                     multiprocessing: bool = False, image_names: List[List[str]] = None, image_paths: List[str] = None) -> None:
    """

    Args:
        animal_name:
        fit_method:
        opt_method:
        skeleton2d_paths:
        skeleton3d_path:
        init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
        dlt_path:
        save_path:
        frame_rate:

        show_plot:
        multiprocessing: Whether to use multiprocessing to speed up processing (then won't be able to plot results during optimization)
        image_names:
        image_paths:
    """

    csv_paths = glob.glob(os.path.join(skeleton3d_path, '*-3d_points.csv'))
    filenames = [os.path.basename(path) for path in csv_paths]
    obj_names = np.unique([filename[:filename.index('-')] for filename in filenames])

    if len(csv_paths) == 0:
        raise ValueError("Couldn't find any *-3d_points.csv file in path: {0}".format(os.path.join(skeleton3d_path)))

    for obj_name in obj_names:
        skeleton_fitting = SkeletonFitting.load_dlt_from_csv(animal_name, init_yaml_path, dlt_path, frame_rate)

        skeleton_fitting.load_skeleton3d_from_csv(obj_name, skeleton3d_path)
        skeleton_fitting.load_skeleton2d_from_csv(obj_name, skeleton2d_paths)

        skeleton_fitting.optim_fit_skeleton(fit_method, opt_method, obj_name, image_names, image_paths, save_path,
                                            multiprocessing=multiprocessing, show_plot=show_plot)


def plot_all_skeleton(animal_name: str, image_names: List[str], image_paths: Dict[str, any], skeleton2d_paths: Dict[str, any],
                      csv_path: str, init_yaml_path: str, dlt_path: str, save_paths: Dict[str, any], frame_rate: int,
                      step_frame: int = -1, save_images: bool = False) -> None:
    """

    Args:
        animal_name:
        image_names:
        image_paths:
        skeleton2d_paths:
        csv_path:
        init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
        dlt_path:
        save_paths:
        frame_rate:
        step_frame:
        save_images:
    """

    csv_paths = glob.glob(os.path.join(csv_path, 'skeleton_parameters-*.csv'))
    obj_names = np.unique([path[path.index('skeleton_parameters-') + len('skeleton_parameters-'):
                                path.index('.csv')] for path in csv_paths])

    if len(csv_paths) == 0:
        raise ValueError("Couldn't find any skeleton_parameters-.csv file (path: {0})".format(os.path.join(csv_path)))

    for obj_name in obj_names:
        skel_fit = SkeletonFitting.load_dlt_from_csv(animal_name, init_yaml_path, dlt_path, frame_rate)

        path = os.path.join(csv_path, 'skeleton_parameters-' + obj_name + '.csv')

        skel_fit.load_all_body_params_from_csv(path)
        skel_fit.load_all_limbs_params_from_csv(path)

        skel_fit.load_skeleton2d_from_csv(obj_name, skeleton2d_paths)

        skel_fit.plot_skeleton_over_images(image_names, image_paths, modulo=step_frame, save_images=save_images,
                                           save_paths=save_paths, obj_name=obj_name)


class SkeletonFitting:
    def __init__(self, animal_name: str, init_yaml_path: str, dlt_coefs: List[List[float]], frame_rate: int) -> None:
        """
        Class use to fit 3d skeleton (or 2d re-projection) to the 2d/3d coordinates of animals joints.

        Args:
            animal_name:
            init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
            dlt_coefs: DLT coefficients (11*nb_cam)
            frame_rate:
        """

        self.animal_name = animal_name  # type: str
        self.dlt_coefs = dlt_coefs  # type: List[List[float]]
        self.nb_cam = len(dlt_coefs)  # type: int

        assert len(self.dlt_coefs[0]) == 11
        assert self.nb_cam >= 2

        # TODO use function arguments instead?
        self.nb_process = 5  # type: int # Can be optimized depending on the used PC
        self.cutoff_frequency = 100  # type: int # Hz (for Butterworth filter on body params)

        self.animal = import_module('skeleton_fitter.animals.' + self.animal_name)
        self.animal_sets = self.animal.AnimalSettings

        self.body = import_module('skeleton_fitter.modules.bodies.' + self.animal_sets.body_module_name)
        self.body_sets = self.body.BodyModuleSettings(init_yaml_path)

        self.body_params = copy.deepcopy(self.body_sets.params_init)  # type: Dict[str, Dict[str, any]]

        self._sides = ['left', 'right']

        self.limbs, self.limbs_sets, self.limbs_params = {}, {}, {}
        for num_limb, limb_name in enumerate(self.animal_sets.limb_module_names):
            self.limbs[limb_name] = import_module('skeleton_fitter.modules.limbs.' + limb_name)

            self.limbs_sets[limb_name] = self.limbs[limb_name].LimbsModuleSettings(init_yaml_path)
            self.limbs_params[limb_name] = copy.deepcopy(self.limbs_sets[limb_name].params_init)

        self.skeleton2d, self.skeleton3d = None, None  # type: Dict[str, any]

        self.frame_rate = frame_rate  # type: int

    @staticmethod
    def load_dlt_from_csv(animal_name: str, init_yaml_path: str, dlt_path: str, frame_rate: int):
        """
        Load DLT coefficient from a csv file.

        Args:
            animal_name:
            init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
            dlt_path: path of the .csv file with DLT (11) coefficients (along rows) for all camera (along column)
            frame_rate:
        """

        dlt_coefs = calib.read_dlt_coefs(dlt_path)

        return SkeletonFitting(animal_name, init_yaml_path, dlt_coefs, frame_rate)

    def load_all_body_params_from_csv(self, path):
        """
        Load body parameters from a csv file.

        Args:
            path: path of the csv file
        """
        self.body_params = load_body_params_from_csv(path, self.body_sets.param_names)

    def load_all_limbs_params_from_csv(self, path):
        """
        Load all limbs parameters from csv files.

        Args:
            path: path of the csv file
        """

        # TODO should it get different path for each limb type and number?
        #  Or at least it should be able to get different info for each limb type and number in the csv file

        self.limbs_params = {}
        for limb_name in self.animal_sets.limb_module_names:

            self.limbs_params[limb_name] = \
                load_limbs_params_from_csv(path, self.body_sets.param_names, self.limbs_sets[limb_name].param_names)

    def load_skeleton2d_from_csv(self, obj_name, paths):
        """
        Load 2d skeleton coordinates and parameters from csv files (one per camera view).

        Args:
            obj_name:
            paths: path of the csv files
        """
        label_names = self._get_all_label_names()

        self.skeleton2d = {'frames': [], 'label_names': label_names}
        for label in label_names:
            self.skeleton2d[label] = {1: {}}
            for camn in range(1, len(paths.keys()) + 1):
                pts_csv = np.genfromtxt(os.path.join(paths[camn], obj_name + '-' + label + '-2d_points.csv'),
                                        delimiter=',', skip_header=0, names=True)

                self.skeleton2d['frames'] = np.unique(np.concatenate((self.skeleton2d['frames'], pts_csv['frame'])))
                self.skeleton2d[label][camn] = {'x': pts_csv['x_px'], 'y': pts_csv['y_px'], 'likelihood': pts_csv['likelihood']}

    def load_skeleton3d_from_csv(self, obj_name, path):
        """
        Load 3d skeleton coordinates and parameters from a csv file.

        Args:
            obj_name:
            path: path of the csv file
        """
        label_names = self._get_all_label_names()

        self.skeleton3d = {'frames': [], 'label_names': label_names}
        for label in label_names:
            pts_csv = np.genfromtxt(os.path.join(path, obj_name + '-' + label + '-3d_points.csv'),
                                    delimiter=',', skip_header=0, names=True)

            self.skeleton3d['frames'] = np.unique(np.concatenate((self.skeleton3d['frames'], pts_csv['frame'])))
            self.skeleton3d[label] = {'x': pts_csv['x'], 'y': pts_csv['y'], 'z': pts_csv['z']}

    def _get_all_label_names(self):
        limbs_label_names = []
        for limb_name in self.animal_sets.limb_module_names:
            for label in self.limbs_sets[limb_name].labels:
                for side in ['left', 'right']:
                    limbs_label_names.append(side + '_' + self.limbs_sets[limb_name].name + '_' + label)

        label_names = np.unique(np.concatenate((self.body_sets.labels, limbs_label_names)))

        return label_names

    def _estimate_body_params(self, interpolate_nans=False, filter_high_freq=False, show_plot=False):
        body_param_names = list(self.body_params.keys())

        body_params = {}
        frames = self.skeleton3d['frames']
        
        for param_name in body_param_names:
            body_params[param_name] = np.nan * np.ones(frames.shape)

        # Initial estimation of the body parameters using simple geometric relationships
        for i, frame in enumerate(frames):
            cur_body_skeleton3d = {}
            for body_label in self.body_sets.labels:
                cur_body_skeleton3d[body_label] = np.array([self.skeleton3d[body_label]['x'][i],
                                                            self.skeleton3d[body_label]['y'][i],
                                                            self.skeleton3d[body_label]['z'][i]])

            cur_body_params = self.body.estimate_params_from_skeleton3d(cur_body_skeleton3d, self.body_sets.params_init)
            for param_name in body_param_names:
                body_params[param_name][i] = cur_body_params[param_name]

        if show_plot:
            # Plot body params that will be kept cst
            fig, axs = plt.subplots(len(self.body_sets.param_names_to_keep_cst), 1)
            for i, param_name in enumerate(self.body_sets.param_names_to_keep_cst):
                y = body_params[param_name]
                axs[i].plot(frames, y, '.', label='estim')
                axs[i].plot(frames, np.nanmean(y) * np.ones(frames.shape), '-', label='mean')
                axs[i].plot(frames, np.nanmedian(y) * np.ones(frames.shape), '-', label='median')
                axs[i].plot(frames, stats.mode(y)[0] * np.ones(frames.shape), '-', label='mode')

                axs[i].set_ylabel(param_name)

            axs[i].set_xlabel('frames')
            fig.suptitle('Body parameters in function of time')
            fig.legend()

            plt.show()

        # Compute median values of body lengths (should be constant for one animal)
        for param_name in self.body_sets.param_names_to_keep_cst:
            body_params[param_name] = np.nanmedian(body_params[param_name]) * np.ones(frames.shape)
             # print('>>> mean {0}: {1}'.format(label, body_params[param_name][0]))

        body_params = filter_interp_params(self.cutoff_frequency, self.frame_rate, body_params, body_param_names,
                                           interpolate_nans, filter_high_freq, show_plot)

        return body_params

    def _estimate_all_limbs_params(self, interpolate_nans=False, filter_high_freq=False, show_plot=False):
        limbs_params = {}
        for limb_name in self.animal_sets.limb_module_names:
            limbs_params[limb_name] = self.limbs_params[limb_name] = \
                self._estimate_limbs_params(self.limbs[limb_name], self.limbs_sets[limb_name], interpolate_nans=interpolate_nans,
                                            filter_high_freq=filter_high_freq, show_plot=show_plot)

            # TODO delete commented parts (estimate_wing_hinges, unscramble_sides3d_dlc, correct_body_wings_params
            # limbs_params[limb_name] =
            #     self.estimate_wing_hinges(self.limbs_sets[limb_name],
            #                               limbs_params[limb_name], interpolate_nans=True, filter_high_freq=True)

        return limbs_params

    def _estimate_limbs_params(self, limbs, limbs_sets, interpolate_nans=False, filter_high_freq=False, show_plot=False):
        limbs_params = {}
        frames = self.skeleton3d['frames']

        for side in self._sides:
            limbs_params[side] = {}
            for label in limbs_sets.param_names:
                limbs_params[side][label] = np.nan * np.ones(frames.shape)

        # Initial estimation of the limbs parameters using simple geometric relationships
        for i, frame in enumerate(frames):

            cur_body_params = {}
            for body_param_name in self.body_params.keys():
                cur_body_params[body_param_name] = self.body_params[body_param_name][i]

            cur_limbs_skeleton3d = {}
            for side in self._sides:

                cur_limbs_skeleton3d[side] = {}
                for limb_label in limbs_sets.labels:
                    label = side + '_' + limbs_sets.name + '_' + limb_label
                    cur_limbs_skeleton3d[side][limb_label] = np.array([self.skeleton3d[label]['x'][i],
                                                                       self.skeleton3d[label]['y'][i],
                                                                       self.skeleton3d[label]['z'][i]])

                limb_param = limbs.estimate_params_from_skeleton3d(cur_limbs_skeleton3d[side], cur_body_params,
                                                                   limbs_sets.params_init[side], side)

                for param_name in limbs_sets.param_names:
                    limbs_params[side][param_name][i] = limb_param[param_name]

        # Animal specific postprocessing function
        limbs_params = limbs.postprocessing_estimate_limbs_params(limbs_params, limbs_sets)

        if show_plot:
            # Plot limbs params that will be kept cst
            fig, axs = plt.subplots(len(limbs_sets.param_names_to_keep_cst), 1)
            for i, label in enumerate(limbs_sets.param_names_to_keep_cst):
                for side in self._sides:
                    y = limbs_params[side][label]
                    axs[i].plot(frames, y, '.', label='estim_' + side)
                    axs[i].plot(frames, np.nanmean(y) * np.ones(frames.shape), '-', label='mean_' + side)
                    axs[i].plot(frames, np.nanmedian(y) * np.ones(frames.shape), '-', label='median_' + side)
                    axs[i].plot(frames, stats.mode(y)[0] * np.ones(frames.shape), '-', label='mode_' + side)
                axs[i].set_ylabel(label)
            axs[i].set_xlabel('frames')
            fig.suptitle('Limbs parameters (which will be kept constant) in function of time')
            fig.legend()

            plt.show()

        # Compute median values of limbs lengths (should be constant for one animal)
        for label in limbs_sets.param_names_to_keep_cst:
            median_over_sides = np.nanmedian([np.nanmedian(limbs_params[side][label]) for side in self._sides])
            for side in self._sides:
                limbs_params[side][label] = median_over_sides * np.ones(frames.shape)
            # print('>>> mean {0}: {1} (right and left)'.format(label, limbs_params['right'][label][0]))

        for side in self._sides:
            limbs_params[side] = \
                filter_interp_params(self.cutoff_frequency, self.frame_rate, limbs_params[side],
                                     ['x_hinge', 'y_hinge', 'z_hinge'], False, filter_high_freq, show_plot)
            limbs_params[side] = \
                filter_interp_params(self.cutoff_frequency, self.frame_rate, limbs_params[side],
                                     limbs_sets.param_names, interpolate_nans, False, show_plot)

        return limbs_params

    # def estimate_wing_hinges(self, limbs_sets, limbs_params, interpolate_nans=False, filter_high_freq=False, show_plot=False):
    #
    #     dpt = 3
    #     x, y, z = symbols('x y z')
    #
    #     frames = self.skeleton3d['frames']
    #
    #     if show_plot:
    #         init_hinges = {}
    #         for side in self._sides:
    #             init_hinges[side] = {'x': limbs_params[side]['x_hinge'].copy(),
    #                                  'y': limbs_params[side]['y_hinge'].copy(),
    #                                  'z': limbs_params[side]['z_hinge'].copy()}
    #
    #     for side in self._sides:
    #         for param_name in limbs_sets.param_names:
    #             limbs_params[side][param_name] = np.nan * np.ones(frames.shape)
    #
    #     for side in self._sides:
    #         a = np.ones(np.shape(frames)) * np.nan
    #         b, c, d = a.copy(), a.copy(), a.copy()
    #         for i, frame in enumerate(frames):
    #             x_coords = np.ones((len(self.limbs_sets[limb_name].labels), 1)) * np.nan
    #             y_coords, z_coords = x_coords.copy(), x_coords.copy()
    #             for j, limb_label in enumerate(self.limbs_sets[limb_name].labels):
    #                 label = side + '_' + limbs_sets.name + '_' + limb_label
    #
    #                 x_coords[j] = self.skeleton3d[label]['x'][i]
    #                 y_coords[j] = self.skeleton3d[label]['y'][i]
    #                 z_coords[j] = self.skeleton3d[label]['z'][i]
    #
    #             if len(x_coords) < 3: continue
    #             params = minimize_perp_distance(x_coords, y_coords, z_coords)
    #             a[i], b[i], c[i], d[i] = params
    #
    #             if i >= 2*dpt:
    #                 eq = [a[i - 2*dpt] * x + b[i - 2*dpt] * y + c[i - 2*dpt] * z + d[i - 2*dpt],
    #                       a[i - 1*dpt] * x + b[i - 1*dpt] * y + c[i - 1*dpt] * z + d[i - 1*dpt],
    #                       a[i] * x + b[i] * y + c[i] * z + d[i]]
    #                 sol = solve(eq, dict=True)
    #
    #                 if len(sol) > 0 and x in sol[0].keys() and y in sol[0].keys() and z in sol[0].keys():
    #                     limbs_params[side]['x_hinge'][i] = sol[0][x]
    #                     limbs_params[side]['y_hinge'][i] = sol[0][y]
    #                     limbs_params[side]['z_hinge'][i] = sol[0][z]
    #
    #     limbs_params = {}
    #     for side in self._sides:
    #         limbs_params[side] = \
    #             filter_interp_params(self.cutoff_frequency, self.frame_rate,
    #                                  limbs_params[side],
    #                                  ['x_hinge', 'y_hinge', 'z_hinge'], interpolate_nans, filter_high_freq, show_plot)
    #
    #     if show_plot:
    #         fig, axs = plt.subplots(1, 3)
    #         for i, coord in enumerate(['x', 'y', 'z']):
    #             axs[i].clear()
    #             for side in self._sides:
    #                 axs[i].scatter(frames, init_hinges[side][coord], marker='o', label='init_' + side)
    #                 axs[i].scatter(frames, limbs_params[side][coord + '_hinge'], marker='+', label='est_' + side)
    #             axs[i].set_ylabel(coord + ' pos [m]')
    #             axs[i].set_xlabel('frames')
    #         plt.legend()
    #         plt.show()
    #
    #     return limbs_params

    def optim_fit_skeleton(self, fit_method: str, opt_method: str, obj_name: str, image_names: List[List[str]], 
                           image_paths: List[str], save_path: str,
                           show_plot: bool = False, multiprocessing: bool = True) -> None:
        """

        Will fit the 3d skeleton (or 2d re-projections) to the previously loaded 2d/3d coordinates of the animal joints.

        Args:
            fit_method:
            opt_method: optimization method ('nelder-mead', 'powell', 'least_squares', 'leastsq')
            obj_name:
            image_names:
            image_paths:
            save_path:
            
            show_plot:
            multiprocessing: Whether to use multiprocessing to speed up processing (won't be able to plot at same time)

        Raises:
            ValueError: If skeleton2d or skeleton3d are None, should be loaded (e.g. from csv) before

        """

        if self.skeleton2d is None:
            raise ValueError('skeleton2d need to be a dictionary (solution: use load_skeleton2d_from_csv before)')
        if self.skeleton3d is None:
            raise ValueError('skeleton3d need to be a dictionary (solution: use load_skeleton3d_from_csv before)')

        self.body_params = self._estimate_body_params(interpolate_nans=True, filter_high_freq=True, show_plot=show_plot)
        self.limbs_params = self._estimate_all_limbs_params(interpolate_nans=True, filter_high_freq=True, show_plot=show_plot)
        if show_plot: plt.show()

        self._save_skeleton_params_in_csv(obj_name, save_path)
        
        if show_plot:
            body_params_1rst_est = copy.deepcopy(self.body_params)
            limbs_params_1rst_est = copy.deepcopy(self.limbs_params)

        start = time.time()
        print('> Fitting of skeleton model to {0} coordinates (with {1} method, and multiprocessing = {2})'.format(fit_method, opt_method, multiprocessing))
        if hasattr(self.animal_sets, 'use_hybrid_body') and self.animal_sets.use_hybrid_body:
            self.body_params, nb_visible_pts_body, rmse_body, nb_iterations_body = \
                self._optim_fit_all_hybrid_params(self.body_params, self.limbs_params, fit_method, opt_method,
                                                  interpolate_nans=False, filter_high_freq=True, 
                                                  show_plot=show_plot, multiprocessing=multiprocessing, 
                                                  image_names=image_names, image_paths=image_paths)
            print('>> fitting of hybrid body done! (time elapsed: {0:.4f} s)'.format(time.time() - start))

        else:
            self.body_params, nb_visible_pts_body, rmse_body, nb_iterations_body = \
                self._optim_fit_all_body_params(self.body_params, fit_method, opt_method,
                                                interpolate_nans=False, filter_high_freq=True,
                                                show_plot=show_plot, multiprocessing=multiprocessing,
                                                image_names=image_names, image_paths=image_paths)
            print('>> fitting of body done! (time elapsed: {0:.4f} s)'.format(time.time() - start))

        # self.skeleton2d, skeleton3d = self.unscramble_sides3d_dlc(self.skeleton3d, self.skeleton2d,
        #                                                           body_skeleton3d_init, body_params, show_plot=False

        start = time.time()
        self.limbs_params, nb_visible_pts_limbs, rmse_limbs, nb_iterations_limbs = \
            self._optim_fit_all_limbs_params(self.limbs_params, fit_method, opt_method,
                                             interpolate_nans=False, filter_high_freq=True,
                                             show_plot=show_plot, multiprocessing=multiprocessing,
                                             image_names=image_names, image_paths=image_paths)
        print('>> fitting of all limbs done! (time elapsed: {0:.4f} s)'.format(time.time() - start))

        #self.correct_body_wings_params(self.limbs[limb_name], self.limbs_sets[limb_name],
        #                               self.limbs_params[limb_name], angles_to_correct, image_paths)

        if show_plot:
            frames = list(range(0, len(self.body_params[self.body_sets.param_names[0]])))

            # Plot body params
            fig1, axs1 = plt.subplots(len(self.body_sets.param_names), 1)
            for i, label in enumerate(self.body_sets.param_names):
                axs1[i].plot(frames, body_params_1rst_est[label], '-', label='init')
                axs1[i].plot(frames, self.body_params[label], '-', label='optim')
                axs1[i].set_ylabel(label)
            axs1[i].set_xlabel('frames')
            fig1.suptitle('Body parameters in function of time')
            fig1.legend()

            # Plot all limbs params
            for limb_name in self.animal_sets.limb_module_names:
                fig2, axs2 = plt.subplots(len(self.limbs_sets[limb_name].param_names), 1)
                for i, label in enumerate(self.limbs_sets[limb_name].param_names):
                    for limb_name in self.animal_sets.limb_module_names:
                        for side in self._sides:
                            axs2[i].plot(frames, limbs_params_1rst_est[limb_name][side][label], '-', label='init_' + side)
                            axs2[i].plot(frames, self.limbs_params[limb_name][side][label], '-', label='optim_' + side)
                        axs2[i].set_ylabel(label)

                axs2[i].set_xlabel('frames')
                fig2.suptitle('Limbs parameters in function of time')
                fig2.legend()

            plt.show()

        self._save_skeleton_params_in_csv(obj_name, save_path)

        self._save_obj_metric_in_csv(nb_visible_pts_body, 'nb_visible_pts-body', obj_name, save_path)
        self._save_obj_metric_in_csv(nb_iterations_body, 'nb_iterations-body', obj_name, save_path)
        self._save_obj_metric_in_csv(rmse_body, 'rmse-body', obj_name, save_path)

        for limb_name in self.animal_sets.limb_module_names:
            self._save_obj_metric_in_csv(nb_visible_pts_limbs[limb_name], 'nb_visible_pts-' + limb_name + 's', obj_name, save_path)
            self._save_obj_metric_in_csv(nb_iterations_limbs[limb_name], 'nb_iterations-' + limb_name + 's', obj_name, save_path)
            self._save_obj_metric_in_csv(rmse_limbs[limb_name], 'rmse-' + limb_name + 's', obj_name, save_path)

    def _optim_fit_all_body_params(self, body_param_init, fit_method, opt_method,
                                   interpolate_nans=False, filter_high_freq=False, show_plot=False, multiprocessing=True,
                                   image_names: List[str] = None, image_paths: List[str] = None):

        fit_method = 'body_' + fit_method

        frames = self.skeleton2d['frames'] if '2d' in fit_method else self.skeleton3d['frames']
        # frames = [frame for i, frame in enumerate(frames) if i % 100 == 0.0]

        shape_frame = np.array(frames).shape
        nb_iterations, rmse, nb_visible_pts = np.zeros(shape_frame), np.ones(shape_frame) * np.nan, np.ones(shape_frame) * np.nan

        body_param_ests = {}
        for param_name in self.body_sets.param_names:
            body_param_ests[param_name] = np.ones(shape_frame) * self.body_sets.params_init[param_name]

        if show_plot and not multiprocessing:
            fig, axs = plt.subplots(1, self.nb_cam)

        if multiprocessing: args = [(frame,) for frame in frames]

        # for i, frame in enumerate(tqdm.tqdm(frames)):  # to print progress bar
        for i, frame in enumerate(frames):
            body_param_init_frame = {}
            for param_name in self.body_sets.param_names:
                body_param_init_frame[param_name] = body_param_init[param_name][i]

            param_names_to_optimize = list(set(self.body_sets.param_names) - set(self.body_sets.param_names_to_keep_cst))

            nb_points = len(self.body_sets.skeleton3d_init.keys())
            nb_visible_pts[i] = nb_points
            if '3d' in fit_method:
                body_skeleton3d = {}
                for body_label in self.body_sets.labels:
                    body_skeleton3d[body_label] = \
                        np.array([self.skeleton3d[body_label]['x'][i],
                                  self.skeleton3d[body_label]['y'][i],
                                  self.skeleton3d[body_label]['z'][i]])

                    if np.isnan(body_skeleton3d[body_label]).any(): nb_visible_pts[i] -= 1

            elif '2d' in fit_method:
                body_skeleton2d = {}
                for body_label in self.body_sets.labels:
                    body_skeleton2d[body_label] = {1: np.array([])}
                    for camn in range(1, self.nb_cam + 1):

                        if self.skeleton2d[body_label][camn]['likelihood'][i] > self.body_sets.threshold_likelihood:
                            body_skeleton2d[body_label][camn] = \
                                np.array([self.skeleton2d[body_label][camn]['x'][i],
                                          self.skeleton2d[body_label][camn]['y'][i],
                                          self.skeleton2d[body_label][camn]['likelihood'][i]])
                        else:
                            body_skeleton2d[body_label][camn] = \
                                np.array([np.nan, np.nan, self.skeleton2d[body_label][camn]['likelihood'][i]])

                            nb_visible_pts[i] -= 1 / self.nb_cam

            return_nan = nb_visible_pts[i] <= self.body_sets.threshold_nb_pts

            if not multiprocessing:
                if '3d' in fit_method:
                    body_param_ests_frame, rmse[i], nb_iterations[i] = \
                        optim_fit_body_params(self.animal_name, self.body_sets.skeleton3d_init, body_skeleton3d,
                                              body_param_init_frame, param_names_to_optimize,
                                              fit_method, opt_method, self.body_sets.bounds_init, return_nan)

                elif '2d' in fit_method:
                    body_param_ests_frame, rmse[i], nb_iterations[i] = \
                        optim_fit_body_params(self.animal_name, self.body_sets.skeleton3d_init, body_skeleton2d,
                                              body_param_init_frame, param_names_to_optimize,
                                              fit_method, opt_method, self.body_sets.bounds_init, return_nan,
                                              dlt_coefs=self.dlt_coefs)

                for param_name in self.body_sets.param_names:
                    body_param_ests[param_name][i] = body_param_ests_frame[param_name]

                if show_plot and (frame % 20 == 0.0 or frame == 1):

                    body_skeleton3d_init = copy.deepcopy(self.body_sets.skeleton3d_init)
                    body_skeleton3d_init = self.body.build_skeleton3d(body_skeleton3d_init, body_param_init_frame)
                    body_skeleton3d_init = self.body.rotate_skeleton3d(body_skeleton3d_init, body_param_init_frame)
                    body_skeleton3d_init = self.body.translate_skeleton3d(body_skeleton3d_init, body_param_init_frame)
                    body_skeleton2d_init = reproject_skeleton3d_to2d(body_skeleton3d_init, self.dlt_coefs)

                    body_skeleton3d_new = copy.deepcopy(self.body_sets.skeleton3d_init)
                    body_skeleton3d_new = self.body.build_skeleton3d(body_skeleton3d_new, body_param_ests_frame)
                    body_skeleton3d_new = self.body.rotate_skeleton3d(body_skeleton3d_new, body_param_ests_frame)
                    body_skeleton3d_new = self.body.translate_skeleton3d(body_skeleton3d_new, body_param_ests_frame)
                    body_skeleton2d_new = reproject_skeleton3d_to2d(body_skeleton3d_new, self.dlt_coefs)

                    # Compute reprojection of 3d reconstruction from dlc 2d points
                    body_skeleton3d_repro2d = {}
                    for label in self.body_sets.labels:
                        body_skeleton3d_repro2d[label] = {1: []}
                        for j, camn in enumerate(range(1, self.nb_cam + 1)):
                            uv = calib.find2d(1, self.dlt_coefs[j], np.array([[self.skeleton3d[label]['x'][i],
                                                                             self.skeleton3d[label]['y'][i], self.skeleton3d[label]['z'][i]]]))
                            body_skeleton3d_repro2d[label][camn] = np.array([uv[0][0], uv[0][1], 1.0])

                    if '3d' in fit_method:
                        body_skeleton2d = reproject_skeleton3d_to2d(body_skeleton3d, self.dlt_coefs)

                    # Plot 2d repojection of body skeleton
                    for j, camn in enumerate(range(1, self.nb_cam +1)):
                        center_of_mass_xy = np.nanmean(
                            [body_skeleton2d_new[label][camn] for label in body_skeleton2d_new.keys()])

                        xy_max = np.nanmax([body_skeleton2d_new[label][camn] for label in body_skeleton2d_new.keys()])
                        xy_min = np.nanmin([body_skeleton2d_new[label][camn] for label in body_skeleton2d_new.keys()])

                        x_width = xy_max[0] - xy_min[0]
                        y_width = xy_max[1] - xy_min[1]  # TODO: check that widths and center_of_mass_xy are correct

                        if np.isnan(center_of_mass_xy).any(): continue

                        axs[j].clear()
                        if len(image_names[camn]) > i and image_names[camn][i] is not None:
                            axs[j].imshow(Image.open(os.path.join(image_paths[camn], image_names[camn][i])))

                        for label in body_skeleton2d_new.keys():
                            if body_skeleton2d[label][camn][2] > self.body_sets.threshold_likelihood:
                                axs[j].scatter(body_skeleton2d[label][camn][0],
                                               body_skeleton2d[label][camn][1], marker='.', color='k')
                                axs[j].scatter(body_skeleton3d_repro2d[label][camn][0],
                                               body_skeleton3d_repro2d[label][camn][1], marker='o', facecolors='none', edgecolors='k')
                            axs[j].scatter(body_skeleton2d_init[label][camn][0],
                                           body_skeleton2d_init[label][camn][1], marker='o', facecolors='none', edgecolors='b')
                            axs[j].scatter(body_skeleton2d_new[label][camn][0],
                                           body_skeleton2d_new[label][camn][1], marker='+', color='r')

                        axs[j].set_xlim((center_of_mass_xy[0] - x_width/2, center_of_mass_xy[0] + x_width/2))
                        axs[j].set_ylim((center_of_mass_xy[1] + y_width/2, center_of_mass_xy[1] - y_width/2))

                    plt.pause(0.05)
                    # plt.show()

            else:
                if '3d' in fit_method:
                    args[i] = (self.animal_name, self.body_sets.skeleton3d_init, body_skeleton3d, body_param_init_frame,
                               param_names_to_optimize, fit_method, opt_method, self.body_sets.bounds_init, return_nan,)

                elif '2d' in fit_method:
                    args[i] = (self.animal_name, self.body_sets.skeleton3d_init, body_skeleton2d, body_param_init_frame,
                               param_names_to_optimize, fit_method, opt_method, self.body_sets.bounds_init, return_nan,)

        if multiprocessing:
            pool_body = Pool(self.nb_process)
            if '3d' in fit_method:
                results = pool_body.starmap(optim_fit_body_params, args)

            elif '2d' in fit_method:
                results = pool_body.starmap(partial(optim_fit_body_params, dlt_coefs=self.dlt_coefs), args)

            pool_body.close()
            for i, frame in enumerate(frames):
                body_param_ests_frame, rmse[i], nb_iterations[i] = results[i]
                for param_name in self.body_sets.param_names:
                    body_param_ests[param_name][i] = body_param_ests_frame[param_name]

        body_param_ests = filter_interp_params(self.cutoff_frequency, self.frame_rate, body_param_ests,
                                               self.body_sets.param_names, interpolate_nans, filter_high_freq, show_plot)

        return body_param_ests, nb_visible_pts, rmse, nb_iterations

    def _optim_fit_all_limbs_params(self, limbs_param_init, fit_method, opt_method,
                                    interpolate_nans=False, filter_high_freq=False, show_plot=False, multiprocessing=True,
                                    image_names: List[str] = None, image_paths: List[str] = None):

        limbs_params, nb_visible_pts_limbs, rmse_limbs, nb_iterations_limbs = {}, {}, {}, {}
        for limb_name in self.animal_sets.limb_module_names:
            limbs_params[limb_name], nb_visible_pts_limbs[limb_name], rmse_limbs[limb_name], nb_iterations_limbs[limb_name] = \
                self._optim_fit_all_limb_params(self.limbs[limb_name], limbs_param_init[limb_name], self.limbs_sets[limb_name], 
                                                fit_method, opt_method,
                                                interpolate_nans=interpolate_nans, filter_high_freq=filter_high_freq,
                                                show_plot=show_plot, multiprocessing=multiprocessing,
                                                image_names=image_names, image_paths=image_paths)

        return limbs_params, nb_visible_pts_limbs, rmse_limbs, nb_iterations_limbs

    def _optim_fit_all_limb_params(self, limb, limb_param_init, limb_sets, fit_method, opt_method,
                                   interpolate_nans=False, filter_high_freq=False, show_plot=False, multiprocessing=True,
                                   image_names: List[str] = None, image_paths: List[str] = None):

        fit_method = 'limb_' + fit_method

        for side in self._sides:
            for param_name in self.body_sets.param_names:
                limb_sets.params_init[side][param_name] = self.body_sets.params_init[param_name]

        frames = self.skeleton2d['frames'] if '2d' in fit_method else self.skeleton3d['frames']
        # frames = [frame for i, frame in enumerate(frames) if i % 100 == 0.0]

        if show_plot and not multiprocessing:
            fig, axs = plt.subplots(1, self.nb_cam)

        if multiprocessing: args = [(frame,) for frame in frames]

        shape_frame = np.array(frames).shape

        limb_params_ests, rmse, nb_iterations, nb_visible_pts = {}, {}, {}, {}
        for side in self._sides:
            limb_params_ests[side] = {}
            for label in limb_sets.param_names:
                limb_params_ests[side][label] = np.ones(shape_frame) * limb_sets.params_init[side][label]

            rmse[side] = np.ones(shape_frame) * np.nan
            nb_iterations[side] = np.zeros(shape_frame)
            nb_visible_pts[side] = np.ones(shape_frame) * np.nan

        #for i, frame in enumerate(tqdm.tqdm(frames)):   # to print progress bar
        for i, frame in enumerate(frames):
            limb_param_init_frame = {}
            for side in self._sides:
                limb_param_init_frame[side] = {}
                for param_name in limb_sets.param_names:
                    limb_param_init_frame[side][param_name] = limb_param_init[side][param_name][i]

            param_names_to_optimize = list(set(limb_sets.param_names) - set(limb_sets.param_names_to_keep_cst))
            param_names_to_optimize = list(set(param_names_to_optimize) - set(self.body_sets.param_names))

            nb_points = len(self.body_sets.skeleton3d_init.keys())
            for side in self._sides:
                nb_visible_pts[side][i] = nb_points

            if '3d' in fit_method:
                limb_skeleton3d = {}
                for side in self._sides:
        
                    limb_skeleton3d[side] = {}
                    for limb_label in limb_sets.labels:
                        label = side + '_' + limb_sets.name + '_' + limb_label
                        limb_skeleton3d[side][limb_label] = np.array([self.skeleton3d[label]['x'][i],
                                                                       self.skeleton3d[label]['y'][i],
                                                                       self.skeleton3d[label]['z'][i]])

                        if np.isnan(limb_skeleton3d[side][limb_label]).any():
                            nb_visible_pts[side][i] -= 1

            elif '2d' in fit_method:
                limb_skeleton2d = {}
                for side in self._sides:
                    limb_skeleton2d[side] = {}
                    for limb_label in limb_sets.labels:

                        limb_skeleton2d[side][limb_label] = {1: []}
                        for camn in range(1, self.nb_cam + 1):
                            label = side + '_' + limb_sets.name + '_' + limb_label

                            if self.skeleton2d[label][camn]['likelihood'][i] > limb_sets.threshold_likelihood:
                                limb_skeleton2d[side][limb_label][camn] = \
                                    np.array([self.skeleton2d[label][camn]['x'][i],
                                              self.skeleton2d[label][camn]['y'][i],
                                              self.skeleton2d[label][camn]['likelihood'][i]])
                            else:
                                limb_skeleton2d[side][limb_label][camn] = \
                                    np.array([np.nan, np.nan, self.skeleton2d[label][camn]['likelihood'][i]])

                                nb_visible_pts[side][i] -= 1 / self.nb_cam

            body_params_frame = {}
            for param_name in self.body_sets.param_names:
                body_params_frame[param_name] = self.body_params[param_name][i]

            body_skeleton3d = copy.deepcopy(self.body_sets.skeleton3d_init)
            body_skeleton3d = self.body.build_skeleton3d(body_skeleton3d, body_params_frame)
            body_skeleton3d = self.body.rotate_skeleton3d(body_skeleton3d, body_params_frame)
            body_skeleton3d = self.body.translate_skeleton3d(body_skeleton3d, body_params_frame)

            return_nan = [False, False]
            for j, side in enumerate(limb_sets.skeleton3d_init.keys()):
                [limb_param_init_frame[side]['x_hinge'],
                 limb_param_init_frame[side]['y_hinge'],
                 limb_param_init_frame[side]['z_hinge']] = \
                    body_skeleton3d[side + '_' + limb_sets.name + '_hinge']

                return_nan[j] = nb_visible_pts[side][i] <= limb_sets.threshold_nb_pts
                if return_nan[j]:
                    for param_name in limb_sets.param_names:
                        limb_params_ests[side][param_name][i] = np.nan
                    continue

                for param_name in body_params_frame.keys():
                    if param_name in limb_param_init_frame[side].keys():
                        limb_param_init_frame[side][param_name] = body_params_frame[param_name]

            if not multiprocessing:
                if '3d' in fit_method:
                    limb_params_est_frame, rmse_frame, nb_iterations_frame = \
                        optim_fit_limbs_params(self.animal_name, limb_sets.skeleton3d_init, limb_skeleton3d,
                                               limb_param_init_frame, param_names_to_optimize,
                                               fit_method, opt_method, limb_sets.bounds_init, return_nan)

                elif '2d' in fit_method:
                    limb_params_est_frame, rmse_frame, nb_iterations_frame = \
                        optim_fit_limbs_params(self.animal_name, limb_sets.skeleton3d_init, limb_skeleton2d,
                                               limb_param_init_frame, param_names_to_optimize,
                                               fit_method, opt_method, limb_sets.bounds_init, return_nan,
                                               dlt_coefs=self.dlt_coefs)

                for side in self._sides:
                    rmse[side][i] = rmse_frame[side]
                    nb_iterations[side][i] = nb_iterations_frame[side]
                    for param_name in limb_sets.param_names:
                        limb_params_ests[side][param_name][i] = limb_params_est_frame[side][param_name]

                if show_plot and frame % 20 == 0.0:

                    body_skeleton2d_new = reproject_skeleton3d_to2d(body_skeleton3d, self.dlt_coefs)

                    limbs_skeleton3d_new = copy.deepcopy(limb_sets.skeleton3d_init)
                    limbs_skeleton3d_init = copy.deepcopy(limb_sets.skeleton3d_init)
                    limbs_skeleton2d_new, limbs_skeleton2d_init, limbs_skeleton3d_repro2d = {}, {}, {}
                    if '3d' in fit_method: limb_skeleton2d = copy.deepcopy(limbs_skeleton2d_new)
                    for side in self._sides:
                        limbs_skeleton3d_init[side] =\
                            limb.build_skeleton3d(limbs_skeleton3d_init[side], limb_param_init_frame[side])
                        limbs_skeleton3d_init[side] = \
                            limb.rotate_and_translate_skeleton3d(limbs_skeleton3d_init[side], limb_param_init_frame[side], side)
                        limbs_skeleton2d_init[side] = \
                            reproject_skeleton3d_to2d(limbs_skeleton3d_init[side], self.dlt_coefs)

                        limbs_skeleton3d_new[side] = \
                            limb.build_skeleton3d(limbs_skeleton3d_new[side], limb_params_est_frame[side])
                        limbs_skeleton3d_new[side] = \
                            limb.rotate_and_translate_skeleton3d(limbs_skeleton3d_new[side], limb_params_est_frame[side], side)
                        limbs_skeleton2d_new[side] = \
                            reproject_skeleton3d_to2d(limbs_skeleton3d_new[side], self.dlt_coefs)

                        # Compute reprojection of 3d reconstruction from dlc 2d points
                        limbs_skeleton3d_repro2d[side] = {}
                        for limb_label in limb_sets.labels:
                            limbs_skeleton3d_repro2d[side][limb_label] = {1: []}
                            for j, camn in enumerate(range(1, self.nb_cam + 1)):
                                label = side + '_' + limb_sets.name + '_' + limb_label
                                uv = calib.find2d(1, self.dlt_coefs[j], np.array([[self.skeleton3d[label]['x'][i],
                                                                                   self.skeleton3d[label]['y'][i],
                                                                                   self.skeleton3d[label]['z'][i]]]))
                                limbs_skeleton3d_repro2d[side][limb_label][camn] = np.array([uv[0][0], uv[0][1], 1.0])

                        if '3d' in fit_method:
                            limb_skeleton2d[side] = reproject_skeleton3d_to2d(limb_skeleton3d[side], self.dlt_coefs)

                    for j, camn in enumerate(range(1, self.nb_cam + 1)):
                        center_of_mass_xy = np.nanmean(
                            [body_skeleton2d_new[label][camn] for label in body_skeleton2d_new.keys()])

                        xy_max = np.nanmax([body_skeleton2d_new[label][camn] for label in body_skeleton2d_new.keys()])
                        xy_min = np.nanmin([body_skeleton2d_new[label][camn] for label in body_skeleton2d_new.keys()])

                        x_width = xy_max[0] - xy_min[0]
                        y_width = xy_max[1] - xy_min[1] # TODO: check that widths and center_of_mass_xy are correct

                        if np.isnan(center_of_mass_xy).any(): continue

                        axs[j].clear()
                        if len(image_names[camn]) > i and image_names[camn][i] is not None:
                            axs[j].imshow(Image.open(os.path.join(image_paths[camn], image_names[camn][i])))

                        for side in self._sides:
                            for label in limbs_skeleton2d_new[side].keys():
                                if limb_skeleton2d[side][label][camn][2] > limb_sets.threshold_likelihood:
                                    axs[j].scatter(limb_skeleton2d[side][label][camn][0],
                                                   limb_skeleton2d[side][label][camn][1], marker='.', color='k')
                                    axs[j].scatter(limbs_skeleton3d_repro2d[side][label][camn][0],
                                                   limbs_skeleton3d_repro2d[side][label][camn][1],
                                                   marker='o', facecolors='none', edgecolors='k')
                                axs[j].scatter(limbs_skeleton2d_init[side][label][camn][0],
                                               limbs_skeleton2d_init[side][label][camn][1],
                                               marker='o', facecolors='none', edgecolors='b')
                                axs[j].scatter(limbs_skeleton2d_new[side][label][camn][0],
                                               limbs_skeleton2d_new[side][label][camn][1], marker='+', color='r')

                        axs[j].set_xlim((center_of_mass_xy[0] - x_width/2, center_of_mass_xy[0] + x_width/2))
                        axs[j].set_ylim((center_of_mass_xy[1] + y_width/2, center_of_mass_xy[1] - y_width/2))

                    plt.pause(0.05)
                    #plt.show()

            else:
                if '3d' in fit_method:
                    args[i] = (self.animal_name, limb_sets.skeleton3d_init, limb_skeleton3d, limb_param_init_frame,
                               param_names_to_optimize, fit_method, opt_method, limb_sets.bounds_init, return_nan)

                elif '2d' in fit_method:
                    args[i] = (self.animal_name, limb_sets.skeleton3d_init, limb_skeleton2d, limb_param_init_frame,
                               param_names_to_optimize, fit_method, opt_method, limb_sets.bounds_init, return_nan)

        if multiprocessing:
            pool_limbs = Pool(self.nb_process)
            if '3d' in fit_method:
                results = pool_limbs.starmap(optim_fit_limbs_params, args)

            elif '2d' in fit_method:
                results = pool_limbs.starmap(partial(optim_fit_limbs_params, dlt_coefs=self.dlt_coefs), args)

            pool_limbs.close()
            for i, frame in enumerate(frames):
                limb_params_est_frame, rmse_frame, nb_iterations_frame = results[i]
                for side in self._sides:
                    rmse[side][i] = rmse_frame[side]
                    nb_iterations[side][i] = nb_iterations_frame[side]
                    for param_name in limb_sets.param_names:
                        limb_params_ests[side][param_name][i] = limb_params_est_frame[side][param_name]

        for side in self._sides:
            limb_params_ests[side] = \
                filter_interp_params(self.cutoff_frequency, self.frame_rate, limb_params_ests[side],
                                     ['x_hinge', 'y_hinge', 'z_hinge'], False, filter_high_freq, show_plot)
            limb_params_ests[side] = \
                filter_interp_params(self.cutoff_frequency, self.frame_rate, limb_params_ests[side],
                                     limb_sets.param_names, interpolate_nans, False, show_plot)

        return limb_params_ests, nb_visible_pts, rmse, nb_iterations

    def _optim_fit_all_hybrid_params(self, hybrid_body_param_init, limbs_param_init, fit_method, opt_method,
                                     interpolate_nans=False, filter_high_freq=False, first_optim_limbs=False,
                                     show_plot=False, multiprocessing=True,
                                     image_names: List[str] = None, image_paths: List[str] = None):

        hybrid_body_sets = copy.deepcopy(self.body_sets)
        if '2d' in fit_method: hybrid_body_sets.skeleton2d = copy.deepcopy(self.skeleton2d)
        else: hybrid_body_sets.skeleton3d = copy.deepcopy(self.skeleton3d)
        
        if first_optim_limbs:
            for limb_name in self.animal_sets.limb_module_names:
                start = time.time()

                # First optim on limbs to estimate limb tips and hinges positions + deviation_a
                limbs_sets = self.limbs_sets[limb_name]
                limbs = self.limbs[limb_name]

                # TODO remove following line
                #limbs_sets.param_names = ['stroke_a', 'deviation_a', 'rotation_a', 'x_hinge', 'y_hinge', 'z_hinge']
                limbs_params, _, _ = \
                    self._optim_fit_all_limbs_params(limbs_param_init, fit_method, opt_method,
                                                     interpolate_nans=False, filter_high_freq=True,
                                                     show_plot=show_plot, image_names=image_names, image_paths=image_paths)

                frames = self.skeleton2d['frames'] if '2d' in fit_method else self.skeleton3d['frames']
                for i, frame in enumerate(frames):

                    limbs_params_frame = {}
                    for side in limbs_sets.sides:
                        limbs_params_frame[side] = {}
                        for param_name in limbs_sets.param_names:
                            limbs_params_frame[side][param_name] = limbs_params[side][param_name][i]

                    limbs_skeleton3d_new = copy.deepcopy(limbs_sets.skeleton3d_init)
                    limbs_skeleton2d_new = {}
                    for side in limbs_sets.sides:
                        limbs_skeleton3d_new[side] = limbs.build_skeleton3d(limbs_skeleton3d_new[side], limbs_params_frame[side])
                        limbs_skeleton3d_new[side] = \
                            limbs.rotate_and_translate_skeleton3d(limbs_skeleton3d_new[side], limbs_params_frame[side], side)

                        if '2d' in fit_method:
                            limbs_skeleton2d_new[side] = reproject_skeleton3d_to2d(limbs_skeleton3d_new[side], self.dlt_coefs)

                        for limb_label in ['tip', 'hinge']:
                            label = side + '_' + limbs_sets.name + '_' + limb_label

                            if '2d' in fit_method:
                                for camn in range(1, self.nb_cam + 1):
                                    hybrid_body_sets.skeleton2d[label][camn]['x'][i] = limbs_skeleton2d_new[side][limb_label][camn][0]
                                    hybrid_body_sets.skeleton2d[label][camn]['y'][i] = limbs_skeleton2d_new[side][limb_label][camn][1]
                                    hybrid_body_sets.skeleton2d[label][camn]['likelihood'][i] = 1.0

                            else:
                                hybrid_body_sets.skeleton3d[label]['x'][i] = limbs_skeleton3d_new[side][limb_label][0]
                                hybrid_body_sets.skeleton3d[label]['y'][i] = limbs_skeleton3d_new[side][limb_label][1]
                                hybrid_body_sets.skeleton3d[label]['z'][i] = limbs_skeleton3d_new[side][limb_label][2]

                print('>> first optim limb done! (time elapsed: {0:.4f} s)'.format(time.time() - start))

        frames = hybrid_body_sets.skeleton2d['frames'] if '2d' in fit_method else hybrid_body_sets.skeleton3d['frames']
        # frames = [frame for i, frame in enumerate(frames) if i % 100 == 0.0]

        # Low pass filter the position of limbs tips and hinges + deviation_a and span
        for param_name in ['deviation_a', 'span']:
            hybrid_body_sets.params_init['wbaverage_' + param_name] = \
                np.mean([np.transpose(
                    filter_interp(limbs_sets.params_init['right'][param_name], self.frame_rate, self.cutoff_frequency)),
                         np.transpose(filter_interp(limbs_sets.params_init['left'][param_name], self.frame_rate, self.cutoff_frequency))], axis=0)

        for side in limbs_sets.sides:
            for label in ['tip', 'hinge']:
                limb_label = side + '_' + limbs_sets.name + '_' + label
                if '2d' in fit_method:
                    for camn in range(1, self.nb_cam + 1):
                        hybrid_body_sets.skeleton2d[limb_label][camn]['x'] = \
                            filter_interp(hybrid_body_sets.skeleton2d[limb_label][camn]['x'], self.frame_rate, self.cutoff_frequency)
                        hybrid_body_sets.skeleton2d[limb_label][camn]['y'] = \
                            filter_interp(hybrid_body_sets.skeleton2d[limb_label][camn]['y'], self.frame_rate, self.cutoff_frequency)
                        hybrid_body_sets.skeleton2d[limb_label][camn]['likelihood'] = \
                            np.ones(np.size(hybrid_body_sets.skeleton2d[limb_label][camn]['x'], self.frame_rate, self.cutoff_frequency))

                else:
                    hybrid_body_sets.skeleton3d[limb_label]['x'] = \
                        filter_interp(hybrid_body_sets.skeleton3d[limb_label]['x'], self.frame_rate, self.cutoff_frequency)
                    hybrid_body_sets.skeleton3d[limb_label]['y'] = \
                        filter_interp(hybrid_body_sets.skeleton3d[limb_label]['y'], self.frame_rate, self.cutoff_frequency)
                    hybrid_body_sets.skeleton3d[limb_label]['z'] = \
                        filter_interp(hybrid_body_sets.skeleton3d[limb_label]['z'], self.frame_rate, self.cutoff_frequency)

        shape_frame = np.array(frames).shape
        nb_iterations = np.zeros(shape_frame)
        nb_visible_pts, rmse = np.ones(shape_frame) * np.nan, np.ones(shape_frame) * np.nan

        hybrid_param_ests = {}
        for param_name in hybrid_body_sets.param_names:
            hybrid_param_ests[param_name] = np.ones(shape_frame) * hybrid_body_sets.params_init[param_name]

        if show_plot and not multiprocessing:
            fig, axs = plt.subplots(1, self.nb_cam)

        if multiprocessing: args = [(frame,) for frame in frames]

        # Do not plot anything if nb of images different from nb of frames
        #if any([len(frames) != image_names[camn] for camn in range(1, self.nb_cam +1)]): show_plot = False  # TODO remove?

        #for i, frame in enumerate(tqdm.tqdm(frames)):  # to print progress bar
        for i, frame in enumerate(frames):
            hybrid_body_param_init_frame = {}
            for param_name in self.body_sets.param_names:
                hybrid_body_param_init_frame[param_name] = hybrid_body_param_init[param_name][i]

            param_names_to_optimize = list(set(hybrid_body_sets.param_names) - set(hybrid_body_sets.param_names_to_keep_cst))

            nb_points = len(hybrid_body_sets.labels)
            nb_visible_pts[i] = nb_points
            if '3d' in fit_method:
                hybrid_skeleton3d = {}
                for hybrid_label in hybrid_body_sets.labels:
                    hybrid_skeleton3d[hybrid_label] = \
                        np.array([hybrid_body_sets.skeleton3d[hybrid_label]['x'][i],
                                  hybrid_body_sets.skeleton3d[hybrid_label]['y'][i],
                                  hybrid_body_sets.skeleton3d[hybrid_label]['z'][i]])

                    if np.isnan(hybrid_skeleton3d[hybrid_label]).any(): nb_visible_pts[i] -= 1

            elif '2d' in fit_method:
                hybrid_skeleton2d = {}
                for hybrid_label in hybrid_body_sets.labels:
                    hybrid_skeleton2d[hybrid_label] = {1: np.array([])}
                    for camn in range(1, self.nb_cam + 1):

                        if hybrid_body_sets.skeleton2d[hybrid_label][camn]['likelihood'][i] > hybrid_body_sets.threshold_likelihood:
                            hybrid_skeleton2d[hybrid_label][camn] = \
                                np.array([hybrid_body_sets.skeleton2d[hybrid_label][camn]['x'][i],
                                          hybrid_body_sets.skeleton2d[hybrid_label][camn]['y'][i],
                                          hybrid_body_sets.skeleton2d[hybrid_label][camn]['likelihood'][i]])
                        else:
                            hybrid_skeleton2d[hybrid_label][camn] = \
                                np.array([np.nan, np.nan, hybrid_body_sets.skeleton2d[hybrid_label][camn]['likelihood'][i]])

                            nb_visible_pts[i] -= 1 / self.nb_cam

            return_nan = nb_visible_pts[i] <= self.body_sets.threshold_nb_pts - 2 # to compensate for the extra limbs tips

            if not multiprocessing:
                if '3d' in fit_method:
                    hybrid_param_ests_frame, rmse[i], nb_iterations[i] = \
                        optim_fit_body_params(self.animal_name, hybrid_body_sets.skeleton3d_init, hybrid_skeleton3d,
                                              hybrid_body_param_init_frame, param_names_to_optimize,
                                              fit_method, opt_method, hybrid_body_sets.bounds_init, return_nan)

                elif '2d' in fit_method:
                    hybrid_param_ests_frame, rmse[i], nb_iterations[i] = \
                        optim_fit_body_params(self.animal_name, hybrid_body_sets.skeleton3d_init, hybrid_skeleton2d,
                                              hybrid_body_param_init_frame, param_names_to_optimize, fit_method,
                                              opt_method, hybrid_body_sets.bounds_init, return_nan, dlt_coefs=self.dlt_coefs)

                for param_name in hybrid_body_sets.param_names:
                    hybrid_param_ests[param_name][i] = hybrid_param_ests_frame[param_name]

                if show_plot and (frame % 20 == 0.0 or frame == 1):

                    hybrid_skeleton3d_init = copy.deepcopy(hybrid_body_sets.skeleton3d_init)
                    hybrid_skeleton3d_init = self.body.build_skeleton3d(hybrid_skeleton3d_init, hybrid_body_param_init_frame)
                    hybrid_skeleton3d_init = self.body.rotate_skeleton3d(hybrid_skeleton3d_init, hybrid_body_param_init_frame)
                    hybrid_skeleton3d_init = self.body.translate_skeleton3d(hybrid_skeleton3d_init, hybrid_body_param_init_frame)
                    hybrid_skeleton2d_init = reproject_skeleton3d_to2d(hybrid_skeleton3d_init, self.dlt_coefs)

                    hybrid_skeleton3d_new = copy.deepcopy(hybrid_body_sets.skeleton3d_init)
                    hybrid_skeleton3d_new = self.body.build_skeleton3d(hybrid_skeleton3d_new, hybrid_param_ests_frame)
                    hybrid_skeleton3d_new = self.body.rotate_skeleton3d(hybrid_skeleton3d_new, hybrid_param_ests_frame)
                    hybrid_skeleton3d_new = self.body.translate_skeleton3d(hybrid_skeleton3d_new, hybrid_param_ests_frame)
                    hybrid_skeleton2d_new = reproject_skeleton3d_to2d(hybrid_skeleton3d_new, self.dlt_coefs)

                    # Compute reprojection of 3d reconstruction from dlc 2d points
                    hybrid_skeleton3d_repro2d = {}
                    for label in hybrid_body_sets.labels:
                        hybrid_skeleton3d_repro2d[label] = {1: []}
                        for j, camn in enumerate(range(1, self.nb_cam + 1)):
                            uv = calib.find2d(1, self.dlt_coefs[j], np.array([[hybrid_body_sets.skeleton3d[label]['x'][i],
                                                                               hybrid_body_sets.skeleton3d[label]['y'][i],
                                                                               hybrid_body_sets.skeleton3d[label]['z'][i]]]))
                            hybrid_skeleton3d_repro2d[label][camn] = np.array([uv[0][0], uv[0][1], 1.0])

                    if '3d' in fit_method:
                        hybrid_skeleton2d = reproject_skeleton3d_to2d(hybrid_skeleton3d, self.dlt_coefs)

                    # Plot 2d repojection of hybrid skeleton
                    for j, camn in enumerate(range(1, self.nb_cam +1)):
                        center_of_mass_xy = np.nanmean(
                            [hybrid_skeleton2d_new[label][camn] for label in hybrid_skeleton2d_new.keys()])

                        xy_max = np.nanmax([hybrid_skeleton2d_new[label][camn] for label in hybrid_skeleton2d_new.keys()])
                        xy_min = np.nanmin([hybrid_skeleton2d_new[label][camn] for label in hybrid_skeleton2d_new.keys()])

                        x_width = xy_max[0] - xy_min[0]
                        y_width = xy_max[1] - xy_min[1]  # TODO: check that widths and center_of_mass_xy are correct

                        if np.isnan(center_of_mass_xy).any(): continue

                        axs[j].clear()
                        if len(image_names[camn]) > i and image_names[camn][i] is not None:
                            axs[j].imshow(Image.open(os.path.join(image_paths[camn], image_names[camn][i])))

                        for label in hybrid_skeleton2d_new.keys():
                            if hybrid_skeleton2d[label][camn][2] > hybrid_body_sets.threshold_likelihood:
                                axs[j].scatter(hybrid_skeleton2d[label][camn][0],
                                               hybrid_skeleton2d[label][camn][1], marker='.', color='k')
                                axs[j].scatter(hybrid_skeleton3d_repro2d[label][camn][0],
                                               hybrid_skeleton3d_repro2d[label][camn][1],
                                               marker='o', facecolors='none', edgecolors='k')
                            axs[j].scatter(hybrid_skeleton2d_init[label][camn][0],
                                           hybrid_skeleton2d_init[label][camn][1],
                                           marker='o', facecolors='none', edgecolors='b')
                            axs[j].scatter(hybrid_skeleton2d_new[label][camn][0],
                                           hybrid_skeleton2d_new[label][camn][1], marker='+', color='r')

                        axs[j].set_xlim((center_of_mass_xy[0] - x_width/2, center_of_mass_xy[0] + x_width/2))
                        axs[j].set_ylim((center_of_mass_xy[1] + y_width/2, center_of_mass_xy[1] - y_width/2))

                    plt.pause(0.05)
                    # plt.show()

                    # param_to_print = ['pitch_a', 'roll_a', 'yaw_a', 'ratio_hybrid']
                    # for label in param_to_print:
                    #     print('{0}: init: {1} and est: {2}'.format(label, hybrid_body_param_init_frame[label], hybrid_param_ests_frame[label]))

            else:
                if '3d' in fit_method:
                    args[i] = (self.animal_name, hybrid_body_sets.skeleton3d_init, hybrid_skeleton3d, hybrid_body_param_init_frame,
                               param_names_to_optimize, fit_method, opt_method, hybrid_body_sets.bounds_init, return_nan,)

                elif '2d' in fit_method:
                    args[i] = (self.animal_name, hybrid_body_sets.skeleton3d_init, hybrid_skeleton2d, hybrid_body_param_init_frame,
                               param_names_to_optimize, fit_method, opt_method, hybrid_body_sets.bounds_init, return_nan,)

        if multiprocessing:
            pool_hybrid = Pool(self.nb_process)
            if '3d' in fit_method:
                results = pool_hybrid.starmap(optim_fit_body_params, args)

            elif '2d' in fit_method:
                results = pool_hybrid.starmap(partial(optim_fit_body_params, dlt_coefs=self.dlt_coefs), args)

            pool_hybrid.close()
            for i, frame in enumerate(frames):
                hybrid_param_ests_frame, rmse[i], nb_iterations[i] = results[i]
                for label in hybrid_body_sets.labels:
                    hybrid_param_ests[label][i] = hybrid_param_ests_frame[label]

        hybrid_param_ests = filter_interp_params(self.cutoff_frequency, self.frame_rate, hybrid_param_ests,
                                                 hybrid_body_sets.param_names, interpolate_nans, filter_high_freq, show_plot)

        return hybrid_param_ests, nb_visible_pts, rmse, nb_iterations

    # TODO delete following lines
    # def correct_body_wings_params(self, limbs, limbs_sets, limbs_params, angle_names_to_correct, image_paths, show_plot=False):
    #     # Will correct asymetrical wing angles (mean value should be the same on both side)
    #     body_params_corrected = copy.deepcopy(self.body_params)
    #     wings_params_corrected = copy.deepcopy(self.limbs_params)
    #
    #
    #     wings_params_mean = {} # Find angles error (btw stroke_a and deviation_a of both sides)
    #     for side in limbs_sets.sides:
    #         wings_params_mean[side] = \
    #             filter_interp_params(self.cutoff_frequency, self.frame_rate, limbs_params[side],
    #                                  angle_names_to_correct, False, True, show_plot)
    #
    #     if 'stroke_a' in angle_names_to_correct:
    #         diff_stroke_a = wings_params_mean['right']['stroke_a'] - wings_params_mean['left']['stroke_a']
    #         # body_params_corrected['yaw_a'] -= diff_stroke_a / 2
    #         # wings_params_corrected['right']['stroke_a'] -= diff_stroke_a / 2
    #         # wings_params_corrected[limb_name]['left']['stroke_a'] += diff_stroke_a / 2
    #         # wings_params_corrected[limb_name]['right']['yaw_a'] -= diff_stroke_a / 2
    #         # wings_params_corrected[limb_name]['left']['yaw_a'] -= diff_stroke_a / 2
    #
    #     if 'deviation_a' in angle_names_to_correct:
    #         diff_deviation_a = wings_params_mean['right']['deviation_a'] - wings_params_mean['left']['deviation_a']
    #         # body_params_corrected['roll_a'] -= diff_deviation_a / 2
    #         # wings_params_corrected['right']['deviation_a'] -= diff_deviation_a / 2
    #         # wings_params_corrected['left']['deviation_a'] += diff_deviation_a / 2
    #         # wings_params_corrected['right']['roll_a'] -= diff_deviation_a / 2
    #         # wings_params_corrected['left']['roll_a'] -= diff_deviation_a / 2
    #
    #     if 'rotation_a' in angle_names_to_correct:
    #         raise ValueError('There is not reason for rotation_a to be corrected!'
    #                          'Any asymmetry btw sides cannot results from estimation error of the pitch angle')
    #
    #     if show_plot:
    #         # Plot wing tips pos
    #         frames = self.skeleton2d['frames']
    #         wing_tips, wing_hinges = {}, {}
    #         for side in limbs_sets.sides:
    #             wing_tips[side], wing_hinges[side] = {}, {}
    #             for coord in ['x', 'y', 'z']:
    #                 wing_tips[side][coord], wing_hinges[side][coord] = np.ones(np.size(frames)) * np.nan, np.ones(
    #                     np.size(frames)) * np.nan
    #
    #         tip_hinge_a = np.ones(np.size(frames)) * np.nan
    #         tip_torso_a = np.ones(np.size(frames)) * np.nan
    #         for i, frame in enumerate(frames):
    #             body_params_frame, wings_params_frame = {}, {}
    #             for label in body_sets.labels:
    #                 body_params_frame[label] = self.body_params[label][i]
    #
    #             body_skeleton3d_est = copy.deepcopy(self.body_sets.skeleton3d_init)
    #             body_skeleton3d_est = self.body.build_skeleton3d(body_skeleton3d_est, body_params_frame)
    #             body_skeleton3d_est = self.body.rotate_skeleton3d(body_skeleton3d_est, body_params_frame)
    #             body_skeleton3d_est = self.body.translate_skeleton3d(body_skeleton3d_est, body_params_frame)
    #
    #             wings_skeleton3d_mean = copy.deepcopy(limbs_sets.skeleton3d_init)
    #             for side in limbs_sets.sides:
    #                 wings_params_frame[side] = {}
    #                 for param_name in limbs_sets.param_names:
    #                     wings_params_frame[side][param_name] = wings_params_mean[side][param_name][i]
    #
    #                 wings_skeleton3d_mean[side] = limbs.build_skeleton3d(wings_skeleton3d_mean[side],
    #                                                                      wings_params_frame[side])
    #                 wings_skeleton3d_mean[side] = limbs.rotate_and_translate_skeleton3d(wings_skeleton3d_mean[side],
    #                                                                                     wings_params_frame[side],
    #                                                                                     side)
    #
    #                 wing_tips[side]['x'][i], wing_tips[side]['y'][i], wing_tips[side]['z'][i] = \
    #                     wings_skeleton3d_mean[side]['tip'][0], wings_skeleton3d_mean[side]['tip'][1], \
    #                     wings_skeleton3d_mean[side]['tip'][2]
    #                 wing_hinges[side]['x'][i], wing_hinges[side]['y'][i], wing_hinges[side]['z'][i] = \
    #                     wings_skeleton3d_mean[side]['hinge'][0], wings_skeleton3d_mean[side]['hinge'][1], \
    #                     wings_skeleton3d_mean[side]['hinge'][2]
    #
    #                 tip_vect = np.array([wing_tips['right']['x'][i], wing_tips['right']['y'][i], wing_tips['right']['z'][i]]) \
    #                            - np.array([wing_tips['left']['x'][i], wing_tips['left']['y'][i], wing_tips['left']['z'][i]])
    #                 hinge_vect = np.array([wing_hinges['right']['x'][i], wing_hinges['right']['y'][i], wing_hinges['right']['z'][i]]) \
    #                              - np.array([wing_hinges['left']['x'][i], wing_hinges['left']['y'][i], wing_hinges['left']['z'][i]])
    #
    #                 torso_vect = body_skeleton3d_est['head_torso_joint'] - body_skeleton3d_est['torso_abdomen_joint']
    #
    #                 tip_hinge_a[i] = get_rotation_angle_btw_vectors(tip_vect, hinge_vect)
    #                 tip_torso_a[i] = get_rotation_angle_btw_vectors(tip_vect, torso_vect)
    #
    #         fig_test, axs_test = plt.subplots(1, 3)
    #         fig_test.suptitle('Average position of wing tip over time')
    #         for side in limbs_sets.sides:
    #             axs_test[0].plot(wing_tips[side]['x'] - self.body_params['x_com'], label=side + '_tip')
    #             axs_test[1].plot(wing_tips[side]['y'] - self.body_params['y_com'], label=side + '_tip')
    #             axs_test[2].plot(wing_tips[side]['z'] - self.body_params['z_com'], label=side + '_tip')
    #
    #             axs_test[0].plot(wing_hinges[side]['x'] - self.body_params['x_com'], label=side + '_hinge')
    #             axs_test[1].plot(wing_hinges[side]['y'] - self.body_params['y_com'], label=side + '_hinge')
    #             axs_test[2].plot(wing_hinges[side]['z'] - self.body_params['z_com'], label=side + '_hinge')
    #
    #             axs_test[0].set_ylabel('x [m]'), axs_test[1].set_ylabel('y [m]'), axs_test[2].set_ylabel('z [m]')
    #
    #         axs_test[2].set_xlabel('frames')
    #         fig_test.legend()
    #
    #         fig_test2, axs_test2 = plt.subplots(1, 2)
    #         fig_test2.suptitle('Average angle btw  wing tip vector and torso vector over time')
    #         axs_test2[0].plot(tip_hinge_a, label='tip_hinge_a')
    #         axs_test2[0].plot(diff_stroke_a, label='diff_stroke_a')
    #         axs_test2[0].plot(diff_deviation_a, label='diff_deviation_a')
    #         # axs_test2[0].plot(diff_rotation_a, label='diff_rotation_a')
    #         axs_test2[1].plot(tip_torso_a, label='tip_torso_a')
    #         axs_test2[0].set_ylabel('angle (deg)')
    #
    #         fig_test2.legend()
    #
    #         skel_fit.plot_skeleton2d(image_names, image_paths, modulo=step_frame, save_images=save_images,
    #                                  save_paths=save_paths, obj_name=obj_name)
    #
    #     return body_params_corrected, wings_params_corrected

    def plot_skeleton_over_images(self, image_names, image_paths, modulo=1, show_plot=False, save_images=False, save_paths=None,
                                  obj_name=None, show_body=True, show_limbs=True):
        """

        Plot the 3d skeleton re-projected into the various camera views (optional: can save the images).

        Args:
            image_names:
            image_paths:
            modulo:
            show_plot:
            save_images:
            save_paths:
            obj_name:
            show_body:
            show_limbs:

        """

        assert self.nb_cam == len(image_paths.keys()) == len(image_names.keys())

        if not show_plot and not save_images or not show_body and not show_limbs: return

        if show_plot:
            fig, axs = plt.subplots(1, self.nb_cam, figsize=(15, 20))
            plt.axis('off')

        frames = self.skeleton2d['frames']

        # Do not plot anything if nb of images different from nb of frames
        if any([len(frames) != image_names[camn] for camn in range(1, self.nb_cam +1)]): show_plot = False

        for i, frame in enumerate(frames):
            if not frame % modulo == 0.0 and not frame == 1: continue

            body_skeleton2d_dlc = {}
            for body_label in self.skeleton2d['label_names']:
                body_skeleton2d_dlc[body_label] = {1: np.array([])}
                for camn in range(1, self.nb_cam + 1):
                    if self.skeleton2d[body_label][camn]['likelihood'][i] > self.body_sets.threshold_likelihood:
                        body_skeleton2d_dlc[body_label][camn] = \
                            np.array([self.skeleton2d[body_label][camn]['x'][i],
                                      self.skeleton2d[body_label][camn]['y'][i],
                                      self.skeleton2d[body_label][camn]['likelihood'][i]])
                    else:
                        body_skeleton2d_dlc[body_label][camn] = \
                            np.array([np.nan, np.nan, self.skeleton2d[body_label][camn]['likelihood'][i]])

            if show_body:
                body_params_frame = {}
                for param_name in self.body_sets.param_names:
                    body_params_frame[param_name] = self.body_params[param_name][i]

                body_skeleton3d = copy.deepcopy(self.body_sets.skeleton3d_init)
                body_skeleton3d = self.body.build_skeleton3d(body_skeleton3d, body_params_frame)
                body_skeleton3d = self.body.rotate_skeleton3d(body_skeleton3d, body_params_frame)
                body_skeleton3d = self.body.translate_skeleton3d(body_skeleton3d, body_params_frame)
                body_skeleton2d = reproject_skeleton3d_to2d(body_skeleton3d, self.dlt_coefs)

            if show_limbs:
                for limb_name in self.animal_sets.limb_module_names:
                    limbs_skeleton3d = copy.deepcopy(self.limbs_sets[limb_name].skeleton3d_init)

                    limbs_params_frame, limbs_skeleton2d, limbs_skeleton2d_dlc = {}, {}, {}
                    for side in self.limbs_sets[limb_name].sides:
                        limbs_params_frame[side] = {}
                        for param_name in self.limbs_sets[limb_name].param_names:
                            limbs_params_frame[side][param_name] = self.limbs_params[limb_name][side][param_name][i]

                        limbs_skeleton3d[side] = self.limbs[limb_name].build_skeleton3d(limbs_skeleton3d[side], limbs_params_frame[side])
                        limbs_skeleton3d[side] = self.limbs[limb_name].rotate_and_translate_skeleton3d(limbs_skeleton3d[side], limbs_params_frame[side], side)
                        limbs_skeleton2d[side] = reproject_skeleton3d_to2d(limbs_skeleton3d[side], self.dlt_coefs)

                        limbs_skeleton2d_dlc[side] = {}
                        for limb_label in self.limbs_sets[limb_name].labels:
                            limbs_skeleton2d_dlc[side][limb_label] = {1: []}
                            for camn in range(1, self.nb_cam + 1):
                                label = side + '_' + self.limbs_sets[limb_name].name + '_' + limb_label
                                if self.skeleton2d[label][camn]['likelihood'][i] > self.limbs_sets[limb_name].threshold_likelihood:
                                    limbs_skeleton2d_dlc[side][limb_label][camn] = \
                                        np.array([self.skeleton2d[label][camn]['x'][i],
                                                  self.skeleton2d[label][camn]['y'][i],
                                                  self.skeleton2d[label][camn]['likelihood'][i]])
                                else:
                                    limbs_skeleton2d_dlc[side][limb_label][camn] = \
                                        np.array([np.nan, np.nan, self.skeleton2d[label][camn]['likelihood'][i]])

            for j, camn in enumerate(range(1, self.nb_cam + 1)):
                center_of_mass_xy = np.nanmean([body_skeleton2d[label][camn] for label in body_skeleton2d.keys()])

                xy_max = np.nanmax([body_skeleton2d[label][camn] for label in body_skeleton2d.keys()])
                xy_min = np.nanmin([body_skeleton2d[label][camn] for label in body_skeleton2d.keys()])

                x_width = xy_max[0] - xy_min[0]
                y_width = xy_max[1] - xy_min[1]  # TODO: check that widths and center_of_mass_xy are correct

                if np.isnan(center_of_mass_xy).any(): continue
                if image_names[camn] is None: continue

                if show_plot or save_images:
                    img = read_image(os.path.join(image_paths[camn], image_names[camn][i]))

                if show_plot:
                    axs[j].clear()
                    axs[j].imshow(img)

                    if show_body:
                        for label in body_skeleton2d.keys():
                            if body_skeleton2d_dlc[label][camn][2] > self.body_sets.threshold_likelihood:
                                axs[j].scatter(body_skeleton2d_dlc[label][camn][0],
                                               body_skeleton2d_dlc[label][camn][1], marker='.', color='k')
                            axs[j].scatter(body_skeleton2d[label][camn][0],
                                           body_skeleton2d[label][camn][1], marker='+', color='r')

                    if show_limbs:
                        for side in self.limbs_sets[limb_name].sides:
                            for label in self.limbs_sets[limb_name].labels:
                                if limbs_skeleton2d_dlc[side][label][camn][2] > self.limbs_sets[limb_name].threshold_likelihood:
                                    axs[j].scatter(limbs_skeleton2d_dlc[side][label][camn][0],
                                                   limbs_skeleton2d_dlc[side][label][camn][1], marker='.', color='k')
                                axs[j].scatter(limbs_skeleton2d[side][label][camn][0],
                                               limbs_skeleton2d[side][label][camn][1], marker='+', color='r')

                    axs[j].set_xlim((center_of_mass_xy[0] - x_width/2, center_of_mass_xy[0] + x_width/2))
                    axs[j].set_ylim((center_of_mass_xy[1] + y_width/2, center_of_mass_xy[1] - y_width/2))

                    if camn == self.nb_cam:
                        plt.pause(0.05)
                        # fig.savefig(os.path.join(save_paths[main_camn], image_names[camn][i]),
                        #             bbox_inches='tight', transparent=True, pad_inches=0)

                if save_images:
                    img = img.convert('RGB')
                    draw = ImageDraw.Draw(img)
                    if show_body:
                        body_line = []
                        for label in body_skeleton2d.keys():
                            if body_skeleton2d_dlc[label][camn][2] > self.body_sets.threshold_likelihood:
                                draw.point((body_skeleton2d_dlc[label][camn][0], body_skeleton2d_dlc[label][camn][1]), fill='blue')
                            draw.point((body_skeleton2d[label][camn][0], body_skeleton2d[label][camn][1]), fill='red')

                            if not 'hinge' in label:
                                body_line.append((body_skeleton2d[label][camn][0], body_skeleton2d[label][camn][1]))
                        draw.line(body_line, fill='red', width=2)

                    if show_limbs:
                        for side in self.limbs_sets[limb_name].sides:
                            limb_line = []
                            for label in self.limbs_sets[limb_name].labels:
                                if limbs_skeleton2d_dlc[side][label][camn][2] > self.limbs_sets[limb_name].threshold_likelihood:
                                    draw.point((limbs_skeleton2d_dlc[side][label][camn][0], limbs_skeleton2d_dlc[side][label][camn][1]), fill='blue')

                                draw.point((limbs_skeleton2d[side][label][camn][0], limbs_skeleton2d[side][label][camn][1]), fill='red')
                                limb_line.append((limbs_skeleton2d[side][label][camn][0], limbs_skeleton2d[side][label][camn][1]))

                            frst_label = list(limbs_skeleton2d[side].keys())[0]
                            limb_line.append((limbs_skeleton2d[side][frst_label][camn][0], limbs_skeleton2d[side][frst_label][camn][1]))
                            draw.line(limb_line, fill='red', width=2)

                    left, right = int(center_of_mass_xy[0] - x_width/2), int(center_of_mass_xy[0] + x_width/2)
                    top, bottom = int(center_of_mass_xy[1] - y_width/2), int(center_of_mass_xy[1] + y_width/2)
                    img = img.crop((left, top, right, bottom))

                    save_name = image_names[camn][i][:image_names[camn][i].rfind('.')] + '-' + obj_name \
                                + image_names[camn][i][image_names[camn][i].rfind('.'):]
                    img.save(os.path.join(save_paths[camn], save_name), compression='lzw')
                    img.close()

    def _save_skeleton_params_in_csv(self, obj_name, save_path):

        body_param_names = self.body_params.keys()

        param_list, param_names = [self.skeleton3d['frames']], 'frame'
        for param_name in body_param_names:
            param_list.append(self.body_params[param_name])
            param_names += ',' + param_name

        for limb_name in self.animal_sets.limb_module_names:
            for side in self._sides:
                for param_name in self.limbs_params[limb_name][side].keys():
                    if param_name in self.body_sets.param_names: continue
                    param_list.append(self.limbs_params[limb_name][side][param_name])
                    param_names += ',' + param_name + '_' + side

        np.savetxt(os.path.join(save_path, 'skeleton_parameters-' + obj_name + '.csv'),
                   np.c_[np.transpose(param_list)], delimiter=',', header=param_names)

    def _save_obj_metric_in_csv(self, list_dict, list_name, obj_name, save_path):
        param_list, param_names = [self.skeleton3d['frames']], 'frame'
        if isinstance(list_dict, dict):
            for side in self._sides:
                param_list.append(list_dict[side])
                param_names += ',' + list_name + '_' + side
        else:
            param_list.append(list_dict)
            param_names += ',' + list_name

        np.savetxt(os.path.join(save_path, list_name + '-' + obj_name + '.csv'),
                   np.c_[np.transpose(param_list)], delimiter=',', header=param_names)


def minimize_perp_distance(x, y, z):
    """

    Args:
        x:
        y:
        z:

    Returns:

    """
    def model(params, xyz):
        a, b, c, d = params
        x, y, z = xyz
        length_squared = a ** 2 + b ** 2 + c ** 2
        return ((a * x + b * y + c * z + d) ** 2 / length_squared).sum()

    def unit_length(params):
        a, b, c, d = params
        return a ** 2 + b ** 2 + c ** 2 - 1

    # Initial guess of params using first three points
    p1 = np.squeeze(np.transpose([x[0], y[0], z[0]]))
    p2 = np.squeeze(np.transpose([x[1], y[1], z[1]]))
    p3 = np.squeeze(np.transpose([x[2], y[2], z[2]]))

    v1, v2 = p3 - p1, p2 - p1  # These two vectors are in the plane
    cp = np.cross(v1, v2)  # the cross product is a vector normal to the plane
    d = np.dot(cp, p3)  # This evaluates a * x3 + b * y3 + c * z3 which equals d
    a, b, c = cp

    # constrain the vector perpendicular to the plane be of unit length
    cons = ({'type': 'eq', 'fun': unit_length})
    sol = optimize.minimize(model, (a, b, c, d), args=[x, y, z], constraints=cons)

    return tuple(sol.x)

# TODO remove following lines
# def unscramble_sides3d_dlc(skeleton3d, skeleton2d, body_skeleton3d_init, body_params, show_plot=False):
#     # Unscramble case where side is wrong on all cam views
#     label_names_right = [s for s in skeleton3d['label_names'] if 'right' in s]
#
#     if (skeleton2d['frames'] != skeleton3d['frames']).any():
#         raise ValueError('frames should be the same for skeleton2d and skeleton3d')
#
#     if show_plot:
#         nb_cam = len(skeleton2d[label_names_right[0]].keys())
#
#         fig1, axs1 = plt.subplots(nb_cam, 1)
#         for i, camn in enumerate(range(1, nb_cam + 1)):
#             axs1[i].clear()
#             for label_right in label_names_right:
#                 label_left = label_right.replace('right', 'left')
#                 x_right = skeleton2d[label_right][camn]['x']
#                 y_right = skeleton2d[label_right][camn]['y']
#                 x_left = skeleton2d[label_left][camn]['x']
#                 y_left = skeleton2d[label_left][camn]['y']
#
#                 axs1[i].plot(np.arctan2(y_right - y_left, x_right - x_left))  # angle of the line btw the two sides
#             axs1[i].set_ylabel('angle btw right and left (rad)')
#             axs1[i].set_xlabel('frames - ' + label_right)
#
#     for i, frame in enumerate(skeleton3d['frames']):
#         body_params_frame = {}
#         for label in body_params.keys():
#             body_params_frame[label] = body_params[label][i].copy()
#
#         # TODO get rid of dependency to skeleton_fitter.modules.insect_body_slim?
#         body_skeleton3d = copy.deepcopy(body_skeleton3d_init)
#         body_skeleton3d = body.build_skeleton3d(body_skeleton3d, body_params_frame)
#         body_skeleton3d = body.rotate_skeleton3d(body_skeleton3d, body_params_frame)
#         body_skeleton3d = body.translate_skeleton3d(body_skeleton3d, body_params_frame)
#         # body_skeleton2d = body.reproject_skeleton3d_to2d(body_skeleton3d, self.dlt_coefs)
#
#         for label_right in label_names_right:
#             label_left = label_right.replace('right', 'left')
#
#             x_right = skeleton3d[label_right]['x'][i].copy()
#             y_right = skeleton3d[label_right]['y'][i].copy()
#             z_right = skeleton3d[label_right]['z'][i].copy()
#
#             x_left = skeleton3d[label_left]['x'][i].copy()
#             y_left = skeleton3d[label_left]['y'][i].copy()
#             z_left = skeleton3d[label_left]['z'][i].copy()
#
#             xyz_right = [x_right, y_right, z_right]
#             xyz_left = [x_left, y_left, z_left]
#
#             dist_right2right_hinge = np.linalg.norm(body_skeleton3d['right_wing_hinge'] - xyz_right)
#             dist_left2right_hinge = np.linalg.norm(body_skeleton3d['right_wing_hinge'] - xyz_left)
#             dist_right2left_hinge = np.linalg.norm(body_skeleton3d['left_wing_hinge'] - xyz_right)
#             dist_left2left_hinge = np.linalg.norm(body_skeleton3d['left_wing_hinge'] - xyz_left)
#
#             if dist_right2right_hinge > dist_right2left_hinge and dist_left2left_hinge > dist_left2right_hinge:
#                 x_right, y_right, z_right, x_left, y_left, z_left = x_left, y_left, z_left, x_right, y_right, z_right
#
#                 skeleton3d[label_right]['x'][i], skeleton3d[label_right]['y'][i], skeleton3d[label_right]['z'][i] = \
#                     x_right, y_right, z_right
#
#                 skeleton3d[label_left]['x'][i], skeleton3d[label_left]['y'][i], skeleton3d[label_left]['z'][i] = \
#                     x_left, y_left, z_left
#
#                 for camn in range(1, nb_cam + 1):
#                     skeleton2d[label_right][camn]['x'][i], skeleton2d[label_left][camn]['x'][i] = \
#                         skeleton2d[label_left][camn]['x'][i], skeleton2d[label_right][camn]['x'][i]
#
#                     skeleton2d[label_right][camn]['y'][i], skeleton2d[label_left][camn]['y'][i] = \
#                         skeleton2d[label_left][camn]['y'][i], skeleton2d[label_right][camn]['y'][i]
#
#     if show_plot:
#         fig2, axs2 = plt.subplots(nb_cam, 1)
#         for i, camn in enumerate(range(1, nb_cam + 1)):
#             axs2[i].clear()
#             for label_right in label_names_right:
#                 label_left = label_right.replace('right', 'left')
#                 x_right = skeleton2d[label_right][camn]['x']
#                 y_right = skeleton2d[label_right][camn]['y']
#                 x_left = skeleton2d[label_left][camn]['x']
#                 y_left = skeleton2d[label_left][camn]['y']
#
#                 axs2[i].plot(
#                     np.arctan2(y_right - y_left, x_right - x_left))  # angle of the line btw the two sides
#             axs2[i].set_ylabel('angle btw right and left (rad)')
#             axs2[i].set_xlabel('frames - ' + label_right)
#
#         plt.show()
#
#     return skeleton3d, skeleton2d