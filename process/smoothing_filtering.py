import copy

import numpy as np
import matplotlib.pyplot as plt

from scipy import signal

from typing import Dict, List


def filter_interp_params(cutoff_frequency: int, frame_rate: int, params: Dict[str, any], param_names: List[str],
                         interpolate_nans: bool = False, filter_high_freq: bool = False, show_plot: bool = False) -> Dict[str, any]:
    """

    Args:
        cutoff_frequency:
        frame_rate:
        params:
        param_names:
        interpolate_nans:
        filter_high_freq:
        show_plot:

    Returns:
        new_params:
    """

    if not interpolate_nans and not filter_high_freq: return params
    new_params = copy.deepcopy(params)

    for i, param_name in enumerate(param_names):
        is_nan, x = np.isnan(new_params[param_name]), lambda z: z.nonzero()[0]
        if sum(~is_nan) == 0: continue  # only nans

        if '_a' in param_name:
            # fig2 = plt.figure()
            # ax2 = fig2.add_subplot(111)
            # ax2.plot(new_params[label], label='raw')

            new_params[param_name][~is_nan] = np.unwrap(np.deg2rad(new_params[param_name][~is_nan]))
            # ax2.plot(np.rad2deg(new_params[label]), label='unwrapped')

        if sum(is_nan) > 0:  # interpolation needed for filter_high_freq
            new_params[param_name][is_nan] = np.interp(x(is_nan), x(~is_nan), new_params[param_name][~is_nan])

        if filter_high_freq and np.nanstd(new_params[param_name]) > 10e-6:
            w = cutoff_frequency / (frame_rate / 2)  # Normalize the frequency
            b, a = signal.butter(2, w, 'low')

            new_params[param_name] = signal.filtfilt(b, a, new_params[param_name])

            if not interpolate_nans and sum(is_nan) > 0:  # to fill back with nan if didnt want to interpolate
                new_params[param_name][is_nan] = np.nan

        if '_a' in param_name:  # Wrap angles on [-pi, pi)
            new_params[param_name] = np.rad2deg((new_params[param_name] + np.pi) % (2 * np.pi) - np.pi)
            # ax2.plot(new_params[label], label='wrapped')
            # ax2.set_ylabel(label)
            # ax2.set_xlabel('frames')

    if show_plot:
        # Will plot the non-constant parameters over time before and after filtering
        param_names_to_plot = [param_name for i, param_name in enumerate(param_names)
                               if not np.all(params[param_name] == params[param_name][0])]

        fig, axs = plt.subplots(len(param_names_to_plot), 1)
        for i, param_name in enumerate(param_names_to_plot):
            axs[i].plot(params[param_name], label='raw')
            axs[i].plot(new_params[param_name], label='filt')
            axs[i].set_ylabel(param_name)
            axs[i].set_xlabel('frames')

        fig.suptitle('Body/Limbs non-constant parameters (before/after filtering) in function of time')

        plt.legend()
        #plt.show()

    return new_params


def filter_interp(y: List[float], frame_rate: int, cutoff_frequency: int) -> List[float]:
    """

    Args:
        y:
        frame_rate:
        cutoff_frequency:

    Returns:
        y_filtered:
    """
    y_filtered = y.copy()
    is_nan, x = np.isnan(y), lambda z: z.nonzero()[0]

    if sum(~is_nan) == 0: return y_filtered # only nans
    if sum(is_nan) > 0:  # interpolation needed for filter_high_freq
        y_filtered[is_nan] = np.interp(x(is_nan), x(~is_nan), y_filtered[~is_nan])

    w = cutoff_frequency / (frame_rate / 2)  # Normalize the frequency
    b, a = signal.butter(2, w, 'low')

    y_filtered = signal.filtfilt(b, a, y_filtered)
    y_filtered[is_nan] = np.nan

    return y_filtered