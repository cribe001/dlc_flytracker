import os, time, shutil, yaml, glob, copy

import cv2

from typing import Dict, Tuple

import images.utils as img_utils
import process.utils as process_utils

from utils.header import print_header
from utils.settings import Settings, ProtocolSettings, BatchSettings
from point_tracker.tracker2d import BlobDetectorSettings, BackgroundSubtractorSettings
from point_tracker.reconstructor3d import Reconstructor3DSettings

from images.multiimagesequence import MultiImageSequence

from process.dlc_postprocessing import load_dlc_from_full_pickle, save_dlc2d_points_in_csv, save_dlc3d_points_in_csv, \
    unscramble_views2d_dlc, save_stroboscopic_images, reverse_processes_2d_points, recon3d_dlc
from process.images_preprocessing import crop_all_images, crop_image, stitch_all_images, stitch_images_multiprocessing, save_avi
from process.point_tracking import reconstruct_3d_points, reconstruct_3d_tracks, track_2d_objects, manual_3d_tracking, \
                                   load_2d_coords, save_2d_tracks_from_paths, save_2d_tracks_from_dict, save_3d_track
from process.skeleton_fitting import fit_all_skeleton, plot_all_skeleton

from images.read import *

import_dlc = False

if import_dlc:
    import deeplabcut
    import tensorflow as tf

    tf_config = tf.compat.v1.ConfigProto()
    tf_config.gpu_options.allow_growth = True
    tf_config.log_device_placement = True
    sess = tf.compat.v1.Session(config=tf_config)


def batch_processing(settings_yaml_path: str) -> Tuple["BatchProcessing", Settings]:
    """

    Initiate and run BatchProcessing from settings contains in a yaml file.

    Args:
        settings_yaml_path: Path of yaml file that contains all the settings for the batch processing

    Returns:
        batch_process: Instance of BatchProcessing
        settings : Instance of Settings
    """

    settings = Settings.from_yaml(settings_yaml_path)

    print_header(settings)

    rec_names_to_process = settings.batch.recording_names_to_process
    delete_previous = settings.batch.delete_previous

    batch_process = BatchProcessing.from_settings(settings.batch)

    # Will run protocol steps (e;g. processes) in settings automatically by using 'multi_processes'
    batch_process.multi_processes(settings.protocol.to_dict(),
                                  rec_names_to_process=rec_names_to_process, delete_previous=delete_previous)

    return batch_process, settings


class BatchProcessing:
    """
        Class used to process batches of recordings (each consisting of sequences of images).

        Can be initialised using from_directory_by_dates or from_directory_by_names. Images can be pre-processed
        using functions like sample_images, track2d, recon3d, crop_images and stitch_images. The recordings can be
        saved to .avi using save_avi, and analysed using analyse_dlc (deeplabcut). Then the tracking results from
        deeplabcut can be loaded using load_dlc and a 3d skeleton can be fitted to these results using fit_skeleton.

    """
    def __init__(self, recordings: List[MultiImageSequence], save_path: str, nb_cam: int = 2,
                 recording_matching_settings=None) -> None:
        """

        Args:
            recordings: A sequence of MultiImageSequence consisting of sequences of images for each camera
            save_path: Path to the directory where the results of the processing will be saved.
            nb_cam: The number of camera (need to be superior or equal to 2)
        """

        self.recordings = recordings  # type: List[MultiImageSequence]
        self.nb_recordings = len(self.recordings)  # type: int
        assert self.nb_recordings > 0

        self.save_path = save_path  # type: str

        if nb_cam < 2:
            raise ValueError('The number of camera (nb_cam) should be superior or equal to 2')
        assert all([nb_cam == r.nb_sequences for r in self.recordings])
        self.nb_cam = nb_cam  # type: int

        self.recording_matching_settings = recording_matching_settings

        self.recording_names = \
            [{camn: os.path.basename(os.path.normpath(r.get_path(r.names[camn-1])))
             for camn in range(1, self.nb_cam + 1)} for r in self.recordings]  # type: List[Dict[str: str]]

        self.recording_paths = \
            [{camn: os.path.dirname(r.get_path(r.names[camn-1]))
             for camn in range(1, self.nb_cam + 1)} for r in self.recordings]  # type: List[Dict[str: str]]

        self.main_camn = 1  # type: int

    @staticmethod
    def from_settings(batch_settings: BatchSettings) -> "BatchProcessing":
        """
            Initiate BatchProcessing by gathering recording files from settings contains a BatchSettings class.
            It will match the recording folders for all cameras by checking the recording dates in their name.

        Args:
            batch_settings: BatchSettings
            
        Returns:
            BatchProcessing
        """

        recording_paths = [value for value in batch_settings.recording_paths.values()]

        # TODO use different framerates for the various cameras
        # TODO use dynamic framerate (e.g. with burst of high framerate parts)
        frame_rate = np.unique([value for value in batch_settings.frame_rates.values()])
        assert len(frame_rate) == 1
        frame_rate = frame_rate[0]

        if batch_settings.save_path is None: batch_settings.save_path = recording_paths[0]

        if batch_settings.recording_matching_settings['match_recordings_by'] == 'date':
            expr = batch_settings.recording_matching_settings['expr']
            format_date_str = batch_settings.recording_matching_settings['format_date_str']
            threshold_s = batch_settings.recording_matching_settings['threshold_s']

            return BatchProcessing.from_directory_by_dates(recording_paths, frame_rate, batch_settings.save_path, expr=expr,
                                                           format_date_str=format_date_str, threshold_s=threshold_s,
                                                           move_to_delete=batch_settings.move_to_delete)

        elif batch_settings.recording_matching_settings['match_recordings_by'] == 'name':
            threshold_ratio_diff = batch_settings.recording_matching_settings['threshold_ratio_diff']
            threshold_nb_diff_char = batch_settings.recording_matching_settings['threshold_nb_diff_char']

            return BatchProcessing.from_directory_by_names(recording_paths, frame_rate, batch_settings.save_path,
                                                           threshold_ratio_diff=threshold_ratio_diff,
                                                           threshold_nb_diff_char=threshold_nb_diff_char,
                                                           move_to_delete=batch_settings.move_to_delete)

        else:
            raise ValueError("batch_settings.recording_matching_settings['match_recordings_by'] should be 'name' or 'date")

    @staticmethod
    def from_directory_by_dates(recording_paths: List[str], frame_rate: int, save_path: str = None,
                                expr: str = r"\d{8}_\d{6}", format_date_str: str = '%Y%m%d_%H%M%S', threshold_s: int = 25,
                                move_to_delete: bool = False) -> "BatchProcessing":
        """
            Initiate BatchProcessing by gathering recording files from directory.
            It will match the recording folders for all cameras by checking the recording dates in their name.

        Args:
            recording_paths: List of paths to directories containing each with a recording sequence.
            frame_rate: The frame_rate of all recordings (we assume it is the same for all).

            save_path: Path to the directory where the results of the processing will be saved.
                If None, this path will be in the first recording path.
            expr: Regular expression pattern matching recording folders with date.
            format_date_str: format of the date and time in the folder names.
            threshold_s: Will check if the date in the folder names differ from less than threshold in second
            move_to_delete: Whether to move the unmatched recording folders to a to_delete folder
                (will be in the same parent directory as the recording folders)
        
        Returns:
            BatchProcessing
        """

        if save_path is None:
            save_path = recording_paths[0]

        nb_cam = len(recording_paths)
        cam_names = [camn for camn in range(1, nb_cam + 1)]

        matched_folder_names = process_utils.match_recordings_by_date(recording_paths, expr=expr,
                                                                      format_date_str=format_date_str,
                                                                      threshold_s=threshold_s)

        if move_to_delete:
            un_matched_folder_names = process_utils.get_unmatched_recordings(recording_paths, matched_folder_names)

            if un_matched_folder_names:
                for ind_cam in range(0, len(recording_paths)):
                    destination_path = os.path.join(recording_paths[ind_cam], 'to_delete')

                    if not os.path.exists(destination_path):
                        os.makedirs(destination_path)

                    for i in range(0, len(un_matched_folder_names)):
                        if un_matched_folder_names[i]:
                            os.rename(os.path.join(recording_paths[ind_cam], un_matched_folder_names[i][ind_cam]),
                                      os.path.join(destination_path, un_matched_folder_names[i][ind_cam]))

        recordings = []
        for i in range(0, len(matched_folder_names)):
            matched_directories = [os.path.join(recording_paths[j], f) for j, f in enumerate(matched_folder_names[i])]
            recordings.append(MultiImageSequence.from_directory(matched_directories, frame_rate, cam_names,
                                                                expr=expr + r"\d+.(tif|png|jpg|bmp)"))

        recording_matching_settings = {'match_recordings_by': 'date', 'expr': expr,
                                       'format_date_str': format_date_str, 'threshold_s': threshold_s}

        return BatchProcessing(recordings, save_path, nb_cam, recording_matching_settings=recording_matching_settings)

    @staticmethod
    def from_directory_by_names(recording_paths: List[str], frame_rate: int, save_path: str = None,
                                threshold_ratio_diff: float = None, threshold_nb_diff_char: int = None,
                                move_to_delete: bool = True) -> "BatchProcessing":
        """
            Initiate BatchProcessing by gathering recording files from directory.
            It will match the recording folders for all cameras by checking their names.

        Args:
            recording_paths: List of paths to directories containing each with a recording sequence.
            frame_rate: The frame_rate of all recordings (we assume it is the same for all).
            save_path: Path to the directory where the results of the processing will be saved.
                If None, this path will be in the first recording path.
            threshold_ratio_diff: Will check the similarity percentage (between 0 and 1)
            threshold_nb_diff_char: Will check how many characters are different between the file names.
            move_to_delete: Whether to move the unmatched recording folders to a to_delete folder
                (will be in the same parent directory as the recording folders)
                
        Returns:
            BatchProcessing
        """

        if save_path is None:
            save_path = recording_paths[0]

        nb_cam = len(recording_paths)
        cam_names = [camn for camn in range(1, nb_cam + 1)]

        matched_folder_names = \
            process_utils.match_recordings_by_name(recording_paths, threshold_ratio_diff=threshold_ratio_diff,
                                                   threshold_nb_diff_char=threshold_nb_diff_char)
        if move_to_delete:
            un_matched_folder_names = process_utils.get_unmatched_recordings(recording_paths, matched_folder_names)

            if un_matched_folder_names:
                for ind_cam in range(0, len(recording_paths)):
                    destination_path = os.path.join(recording_paths[ind_cam], 'to_delete')

                    if not os.path.exists(destination_path):
                        os.makedirs(destination_path)

                    for i in range(0, len(un_matched_folder_names)):
                        if un_matched_folder_names[i]:
                            os.rename(os.path.join(recording_paths[ind_cam], un_matched_folder_names[i][ind_cam]),
                                      os.path.join(destination_path, un_matched_folder_names[i][ind_cam]))

        recordings = []
        for i in range(0, len(matched_folder_names)):
            matched_directories = [os.path.join(recording_paths[j], f) for j, f in enumerate(matched_folder_names[i])]
            recordings.append(MultiImageSequence.from_directory(matched_directories, frame_rate, cam_names))

        recording_matching_settings = {'match_recordings_by': 'name', 'threshold_ratio_diff': threshold_ratio_diff,
                                       'threshold_nb_diff_char': threshold_nb_diff_char}

        return BatchProcessing(recordings, save_path, nb_cam, recording_matching_settings=recording_matching_settings)

    def multi_processes(self, protocol_dict: Dict[int, any], **kwargs: any) -> None:
        """
            Running multiple processes in parallel (much faster tha one by one) from yaml file

        Args:
            protocol_dict: Dictionary with the protocol steps of the multiples processes to run in parallel

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        kwargs['protocol_dict'] = protocol_dict
        self._do_batch('multi_processes', **kwargs)

    def sample_images(self, step_frame: int, **kwargs: any) -> None:
        """
            Copy samples of the images sequences for all recordings

        Args:
            step_frame: The number of images between samples to take

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        kwargs['step_frame'] = step_frame
        self._do_batch('sample', **kwargs)

    def resize_images(self, resize_ratio: float, **kwargs: any) -> None:
        """
            Up/Down-sizing of the images from a resize_ratio

        Args:
            resize_ratio: Ratio for up- or down-sizing of the images

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        kwargs['resize_ratio'] = resize_ratio
        self._do_batch('resize', **kwargs)
   
    def enhance_images(self, fn_name: str, factor: float, **kwargs: any) -> None:
        """
            Enhance the 'contrast', brightness', 'sharpness' or 'color' of the images of all recordings. 
        
        Args:
            fn_name: image characteristic to enhance, can be: 'contrast', 'brightness', 'sharpness' or 'color'
            factor: enhancing factor: 1.0 always returns a copy of the original image,
                    lower factors mean less color (brightness, contrast, etc), and higher values more

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        kwargs['factor'] = factor
        if fn_name == 'contrast':  # Enhancing contrast of images
            self._do_batch('enhance_contrast', **kwargs)

        elif fn_name == 'enhance_brightness':  # Enhancing brightness of images
            self._do_batch('enhance_color', **kwargs)

        elif fn_name == 'sharpness':  # Enhancing sharpness of images
            self._do_batch('enhance_color', **kwargs)

        elif fn_name == 'color':  # Enhancing color of images
            self._do_batch('enhance_color', **kwargs)

        else:
            raise ValueError("fn_name unknown. It has to be either 'contrast', 'brightness', 'sharpness' or 'color'")

    def auto_contrast_images(self, resize_ratio: float, **kwargs: any) -> None:
        """
            Will run an auto-contrasting algorythm on each image

        Args:
            resize_ratio: Ratio for up- or down-sizing of the images

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        kwargs['resize_ratio'] = resize_ratio
        self._do_batch('autocontrast', **kwargs)

    def equalize_images(self, **kwargs: any) -> None:
        """
            Will equalize all images of each sequence together

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        self._do_batch('equalize', **kwargs)

    def static_crop_images(self, x_crop: int, y_crop: int, height: int, width: int, **kwargs: any) -> None:
        """
            Crop images  at the given height and width around statics 2d coordinates in pixels

        Args:
            x_crop: list of x coordinates (one per cam) of the center around which the images will each be cropped (nb_cam*1)
            y_crop: dlist of x coordinates (one per cam) of the center around which the images will each be cropped (nb_cam*1)
            height: height to which the images will each be cropped
            width: width to which the images will each be cropped
            
        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        kwargs['x_crop'], kwargs['y_crop'] = x_crop, y_crop
        kwargs['crop_height'], kwargs['crop_width'] = height, width
        kwargs['dynamic_crop'] = False
        self._do_batch('crop', **kwargs)

    def dynamic_crop_images(self, crop_height: int, crop_width: int, from_tracks: bool = False,
                            **kwargs: any) -> None:
        """
            Crop images at the given height and width around dynamic 2d coordinates in pixels

        Args:
            crop_height: height to which the images will each be cropped
            crop_width: width to which the images will each be cropped
            from_tracks: Whether to use the coordinates from the tracked objects for the cropping
            
        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        kwargs['crop_height'], kwargs['crop_width'] = crop_height, crop_width
        kwargs['from_tracks'] = from_tracks
        kwargs['dynamic_crop'] = True
        self._do_batch('crop', **kwargs)

    def rotate_images(self, degrees: int, from_fn_name: str = 'sample', **kwargs: any) -> None:
        """
            Rotating cameras views of a given number of degrees

        Args:
            degrees: Angle at which the images will be rotated counterclockwise around its centre
            from_fn_name: the name of the previous processing function, that saved the images to be rotated
                          (e.g. 'sample' or 'enhance_contrast')

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            delete_previous: Whether to erase previous results of the chosen process
        """

        kwargs['degrees'] = degrees
        kwargs['from_fn_name'] = from_fn_name
        self._do_batch('rotate', **kwargs)

    def stitch_images(self, **kwargs: any) -> None:
        """
            Stitch all cameras views together to make one image per frame

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        self._do_batch('stitch', **kwargs)

    def track2d(self, back_subtractor_settings: BackgroundSubtractorSettings = None,
                blob_detector_settings: BlobDetectorSettings = None, **kwargs: any) -> None:
        """
            Track 2d coordinates of blobs detected on the images using background subtraction

        Args:
            back_subtractor_settings: BackgroundSubtractorSettings class
            blob_detector_settings: BlobDetectorSettings class

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        kwargs['back_subtractor_settings'] = \
            back_subtractor_settings if back_subtractor_settings is not None else BackgroundSubtractorSettings()
        kwargs['blob_detector_settings'] = \
            blob_detector_settings if blob_detector_settings is not None else BlobDetectorSettings()
        self._do_batch('track2d', **kwargs)

    def recon3d(self, dlt_path: str, reconstructor_settings: Reconstructor3DSettings = None, **kwargs: any) -> None:
        """
            Reconstructing 3d tracks from 2d coordinates (previously computed using track2d)

        Args:
            dlt_path: path of the .csv file containing th 11*nb_cam DLT coefficients (3d calibration)
            reconstructor_settings: Reconstructor3DSettings class

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        kwargs['dlt_path'] = dlt_path
        kwargs['reconstructor_settings'] = \
            reconstructor_settings if reconstructor_settings is not None else Reconstructor3DSettings()
        self._do_batch('recon3d', **kwargs)

    def manual_tracking(self, dlt_path: str, index_to_track: List[int] = [0, -1],
                        reconstructor_settings: Reconstructor3DSettings = None, **kwargs: any) -> None:
        """
            Track 2d coordinates of animal(s) manually, and then reconstruct 3d tracks.
        
        Args:
            dlt_path: path of the .csv file containing th 11*nb_cam DLT coefficients (3d calibration)
            index_to_track: List of indexes to select image where the manual tracking will be done
            reconstructor_settings: Reconstructor3DSettings class

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images)
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        kwargs['dlt_path'] = dlt_path
        kwargs['index_to_track'] = index_to_track
        kwargs['reconstructor_settings'] = \
            reconstructor_settings if reconstructor_settings is not None else Reconstructor3DSettings()
        self._do_batch('manual_tracking', **kwargs)

    def load_2d_tracks(self, csv_paths: Dict[str, str] = None,
                        tracks_dict: Dict[str, Dict[int, List[float]]] = None,  **kwargs: any) -> None:
        """
           Will load 2d tracks either using paths or a dictionary with 2d coordinates.

        Args:
            csv_paths: Dictionary containing paths towards csv files with 2d object coordinates
                       The structure of the dictionary should be csv_paths[obj_name][camn][csv_path]
            tracks_dict: Dictionary containing 2d coordinates of the tracks.
                         The structure of the dictionary should be tracks_dict[obj_name][camn][frames, x_px and y_px]

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images)
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        assert csv_paths is not None or tracks_dict is not None

        kwargs['csv_paths'] = csv_paths
        kwargs['tracks_dict'] = tracks_dict
        self._do_batch('load_2d_tracks', **kwargs)

    def save_tiff(self, **kwargs: any) -> None:
        """
            Save images as 8 bits .tiff

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
        """

        self._do_batch('save_tiff', **kwargs)

    def save_stroboscopic_image(self, back_subtractor_settings: BackgroundSubtractorSettings = None,
                                blob_detector_settings: BlobDetectorSettings = None,
                                from_tracks: bool = False, **kwargs: any) -> None:
        """
            Generate stroboscopic images (composite image base on sampled images and 2d track of the objects)
            Then save one stroboscopic image per object, per view and per recording

        Args:
            back_subtractor_settings: BackgroundSubtractorSettings class
            blob_detector_settings: BlobDetectorSettings class
            from_tracks: Whether to use the coordinates from the tracked objects

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        kwargs['back_subtractor_settings'] = \
            back_subtractor_settings if back_subtractor_settings is not None else BackgroundSubtractorSettings()
        kwargs['blob_detector_settings'] = \
            blob_detector_settings if blob_detector_settings is not None else BlobDetectorSettings()
        kwargs['from_tracks'] = from_tracks
        self._do_batch('save_strobe', **kwargs)

    def save_avi(self, frame_rate: int = 24,  lossless: bool = False, **kwargs: any) -> None:
        """
            Save the recordings as .avi

        Args:
            frame_rate: the frame rate at which the video will be saved
            lossless: Whether to save lossless video (e.g. not compressed)

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        kwargs['frame_rate'] = frame_rate
        kwargs['lossless'] = lossless
        self._do_batch('save_avi', **kwargs)

    def analyse_dlc(self, cfg_path: str, shuffle: int, trainingsetindex: int, batch_size: int, model_name: str,
                    save_avi: bool = False, **kwargs: any) -> None:
        """
            Analysing videos with DeepLabCut

        Args:
            cfg_path:
            shuffle:
            trainingsetindex:
            batch_size:
            save_avi:
            model_name:

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        kwargs['cfg_path'] = cfg_path
        kwargs['shuffle'] = shuffle
        kwargs['trainingsetindex'] = trainingsetindex
        kwargs['batch_size'] = batch_size
        kwargs['save_avi'] = save_avi
        kwargs['model_name'] = model_name
        self._do_batch('analyse_dlc', **kwargs)

    def load_dlc(self, model_name: str, tracker_method: str, dlt_path: str, threshold_likelihood: float = 0.9,
                 **kwargs: any) -> None:
        """
            Load points from DeepLabCut (unscramble, reconstruct3d ...

        Args:
            model_name:
            tracker_method:
            dlt_path: path of the .csv file containing th 11*nb_cam DLT coefficients (3d calibration)
            threshold_likelihood:

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
            reconstructor_settings:
        """

        kwargs['model_name'] = model_name
        kwargs['tracker_method'] = tracker_method
        kwargs['dlt_path'] = dlt_path
        kwargs['threshold_likelihood'] = threshold_likelihood
        self._do_batch('load_dlc', **kwargs)

    def fit_skeleton(self, animal_name: str, model_name: str, fit_method: str, opt_method: str, init_yaml_path: str,
                     dlt_path: str, multiprocessing: bool = False, **kwargs: any) -> None:
        """
            Optimizing fit btw skeletons to get transformation/rotation parameters

        Args:
            animal_name:
            model_name:
            fit_method:
            opt_method: optimization method ('nelder-mead', 'powell', 'least_squares', 'leastsq')
            init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
            dlt_path: path of the .csv file containing th 11*nb_cam DLT coefficients (3d calibration)

            multiprocessing: Whether to use multiprocessing to speed up processing (then won't be able to plot results during optimization)

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
            show_plot:
        """

        kwargs['animal_name'] = animal_name
        kwargs['model_name'] = model_name
        kwargs['fit_method'], kwargs['opt_method'] = fit_method, opt_method
        kwargs['init_yaml_path'] = init_yaml_path
        kwargs['dlt_path'] = dlt_path
        kwargs['multiprocessing'] = multiprocessing
        self._do_batch('fit_skeleton', **kwargs)

    def plot_skeleton(self, animal_name: str, init_yaml_path: str, dlt_path: str,
                      save_images: bool = False, **kwargs: any) -> None:
        """
            Plot (or save) skeleton on raw images

        Args:
            animal_name:
            init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
            dlt_path: path of the .csv file containing th 11*nb_cam DLT coefficients (3d calibration)
            save_images: Whether to save the plotted image

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
        """

        kwargs['animal_name'] = animal_name
        kwargs['init_yaml_path'] = init_yaml_path
        kwargs['dlt_path'] = dlt_path
        kwargs['save_images'] = save_images
        self._do_batch('plot_skeleton', **kwargs)

    def delete(self, to_del_names: Dict[str, List[str]], **kwargs: any) -> None:
        """
            Move list of recording folder in a 'to_delete' folder

        Args:
            to_del_names: List of recording names to delete (will be moved to to_delete folder)

        Kwargs:
            rec_names_to_process: a list of recording names (folder containing images) 
            camn_to_process: a list of camera numbers for which the recordings will be processed
            from_fn_name: the name of the previous processing function, from which the current process as to follow up
            delete_previous: Whether to erase previous results of the chosen process
        """

        kwargs['to_del_names'] = to_del_names
        self._do_batch('delete', **kwargs)

    def _do_batch(self, fn_name: str, **kwargs: any) -> None:
        """
            Function used to run batch processing on all the recordings

        Args:
            fn_name: Name of the function to run the batch on (e.g. 'track2d', 'crop', 'analyse_dlc', 'fit_skeleton')
            **kwargs: A dictionary with the process parameters
        """

        start = time.time()
        self._print_process(fn_name, kwargs)

        step_frame = 1 if 'step_frame' not in kwargs.keys() else kwargs['step_frame']
        show_plot = False if 'show_plot' not in kwargs.keys() else kwargs['show_plot']
        delete_previous = 'delete_previous' in kwargs.keys() and kwargs['delete_previous']

        kwargs['camn_to_process'] = \
            list(range(1, self.nb_cam +1)) if 'camn_to_process' not in kwargs.keys() else kwargs['camn_to_process']

        if fn_name in ['autocontrast', 'equalize']:
            kwargs['factor'] = None

        if fn_name in 'analyse_dlc':
            if 'trainingsetindex' not in kwargs.keys(): kwargs['trainingsetindex'] = 0
            if 'batch_size' not in kwargs.keys(): kwargs['batch_size'] = 2

        if fn_name in ['fit_skeleton', 'plot_skeleton']:
            frame_rates = [[self.recordings[i].frame_rates[camn] for camn in range(1, self.nb_cam + 1)]
                           for i in range(0, len(self.recordings))]
            assert (np.all(np.asarray(frame_rates) == frame_rates[0][0]))
            frame_rate = frame_rates[0][0]

        all_main_rec_names = [names[self.main_camn] for names in self.recording_names]
        assert len(all_main_rec_names) != 0

        if 'rec_names_to_process' in kwargs.keys():
            if kwargs['rec_names_to_process'] == 'all':
                main_rec_names_to_process = all_main_rec_names

            else:
                if type(kwargs['rec_names_to_process']) is str:
                    kwargs['rec_names_to_process'] = [kwargs['rec_names_to_process']]
                else:
                    assert type(kwargs['rec_names_to_process']) is list
                
                main_rec_names_to_process = [name for name in all_main_rec_names if name in kwargs['rec_names_to_process']]
        else:
            main_rec_names_to_process = all_main_rec_names

        from_fn_path = self._get_path_process(kwargs['from_fn_name']) if 'from_fn_name' in kwargs.keys() else None
        process_path = self._get_path_process(fn_name)
        if process_path is None: process_path = from_fn_path

        assert len(main_rec_names_to_process) != 0

        # Main process loop: Will do each process step one at a time for each recording
        for main_rec_name in main_rec_names_to_process:
            start_rec = time.time()
            ind_rec = all_main_rec_names.index(main_rec_name)
            will_delete_rec = False

            recording = self.recordings[ind_rec]
            rec_names = self.recording_names[ind_rec]
            rec_paths = self.recording_paths[ind_rec]

            if from_fn_path is not None:
                load_process_path = os.path.join(from_fn_path, rec_names[self.main_camn])
                load_process_paths = {camn: os.path.join(load_process_path, 'cam{0}'.format(camn)) for camn in range(1, self.nb_cam + 1)}

                image_paths = load_process_paths
                image_names = {camn: process_utils.get_image_names_in_directory(image_paths[camn])
                               for camn in range(1, self.nb_cam + 1)}
                frames = {camn: recording.get(camn).get_numbers_from_names(image_names[camn]) for camn in range(1, self.nb_cam + 1)}

                settings_dict = yaml.load(open(os.path.join(load_process_path,
                                                            'process_history.yaml')), Loader=yaml.SafeLoader)

                settings = Settings.from_dict(settings_dict)

            else:
                image_names = {camn: recording.get(camn).get_names() for camn in range(1, self.nb_cam + 1)}
                image_paths = {camn: recording.get(camn).get_path() for camn in range(1, self.nb_cam + 1)}
                frames = {camn: recording.get(camn).get_numbers() for camn in range(1, self.nb_cam + 1)}

                batch_dict = self._init_batch_dict(recording, rec_paths, frames)
                settings = Settings.from_dicts(batch_dict, {})

                settings.batch.recording_matching_settings = self.recording_matching_settings

                if 'rec_names_to_process' in kwargs.keys():
                    settings.batch.recording_names_to_process = kwargs['rec_names_to_process']

            print('--------------------------------------------------------------------------------------')
            if fn_name == 'multi_processes':
                print('>> Processing {0}...'.format(main_rec_name))

                settings.protocol = ProtocolSettings.from_dict(kwargs['protocol_dict'])

                self._multi_processes(ind_rec, settings, show_plot)

                print('>> {0} as been processed (time elapsed: {1:.4f} s)'.format(main_rec_name, time.time() - start_rec))
                continue

            print('>> Starting processing of {0} (fn: {1})...'.format(main_rec_name, fn_name))

            save_process_path = os.path.join(process_path, rec_names[self.main_camn])
            save_process_paths = {camn: os.path.join(save_process_path, 'cam{0}'.format(camn)) for camn in range(1, self.nb_cam + 1)}

            process_num = 1
            while process_num in settings.protocol.nums: process_num += 1
            settings.protocol.add_step(fn=fn_name, kwargs=process_utils.convert_kwargs_to_yaml(kwargs))

            if fn_name == 'stitch':
                stitch_all_images(image_names, image_paths, frames, save_process_paths, self.main_camn)

            elif fn_name == 'manual_tracking':
                if not delete_previous and os.path.exists(save_process_path): continue

                recon_sets = Reconstructor3DSettings() if 'reconstructor_settings' not in kwargs.keys() \
                                                       else kwargs['reconstructor_settings']

                manual_3d_tracking(image_names, image_paths, frames,
                                   kwargs['dlt_path'], rec_names, save_process_paths, recon_sets,
                                   index_to_track=kwargs['index_to_track'], show_plot=show_plot)

            elif fn_name == 'recon3d':
                pts_csv = {}
                for camn in range(1, self.nb_cam + 1):
                    pts_csv[camn] = np.genfromtxt(os.path.join(save_process_paths[camn], rec_names[camn] + '-2d_points.csv'),
                                                  delimiter=',', skip_header=0, names=True)

                recon_sets = Reconstructor3DSettings() if 'reconstructor_settings' not in kwargs.keys() \
                                                       else kwargs['reconstructor_settings']
                reconstruct_3d_tracks(pts_csv, kwargs['dlt_path'], rec_names, save_process_paths,
                                      show_plot=show_plot, reconstructor_settings=recon_sets)

            elif fn_name == 'load_2d_tracks':
                if 'csv_paths' in kwargs.keys() and kwargs['csv_paths'] is not None:
                    save_2d_tracks_from_paths(kwargs['csv_paths'], rec_names, save_process_paths)

                else:
                    save_2d_tracks_from_dict(kwargs['tracks_dict'], rec_names, save_process_paths)

            elif fn_name == 'analyse_dlc':
                video_paths = glob.glob(os.path.join(load_process_paths[self.main_camn], '*.avi'))
                assert len(video_paths) != 0

                if not os.path.exists(save_process_path): os.makedirs(save_process_path)

                deeplabcut.analyze_videos(kwargs['cfg_path'], video_paths, destfolder=save_process_path,
                                          shuffle=kwargs['shuffle'], trainingsetindex=kwargs['trainingsetindex'],
                                          batchsize=kwargs['batch_size'], save_as_csv=True, auto_track=False)

                if 'save_avi' in kwargs.keys() and kwargs['save_avi']:
                    deeplabcut.create_video_with_all_detections(kwargs['cfg_path'], video_paths,
                                                                shuffle=kwargs['shuffle'],
                                                                trainingsetindex=kwargs['trainingsetindex'],
                                                                destfolder=save_process_path)

            elif fn_name == 'load_dlc':
                # TODO Give options to load from csv, pickle or hdf5?
                # skeleton2d = self.load_dlc_from_csv(settings.batch, kwargs['model_name'], load_process_path)
                # skeleton2d = self.load_dlc_from_hdf5(settings.batch, kwargs['model_name'], load_process_path)
                skeleton2d = load_dlc_from_full_pickle(settings.batch, kwargs['model_name'], load_process_path)

                skeleton2d = unscramble_views2d_dlc(skeleton2d, settings.protocol)

                image_names = {camn: recording.get(camn).get_names() for camn in range(1, self.nb_cam + 1)}
                image_paths = {camn: recording.get(camn).get_path() for camn in range(1, self.nb_cam + 1)}
                skeleton2d = reverse_processes_2d_points(skeleton2d, settings, image_names, image_paths,
                                                         kwargs['threshold_likelihood'], settings.batch.image_sizes,
                                                         self.save_path, show_plot=show_plot, main_camn=self.main_camn)

                save_dlc2d_points_in_csv(skeleton2d, save_process_paths)

                # self.unscramble_sides2d_and_recon3d_dlc(kwargs['dlt_path'])
                recon_sets = Reconstructor3DSettings() if 'reconstructor_settings' not in kwargs.keys() \
                    else kwargs['reconstructor_settings']
                skeleton3d = recon3d_dlc(skeleton2d, kwargs['dlt_path'], recon_sets)
                save_dlc3d_points_in_csv(skeleton3d, save_process_path)

            elif fn_name == 'fit_skeleton':
                fit_all_skeleton(kwargs['animal_name'], kwargs['fit_method'], kwargs['opt_method'], image_paths,
                                 load_process_path, kwargs['init_yaml_path'], kwargs['dlt_path'], save_process_path, frame_rate,
                                 show_plot=show_plot, image_names=image_names, image_paths=image_paths,
                                 multiprocessing=kwargs['multiprocessing'])

            elif fn_name == 'plot_skeleton':
                save_images = False if 'save_images' not in kwargs.keys() else kwargs['save_images']

                plot_all_skeleton(kwargs['animal_name'], image_names, rec_paths, image_paths, load_process_path,
                                  kwargs['init_yaml_path'], kwargs['dlt_path'], save_process_paths,
                                  frame_rate, save_images=save_images)

            elif will_delete_rec:
                rec_names_to_del = {camn: recording.get(camn).get_names() for camn in range(1, self.nb_cam + 1)}
                rec_paths_to_del = {camn: recording.get(camn).get_path() for camn in range(1, self.nb_cam + 1)}
                self._delete_recordings(rec_names_to_del, rec_paths_to_del)

            else:
                for camn in range(1, self.nb_cam + 1):
                    if will_delete_rec or camn not in kwargs['camn_to_process']: continue

                    if os.path.exists(save_process_paths[camn]) and rec_paths[camn] != save_process_paths[camn]:
                        if delete_previous:
                            shutil.rmtree(save_process_paths[camn], ignore_errors=True)
                            os.makedirs(save_process_paths[camn])
                    else:
                        os.makedirs(save_process_paths[camn])

                    image_sizes = settings.batch.image_sizes

                    if fn_name == 'track2d':
                        back_sub_sets = BackgroundSubtractorSettings() if 'back_subtractor_settings' not in kwargs.keys() \
                                                                       else kwargs['back_subtractor_settings']
                        blob_det_sets = BlobDetectorSettings() if 'blob_detector_settings' not in kwargs.keys() \
                                                               else kwargs['blob_detector_settings']

                        track_2d_objects(image_names[camn], image_paths[camn], rec_names[camn], save_process_paths[camn],
                                         back_subtractor_settings=back_sub_sets, blob_detector_settings=blob_det_sets,
                                         frames=frames[camn], show_plot=show_plot)

                    elif fn_name == 'crop':
                        if kwargs['dynamic_crop']:
                            track_path = os.path.join(self._get_path_process('track2d'), rec_names[self.main_camn])
                            track_paths = {camn: os.path.join(track_path, 'cam{0}'.format(camn)) for camn in range(1, self.nb_cam + 1)}
                            coords_objs = load_2d_coords(frames[camn], track_paths[camn], save_process_paths[camn],
                                                         image_sizes[camn], kwargs['from_tracks'], show_plot=show_plot)

                        else:
                            coords_objs = {'obj0': {'frame': frames[camn],
                                                    'x_px': np.ones(np.shape(frames[camn]))*kwargs['x_crop'][camn-1],
                                                    'y_px': np.ones(np.shape(frames[camn]))*kwargs['y_crop'][camn-1]}}

                        if coords_objs:
                            crop_all_images(coords_objs, image_names[camn], image_paths[camn], save_process_paths[camn],
                                            kwargs['crop_height'], kwargs['crop_width'])
                        else:
                            print('>> Could not find any object')

                    elif fn_name == 'save_strobe':
                        radius = 50 if 'radius' not in kwargs.keys() else kwargs['radius']
                        shape = 'rectangle' if 'shape' not in kwargs.keys() else kwargs['shape']

                        coords_objs = load_2d_coords(frames[camn], load_process_paths[camn], save_process_paths[camn],
                                                     image_sizes[camn], kwargs['from_tracks'], show_plot=show_plot)

                        back_sub_sets = BackgroundSubtractorSettings() if 'back_subtractor_settings' not in kwargs.keys() \
                                                                       else kwargs['back_subtractor_settings']
                        blob_det_sets = BlobDetectorSettings() if 'blob_detector_settings' not in kwargs.keys() \
                                                               else kwargs['blob_detector_settings']

                        save_stroboscopic_images(recording.get(camn), frames[camn], coords_objs, image_names[camn][0],
                                                 save_process_paths[camn], radius, shape, step_frame,
                                                 back_subtractor_settings=back_sub_sets,
                                                 blob_detector_settings=blob_det_sets)

                    elif fn_name == 'save_avi':
                        if os.path.exists(image_paths[camn]):
                            frame_rate = 24 if 'frame_rate' not in kwargs.keys() else kwargs['frame_rate']
                            lossless = False if 'lossless' not in kwargs.keys() else kwargs['lossless']

                            save_avi(image_names[camn], image_paths[camn], rec_names[camn], save_process_paths[camn],
                                     frame_rate, step_frame, lossless)

                    elif fn_name == 'delete':
                        if rec_names[camn] in kwargs['to_del_names']:
                            will_delete_rec = True

                    else:
                        for ind_img, image_name in enumerate(image_names[camn]):
                            if step_frame != 1 and (frames[camn][ind_img] + 1) % step_frame != 0: continue

                            img = read_image(os.path.join(image_paths[camn], image_name))
                            image_save_name = image_name

                            if fn_name == 'rotate':
                                img = img.rotate(kwargs['degrees'])

                            elif fn_name == 'resize':
                                img = img_utils.resize(img, kwargs['resize_ratio'])

                            elif fn_name in ['enhance_contrast', 'enhance_brightness', 'enhance_sharpness',
                                             'enhance_color', 'autocontrast', 'equalize']:
                                img = img_utils.enhance(img, fn_name, kwargs['factor'])

                            elif fn_name == 'save_tiff':
                                img = Image.open(os.path.join(image_paths[camn], image_name))
                                img_utils.save_tiff(img, os.path.join(save_process_paths[camn], image_name))

                            if fn_name in ['rotate', 'resize', 'sample', 'enhance_contrast', 'enhance_brightness',
                                           'enhance_sharpness', 'enhance_color', 'autocontrast', 'equalize']:
                                img.save(os.path.join(save_process_paths[camn], image_save_name))
                                img.close()

                    if os.path.exists(save_process_paths[camn]) and not os.listdir(save_process_paths[camn]):  # if save_path empty
                        shutil.rmtree(save_process_paths[camn], ignore_errors=True)

            # Save process history in yaml file
            settings.to_yaml(os.path.join(save_process_path, 'process_history.yaml'))
            
            print('>> {0} as been processed (time elapsed: {1:.4f} s)'.format(main_rec_name, time.time() - start_rec))

        print('> Full batch as been processed (total time elapsed: {0:.4f} s)'.format(time.time() - start))

    def _multi_processes(self, ind_rec: int, settings: Settings, show_plot=False) -> None:
        image_sizes = settings.batch.image_sizes

        img, obj_names = {}, []
        multi_processes_nums = settings.protocol.nums
        for multi_processes_num in multi_processes_nums[1:]:
            start = time.time()
            fn_name = settings.protocol.get(multi_processes_num).fn
            kwargs = settings.protocol.get(multi_processes_num).kwargs
            step_frame = 1 if 'step_frame' not in kwargs.keys() else kwargs['step_frame']
            kwargs['camn_to_process'] = list(range(1, self.nb_cam + 1) if 'camn_to_process' not in kwargs.keys()
                                                                       else kwargs['camn_to_process'])
            recording = self.recordings[ind_rec]
            rec_names = self.recording_names[ind_rec]
            rec_paths = self.recording_paths[ind_rec]

            from_fn_path = self._get_path_process(kwargs['from_fn_name']) if 'from_fn_name' in kwargs.keys() else None
            process_path = self._get_path_process(fn_name)
            if process_path is None: process_path = from_fn_path

            image_names = {camn: self.recordings[ind_rec].get(camn).get_names() for camn in range(1, self.nb_cam + 1)}
            image_paths = {camn: recording.get(camn).get_path() for camn in range(1, self.nb_cam + 1)}

            save_process_path = os.path.join(process_path, rec_names[self.main_camn])
            save_process_paths = {camn: os.path.join(save_process_path,
                                                     'cam{0}'.format(camn)) for camn in range(1, self.nb_cam + 1)}

            frames = {camn: recording.get(camn).get_numbers() for camn in range(1, self.nb_cam + 1)}

            for camn in range(1, self.nb_cam + 1):
                if 'delete_previous' in kwargs.keys() and kwargs['delete_previous']:
                    if os.path.exists(save_process_paths[camn]) and rec_paths[camn] != save_process_paths[camn]:
                        shutil.rmtree(save_process_paths[camn], ignore_errors=True)

            for camn in range(1, self.nb_cam +1):
                if not obj_names or 'obj0' in obj_names:
                    file_names = sorted(os.listdir(image_paths[camn]), key=lambda s: s[:s.rfind('.')])
                    csv_names = [s for s in file_names if '-2d_points.csv' in s and '-obj' in s]

                    if csv_names:
                        obj_names = np.unique([s[process_utils.find_char(s, '-')[0]
                                                 + 1:process_utils.find_char(s, '-')[1]] for s in csv_names])
                        if 'obj0' in img[camn].keys(): img[camn] = {}
                    else:
                        obj_names = ['obj0']

                if not os.path.exists(save_process_paths[camn]): os.makedirs(save_process_paths[camn])
                if not camn in kwargs['camn_to_process']: continue
                if fn_name in ['recon3d', 'stitch']: continue

                if fn_name == 'track2d':
                    back_sub_sets = BackgroundSubtractorSettings() if 'back_subtractor_settings' not in kwargs.keys() \
                        else BackgroundSubtractorSettings.from_dict(kwargs['back_subtractor_settings'])
                    blob_det_sets = BlobDetectorSettings() if 'blob_detector_settings' not in kwargs.keys() \
                        else BlobDetectorSettings.from_dict(kwargs['blob_detector_settings'])

                    track_2d_objects(recording.get(camn), rec_names[camn], save_process_paths[camn],
                                     back_subtractor_settings=back_sub_sets,
                                     blob_detector_settings=blob_det_sets, show_plot=show_plot)
                    continue

                elif fn_name == 'manual_tracking':
                    pts_csv = {}
                    min_delay_frame = np.zeros((self.nb_cam,))

                    commom_frames = list(set.intersection(*[set(frames[camn]) for camn in range(1, self.nb_cam + 1)]))
                    frames_to_track = [commom_frames[index] for index in kwargs['index_to_track']]

                    for camn in range(1, self.nb_cam + 1):
                        image_names_to_track = [image_names[camn][index] for index in kwargs['index_to_track']]
                        pts_csv[camn] = manual_2d_tracking(image_names_to_track, image_paths[camn], frames_to_track)
                        min_delay_frame[camn - 1] = min(np.array(frames_to_track[1:]) - np.array(frames_to_track[0:-1]))

                    recon_sets = Reconstructor3DSettings() if 'reconstructor_settings' not in kwargs.keys() \
                                                           else kwargs['reconstructor_settings']
                    xs_pose, ys_pose, zs_pose, frames_pose, indexes, _ = \
                        reconstruct_3d_points(pts_csv, kwargs['dlt_path'], rec_names, save_process_paths,
                                              reconstructor_settings=recon_sets)

                    tracks_dict = {'obj': {1: {'x': np.squeeze(xs_pose), 'y': np.squeeze(ys_pose), 'z': np.squeeze(zs_pose),
                                               'frame': np.squeeze(frames_pose), 'index_pts': np.squeeze(indexes)}}}

                    save_3d_track(tracks_dict, kwargs['dlt_path'], rec_names, save_process_paths, show_plot=show_plot)
                    continue

                elif fn_name == 'save_strobe':
                    radius = 50 if 'radius' not in kwargs.keys() else kwargs['radius']
                    shape = 'rectangle' if 'shape' not in kwargs.keys() else kwargs['shape']

                    csv_path = os.path.join(from_fn_path, rec_names[self.main_camn], 'cam{0}'.format(camn))
                    coords_objs = load_2d_coords(frames[camn], csv_path, save_process_paths[camn],
                                                 image_sizes[camn], kwargs['from_tracks'], show_plot=show_plot)

                    back_sub_sets = BackgroundSubtractorSettings() if 'back_subtractor_settings' not in kwargs.keys() \
                        else kwargs['back_subtractor_settings']
                    blob_det_sets = BlobDetectorSettings() if 'blob_detector_settings' not in kwargs.keys() \
                        else kwargs['blob_detector_settings']

                    save_stroboscopic_images(recording.get(camn), frames[camn], coords_objs, image_names[camn][0],
                                             save_process_paths[camn], radius, shape, step_frame,
                                             back_subtractor_settings=back_sub_sets,
                                             blob_detector_settings=blob_det_sets)
                    continue

                elif fn_name == 'crop':
                    if kwargs['dynamic_crop']:
                        from_tracks = kwargs['from_tracks'] if 'from_tracks' in kwargs.keys() else False
                        csv_path = os.path.join(from_fn_path, rec_names[self.main_camn], 'cam{0}'.format(camn))
                        coords_objs = load_2d_coords(frames[camn], csv_path, save_process_paths[camn],
                                                     image_sizes[camn],
                                                     from_tracks, show_plot=show_plot)

                    else:
                        coords_objs = {'obj0': {'frame': frames[camn],
                                                'x_px': np.ones(np.shape(frames[camn])) * kwargs['x_crop'][camn-1],
                                                'y_px': np.ones(np.shape(frames[camn])) * kwargs['y_crop'][camn-1]}}

                    obj_names = coords_objs.keys()

                elif fn_name == 'save_avi':
                    frame_rate = 24 if 'frame_rate' not in kwargs.keys() else kwargs['frame_rate']
                    lossless = False if 'lossless' not in kwargs.keys() else kwargs['lossless']

                if camn not in img.keys(): img[camn] = {}

                for obj_name in obj_names:
                    if obj_name not in img[camn].keys():
                        img[camn][obj_name] = {}

                    # if fn_name not in ['track2d', 'recon3d'] \
                    #        and obj_name == 'obj0' and 'from_fn_name' in kwargs.keys(): break

                    if fn_name == 'save_avi':
                        if 'obj0' == obj_name: save_name = rec_names[camn] + '.avi'
                        else: save_name = rec_names[camn] + '-' + obj_name + '.avi'

                        if len(np.shape(np.asarray(img[camn][obj_name][0]))) != 2: break
                        height_img, width_img = np.shape(np.asarray(img[camn][obj_name][0]))

                        codec = cv2.VideoWriter_fourcc(*'HFYU') if lossless else cv2.VideoWriter_fourcc(*'MP42')
                        writer = cv2.VideoWriter(os.path.join(save_process_paths[camn], save_name), codec, frame_rate,
                                                 (width_img, height_img))

                    for ind_img, image_name in enumerate(image_names[camn]):
                        if step_frame != 1 and (frames[camn][ind_img] + 1) % step_frame != 0: continue

                        if ind_img not in img[camn][obj_name].keys():
                            img[camn][obj_name][ind_img] = read_image(os.path.join(image_paths[camn], image_name))

                        if not img[camn][obj_name][ind_img]: continue

                        if fn_name == 'crop':
                            coords_obj = {key: coords_objs[obj_name][key][ind_img] for key in coords_objs[obj_name].keys()}
                            
                            img[camn][obj_name][ind_img] = crop_image(img[camn][obj_name][ind_img], coords_obj,
                                                                      kwargs['crop_height'], kwargs['crop_width'])

                        elif fn_name == 'rotate':
                            img[camn][obj_name][ind_img] = img[camn][obj_name][ind_img].rotate(kwargs['degrees'])

                        elif fn_name == 'resize':
                            img[camn][obj_name][ind_img] = img_utils.resize(img[camn][obj_name][ind_img], kwargs['resize_ratio'])

                        elif fn_name in ['enhance_contrast', 'enhance_brightness', 'enhance_sharpness', 'enhance_color',
                                         'autocontrast', 'equalize']:
                            img[camn][obj_name][ind_img] = img_utils.enhance(img[camn][obj_name][ind_img], fn_name, kwargs['factor'])

                        elif fn_name == 'save_avi':
                            writer.write(cv2.cvtColor(np.asarray(img[camn][obj_name][ind_img]), cv2.COLOR_GRAY2BGR))

                    if fn_name == 'save_avi': writer.release()

                    if 'step_frame' in kwargs.keys() or multi_processes_num == multi_processes_nums[-1]:  # Will save images
                        if fn_name in ['crop', 'rotate', 'resize', 'sample', 'enhance_contrast', 'enhance_brightness',
                                       'enhance_sharpness', 'enhance_color', 'autocontrast', 'equalize']:
                            for ind_img, image_name in enumerate(image_names[camn]):
                                if 'obj0' == obj_name:
                                    image_save_name = image_name

                                else:
                                    image_save_name = image_name[:image_name.rfind('.')] + '-' + obj_name + image_name[image_name.rfind('.'):]

                                img[camn][obj_name][ind_img].save(os.path.join(save_process_paths[camn], image_save_name))

                if os.path.exists(save_process_paths[camn]) and not os.listdir(save_process_paths[camn]):  # if save_path empty
                    shutil.rmtree(save_process_paths[camn], ignore_errors=True)

            if fn_name == 'recon3d':
                pts_csv = {}
                for camn in range(1, self.nb_cam + 1):
                    pts_csv[camn] = np.genfromtxt(os.path.join(save_process_paths[camn], rec_names[camn] + '-2d_points.csv'),
                                                  delimiter=',', skip_header=0, names=True)

                recon_sets = Reconstructor3DSettings() if 'reconstructor_settings' not in kwargs.keys() \
                    else Reconstructor3DSettings.from_dict(kwargs['reconstructor_settings'])
                reconstruct_3d_tracks(pts_csv, kwargs['dlt_path'], rec_names, save_process_paths, show_plot=show_plot,
                                      reconstructor_settings=recon_sets)

            elif fn_name == 'load_2d_tracks':
                if 'csv_paths' in kwargs.keys() and kwargs['csv_paths'] is not None:
                    save_2d_tracks_from_paths(kwargs['csv_paths'], rec_names, save_process_paths)

                else:
                    save_2d_tracks_from_dict(kwargs['tracks_dict'], rec_names, save_process_paths)

            elif fn_name == 'stitch':
                img = stitch_images_multiprocessing(img, image_names, image_paths, frames, save_process_paths, obj_names,
                                                    multi_processes_num, multi_processes_nums, self.main_camn)

            elif fn_name in ['save_tiff', 'analyse_dlc', 'load_dlc', 'fit_skeleton', 'plot_skeleton', 'delete']:
                raise ValueError("Function {0} has not been implemented yet to be done using multiprocessing".format(fn_name))

            # Save process history in yaml file
            cur_settings = copy.deepcopy(settings)
            for i, step in enumerate(cur_settings.protocol.steps):
                if cur_settings.protocol.nums[i] > multi_processes_num:
                    del cur_settings.protocol.steps[i]

            cur_settings.to_yaml(os.path.join(save_process_path, 'process_history.yaml'))

            print('>> fn {0} as been performed (time elapsed: {1:.4f} s)'.format(fn_name, time.time() - start))

    @staticmethod
    def _print_process(fn_name: str, kwargs: Dict[str, any]) -> None:
        print('======================================================================================')

        if fn_name == 'multi_processes':
            print('> Starting batch images: Running multiple processes')
        elif fn_name == 'sample':
            print('> Starting batch images: Copying samples')
        elif fn_name == 'resize':
            print('> Starting batch images: Up/Down-sizing images (ratio: {0})'.format(kwargs['resize_ratio']))
        elif fn_name == 'enhance_contrast':
            print('> Starting batch images: Enhancing contrast of images (factor: {0})'.format(kwargs['factor']))
        elif fn_name == 'enhance_brightness':
            print('> Starting batch images: Enhancing brightness of images (factor: {0})'.format(kwargs['factor']))
        elif fn_name == 'enhance_sharpness':
            print('> Starting batch images: Enhancing sharpness of images (factor: {0})'.format(kwargs['factor']))
        elif fn_name == 'enhance_color':
            print('> Starting batch images: Enhancing color of images (factor: {0})'.format(kwargs['factor']))
        elif fn_name == 'autocontrast':
            print('> Starting batch images: Auto-Enhancing contrast of images')
        elif fn_name == 'equalize':
            print('> Starting batch images: Equalizing of images')
        elif fn_name == 'crop':
            print('> Starting batch images: Cropping images (height: {0}, width: {1})'.format(kwargs['crop_height'],
                                                                                              kwargs['crop_width']))
        elif fn_name == 'rotate':
            print('> Starting batch images: Rotating cameras views {0} at {1} degrees '.format(kwargs['camn_to_process'],
                                                                                               kwargs['degrees']))
        elif fn_name == 'stitch':
            print('> Starting batch images: Stitching all cameras views together')
        elif fn_name == 'track2d':
            print('> Starting batch images: Tracking 2d coordinates of blobs')
        elif fn_name == 'recon3d':
            print('> Starting batch images: Reconstructing 3d tracks from 2d coordinates')
        elif fn_name == 'save_tiff':
            print('> Starting batch images: Saving recordings as 8 bits .tiff')
        elif fn_name == 'save_strobe':
            print('> Starting batch images: Generating stroboscopic images from 2d coordinates')
        elif fn_name == 'save_avi':
            print('> Starting batch images: Saving recordings as .avi')
        elif fn_name == 'analyse_dlc':
            print('> Starting batch images: Analysing videos with DeepLabCut (model: {0})'.format(kwargs['model_name']))
        elif fn_name == 'load_dlc':
            print('> Starting batch images: Load points from DeepLabCut '
                  '(unscramble, reconstruct3d ... - model: {0})'.format(kwargs['model_name']))
        elif fn_name == 'fit_skeleton':
            print('> Starting batch images: Optimizing fit btw skeletons to get transformation/rotation parameters')
        elif fn_name == 'plot_skeleton':
            print('> Starting batch images: Plot (or save) skeleton on raw images')
        elif fn_name == 'delete':
            print('> Starting batch images: Deleting following recordings: {0}'.format(kwargs['to_del_names']))

    def _get_path_process(self, fn_name: str = '') -> str:

        if fn_name == 'crop':
            process_path = os.path.join(self.save_path, '_Cropped')
        elif fn_name == 'sample':
            process_path = os.path.join(self.save_path, '_Sample')
        elif fn_name == 'resize':
            process_path = os.path.join(self.save_path, '_Resized')
        elif fn_name in ['enhance_contrast', 'enhance_brightness', 'enhance_sharpness',
                         'enhance_color', 'autocontrast', 'equalize']:
            process_path = os.path.join(self.save_path, '_Enhanced')
        elif fn_name == 'save_tiff':
            process_path = os.path.join(self.save_path, '_Tiff')
        elif fn_name == 'save_strobe':
            process_path = os.path.join(self.save_path, '_Strobe')
        elif fn_name == 'save_avi':
            process_path = os.path.join(self.save_path, '_Avi')
        elif fn_name == 'stitch':
            process_path = os.path.join(self.save_path, '_Stitched')
        elif fn_name in ['analyse_dlc', 'load_dlc', 'fit_skeleton']:
            process_path = os.path.join(self.save_path, '_DLC')
        elif fn_name == 'plot_skeleton':
            process_path = os.path.join(self.save_path, '_Plots')

        elif fn_name in ['track2d', 'recon3d', 'load_2d_tracks', 'manual_tracking']:
            process_path = os.path.join(self.save_path, '_Tracking')

        elif fn_name in ['', 'delete', 'multi_processes', 'rotate']:
            process_path = None

        else:
            raise ValueError('Unknown function name (fn_name = {0})'.format(fn_name))

        return process_path

    def _init_batch_dict(self, recording: MultiImageSequence, rec_paths: Dict[str, str],
                         frames: Dict[str, List[int]]) -> Dict[any, any]:

        all_frames = []
        for camn in range(1, self.nb_cam + 1):
            all_frames = list(set(all_frames + frames[camn])) if all_frames else frames[camn]

        image_sizes, frame_rates, missing_frames = {}, {}, {}
        for camn in range(1, self.nb_cam + 1):
            missing_frames[camn] = list(set(all_frames) - set(frames[camn])).sort()

            image_sizes[camn] = \
                Image.open(os.path.join(recording.get(camn).get_path(), recording.get(camn).get_names()[0])).size

            frame_rates[camn] = recording.frame_rates[camn]

        batch_dict = {'nb_cam': self.nb_cam, 'image_sizes': image_sizes, 'frame_rates': frame_rates,
                      'first_frame': all_frames[0], 'last_frame': all_frames[-1], 'missing_frames': missing_frames,
                      'recording_paths': rec_paths, 'save_path': self.save_path}
        
        return batch_dict

    @staticmethod
    def _delete_recordings(rec_names: Dict[str, List[str]], rec_paths: Dict[str, List[str]]) -> None:

        for camn in rec_paths.keys():

            if not len(rec_names[camn]) == 0:
                if not os.path.exists(os.path.join(rec_paths[camn], 'to_delete')):
                    os.makedirs(os.path.join(rec_paths[camn], 'to_delete'))

                for rec_name in rec_names:
                    os.rename(os.path.join(rec_paths[camn], rec_name),
                              os.path.join(rec_paths[camn], 'to_delete', rec_name))
                    # shutil.rmtree(os.path.join(self.cam_paths[camn], rec_name), ignore_errors=True)

                    # print('>> recording from {0} has been deleted'.format(time.strftime('%Y%m%d_%H%M%S', rec_dates[camn])))




