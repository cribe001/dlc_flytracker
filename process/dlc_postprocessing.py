import copy, glob, os, pickle

import pandas as pd
import numpy as np
import yaml

import matplotlib.pyplot as plt
from PIL import Image

from typing import Dict, List, Tuple

from images.multiimagesequence import MultiImageSequence

from point_tracker import Reconstructor3D
from point_tracker.reconstructor3d import Reconstructor3DSettings

from point_tracker.tracker2d import Tracker2D, BlobDetectorSettings, BackgroundSubtractorSettings

from utils.settings import Settings, BatchSettings, ProtocolSettings


def load_dlc_from_csv(batch_settings: BatchSettings, model_name: str, tracker_method: str, path: str) -> Dict[str, any]:
    """

    Args:
        batch_settings:
        model_name:
        tracker_method:
        path:

    Returns:
        skeleton2d:
    """
    frames = list(range(batch_settings.first_frame, batch_settings.last_frame))

    nb_cam = batch_settings.nb_cam
    missing_frames = list(np.unique(np.concatenate([batch_settings.missing_frames[camn] for camn in range(1, nb_cam + 1)])))
    [missing_frames.remove(missing_frame) for missing_frame in list(set(missing_frames) - set(frames))]
    [frames.remove(missing_frame) for missing_frame in missing_frames]

    if tracker_method == 'skeleton': tracker_method_str = '_sk'
    elif tracker_method == 'box': tracker_method_str = '_bk'

    dlc_csv_paths = glob.glob(os.path.join(path, '*' + model_name + tracker_method_str + '.csv'))

    obj_names = []
    for dlc_csv_path in dlc_csv_paths: # go throught the various obj
        obj_names.append(dlc_csv_path[len(path) + 1:dlc_csv_path.index(model_name)])

    skeleton2d = {'obj_names': obj_names, 'model_name': model_name}
    for i, obj_name in enumerate(obj_names):
        df = pd.read_csv(dlc_csv_paths[i], header=[1, 2])
        label_names = np.unique([s[0][:-2] for s in df.keys()[1:]])
        new_label_names = [label.replace(' ', '_') for label in label_names]

        skeleton2d[obj_name] = {'frames': frames, 'label_names': new_label_names}
        for j, label in enumerate(label_names):
            new_label = new_label_names[j]

            skeleton2d[obj_name][new_label] = {1: {}}
            for camn in range(1, nb_cam + 1):
                label_key = label + ' {0}'.format(camn)

                skeleton2d[obj_name][new_label][camn] = {'x': np.array(df[(label_key, 'x')]),
                                                              'y': np.array(df[(label_key, 'y')]),
                                                              'likelihood': np.array(df[(label_key, 'likelihood')])}

    return skeleton2d


def load_dlc_from_full_pickle(batch_settings: BatchSettings, model_name: str, path: str):
    """

    Args:
        batch_settings:
        model_name:
        path:

    Returns:
        skeleton2d:
    """
    all_frames = list(range(batch_settings.first_frame, batch_settings.last_frame + 1))

    nb_cam = batch_settings.nb_cam

    if hasattr(batch_settings, 'missing_frames'):
        missing_frames = list(np.unique(np.concatenate([batch_settings.missing_frames[camn] for camn in range(1, nb_cam + 1)])))
        [missing_frames.remove(missing_frame) for missing_frame in list(set(missing_frames) - set(all_frames))]
        [all_frames.remove(missing_frame) for missing_frame in missing_frames]

    dlc_pickle_paths = glob.glob(os.path.join(path, '*' + model_name + '_full.pickle'))

    obj_names = []
    for dlc_pickle_path in dlc_pickle_paths:  # go throught the various obj
        file_name = dlc_pickle_path[len(path) + 1:]
        obj_names.append(file_name[file_name.index('obj'):file_name.index(model_name)])

    assert len(obj_names) > 0, "ERROR: No pickle files were found! (The file name might be incorrect)"

    skeleton2d = {'obj_names': obj_names, 'model_name': model_name}
    for obj_name in obj_names:
        with open(dlc_pickle_paths[0], "rb") as file:
            full_dict = pickle.load(file)

        header = full_dict.pop("metadata")
        label_names = np.unique([s[:-2] for s in header["all_joints_names"]])
        new_label_names = [label.replace(' ', '_') for label in label_names]

        frame_names = list(full_dict.keys())
        frames = all_frames[batch_settings.last_frame - (int(frame_names[-1][5:]) + 1):]

        skeleton2d[obj_name] = {'frames': frames, 'label_names': new_label_names}
        for i, label in enumerate(label_names):
            new_label = new_label_names[i]

            skeleton2d[obj_name][new_label] = {1: {}}
            for camn in range(1, nb_cam + 1):
                skeleton2d[obj_name][new_label][camn] = {'x': np.nan * np.ones(len(frames)),
                                                         'y': np.nan * np.ones(len(frames)),
                                                         'likelihood': np.nan * np.ones(len(frames))}

        for frame_in, frame_name in enumerate(frame_names):
            for i, label in enumerate(label_names):
                new_label = new_label_names[i]

                for camn in range(1, nb_cam + 1):
                    label_in = header["all_joints_names"].index(label + ' {0}'.format(camn))

                    if len(full_dict[frame_name]['confidence'][label_in]) == 0: continue
                    best_in = np.argmax(full_dict[frame_name]['confidence'][label_in])

                    skeleton2d[obj_name][new_label][camn]['x'][frame_in] = \
                        full_dict[frame_name]['coordinates'][0][label_in][best_in][0]
                    skeleton2d[obj_name][new_label][camn]['y'][frame_in] = \
                        full_dict[frame_name]['coordinates'][0][label_in][best_in][1]
                    skeleton2d[obj_name][new_label][camn]['likelihood'][frame_in] = \
                        full_dict[frame_name]['confidence'][label_in][best_in]

        # Keep only the existing frames
        is_nan_frame = np.isnan(skeleton2d[obj_name]['frames'])
        for camn in range(1, nb_cam + 1):
            is_nan_frame = is_nan_frame & np.isnan(skeleton2d[obj_name][new_label][camn]['likelihood'])

        in_is_nan_frame = np.where(~is_nan_frame)[0]

        skeleton2d[obj_name]['frames'] = np.asarray(skeleton2d[obj_name]['frames'])[in_is_nan_frame]
        for label in skeleton2d[obj_name]['label_names']:
            for camn in range(1, nb_cam + 1):
                for key in skeleton2d[obj_name][label][camn].keys():
                    skeleton2d[obj_name][label][camn][key] = \
                        np.asarray(skeleton2d[obj_name][label][camn][key])[in_is_nan_frame]

    return skeleton2d


def load_dlc_from_hdf5(batch_settings: BatchSettings, model_name: str, tracker_method: str, path: str) -> Dict[str, any]:
    """

    Args:
        batch_settings:
        model_name:
        tracker_method:
        path:

    Returns:
        skeleton2d:
    """

    frames = list(range(batch_settings.first_frame, batch_settings.last_frame))

    nb_cam = batch_settings.nb_cam
    missing_frames = list(np.unique(np.concatenate([batch_settings.missing_frames[camn] for camn in range(1, nb_cam + 1)])))
    [frames.remove(missing_frame) for missing_frame in missing_frames]

    if tracker_method == 'skeleton': tracker_method_str = '_sk'
    elif tracker_method == 'box': tracker_method_str = '_bk'

    dlc_hdf5_paths = glob.glob(os.path.join(path, '*' + model_name + tracker_method_str + '.h5'))

    obj_names = []
    for dlc_hdf5_path in dlc_hdf5_paths:  # go throught the various obj
        obj_names.append(dlc_hdf5_path[len(path) + 22:dlc_hdf5_path.index(model_name)])

    skeleton2d = {'obj_names': obj_names, 'model_name': model_name}
    for i, obj_name in enumerate(obj_names):
        hdf5_dict = pd.read_hdf(dlc_hdf5_paths[i], 'df_with_missing')
        # hdf5_dict.to_csv(os.path.join(save_path, hdf5_name.split(".h5")[0] + '.csv'))

        # network_name = hdf5_dict.columns[0][0]
        hdf5_dict.columns = hdf5_dict.columns.droplevel([0, 1])  # remove first two lines of header (network_name, individual)

        label_names = np.unique([s[:-2] for s in hdf5_dict.columns.droplevel(1)])
        new_label_names = [label.replace(' ', '_') for label in label_names]
        # frames = hdf5_dict.index

        skeleton2d[obj_name] = {'frames': frames, 'label_names': new_label_names}
        for i, label in enumerate(label_names):
            new_label = new_label_names[i]

            skeleton2d[obj_name][new_label] = {1: {}}
            for camn in range(1, nb_cam + 1):
                label_key = label + ' {0}'.format(camn)

                skeleton2d[obj_name][new_label][camn] = {'x': np.array(hdf5_dict[(label_key, 'x')]),
                                                         'y': np.array(hdf5_dict[(label_key, 'y')]),
                                                         'likelihood': np.array(hdf5_dict[(label_key, 'likelihood')])}

                # skeleton2d[obj_name][new_label][camn] = [[1]*3 for i in range(len(hdf5_dict[(label_key, 'x')]))]
                # # skeleton2d[obj_name][new_label][camn] = np.ones((len(hdf5_dict[(label_key, 'x')]), 3))
                # skeleton2d[obj_name][new_label][camn][:, 0] = np.array(hdf5_dict[(label_key, 'x')])
                # skeleton2d[obj_name][new_label][camn][:, 1] = np.array(hdf5_dict[(label_key, 'y')])
                # skeleton2d[obj_name][new_label][camn][:, 2] = np.array(hdf5_dict[(label_key, 'likelihood')])

    return skeleton2d


def save_dlc2d_points_in_csv(skeleton2d: Dict[str, any], save_paths: Dict[any, str]) -> None:
    """

    Args:
        skeleton2d:
        save_paths:
    """

    for obj_name in skeleton2d['obj_names']:
        for label in skeleton2d[obj_name]['label_names']:

            nb_cam = len(skeleton2d[obj_name][label].keys())
            for camn in range(1, nb_cam + 1):
                if not os.path.exists(save_paths[camn]): os.makedirs(save_paths[camn])
                np.savetxt(os.path.join(save_paths[camn], obj_name + '-' + label + '-2d_points.csv'),
                           np.c_[skeleton2d[obj_name]['frames'], skeleton2d[obj_name][label][camn]['x'],
                                 skeleton2d[obj_name][label][camn]['y'],
                                 skeleton2d[obj_name][label][camn]['likelihood']],
                           delimiter=',', header='frame,x_px,y_px,likelihood')

            # # To save all in same .csv file
            # for camn in range(1, nb_cam + 1):
            #     if not os.path.exists(save_paths[camn]): os.makedirs(save_paths[camn])
            #     header = header + ', x{0}_px, y{0}_px, likelihood{0}'.format(camn)
            #     array_csv = np.concatenate((array_csv, np.c_[skeleton2d[obj_name][label][camn]['x'],
            #                                                  skeleton2d[obj_name][label][camn]['y'],
            #                                                  skeleton2d[obj_name][label][camn]['likelihood']), axis=1)
            #
            # np.savetxt(os.path.join(save_paths[self.main_camn], obj_name + '-' + label + '-2d_points.csv'),
            #            array_csv, delimiter=',', header=header)


def save_dlc3d_points_in_csv(skeleton3d: Dict[str, any], save_path: str) -> None:
    """

    Args:
        skeleton3d:
        save_path:
    """
    for obj_name in skeleton3d['obj_names']:
        for label in skeleton3d[obj_name]['label_names']:
            if not os.path.exists(save_path): os.makedirs(save_path)
            np.savetxt(os.path.join(save_path, obj_name + '-' + label + '-3d_points.csv'),
                       np.c_[skeleton3d[obj_name]['frames'], skeleton3d[obj_name][label]['x'],
                             skeleton3d[obj_name][label]['y'], skeleton3d[obj_name][label]['z']],
                       delimiter=',', header='frame,x,y,z')


def unscramble_views2d_dlc(skeleton2d: Dict[str, any], protocol_settings: ProtocolSettings) -> Dict[str, any]:
    """

    Args:
        skeleton2d:
        protocol_settings: ProtocolSettings

    Returns:
        skeleton2d:
    """

    # Run on unconverted 2d coords from dlc
    # shared_axis: 0 if the combination of cam share the x axis (top-left to top-right)
    # or 1 if they share the y axis (top-left to bottom left)

    process_functions = protocol_settings.fns
    crop_num = process_functions.index('crop') + 1
    crop_num = crop_num[-1] if isinstance(crop_num, list) else crop_num
    width_crop = protocol_settings.get(crop_num).kwargs['width_crop']
    height_crop = protocol_settings.get(crop_num).kwargs['height_crop']

    for obj_name in skeleton2d['obj_names']:

        # Detect body parts in wrong view
        for label in skeleton2d[obj_name]['label_names']:
            is_nan, new_camns = {}, {}
            sum_in_out = np.zeros(np.size(skeleton2d[obj_name]['frames']))

            nb_cam = len(skeleton2d[obj_name][label].keys())
            for camn in range(1, nb_cam + 1):
                is_nan[camn] = np.isnan(skeleton2d[obj_name][label][camn]['x'])

                # in_out will be 0 if the camera view is correct, otherwise an int (-1, 1, 2, etc)
                in_out_x = np.ceil((skeleton2d[obj_name][label][camn]['x'] - (camn - 1) * width_crop) / width_crop) - 1
                in_out_y = np.ceil((skeleton2d[obj_name][label][camn]['y']) / height_crop) - 1

                new_camns[camn] = np.ones(np.size(in_out_x)) *camn
                for i, frame in enumerate(skeleton2d[obj_name]['frames']): # assumes that stitching was done along x axis
                    if (in_out_x[i] + camn) < 1 or (in_out_x[i] + camn) > nb_cam: new_camns[camn][i] = np.nan
                    elif in_out_y[i] < 0 or in_out_y[i] > 0: new_camns[camn][i] = np.nan
                    else: new_camns[camn][i] = in_out_x[i] + camn

                sum_in_out += new_camns[camn] - camn

            skeleton2d_cur_bodypart = copy.deepcopy(skeleton2d[obj_name][label])
            for camn in range(1, nb_cam + 1):
                skeleton2d_cur_bodypart[camn]['x'] *= np.nan
                skeleton2d_cur_bodypart[camn]['y'] *= np.nan

            for i, frame in enumerate(skeleton2d[obj_name]['frames']):
                for camn in range(1, nb_cam + 1):
                    new_camn = new_camns[camn][i]
                    if np.isnan(new_camn) or is_nan[camn][i]: continue

                    if sum_in_out[i] == 0 or (is_nan[new_camn][i] and sum_in_out[i] != 0):
                        skeleton2d_cur_bodypart[new_camn]['x'][i] = skeleton2d[obj_name][label][camn]['x'][i]
                        skeleton2d_cur_bodypart[new_camn]['y'][i] = skeleton2d[obj_name][label][camn]['y'][i]

                    elif not np.isnan(new_camns[new_camn][i]):
                        skeleton2d_cur_bodypart[new_camn]['x'][i] = skeleton2d[obj_name][label][new_camn]['x'][i]
                        skeleton2d_cur_bodypart[new_camn]['y'][i] = skeleton2d[obj_name][label][new_camn]['y'][i]

            # if self.show_plot:
            #     fig, axs = plt.subplots(1, nb_cam)
            #     for i, camn in enumerate(range(1, nb_cam + 1)):
            #         axs[i].clear()
            #         axs[i].plot(skeleton2d[obj_name][label][camn]['x'], skeleton2d[obj_name][label][camn]['y'])
            #         axs[i].plot(skeleton2d_cur_bodypart[camn]['x'], skeleton2d_cur_bodypart[camn]['y'])
            #         axs[i].set_ylabel('y pos (m) - ' + label)
            #         axs[i].set_xlabel('x pos [m]')
            #     plt.show()

            skeleton2d[obj_name][label] = skeleton2d_cur_bodypart

    return skeleton2d


def save_stroboscopic_images(image_sequence: MultiImageSequence, frames: List[int], coords_objs: Dict[str, Dict[str, float]],
                             save_name: str, save_path: str, radius: int, shape, step_frame: int,
                             back_subtractor_settings: BackgroundSubtractorSettings,
                             blob_detector_settings: BlobDetectorSettings) -> None:
    """

    Args:
        image_sequence:
        frames:
        coords_objs:
        save_name:
        save_path:
        radius:
        shape:
        step_frame:
        back_subtractor_settings:
        blob_detector_settings:
    """

    if len(coords_objs.keys()) == 0:
        print('WARM: coords_objs is empty!')
        return

    tracker = Tracker2D.load_images(image_sequence.get_names(), image_sequence.get_path(), frames)

    tracker.get_back_subtractor(back_subtractor_settings)
    tracker.get_blob_detector(blob_detector_settings)

    tracker.dist2Threshold = 7

    for obj_name in coords_objs.keys():
        tracker.points = {'x': np.array(coords_objs[obj_name]['x_px']), 'y': np.array(coords_objs[obj_name]['y_px']),
                          'frame': np.array(coords_objs[obj_name]['frame']),
                          'area': np.ones(np.size(coords_objs[obj_name]['x_px'])) * np.nan}

        tracker.gen_stroboscopic_image(radius, shape, step_frame, flip_time=True)
        tracker.save_stroboscopic_image(save_name + '-' + obj_name, save_path)


# def unscramble_sides2d_and_recon3d_dlc(cutoff_frequency, frame_rate, skeleton2d, dlt_path,
#                                        reconstructor_settings: Reconstructor3DSettings, show_plot=False):
#     # TODO! check why do not work as planed or delete
#
#     reconstructor = Reconstructor3D.read_dlt_coefs(dlt_path)
#     reconstructor.settings = reconstructor_settings
#     nb_cam = reconstructor.nb_cam
#
#     # Unscamble case where side is wrong on only a minority of cam views
#     skeleton3d = {'obj_names': skeleton2d['obj_names'], 'model_name': skeleton2d['model_name']}
#     for obj_name in skeleton2d['obj_names']:
#         label_names_right = [s for s in skeleton2d[obj_name]['label_names'] if 'right' in s]
#         other_label_names = [s for s in skeleton2d[obj_name]['label_names'] if
#                              'right' not in s or 'left' not in s]
#
#         skeleton3d[obj_name] = {'frames': skeleton2d[obj_name]['frames'],
#                                      'label_names': skeleton2d[obj_name]['label_names']}
#         for label in skeleton3d[obj_name]['label_names']:
#             skeleton3d[obj_name][label] = {'x': np.nan * np.ones(np.size(skeleton2d[obj_name]['frames'])),
#                                                 'y': np.nan * np.ones(np.size(skeleton2d[obj_name]['frames'])),
#                                                 'z': np.nan * np.ones(np.size(skeleton2d[obj_name]['frames']))}
#
#         # Compute angles btw the right and left limb/bodyparts
#         angles, angles_out, mean_angles = {}, {}, {}
#         for i, camn in enumerate(range(1, nb_cam + 1)):
#             angles[camn], angles_out[camn] = {}, {}
#             mean_angles[camn] = np.zeros(np.size(skeleton2d[obj_name][label_names_right[0]][camn]['x']))
#             for label_right in label_names_right:
#                 label_left = label_right.replace('right', 'left')
#                 label = label_right.replace('right', '')
#
#                 x_right = skeleton2d[obj_name][label_right][camn]['x']
#                 y_right = skeleton2d[obj_name][label_right][camn]['y']
#                 x_left = skeleton2d[obj_name][label_left][camn]['x']
#                 y_left = skeleton2d[obj_name][label_left][camn]['y']
#
#                 angles[camn][label] = np.arctan2(y_right - y_left, x_right - x_left)
#                 #angles[camn][label] = np.arctan((y_right - y_left) / (x_right - x_left))
#
#                 angles[camn][label] = abs(angles[camn][label]) # doesn't not work when pi jump btw pi/2 and -pi/2
#
#                 # angles[camn][label] = np.fmod(angles[camn][label], 2 * np.pi)
#                 # angles[camn][label][~np.isnan(angles[camn][label])] = \
#                 #     np.unwrap(angles[camn][label][~np.isnan(angles[camn][label])], discont=2 * np.pi)
#                 # # angles[camn][label][~np.isnan(angles[camn][label])] = \
#                 # #    np.unwrap(angles[camn][label][~np.isnan(angles[camn][label])] * 2, discont=2 * np.pi * 0.8) / 2
#
#                 is_nan, x = np.isnan(angles[camn][label]), lambda z: z.nonzero()[0]
#                 if sum(is_nan) > 0:  # interpolation needed for filter_high_freq
#                     angles[camn][label][is_nan] = np.interp(x(is_nan), x(~is_nan), angles[camn][label][~is_nan])
#
#                 w = cutoff_frequency / (frame_rate / 2)  # Normalize the frequency
#                 b, a = signal.butter(2, w, 'low')
#
#                 mean_angles[camn] += signal.filtfilt(b, a, angles[camn][label])
#
#             mean_angles[camn] = mean_angles[camn] / len(label_names_right)
#
#             min_angle = 0
#             for label_right in label_names_right:
#                 label = label_right.replace('right', '')
#                 in_out = np.where(abs(angles[camn][label] - mean_angles[camn]) > np.pi / 2)
#                 angles_out[camn][label] = [s if i in in_out[0] else np.nan for i, s in enumerate(angles[camn][label])]
#                 min_angle = np.min([np.min(angles[camn][label]), min_angle])
#
#         if show_plot:
#             fig1, axs1 = plt.subplots(nb_cam, 1)
#             fig1.suptitle('Angle of the line btw right and left limb part - Before unscrambling')
#             for i, camn in enumerate(range(1, nb_cam + 1)):
#                 axs1[i].clear()
#                 for label_right in label_names_right:
#                     label = label_right.replace('right', '')
#                     frames = list(range(0, len(angles_out[camn][label])))
#
#                     axs1[i].scatter(frames, angles[camn][label], label=label, marker='.')
#                     axs1[i].scatter(frames, angles_out[camn][label], c='r', label='out', marker='*')
#
#                 axs1[i].plot(mean_angles[camn], 'k', label='mean')
#                 axs1[i].set_ylabel('angle (rad)')
#                 axs1[i].set_xlabel('frames - ' + label_right)
#
#         for i, frame in enumerate(skeleton2d[obj_name]['frames']):
#             for label_right in label_names_right:
#                 label_left = label_right.replace('right', 'left')
#                 label = label_right.replace('right', '')
#
#                 #if all([np.isnan(angles_out[camn][label][i]) for camn in range(1, nb_cam + 1)]): continue
#
#                 xs_coord, ys_coord, frames_coord, likelihood_coords = [], [], [], []
#                 for camn in range(1, nb_cam + 1):
#                     if not np.isnan(angles_out[camn][label][i]):  # Switch side
#                         skeleton2d[obj_name][label_right][camn]['x'][i], skeleton2d[obj_name][label_left][camn]['x'][i] = \
#                             skeleton2d[obj_name][label_left][camn]['x'][i], skeleton2d[obj_name][label_right][camn]['x'][i]
#                         skeleton2d[obj_name][label_right][camn]['y'][i], skeleton2d[obj_name][label_left][camn]['y'][i] = \
#                             skeleton2d[obj_name][label_left][camn]['y'][i], skeleton2d[obj_name][label_right][camn]['y'][i]
#                         skeleton2d[obj_name][label_right][camn]['likelihood'][i], skeleton2d[obj_name][label_left][camn]['likelihood'][i] = \
#                             skeleton2d[obj_name][label_left][camn]['likelihood'][i], skeleton2d[obj_name][label_right][camn]['likelihood'][i]
#
#                     x_right = skeleton2d[obj_name][label_right][camn]['x'][i].copy()
#                     y_right = skeleton2d[obj_name][label_right][camn]['y'][i].copy()
#                     x_left = skeleton2d[obj_name][label_left][camn]['x'][i].copy()
#                     y_left = skeleton2d[obj_name][label_left][camn]['y'][i].copy()
#
#                     likelihood_right = skeleton2d[obj_name][label_right][camn]['likelihood'][i]
#                     likelihood_left = skeleton2d[obj_name][label_left][camn]['likelihood'][i]
#
#                     likelihood_coords.append([likelihood_right, likelihood_left])
#                     xs_coord.append([x_right, x_left])
#                     ys_coord.append([y_right, y_left])
#                     frames_coord.append([frame, frame])
#
#                 xs_pose, ys_pose, zs_pose, frames_pose, indexes, rmses, = \
#                     reconstructor.recon_objs(xs_coord, ys_coord, frames_coord, sort_by_rmse=False)
#
#                 # if len(xs_pose) == 0:
#                 #     xs_pose, ys_pose, zs_pose = [[np.nan, np.nan]], [[np.nan, np.nan]], [[np.nan, np.nan]]
#                 #     indexes = [[np.array([0, 0, 0]), np.array([1, 1, 1])]]
#                 #
#                 # else:
#                 #     rmses
#                 #     # order rmse
#                 #
#                 # cpt = 0
#                 # while len(xs_pose[0]) > cpt:
#                 #     # Remove duplicate usage of points and give priority to first rows (with lowest rmse)
#                 #     # should have max two rows in the end (one per side)
#                 #
#                 #     comparison_indexes = np.array([False] * nb_cam)  # False if different from previous rows
#                 #     for j in range(1, cpt + 1):
#                 #         comparison_indexes += np.array(
#                 #             [indexes[0][cpt][k] == indexes[0][cpt - j][k] for k in range(0, nb_cam)])
#                 #
#                 #     # Remove coordinates because it use at least one 2d points that was used already
#                 #     if comparison_indexes.any():
#                 #         xs_pose[0] = [s for k, s in enumerate(xs_pose[0]) if k != cpt]
#                 #         ys_pose[0] = [s for k, s in enumerate(ys_pose[0]) if k != cpt]
#                 #         indexes[0] = [s for k, s in enumerate(indexes[0]) if k != cpt]
#                 #     else:
#                 #         cpt += 1
#                 #
#                 # if len(xs_pose[0]) == 1:
#                 #     xs_pose[0].append(np.nan), ys_pose[0].append(np.nan), zs_pose[0].append(np.nan)
#                 #     indexes[0].append([abs(index - 1) for index in indexes[0]][0])
#                 #
#                 # # if np.nansum(indexes[0][0]) > np.nansum(indexes[0][1]) \
#                 # #         and indexes[0][1][1] == 0:  # use top cam (#2) as reference for identifying side
#                 # if indexes[0][1][1] == 0:  # use top cam (#2) as reference for identifying side
#                 #     indexes[0][0], indexes[0][1] = indexes[0][1], indexes[0][0]
#                 #     xs_pose[0].reverse(), ys_pose[0].reverse(), zs_pose[0].reverse()
#
#                 if len(xs_pose) == 0:
#                     xs_pose, ys_pose, zs_pose = [[np.nan, np.nan]], [[np.nan, np.nan]], [[np.nan, np.nan]]
#                     indexes = [[np.array([0, 0, 0]), np.array([1, 1, 1])]]
#
#                 elif len(xs_pose[0]) == 1:
#                     xs_pose[0].append(np.nan), ys_pose[0].append(np.nan), zs_pose[0].append(np.nan)
#                     indexes[0].append([abs(index - 1) for index in indexes[0]][0])
#                 #
#                 # elif len(xs_pose[0]) > 2:
#                 #     indexes_top_right = np.array([s for s in indexes[0] if s[top_camn - 1] == 0.0]).astype(int)
#                 #     indexes_top_left = np.array([s for s in indexes[0] if s[top_camn - 1] == 1.0]).astype(int)
#                 #     rmses_top_right = np.array([rmses[0][i] for i, s in enumerate(indexes[0]) if s[top_camn - 1] == 0.0])
#                 #     rmses_top_left = np.array([rmses[0][i] for i, s in enumerate(indexes[0]) if s[top_camn - 1] == 1.0])
#                 #
#                 #     indexes = [[np.array([0, 0, 0]), np.array([1, 1, 1])]]
#                 #     xs_pose_new, ys_pose_new, zs_pose_new = [[np.nan, np.nan]], [[np.nan, np.nan]], [[np.nan, np.nan]]
#                 #     if len(rmses_top_right) > 1 and len(rmses_top_left) > 1:
#                 #         combi = list(itertools.product(list(range(0, len(rmses_top_right))), list(range(0, len(rmses_top_left)))))
#                 #
#                 #         new_combi = []
#                 #         for j in range(0, len(combi)):  # Keep only the combinations that are compatible
#                 #             if (indexes_top_right[combi[j][0]] + indexes_top_left[combi[j][1]] == [1, 1, 1]).all():
#                 #                 new_combi.append(combi[j])
#                 #
#                 #         if not len(new_combi) == 0:
#                 #             rmse_combi = [np.sqrt(rmses_top_right[s[0]] ** 2 + rmses_top_left[s[1]] ** 2) for s in new_combi]
#                 #             best_combi = new_combi[np.argsort(rmse_combi)[0]]
#                 #
#                 #             indexes = [[indexes_top_right[best_combi[0]], indexes_top_left[best_combi[1]]]]
#                 #
#                 #         else:
#                 #             in_sort_right = np.argsort(rmses_top_right)
#                 #             in_sort_left = np.argsort(rmses_top_left)
#                 #
#                 #             if rmses_top_right[in_sort_right[0]] < rmses_top_left[in_sort_left[0]]:
#                 #                 indexes = [[indexes_top_right[in_sort_right[0]], abs(indexes_top_right[in_sort_right[0]] -1)]]
#                 #             else:
#                 #                 indexes = [[abs(indexes_top_left[in_sort_left[0]] -1), indexes_top_left[in_sort_left[0]]]]
#                 #
#                 #         xs_pose_new[0][0], ys_pose_new[0][0], zs_pose_new[0][0] = \
#                 #             xs_pose[0][indexes[0][0][0]], ys_pose[0][indexes[0][0][1]], zs_pose[0][indexes[0][0][2]]
#                 #         xs_pose_new[0][1], ys_pose_new[0][1], zs_pose_new[0][1] = \
#                 #             xs_pose[0][indexes[0][1][0]], ys_pose[0][indexes[0][1][1]], zs_pose[0][indexes[0][1][2]]
#                 #
#                 #     elif not len(rmses_top_right) == 0 and len(rmses_top_left) == 0:
#                 #         in_sort_right = np.argsort(rmses_top_right)
#                 #         in_sort_left = np.argsort(rmses_top_left)
#                 #
#                 #         if len(rmses_top_right) == 0:
#                 #             indexes[0][0] = abs(indexes_top_left[in_sort_left[0]] - 1)
#                 #         else:
#                 #             indexes[0][0] = indexes_top_right[in_sort_right[0]]
#                 #             xs_pose_new[0][0], ys_pose_new[0][0], zs_pose_new[0][0] = \
#                 #                 xs_pose[0][indexes[0][0][0]], ys_pose[0][indexes[0][0][1]], zs_pose[0][indexes[0][0][2]]
#                 #
#                 #         if len(rmses_top_left) == 0:
#                 #             indexes[0][1] = abs(indexes_top_right[in_sort_right[0]] - 1)
#                 #         else:
#                 #             indexes[0][1] = indexes_top_left[in_sort_left[0]]
#                 #             xs_pose_new[0][1], ys_pose_new[0][1], zs_pose_new[0][1] = \
#                 #                 xs_pose[0][indexes[0][1][0]], ys_pose[0][indexes[0][1][1]], zs_pose[0][indexes[0][1][2]]
#                 #
#                 #     xs_pose, ys_pose, zs_pose = xs_pose_new, ys_pose_new, zs_pose_new
#                 #
#                 # if indexes[0][1][top_camn-1] == 0:  # use top cam as reference for identifying side
#                 #     indexes[0][0], indexes[0][1] = indexes[0][1], indexes[0][0]
#                 #     xs_pose[0].reverse(), ys_pose[0].reverse(), zs_pose[0].reverse()
#                 #
#                 # if (indexes[0] != np.array([0, 0, 0])).any():
#                 #     for j, camn in enumerate(range(1, nb_cam + 1)):
#                 #         if not np.isnan(indexes[0][0][j]) and not np.isnan(indexes[0][1][j]):
#                 #             in_right = int(indexes[0][0][j])
#                 #             skeleton2d[obj_name][label_right][camn]['x'][i] = xs_coord[j][in_right]
#                 #             skeleton2d[obj_name][label_right][camn]['y'][i] = ys_coord[j][in_right]
#                 #             skeleton2d[obj_name][label_right][camn]['likelihood'][i] = likelihood_coords[j][in_right]
#                 #
#                 #             in_left = int(indexes[0][1][j])
#                 #             skeleton2d[obj_name][label_left][camn]['x'][i] = xs_coord[j][in_left]
#                 #             skeleton2d[obj_name][label_left][camn]['y'][i] = ys_coord[j][in_left]
#                 #             skeleton2d[obj_name][label_left][camn]['likelihood'][i] = likelihood_coords[j][in_left]
#
#                 skeleton3d[obj_name][label_right]['x'][i], skeleton3d[obj_name][label_right]['y'][i], \
#                 skeleton3d[obj_name][label_right]['z'][i] = xs_pose[0][0], ys_pose[0][0], zs_pose[0][0]
#
#                 skeleton3d[obj_name][label_left]['x'][i], skeleton3d[obj_name][label_left]['y'][i], \
#                 skeleton3d[obj_name][label_left]['z'][i] = xs_pose[0][1], ys_pose[0][1], zs_pose[0][1]
#
#             for label in other_label_names:
#                 xs_coord, ys_coord, frames_coord = [], [], []
#                 for camn in range(1, nb_cam + 1):
#                     xs_coord.append([skeleton2d[obj_name][label][camn]['x'][i].copy()])
#                     ys_coord.append([skeleton2d[obj_name][label][camn]['y'][i].copy()])
#                     frames_coord.append([frame])
#
#                 xs_pose, ys_pose, zs_pose, frames_pose, indexes, rmses, = \
#                     reconstructor.recon_objs(xs_coord, ys_coord, frames_coord)
#
#                 if len(xs_pose) == 0:
#                     skeleton3d[obj_name][label]['x'][i], skeleton3d[obj_name][label]['y'][i], \
#                     skeleton3d[obj_name][label]['z'][i] = np.nan, np.nan, np.nan
#                 else:
#                     skeleton3d[obj_name][label]['x'][i], skeleton3d[obj_name][label]['y'][i], \
#                     skeleton3d[obj_name][label]['z'][i] = xs_pose[0][0], ys_pose[0][0], zs_pose[0][0]
#
#         if show_plot:
#             fig2, axs2 = plt.subplots(nb_cam, 1)
#             fig2.suptitle('Angle of the line btw right and left limb part - After unscrambling')
#             for i, camn in enumerate(range(1, nb_cam + 1)):
#                 axs2[i].clear()
#                 for label_right in label_names_right:
#                     label_left = label_right.replace('right', 'left')
#                     x_right = skeleton2d[obj_name][label_right][camn]['x']
#                     y_right = skeleton2d[obj_name][label_right][camn]['y']
#                     x_left = skeleton2d[obj_name][label_left][camn]['x']
#                     y_left = skeleton2d[obj_name][label_left][camn]['y']
#
#                     angles = np.arctan2((y_right - y_left), (x_right - x_left))
#                     #angles = np.arctan((y_right - y_left) / (x_right - x_left))
#                     #angles[~np.isnan(angles)] = np.unwrap(angles[~np.isnan(angles)] * 2, discont=2 * np.pi * 0.8) / 2
#                     angles = np.mod(angles, 2*np.pi)
#
#                     axs2[i].plot(angles, label=label_right.replace('right', ''))
#                 axs2[i].set_ylabel('angle (rad)')
#                 axs2[i].set_xlabel('frames - ' + label_right)
#             plt.legend
#             plt.show()


def reverse_processes_2d_points(skeleton2d: Dict[str, any], settings: Settings,
                                image_names: Dict[any, List[str]], image_paths: Dict[any, str],
                                threshold_likelihood: float, img_sizes: Dict[any, Tuple[int, int]], save_path: str,
                                show_plot: bool = False, main_camn: int=1) -> Dict[str, any]:
    """

    Args:
        skeleton2d:
        settings: Settings
        image_names:
        image_paths:
        threshold_likelihood:
        img_sizes:
        save_path:
        show_plot:
        main_camn:

    Returns:
        skeleton2d:
    """

    nb_cam = settings.batch.nb_cam

    for camn in range(1, nb_cam + 1):
        process_functions = settings.protocol.fns
        [width_img, height_img] = img_sizes[camn]

        for obj_name in skeleton2d['obj_names']:
            if 'stitch' in process_functions or 'rotate' in process_functions:
                if 'crop' in process_functions:
                    crop_num = process_functions.index('crop') +1
                    assert type(crop_num) is not list  # TODO! make it possible to have several time the same function
                    
                    width_crop = settings.protocol.get(crop_num).kwargs['width_crop']
                    height_crop = settings.protocol.get(crop_num).kwargs['height_crop']

                else:
                    width_crop, height_crop = width_img, height_img

            if 'crop' in process_functions:
                rec_name = os.path.basename(image_paths[main_camn])
                crop_path = os.path.join(save_path, '_Cropped')
                csv_path = glob.glob(os.path.join(crop_path, rec_name,
                                                  'cam{0}'.format(camn), '*' + obj_name + '-2d_points.csv'))

                pts_csv = np.genfromtxt(csv_path[0], delimiter=',', skip_header=0, names=True)

                tmp_set_frames = set(skeleton2d[obj_name]['frames'])
                in_frames = [i for i, f in enumerate(pts_csv['frame']) if f in tmp_set_frames]

                width_crop = settings.protocol.get(crop_num).kwargs['width_crop']
                height_crop = settings.protocol.get(crop_num).kwargs['height_crop']

                x_crop = np.interp(skeleton2d[obj_name]['frames'], pts_csv['frame'][in_frames],
                                   pts_csv['x_px'][in_frames], left=np.nan, right=np.nan) - width_crop / 2
                y_crop = np.interp(skeleton2d[obj_name]['frames'], pts_csv['frame'][in_frames],
                                   pts_csv['y_px'][in_frames], left=np.nan, right=np.nan) - height_crop / 2

                for i in range(0, len(x_crop)):
                    x_crop[i] = np.max([np.min([x_crop[i], width_img - width_crop]), 0])
                    y_crop[i] = np.max([np.min([y_crop[i], height_img - height_crop]), 0])

            for label in skeleton2d[obj_name]['label_names']:
                if 'stitch' in process_functions:
                    skeleton2d[obj_name][label][camn]['x'] += - width_crop * (camn - 1)

                if 'rotate' in process_functions:
                    rotate_num = process_functions.index('rotate') + 1
                    assert type(rotate_num) is not list

                    if camn in settings.protocol.get(rotate_num).kwargs['camn_to_process']:
                        x = skeleton2d[obj_name][label][camn]['x'].copy() - width_crop / 2
                        y = skeleton2d[obj_name][label][camn]['y'].copy() - height_crop / 2
                        teta = np.deg2rad(settings.protocol.get(rotate_num).kwargs['degrees'])

                        skeleton2d[obj_name][label][camn]['x'] = x * np.cos(teta) - y * np.sin(teta) + width_crop / 2
                        skeleton2d[obj_name][label][camn]['y'] = x * np.sin(teta) + y * np.cos(teta) + height_crop / 2

                if 'crop' in process_functions:
                    skeleton2d[obj_name][label][camn]['x'] += x_crop
                    skeleton2d[obj_name][label][camn]['y'] += y_crop

            if show_plot:
                modulo = min(round(len(image_names[camn])/50), 1)
                fig = plt.figure()
                for i, image_name in enumerate(image_names[camn]):
                    if i % modulo: continue  # plot every modulo frames
                    plt.clf()
                    plt.imshow(Image.open(os.path.join(image_paths[camn], image_name)))
                    plt.scatter(x_crop[0:i] + width_crop/2, y_crop[0:i] + height_crop/2, marker='+', c='g')
                    for label in skeleton2d[obj_name]['label_names']:
                        if skeleton2d[obj_name][label][camn]['likelihood'][i] > threshold_likelihood:
                            plt.scatter(skeleton2d[obj_name][label][camn]['x'][i],
                                        skeleton2d[obj_name][label][camn]['y'][i], marker='.', c='k')

                    plt.xlim((x_crop[i], x_crop[i] + width_crop))
                    plt.ylim((y_crop[i], y_crop[i] + height_crop))
                    plt.gca().invert_yaxis()
                    plt.title("DLC tracking results after reversing pre-processing "
                              "(cam #{0}, frame {1})".format(camn, skeleton2d[obj_name]['frames'][i]))
                    plt.xlabel('x [m]')
                    plt.ylabel('y [m]')
                    plt.pause(0.1)
                    #plt.show()

    return skeleton2d

# TODO remove following lines?
# def unwrap(skeleton2d, dlt_path, reconstructor_settings: Reconstructor3DSettings):
#     reconstructor = Reconstructor3D.read_dlt_coefs(dlt_path)
#     reconstructor.settings = reconstructor_settings
#     nb_cam = reconstructor.nb_cam
#
#     # Unscamble case where side is wrong on only a minority of cam views
#     skeleton3d = {'obj_names': skeleton2d['obj_names'], 'model_name': skeleton2d['model_name']}
#     for obj_name in skeleton3d['obj_names']:
#         label_names_right = [s for s in skeleton2d[obj_name]['label_names'] if 'right' in s]
#
#         skeleton3d[obj_name] = {'frames': skeleton2d[obj_name]['frames'],
#                                      'label_names': skeleton2d[obj_name]['label_names']}
#         for label in skeleton3d[obj_name]['label_names']:
#             skeleton3d[obj_name][label] = {'x': np.nan * np.ones(np.size(skeleton3d[obj_name]['frames'])),
#                                                 'y': np.nan * np.ones(np.size(skeleton3d[obj_name]['frames'])),
#                                                 'z': np.nan * np.ones(np.size(skeleton3d[obj_name]['frames']))}
#
#         for i, frame in enumerate(skeleton3d[obj_name]['frames']):
#             for label in skeleton3d[obj_name]['label_names']:
#                 xs_coord, ys_coord, frames_coord = [], [], []
#                 for camn in range(1, nb_cam + 1):
#                     xs_coord.append([skeleton2d[obj_name][label][camn]['x'][i].copy()])
#                     ys_coord.append([skeleton2d[obj_name][label][camn]['y'][i].copy()])
#                     frames_coord.append([frame])
#
#                 xs_pose, ys_pose, zs_pose, frames_pose, indexes, rmses, = \
#                     reconstructor.recon_objs(xs_coord, ys_coord, frames_coord)
#
#                 if len(xs_pose) == 0:
#                     skeleton3d[obj_name][label]['x'][i], skeleton3d[obj_name][label]['y'][i], \
#                     skeleton3d[obj_name][label]['z'][i] = np.nan, np.nan, np.nan
#                 else:
#                     skeleton3d[obj_name][label]['x'][i], skeleton3d[obj_name][label]['y'][i], \
#                     skeleton3d[obj_name][label]['z'][i] = xs_pose[0][0], ys_pose[0][0], zs_pose[0][0]
#
#         fig1, axs1 = plt.subplots(nb_cam, 1)
#         for i, camn in enumerate(range(1, nb_cam + 1)):
#             axs1[i].clear()
#             for label_right in label_names_right:
#                 label_left = label_right.replace('right', 'left')
#                 x_right = skeleton2d[obj_name][label_right][camn]['x']
#                 y_right = skeleton2d[obj_name][label_right][camn]['y']
#                 x_left = skeleton2d[obj_name][label_left][camn]['x']
#                 y_left = skeleton2d[obj_name][label_left][camn]['y']
#
#                 angle = np.arctan((y_right - y_left) / (x_right - x_left))
#                 angle[~np.isnan(angle)] = np.unwrap(angle[~np.isnan(angle)] * 2, discont=2 * np.pi * 0.8) / 2
#
#                 angle2 = np.arctan2((y_right - y_left), (x_right - x_left))
#                 median_diff_angle = np.nanmedian(angle2 - angle)
#                 if abs(abs(median_diff_angle) - np.pi) < 0.2:
#                     if median_diff_angle > 0:
#                         angle2 -= np.pi
#                     elif median_diff_angle < 0:
#                         angle2 += np.pi
#
#                 in_diff = np.array([False] * len(angle))
#                 is_not_nan = ~np.isnan(angle) & ~np.isnan(angle2)
#                 in_diff[is_not_nan] = abs(angle2[is_not_nan] - angle[is_not_nan]) > np.pi / 2
#
#                 axs1[i].plot(skeleton2d[obj_name]['frames'], angle)  # angle of the line btw the two sides
#                 axs1[i].plot(np.array(skeleton2d[obj_name]['frames'])[in_diff], angle2[in_diff], marker='*',
#                              linestyle='None')
#
#             axs1[i].set_ylabel('angle btw right and left (rad)')
#             axs1[i].set_xlabel('frames - ' + label_right)
#
#         plt.show()


def recon3d_dlc(skeleton2d: Dict[str, any], dlt_path: str,
                reconstructor_settings: Reconstructor3DSettings) -> Dict[str, any]:
    """

    Args:
        skeleton2d:
        dlt_path:
        reconstructor_settings:

    Returns:
        skeleton3d:
    """

    reconstructor = Reconstructor3D.read_dlt_coefs(dlt_path)
    reconstructor.settings = reconstructor_settings

    # Unscamble case where side is wrong on only a minority of cam views
    skeleton3d = {'obj_names': skeleton2d['obj_names'], 'model_name': skeleton2d['model_name']}
    for obj_name in skeleton3d['obj_names']:
        # label_names_right = [s for s in skeleton2d[obj_name]['label_names'] if 'right' in s]
        # other_label_names = [s for s in skeleton2d[obj_name]['label_names'] if
        #                      'right' not in s or 'left' not in s]

        skeleton3d[obj_name] = {'frames': skeleton2d[obj_name]['frames'],
                                'label_names': skeleton2d[obj_name]['label_names']}
        for label in skeleton3d[obj_name]['label_names']:
            skeleton3d[obj_name][label] = {'x': np.nan * np.ones(np.size(skeleton2d[obj_name]['frames'])),
                                           'y': np.nan * np.ones(np.size(skeleton2d[obj_name]['frames'])),
                                           'z': np.nan * np.ones(np.size(skeleton2d[obj_name]['frames']))}

            for i, frame in enumerate(skeleton2d[obj_name]['frames']):

                xs_coord, ys_coord, frames_coord = [], [], []

                nb_cam = len(skeleton2d[obj_name][label].keys())
                for camn in range(1, nb_cam + 1):
                    xs_coord.append([skeleton2d[obj_name][label][camn]['x'][i].copy()])
                    ys_coord.append([skeleton2d[obj_name][label][camn]['y'][i].copy()])
                    frames_coord.append([frame])

                xs_pose, ys_pose, zs_pose, frames_pose, indexes, rmses = \
                    reconstructor.recon_objs(xs_coord, ys_coord, frames_coord)

                if len(xs_pose) == 0:
                    skeleton3d[obj_name][label]['x'][i], skeleton3d[obj_name][label]['y'][i], \
                        skeleton3d[obj_name][label]['z'][i] = np.nan, np.nan, np.nan
                else:
                    skeleton3d[obj_name][label]['x'][i], skeleton3d[obj_name][label]['y'][i], \
                        skeleton3d[obj_name][label]['z'][i] = xs_pose[0][0], ys_pose[0][0], zs_pose[0][0]

    return skeleton3d

