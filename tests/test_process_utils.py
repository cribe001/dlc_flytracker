import os
import process.utils as process_utils


def test_match_folders() -> None:
    folder_names = [['cam1_20220304_055123', 'cam1_20220304_101111', 'cam1_20220304_110140', 'cam1_20220304_120352'],
                    ['cam2_20220304_055123', 'cam2_20220304_101115', 'cam2_20220304_120402'],
                    ['cam3_20220304_055123', 'cam3_20220304_101111', 'cam3_20220304_111158', 'cam3_20220304_120352']]

    matched_directories = process_utils.match_folders_by_name(folder_names, threshold_nb_diff_char=0)
    assert matched_directories == []

    matched_directories = process_utils.match_folders_by_name(folder_names, threshold_nb_diff_char=1)
    assert matched_directories == [['cam1_20220304_055123', 'cam2_20220304_055123', 'cam3_20220304_055123']]

    matched_directories = process_utils.match_folders_by_date(folder_names, threshold_s=60)
    assert matched_directories == [['cam1_20220304_055123', 'cam2_20220304_055123', 'cam3_20220304_055123'],
                                   ['cam1_20220304_101111', 'cam2_20220304_101115', 'cam3_20220304_101111'],
                                   ['cam1_20220304_120352', 'cam2_20220304_120402', 'cam3_20220304_120352']]


def test_match_recordings() -> None:
    path = os.path.join(os.getcwd(), '../data/mosquito_escapes')
    directories = [os.path.join(path, 'cam1'),
                   os.path.join(path, 'cam2'),
                   os.path.join(path, 'cam3')]

    matched_directories = process_utils.match_recordings_by_date(directories, threshold_s=60,
                                                                 keep_only_folder_with_images=False)
    assert len(matched_directories) == 3


def test_get_unmatched_folder_names() -> None:
    folder_names = [['cam1_20220304_055123', 'cam1_20220304_101111', 'cam1_20220304_110140', 'cam1_20220304_120352'],
                    ['cam2_20220304_055123', 'cam2_20220304_101115', 'cam2_20220304_120402'],
                    ['cam3_20220304_055123', 'cam3_20220304_101111', 'cam3_20220304_111158', 'cam3_20220304_120352']]

    matched_folder_names = [['cam1_20220304_055123', 'cam2_20220304_055123', 'cam3_20220304_055123'],
                            ['cam1_20220304_101111', 'cam2_20220304_101115', 'cam3_20220304_101111'],
                            ['cam1_20220304_120352', 'cam2_20220304_120402', 'cam3_20220304_120352']]

    unmatched_folder_names = process_utils.get_unmatched_folders(folder_names, matched_folder_names)

    assert unmatched_folder_names == [['cam1_20220304_110140'], [], ['cam3_20220304_111158',]]


def main():
    test_match_folders()
    test_match_recordings()
    test_get_unmatched_folder_names()


if __name__ == "__main__":
    main()



