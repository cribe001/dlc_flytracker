import copy, os

from skeleton_fitter import optimiser, utils
from skeleton_fitter.modules.plotting import plot_limbs, plot_body_and_limbs

from importlib import import_module


def test_skeleton_fly(show_plots=False):
    animal_names = ['fly', 'fly_geo', 'fly_hybrid']

    for animal_name in animal_names:
        if '_' in animal_name: skeleton_type = animal_name[animal_name.index('_'):]
        else: skeleton_type = ''
        init_yaml_path = os.path.join(os.getcwd(),
                                      '../data/mosquito_escapes/initial_parameters/mosquito' + skeleton_type + '.yaml')

        animal = import_module('skeleton_fitter.animals.' + animal_name)
        animal_sets = animal.AnimalSettings

        body = import_module('skeleton_fitter.modules.bodies.' + animal_sets.body_module_name)
        body_sets = body.BodyModuleSettings(init_yaml_path)

        body_params = copy.deepcopy(body_sets.params_init)
        body_params['yaw_a'] = 55.0
        body_params['pitch_a'] = -20.0
        body_params['roll_a'] = -5.0
        [body_params['x_com'], body_params['y_com'], body_params['z_com']] = [0.0, 5.0, 3.0]

        body_skeleton3d_1 = body.rotate_skeleton3d(body_sets.skeleton3d_init, body_params)
        body_skeleton3d_1 = body.translate_skeleton3d(body_skeleton3d_1, body_params)
        #plot_body(body_skeleton3d_1)

        limbs, limbs_sets, limbs_params = {}, {}, {}
        for num_limb, limb_name in enumerate(animal_sets.limb_names):
            limbs[limb_name] = import_module('skeleton_fitter.modules.limbs.' + animal_sets.limb_module_names[num_limb])
            limbs_sets[limb_name] = limbs[limb_name].LimbsModuleSettings(init_yaml_path)
            limbs_params[limb_name] = copy.deepcopy(limbs_sets[limb_name].params_init)

        limbs_params['wings']['right']['stroke_a'] = -55.0
        limbs_params['wings']['right']['deviation_a'] = 10.0
        limbs_params['wings']['right']['rotation_a'] = 60.0

        wings_skeleton3d_1 = copy.deepcopy(limbs_sets['wings'].skeleton3d_init)
        for side in limbs_sets['wings'].skeleton3d_init.keys():
            hinge_label = '{0}_wing_hinge'.format(side)
            [limbs_params['wings'][side]['x_hinge'],
             limbs_params['wings'][side]['y_hinge'],
             limbs_params['wings'][side]['z_hinge']] = body_skeleton3d_1[hinge_label]

            [limbs_params['wings'][side]['x_com'],
             limbs_params['wings'][side]['y_com'],
             limbs_params['wings'][side]['z_com']] = [body_params['x_com'], body_params['y_com'], body_params['z_com']]

            wings_skeleton3d_1[side] = limbs['wings'].rotate_and_translate_skeleton3d(limbs_sets['wings'].skeleton3d_init[side],
                                                                                      limbs_params['wings'][side], side)

        if show_plots:
            plot_limbs(wings_skeleton3d_1)
            plot_body_and_limbs(body_skeleton3d_1, wings_skeleton3d_1)

    # Estimate body and wings parameters
    # body_params1_est = estimate_body_parameters(body_skeleton3d_1, body_sets.params_init)
    # limbs_params1_est = {}
    # for side in ['right', 'left']:
    #     limbs_params1_est[side] = estimate_wing_parameters(wings_skeleton3d_1[side], body_params1_est, limbs_sets[limb_name].params_init[side])


def test_skeleton_fitter_fly(show_plots=False):
    max_rmse = 10
    max_nb_iterations = 10000

    dlt_coefs = [[-6.86546837e+02,  1.29528915e+04,  1.02377641e+02,  4.73021647e+02,
                  -7.28479582e+02,  1.16245489e+02, -1.31706744e+04,  5.05410355e+02,
                  -1.75493289e+00, -2.16534662e-01, -8.74025031e-02],
                 [ 1.32835992e+04, -1.30629279e+02, -8.78227221e+02,  4.46517571e+02,
                   -1.21075194e+02, -1.31697555e+04, -8.05482186e+02,  4.24669476e+02,
                   9.01983567e-02,  3.58804475e-02, -1.98002751e+00],
                 [-1.28278296e+04, -7.90718730e+02,  9.37964723e+00,  4.30413100e+02,
                  -6.06304504e+01, -9.31664890e+02, -1.28878959e+04,  5.05777107e+02,
                  1.83401606e-02, -2.16231985e+00, -3.49328202e-02]]

    fit_methods = ['2d', '3d']
    opt_methods = ['nelder-mead', 'powell', 'least_squares', 'leastsq']

    sides = ['left', 'right']

    animal_names = ['fly', 'fly_geo', 'fly_hybrid']

    for animal_name in animal_names:
        if '_' in animal_name: skeleton_type = animal_name[animal_name.index('_'):]
        else: skeleton_type = ''
        init_yaml_path = os.path.join(os.getcwd(),
                                      '../data/mosquito_escapes/initial_parameters/mosquito' + skeleton_type + '.yaml')

        animal = import_module('skeleton_fitter.animals.' + animal_name)
        animal_sets = animal.AnimalSettings()

        body = import_module('skeleton_fitter.modules.bodies.' + animal_sets.body_module_name)
        body_sets = body.BodyModuleSettings(init_yaml_path)
        body_params = copy.deepcopy(body_sets.params_init)

        limbs, limbs_sets, limbs_params = {}, {}, {}
        for num_limb, limb_name in enumerate(animal_sets.limb_names):
            limbs[limb_name] = import_module('skeleton_fitter.modules.limbs.' + animal_sets.limb_module_names[num_limb])
            limbs_sets[limb_name] = limbs[limb_name].LimbsModuleSettings(init_yaml_path)
            limbs_params[limb_name] = copy.deepcopy(limbs_sets[limb_name].params_init)

        # Parameters to generate fake 3d and 2d skeletons
        body_params['yaw_a'] = 2.0
        body_params['pitch_a'] = -1.0
        body_params['roll_a'] = -1.0
        [body_params['x_com'], body_params['y_com'], body_params['z_com']] = [0.01, 0.05, -0.02]

        limbs_params['wings']['left']['stroke_a'], limbs_params['wings']['right']['stroke_a'] = 2.5, -1.0
        limbs_params['wings']['left']['deviation_a'], limbs_params['wings']['right']['deviation_a'] = -1.0, 3.0
        limbs_params['wings']['left']['rotation_a'], limbs_params['wings']['right']['rotation_a'] = -2.0, 1.0

        # Test all fit_methods and opt_methods
        for fit_method in fit_methods:
            body_skeleton = body.rotate_skeleton3d(body_sets.skeleton3d_init, body_params)
            body_skeleton = body.translate_skeleton3d(body_skeleton, body_params)

            wings_skeleton = {}
            for num_limb, limb_name in enumerate(animal_sets.limb_names):

                wings_skeleton[limb_name] = copy.deepcopy(limbs_sets[limb_name].skeleton3d_init)
                for side in sides:
                    wings_skeleton[limb_name][side] = limbs[limb_name].rotate_and_translate_skeleton3d(
                        limbs_sets[limb_name].skeleton3d_init[side], limbs_params[limb_name][side], side)

            if '2d' in fit_method:
                body_skeleton = utils.reproject_skeleton3d_to2d(body_skeleton, dlt_coefs)

                for num_limb, limb_name in enumerate(animal_sets.limb_names):
                    for side in sides:
                        wings_skeleton[limb_name][side] = utils.reproject_skeleton3d_to2d(wings_skeleton[limb_name][side], dlt_coefs)

            for opt_method in opt_methods:

                # Test optimisation fit for the body
                param_names_to_optimize = list(set(body_sets.param_names) - set(body_sets.param_names_to_keep_cst))
                body_param_ests, body_rmse, body_nb_iterations = \
                    optimiser.optim_fit_body_params(animal_name, body_sets.skeleton3d_init, body_skeleton, body_sets.params_init,
                                                    param_names_to_optimize, 'body_' + fit_method, opt_method, body_sets.bounds_init, dlt_coefs=dlt_coefs)

                assert body_rmse <= max_rmse, \
                    "Too high rmse ({0} > {1}) when optimise fitting in {2} with {3}".format(body_rmse, max_rmse, fit_method, opt_method)
                assert body_nb_iterations <= max_nb_iterations, \
                    "Too high nb_iterations ({0} > {1}}) when optimise fitting in {2} with {3}".format(body_nb_iterations, max_nb_iterations, fit_method, opt_method)

                # Test optimisation fit for the limbs
                for num_limb, limb_name in enumerate(animal_sets.limb_names):
                    param_names_to_optimize = list(set(limbs_sets[limb_name].param_names) - set(limbs_sets[limb_name].param_names_to_keep_cst))
                    param_names_to_optimize = list(set(param_names_to_optimize) - set(body_sets.param_names))

                    wings_param_ests, wings_rmse, wings_nb_iterations = \
                        optimiser.optim_fit_limbs_params(animal_name, limbs_sets[limb_name].skeleton3d_init,
                                                         wings_skeleton[limb_name], limbs_sets[limb_name].params_init,
                                                         param_names_to_optimize, 'limb_' + fit_method,
                                                         opt_method, limbs_sets[limb_name].bounds_init, dlt_coefs=dlt_coefs)

                    for side in sides:
                        assert wings_rmse[side] <= max_rmse, \
                            "Too high rmse ({0} > {1}) when optimise fitting in {2} with {3} (side: {4})".\
                                format(wings_rmse[side], max_rmse, fit_method, opt_method, side)

                        assert wings_nb_iterations[side] <= max_nb_iterations, \
                            "Too high nb_iterations ({0} > {1}}) when optimise fitting in {2} with {3} (side: {4})".\
                                format(wings_nb_iterations[side], max_nb_iterations, fit_method, opt_method, side)


def main():
    show_plots = True
    test_skeleton_fly(show_plots)
    test_skeleton_fitter_fly(show_plots)


if __name__ == "__main__":
    main()
