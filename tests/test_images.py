import os.path

from images import ImageSequence, MultiImageSequence


def test_multi_image_sequence() -> None:
    path = os.path.join(os.getcwd(), '../data/mosquito_escapes/')
    seq1 = ImageSequence.from_directory(os.path.join(path, 'cam1/20200216_075901-sample_binned'))
    seq2 = ImageSequence.from_directory(os.path.join(path, 'cam2/20200216_075905-sample_binned'))
    seq3 = ImageSequence.from_directory(os.path.join(path, 'cam3/20200216_075905-sample_binned'))

    seqs = {'cam1': seq1, 'cam2': seq2, 'cam3': seq3}
    frame_rates = {'cam1': 12500, 'cam2': 12500, 'cam3': 12500}
    shifts = {'cam1': 0, 'cam2': 0, 'cam3': 0}

    multi_seq = MultiImageSequence(seqs, frame_rates, shifts)

    assert multi_seq.nb_sequences == 3

    # Or
    paths = [os.path.join(path, 'cam1/20200216_075901-sample_binned'),
             os.path.join(path, 'cam2/20200216_075905-sample_binned'),
             os.path.join(path, 'cam3/20200216_075905-sample_binned')]

    multi_seq = MultiImageSequence.from_directory(paths, 12500, names=['cam1_20200216_075901-sample_binned',
                                                                       'cam2_20200216_075905-sample_binned',
                                                                       'cam3_20200216_075905-sample_binned'])

    assert multi_seq.nb_sequences == 3

    assert multi_seq.get_paths() == paths
    assert multi_seq.names[0] == 'cam1_20200216_075901-sample_binned'


def main():
    test_multi_image_sequence()


if __name__ == "__main__":
    main()
