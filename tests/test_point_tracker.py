import os
import numpy as np

from point_tracker import Tracker2D, Reconstructor3D
from camera.calib import read_dlt_coefs


def test_tracker2d():
    image_format = 'jpg'
    nb_cam = 3

    rec_names = {1: '20200216_075901-selection',
                 2: '20200216_075905-selection',
                 3: '20200216_075905-selection'}

    image_path = os.path.join(os.getcwd(), '../data/mosquito_escapes/')
    image_paths = {1: os.path.join(image_path, 'cam1'),
                   2: os.path.join(image_path, 'cam2'),
                   3: os.path.join(image_path, 'cam3')}

    for camn in range(1, nb_cam + 1):
        tracker = Tracker2D.load_images_from_path(os.path.join(image_paths[camn], rec_names[camn]), image_format)
        tracker.do_tracking()

        tracker.save_csv(rec_names[camn], os.path.join(image_paths[camn], rec_names[camn]))

        assert len(tracker.points['frame']) > 0


def test_reconstructor3d():
    max_rmse = 10

    path = os.path.join(os.getcwd(), '../data/mosquito_escapes')
    rec_names = {1: '20200303_030117', 2: '20200303_030120', 3: '20200303_030120'}
    image_paths = {1: os.path.join(path, 'cam1'), 2: os.path.join(path, 'cam2'), 3: os.path.join(path, 'cam3')}
    calib_path = '../data/mosquito_escapes/calib/20200306_DLTcoefs-py.csv'

    dlt_coefs = read_dlt_coefs(calib_path)

    nb_cam = len(rec_names.keys())

    reconstructor = Reconstructor3D(dlt_coefs)

    # From .csv file, fill xs_pose, ys_pose and frames_pose (2d coordinates of blobs)
    pts_csv = {}
    for camn in range(1, nb_cam + 1):
        pts_csv[camn] = np.genfromtxt(os.path.join(image_paths[camn], rec_names[camn], 'cam{0}_'.format(camn) + rec_names[camn] + '-2d_points.csv'),
                                      delimiter=',', skip_header=0, names=True)

    xs_coord = np.array([pts_csv[camn]['x_px'] for camn in range(1, nb_cam + 1)], dtype=object)
    ys_coord = np.array([pts_csv[camn]['y_px'] for camn in range(1, nb_cam + 1)], dtype=object)
    frames_coord = np.array([pts_csv[camn]['frame'] for camn in range(1, nb_cam + 1)], dtype=object)

    xs_pose, ys_pose, zs_pose, frames_pose, indexes_pts, _, = reconstructor.recon_objs(xs_coord, ys_coord, frames_coord)
    tracks_dict = reconstructor.recon_tracks(xs_pose, ys_pose, zs_pose, frames_pose, indexes_pts)

    for nb_obj in tracks_dict['obj'].keys():

        xs_coord_repro, ys_coord_repro = reconstructor.get_2d_reprojection(tracks_dict['obj'][nb_obj]['x'],
                                                                           tracks_dict['obj'][nb_obj]['y'],
                                                                           tracks_dict['obj'][nb_obj]['z'])
        index = np.array(tracks_dict['obj'][nb_obj]['index_pts']).T

        for i in range(0, nb_cam):

            xs_coord_ori = [xs_coord[i][int(ind)] if ind is not None and not np.isnan(ind) else np.nan for ind in index[i]]
            ys_coord_ori = [ys_coord[i][int(ind)] if ind is not None and not np.isnan(ind) else np.nan for ind in index[i]]

            rmse_x_repro = np.sqrt(np.nansum((xs_coord_ori - xs_coord_repro[i]) ** 2))
            rmse_y_repro = np.sqrt(np.nansum((ys_coord_ori - ys_coord_repro[i]) ** 2))

            assert rmse_x_repro <= max_rmse, 'Mean reprojection x error of tracks is {0:.2f} pixels (> {1:.2f}'.format(rmse_x_repro, max_rmse)
            assert rmse_y_repro <= max_rmse, 'Mean reprojection y error of tracks is {0:.2f} pixels (> {1:.2f}'.format(rmse_x_repro, max_rmse)


def main():
    # test_tracker2d()
    test_reconstructor3d()


if __name__ == "__main__":
    main()


