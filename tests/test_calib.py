import os
import numpy as np

import matplotlib.pyplot as plt

from camera import calib


def test_dlt2d_1views():
    max_mean_err_px = 10
    max_mean_err_cm = 10e-10

    # Test of the 2D DLT
    # 2D (x, y) coordinates (in cm) of the corner of a square (the measurement error is at least 0.2 cm):
    xy = [[0, 0], [0, 12.3], [14.5, 12.3], [14.5, 0]]

    # 2D (u, v) coordinates (in pixels) of 2 different views of the square:
    uv1 = [[1302, 1147], [1110, 976], [1411, 863], [1618, 1012]]

    # Use only one view to perform a 2D calibration of the camera with 4 points of the square:')
    nb_dim = 2
    nb_cam = 1

    # Camera calibration parameters based on view #1 and error of the calibration of view #1 (in pixels):')
    dlt_coef1, repro_err1 = calib.estim_dlt_coef(xy, uv1, nb_dim)

    assert len(dlt_coef1) == 8
    assert repro_err1 <= max_mean_err_px

    # Reconstruction of the same 4 points based on one view and the camera calibration parameters:
    xy1 = np.zeros((len(xy), 2))
    for i in range(len(uv1)):
        xy1[i, :] = calib.recon3d(nb_dim, nb_cam, dlt_coef1, uv1[i])

    # Mean error of the point reconstruction using the DLT (error in cm):
    mean_err = np.mean(np.sqrt(np.sum((np.array(xy1) - np.array(xy)) ** 2, 1)))

    assert mean_err <= max_mean_err_cm, \
        'Mean error of the point reconstruction using the 2D DLT (1 view) ' \
        'is larger than {0} ({1})'.format(max_mean_err_cm, mean_err)


def test_dlt2d_2views():
    max_mean_err_px = 10
    max_mean_err_cm = 10e-10

    # Test of the 2D DLT
    # 2D (x, y) coordinates (in cm) of the corner of a square (the measurement error is at least 0.2 cm):
    xy = [[0, 0], [0, 12.3], [14.5, 12.3], [14.5, 0]]

    # 2D (u, v) coordinates (in pixels) of 2 different views of the square:
    uv1 = [[1302, 1147], [1110, 976], [1411, 863], [1618, 1012]]
    uv2 = [[1094, 1187], [1130, 956], [1514, 968], [1532, 1187]]

    # Use 2 views to perform a 2D calibration of the camera with 4 points of the square:
    nb_dim = 2
    nb_cam = 2

    # Camera calibration parameters based on view #1 and error of the calibration of view #1 (in pixels)
    dlt_coef1, repro_err1 = calib.estim_dlt_coef(xy, uv1, nb_dim)
    dlt_coef2, repro_err2 = calib.estim_dlt_coef(xy, uv2, nb_dim)

    assert len(dlt_coef1) == 8
    assert repro_err1 <= max_mean_err_px
    assert repro_err2 <= max_mean_err_px

    # Reconstruction of the same 4 points based on 2 views and the camera calibration parameters:
    xy12 = np.zeros((len(xy), 2))
    dlt_coefs = [dlt_coef1, dlt_coef2]
    for i in range(len(uv1)):
        xy12[i, :] = calib.recon3d(nb_dim, nb_cam, dlt_coefs, [uv1[i], uv2[i]])

    # Mean error of the point reconstruction using the DLT (error in cm):
    mean_err = np.mean(np.sqrt(np.sum((np.array(xy12) - np.array(xy)) ** 2, 1)))

    assert mean_err <= max_mean_err_cm, \
        'Mean error of the point reconstruction using the 2D DLT (2 views) ' \
        'is larger than {0} ({1})'.format(max_mean_err_cm, mean_err)


def test_dlt3d():
    max_mean_err_px = 10
    max_mean_err_cm = 0.2

    # Test of camera calibration and point reconstruction based on direct linear transformation (DLT).
    # 3D (x,y,z) coordinates (in cm) of the corner of a cube (the measurement error is at least 0.2 cm):
    xyz = [[0, 0, 0], [0, 12.3, 0], [14.5, 12.3, 0], [14.5, 0, 0], [0, 0, 14.5], [0, 12.3, 14.5], [14.5, 12.3, 14.5],
           [14.5, 0, 14.5]]

    # 2D (u, v) coordinates (in pixels) of 4 different views of the cube:
    uv1 = [[1302, 1147], [1110, 976], [1411, 863], [1618, 1012], [1324, 812], [1127, 658], [1433, 564], [1645, 704]]
    uv2 = [[1094, 1187], [1130, 956], [1514, 968], [1532, 1187], [1076, 854], [1109, 647], [1514, 659], [1523, 860]]
    uv3 = [[1073, 866], [1319, 761], [1580, 896], [1352, 1016], [1064, 545], [1304, 449], [1568, 557], [1313, 668]]
    uv4 = [[1205, 1511], [1193, 1142], [1601, 1121], [1631, 1487], [1157, 1550], [1139, 1124], [1628, 1100],
           [1661, 1520]]

    # Use 4 views to perform a 3D calibration of the camera with 8 points of the cube:
    nb_dim = 3
    nb_cam = 4

    for using_modifiedDLT in [False, True]:
        # Camera calibration parameters based on view #1 and error of the calibration of view #1 (in pixels)
        if not using_modifiedDLT:
            # Traditional DLT coefficient
            dlt_coef1, repro_err1 = calib.estim_dlt_coef(xyz, uv1, nb_dim)
            dlt_coef2, repro_err2 = calib.estim_dlt_coef(xyz, uv2, nb_dim)
            dlt_coef3, repro_err3 = calib.estim_dlt_coef(xyz, uv3, nb_dim)
            dlt_coef4, repro_err4 = calib.estim_dlt_coef(xyz, uv4, nb_dim)

        else:
            # Using modified DLT (slighly better accuracy and the rotation matrix will be orthogonal)
            dlt_coef1, repro_err1 = calib.estim_mdlt_coef(xyz, uv1)
            dlt_coef2, repro_err2 = calib.estim_mdlt_coef(xyz, uv2)
            dlt_coef3, repro_err3 = calib.estim_mdlt_coef(xyz, uv3)
            dlt_coef4, repro_err4 = calib.estim_mdlt_coef(xyz, uv4)

        assert len(dlt_coef1) == 11
        assert repro_err1 <= max_mean_err_px
        assert repro_err2 <= max_mean_err_px
        assert repro_err3 <= max_mean_err_px
        assert repro_err4 <= max_mean_err_px

        # Reconstruction of the same 8 points based on 4 views and the camera calibration parameters:
        xyz1234 = np.zeros((len(xyz), 3))
        L1234 = [dlt_coef1, dlt_coef2, dlt_coef3, dlt_coef4]
        for i in range(len(uv1)):
            xyz1234[i, :] = calib.recon3d(nb_dim, nb_cam, L1234, [uv1[i], uv2[i], uv3[i], uv4[i]])

        # Mean error of the point reconstruction using the DLT (error in cm):
        mean_err = np.mean(np.sqrt(np.sum((np.array(xyz1234) - np.array(xyz)) ** 2, 1)))

        assert mean_err <= max_mean_err_cm, \
            'Mean error of the point reconstruction using the 3D DLT (4 views) ' \
            'is larger than {0} ({1})'.format(max_mean_err_cm, mean_err)

        # 2D reprojection of the 8 points
        uv1_repro = calib.find2d(1, dlt_coef1, xyz1234)
        uv2_repro = calib.find2d(1, dlt_coef2, xyz1234)
        uv3_repro = calib.find2d(1, dlt_coef3, xyz1234)
        uv4_repro = calib.find2d(1, dlt_coef4, xyz1234)

        # Mean error of the point reprojection using the DLT (error in px):
        repro_err1 = np.sqrt(np.mean(np.sum((uv1 - uv1_repro) ** 2, 1)))
        repro_err2 = np.sqrt(np.mean(np.sum((uv2 - uv2_repro) ** 2, 1)))
        repro_err3 = np.sqrt(np.mean(np.sum((uv3 - uv3_repro) ** 2, 1)))
        repro_err4 = np.sqrt(np.mean(np.sum((uv4 - uv4_repro) ** 2, 1)))

        assert repro_err1 <= max_mean_err_px
        assert repro_err2 <= max_mean_err_px
        assert repro_err3 <= max_mean_err_px
        assert repro_err4 <= max_mean_err_px


def test_dlt_coef_to_from_intrinsic_extrinsic() -> None:

    xyz = [[0, 0, 0], [0, 12.3, 0], [14.5, 12.3, 0], [14.5, 0, 0], [0, 0, 14.5], [0, 12.3, 14.5], [14.5, 12.3, 14.5], [14.5, 0, 14.5]]
    uv = [[1302, 1147], [1110, 976], [1411, 863], [1618, 1012], [1324, 812], [1127, 658], [1433, 564], [1645, 704]]

    show_plot = False
    decimal = -1

    # Camera calibration parameters (DLT coefficients)
    #dlt_coef, repro_err = calib.estim_dlt_coef(xyz, uv, 3)  # Will no produce dlt_coef from an orthogonal matrix
    dlt_coef, repro_err = calib.estim_mdlt_coef(xyz, uv, show_plot=show_plot)

    # dlt_coefs = calib.read_dlt_coefs('../data/mosquito_escapes/calib/20200306_DLTcoefs-py.csv')
    # dlt_coef = dlt_coefs[0]

    xp, yp, fx, fy = calib.intrinsic_from_dlt_coef(dlt_coef)
    coord, quat = calib.extrinsic_from_dlt_coef(dlt_coef, (xp, yp, fx, fy))

    new_dlt_coef = calib.dlt_coef_from_intrinsic_extrinsic(xp, yp, fx, fy, coord, quat)

    uv2 = calib.find2d(1, new_dlt_coef, xyz)

    if show_plot:
        fig = plt.figure()
        ax = fig.add_subplot(111)
        for i in range(0, len(uv)):
            ax.scatter(uv[i][0], uv[i][1], marker='+', color='g', s=40)
            ax.scatter(uv2[i][0], uv2[i][1], marker='+', color='r', s=40)
        ax.set_xlabel('x [m]')
        ax.set_ylabel('y [m]')
        plt.show()

    np.testing.assert_almost_equal(uv, uv2, decimal=decimal)

    #assert all(dlt_coef == new_dlt_coef)


def test_gen_dlt_coefs_and_save_in_csv() -> None:
    max_rmse = 10

    img_height = 1024
    from_matlab = True

    coord_calib_csv_path = os.path.join(os.getcwd(),
                                        '../data/mosquito_escapes/calib/xyz_calibration_device.csv')
    pts_calib_csv_path = os.path.join(os.getcwd(),
                                      '../data/mosquito_escapes/calib/20200306_xypts.csv')

    dlt_coefs, rmse_errs = calib.gen_dlt_coefs_from_paths(coord_calib_csv_path, pts_calib_csv_path,
                                                          from_matlab=from_matlab, img_height=img_height)

    assert all(rmse_errs <= max_rmse)

    calib.save_dlt_coefs(dlt_coefs, '../data/mosquito_escapes/calib/20200306_DLTcoefs-py.csv')


def main():
    test_dlt2d_1views()
    test_dlt2d_2views()
    test_dlt3d()
    test_dlt_coef_to_from_intrinsic_extrinsic()
    test_gen_dlt_coefs_and_save_in_csv()


if __name__ == "__main__":
    main()

