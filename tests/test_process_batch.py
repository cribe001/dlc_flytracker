import os
import shutil

from process.batch_processing import BatchProcessing, batch_processing
from point_tracker.tracker2d import BlobDetectorSettings
from point_tracker.reconstructor3d import Reconstructor3DSettings

from utils.settings import Settings


def test_batch_processing_images_preprocessing() -> None:
    """
        An example of how recordings (here only one) can be pre-processed (with dynamic cropping)
        in order to generate .avi videos that can then be analysed by DeepLabCut.
    """
    recording_paths = ['../data/mosquito_escapes/cam1', '../data/mosquito_escapes/cam2', '../data/mosquito_escapes/cam3']
    save_path = '../data/mosquito_escapes/_Process'
    dlt_path = os.path.join(os.getcwd(), '../data/mosquito_escapes/calib/20200215_DLTcoefs-py.csv')

    rec_names_to_process = ['20200216_075901-sample_binned']  # names of the recordings to process
    # (Should be the name of the folder with the images of the main camera (by default: #1))

    framerate = 12500

    show_plot = False

    # ------------------------------------------------------------------------------------------------------------------
    # Initialization of the BatchProcessing class
    img_process = BatchProcessing.from_directory_by_names(recording_paths, framerate, save_path,
                                                          threshold_nb_diff_char=2)
    # Alternatively, recordings can be loaded using BatchProcessing.from_directory_by_dates(
    # img_process = BatchProcessing.from_directory_by_dates(recording_paths, framerate, save_path, threshold_s=90)

    # ------------------------------------------------------------------------------------------------------------------
    # Will prepare recordings to be used by Deeplabcut (do each step separately)
    # Convert images to 8 bits and reduce frame_rate (save in save_path + '/_Sample')
    img_process.sample_images(1, rec_names_to_process=rec_names_to_process, delete_previous=True)

    # ------------------------------------------------------------------------------------------------------------------
    # Do 2d tracking (blob detection) on images in save_path + '/_Sample'
    img_process.track2d(blob_detector_settings=BlobDetectorSettings(area_min=10, area_max=1000),
                        rec_names_to_process=rec_names_to_process, from_fn_name='sample', show_plot=show_plot)

    # ------------------------------------------------------------------------------------------------------------------
    # Do 3d reconstruction of tracks
    # use image_ratio=4.0 because the analysed images have been binned compared to the images used for the calibration)
    img_process.recon3d(dlt_path, reconstructor_settings=Reconstructor3DSettings(image_ratio=4.0),
                        rec_names_to_process=rec_names_to_process, from_fn_name='sample', show_plot=show_plot)

    # OR (instead of sample_images, track2d and recond3d)
    # ------------------------------------------------------------------------------------------------------------------
    # # Do manual 2d tracking and then 3d reconstruction of tracks
    # recon_sets = Reconstructor3DSettings(threshold_mean_repro_err=10)
    # img_process.manual_tracking(dlt_path, reconstructor_settings=recon_sets, delete_previous=False,
    #                             rec_names_to_process=rec_names_to_process, show_plot=show_plot)

    # ------------------------------------------------------------------------------------------------------------------
    # Dynamic crop of all frames and save in save_path + '/_Cropped'
    img_process.dynamic_crop_images(50, 50, show_plot=False, from_tracks=True,
                                    rec_names_to_process=rec_names_to_process, from_fn_name='sample', delete_previous=True)

    # ------------------------------------------------------------------------------------------------------------------
    # Rotate view 2 (to always have swatter coming from right side of the image) and save in save_path + '/_Cropped'
    img_process.rotate_images(270, rec_names_to_process=rec_names_to_process, from_fn_name='crop', camn_to_process=[2])

    # ------------------------------------------------------------------------------------------------------------------
    # Stitch all views together (for each frame) and save in save_path + '/_Stitched'
    img_process.stitch_images(rec_names_to_process=rec_names_to_process, from_fn_name='crop', delete_previous=True)

    # ------------------------------------------------------------------------------------------------------------------
    # Save each recording in an .avi in save_path + '/_Avi'
    img_process.save_avi(rec_names_to_process=rec_names_to_process, from_fn_name='stitch', delete_previous=True)


def test_batch_multiprocessing_images_preprocessing() -> None:
    """
        This second example show how the same pre-processing as in the first example above can be done only using
        one line of code and a settings yaml file. This will run pre-processing of one recording in order
        to generate .avi videos that can then be analysed by DeepLabCut.
    """

    settings_yaml_path = '../data/mosquito_escapes/preprocessing_settings-sample_binned.yaml'

    batch_processing(settings_yaml_path)

    settings = Settings.from_yaml(settings_yaml_path)

    rec_names_to_process = settings.batch.recording_names_to_process
    delete_previous = settings.batch.delete_previous

    # ------------------------------------------------------------------------------------------------------------------
    # Initialization of the BatchProcessing class
    batch_process = BatchProcessing.from_settings(settings.batch)

    # ------------------------------------------------------------------------------------------------------------------
    # Will prepare recordings to be used by Deeplabcut (do everything automatically by using 'multi_processes')
    # Do cropping, rotating, stitching, etc at once and save as .avi (save much time and space) in save_path + '/_Avi'
    batch_process.multi_processes(settings.protocol.to_dict(),
                                  rec_names_to_process=rec_names_to_process, delete_previous=delete_previous)


def test_batch_processing_images_preprocessing_static_crop() -> None:
    """
        An example of how recordings (here only one) can be pre-processed (with static cropping)
        in order to generate .avi videos that can then be analysed by DeepLabCut.
    """
    recording_paths = ['../data/mosquito_escapes/cam1', '../data/mosquito_escapes/cam2', '../data/mosquito_escapes/cam3']
    save_path = '../data/mosquito_escapes/_Process'
    dlt_path = os.path.join(os.getcwd(), '../data/mosquito_escapes/calib/20200215_DLTcoefs-py.csv')

    rec_names_to_process = ['20200216_075901-sample_binned']  # names of the recordings to process
    # (Should be the name of the folder with the images of the main camera (by default: #1))

    framerate = 12500

    x_crop = [72, 250, 102]
    y_crop = [44, 184, 10]

    # ------------------------------------------------------------------------------------------------------------------
    # Initialization of the BatchProcessing class
    img_process = BatchProcessing.from_directory_by_names(recording_paths, framerate, save_path,
                                                          threshold_nb_diff_char=2)
    # ------------------------------------------------------------------------------------------------------------------
    # Static crop of all frames and save in save_path + '/_Cropped'
    img_process.static_crop_images(x_crop, y_crop, 50, 50, show_plot=False,
                                   rec_names_to_process=rec_names_to_process, delete_previous=True)

    # ------------------------------------------------------------------------------------------------------------------
    # Rotate view 2 (to always have swatter coming from right side of the image) and save in save_path + '/_Cropped'
    img_process.rotate_images(270, rec_names_to_process=rec_names_to_process, from_fn_name='crop', camn_to_process=[2])

    # ------------------------------------------------------------------------------------------------------------------
    # Stitch all views together (for each frame) and save in save_path + '/_Stitched'
    img_process.stitch_images(rec_names_to_process=rec_names_to_process, from_fn_name='crop', delete_previous=True)

    # ------------------------------------------------------------------------------------------------------------------
    # Save each recording in an .avi in save_path + '/_Avi'
    img_process.save_avi(rec_names_to_process=rec_names_to_process, from_fn_name='stitch', delete_previous=True)


def test_batch_multiprocessing_images_preprocessing2() -> None:
    """
        This third example show the pre-processing of a second dataset using mutliprocessing.
    """

    settings_yaml_path = '../data/mosquito_escapes/preprocessing_settings-selection.yaml'

    batch_processing(settings_yaml_path)

    settings = Settings.from_yaml(settings_yaml_path)

    rec_names_to_process = settings.batch.recording_names_to_process
    delete_previous = settings.batch.delete_previous

    # ------------------------------------------------------------------------------------------------------------------
    # Initialization of the BatchProcessing class
    batch_process = BatchProcessing.from_settings(settings.batch)

    # ------------------------------------------------------------------------------------------------------------------
    # Will prepare recordings to be used by Deeplabcut (do everything automatically by using 'multi_processes')
    # Do cropping, rotating, stitching, etc at once and save as .avi (save much time and space) in save_path + '/_Avi'
    batch_process.multi_processes(settings.protocol.to_dict(),
                                  rec_names_to_process=rec_names_to_process, delete_previous=delete_previous)


def test_batch_processing_dlc_analysis() -> None:
    """
        Run Deeplabcut analysis (track the skeleton joints in 2d on all camera views)
        + load tracking result (filtering, unscrambling, and reverse processes)
        + Reconstruct 3d coordinates of the previously tracked skeleton joints

    """

    settings_yaml_path = '../data/mosquito_escapes/preprocessing_settings-selection.yaml'

    show_plot = False

    img_process, settings = batch_processing(settings_yaml_path)
    rec_names_to_process = settings.batch.recording_names_to_process

    shuffle = 31
    iteration = 70000
    model_name = 'DLC_resnet50_EscapeKinematicsmei26shuffle{0}_{1}'.format(shuffle, iteration)

    pickle_name = '20200216_075901-selection-obj1' + model_name + '_full.pickle'
    pickle_path = os.path.join('../data/mosquito_escapes/_Process/_DLC', rec_names_to_process[0])
    if not os.path.exists(pickle_path): os.makedirs(pickle_path)
    if not os.path.isfile(os.path.join(pickle_path, pickle_name)):
        shutil.copy(os.path.join('../data/mosquito_escapes', pickle_name), os.path.join(pickle_path, pickle_name))

    dlc_cfg_path = "/media/acribellier/MosquitoEscape_Photron1/_maDLC/config.yaml" # See example: '../data/mosquito_escapes/config.yaml'
    dlt_path = os.path.join(os.getcwd(), '../data/mosquito_escapes/calib/20200215_DLTcoefs-py.csv')

    # ------------------------------------------------------------------------------------------------------------------
    # Track features using DeepLabCut (and save in save_path + '/_DLC')
    img_process.analyse_dlc(dlc_cfg_path, shuffle, 0, 5, model_name, save_avi=True,
                            rec_names_to_process=rec_names_to_process, from_fn_name='save_avi', delete_previous=True)

    # ------------------------------------------------------------------------------------------------------------------
    # Load (+ filtering and unscrambling) 2d coords from DLC
    # + Reverse processes (un-stitch, rotate back, un-crop)
    # + Reconstruct 3d coord and save in save_path + '/_DLC'
    img_process.load_dlc(model_name, 'skeleton', dlt_path,
                         rec_names_to_process=rec_names_to_process, from_fn_name='analyse_dlc', show_plot=show_plot)

    # TODO i,g process:recon"ddldc

def test_batch_processing_fit_skeleton() -> None:
    """
        Fit 3D skeleton of a chosen animal (here a fly) to the 2D coordinates of the tracked joints from Deeplabcut.

    """

    recording_paths = ['../data/mosquito_escapes/cam1', '../data/mosquito_escapes/cam2', '../data/mosquito_escapes/cam3']
    save_path = '../data/mosquito_escapes/_Process'

    framerate = 12500

    show_plot = False

    init_yaml_path = os.path.join(os.getcwd(), '../data/mosquito_escapes/initial_parameters/mosquito.yaml')
    dlt_path = os.path.join(os.getcwd(), '../data/mosquito_escapes/calib/20200215_DLTcoefs-py.csv')

    rec_names_to_process = ['20200216_075901-selection']

    shuffle = 31
    iteration = 70000
    model_name = 'DLC_resnet50_EscapeKinematicsmei26shuffle{0}_{1}'.format(shuffle, iteration)

    fit_methods = ['2d', '3d']
    opt_methods = ['powell', 'leastsq', 'least_squares']

    for fit_method in fit_methods:
        for opt_method in opt_methods:

            # ----------------------------------------------------------------------------------------------------------
            # Initialization of the BatchProcessing class
            img_process = BatchProcessing.from_directory_by_names(recording_paths, framerate, save_path,
                                                                  threshold_nb_diff_char=2)

            # ----------------------------------------------------------------------------------------------------------
            # Optimize fit of skeleton to find body and wings angles
            img_process.threshold_likelihood = 0.85
            img_process.fit_skeleton('fly', model_name, fit_method, opt_method, init_yaml_path, dlt_path, show_plot=show_plot,
                                     multiprocessing=True, rec_names_to_process=rec_names_to_process, from_fn_name='load_dlc')


def main():
    test_batch_processing_images_preprocessing()
    test_batch_multiprocessing_images_preprocessing()
    test_batch_processing_images_preprocessing_static_crop()
    test_batch_processing_dlc_analysis()
    test_batch_processing_fit_skeleton()


if __name__ == "__main__":
    main()
