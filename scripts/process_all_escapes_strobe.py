import time, yaml, os
from process.batch_processing import BatchProcessing

from convert_xypts import *

start = time.time()

dlt_path = os.path.join(os.getcwd(), 'data/calib/20200305_DLTcoefs-py.csv')
xyz_path = os.path.join(os.getcwd(), 'data/calib/xyz_calibration_device_big.csv')
csv_wings_geo_path = os.path.join(os.getcwd(), 'acoluzzii_wing.csv')
dlc_cfg_path = '/home/user/Desktop/Antoine/_maDLC/config.yaml'

img_process = BatchProcessing()
img_process.cam_paths = {1: '/media/user/MosquitoEscape_Photron1/Photron1',
                         2: '/media/user/MosquitoEscape_Photron2/Photron2',
                         3: '/media/user/MosquitoEscape_Photron3/Photron3'}
img_process.cam_save_paths = {1: '/media/user/MosquitoEscape_Photron1/Photron1/_Process',
                              2: '/media/user/MosquitoEscape_Photron2/Photron2/_Process',
                              3: '/media/user/MosquitoEscape_Photron3/Photron3/_Process'}

# Fill dict with name of recordings to process (all recordings from given dates)
dates_to_process = ['20200207', '20200209', '20200211', '20200212', '20200213', '20200216', '20200217',
                    '20200224', '20200225', '20200229', '20200301', '20200302', '20200303', '20200304', '20200305']
dates_calib = ['20200208', '20200208', '20200208', '20200208', '20200215', '20200215', '20200215',
               '20200219', '20200221', '20200228', '20200228', '20200305', '20200305', '20200305', '20200305']

img_process.show_plot = False
img_process.update_leading_zero = False
img_process.init_paths_names()

for i, date in enumerate(dates_to_process):

    # Update dlt coefficients
    dlt_path = os.path.join(os.getcwd(), 'data/calib/' + dates_calib[i] + '_DLTcoefs-py.csv')
    img_process.load_dlt_coefs(dlt_path)

    processes_dict = yaml.load(open('processes-strobe.yaml'), Loader=yaml.SafeLoader)
    for processes_num in list(processes_dict.keys())[1:]:
        if 'fn' in processes_dict[processes_num].keys() and processes_dict[processes_num]['fn'] in ['recon3d']:
            processes_dict[processes_num]['kwargs']['dlt_path'] = dlt_path

    with open(os.path.join('processes-strobe_updated.yaml'), 'w') as yaml_file:
        yaml.safe_dump(processes_dict, yaml_file, encoding='utf-8', allow_unicode=True)

    # Select dates to process
    rec_names_to_process = {}
    for camn in range(1, img_process.nb_cam + 1):
        rec_names_to_process[camn] = [s for s in img_process.all_folders_rec_cam[camn] if date in s]

    #rec_names_to_process = {1: ['cam1_20200303_030117'], 2: ['cam2_20200303_030120'], 3: ['cam3_20200303_030120']}

    # # Do cropping, rotating, stitching, etc at once and save as .avi (save much time and space)
    # img_process.do_batch('multi_processes', rec_names_to_process=rec_names_to_process, yaml_path='processes-strobe.yaml', delete_previous=True)
    # img_process.do_batch('stitch', rec_names_to_process=rec_names_to_process, from_fn_name='save_strobe')

    # # Convert images to 8 bits and reduce frame_rate (save in cam_save_paths/_Sample)
    # img_process.do_batch('sample', rec_names_to_process=rec_names_to_process, step_frame=50, delete_previous=True)
    #
    # # Do 2d tracking (blob detection) on images in cam_save_paths/_Sample
    # img_process.do_batch('track2d', rec_names_to_process=rec_names_to_process, from_fn_name='sample')
    #
    # # Do 3d reconstruction of tracks
    # img_process.do_batch('recon3d', rec_names_to_process=rec_names_to_process, from_fn_name='sample', dlt_path=dlt_path)

    # Generate and save stroboscopic images
    img_process._do_batch('save_strobe', rec_names_to_process=rec_names_to_process, from_fn_name='sample', step_frame=5, radius=60, shape='circle')
    # img_process.do_batch('save_strobe', rec_names_to_process=rec_names_to_process, from_fn_name='sample', step_frame=5, radius=60,
    #                      shape='strip_height', camn_to_process=[2])
    # img_process.do_batch('save_strobe', rec_names_to_process=rec_names_to_process, from_fn_name='sample', step_frame=5, radius=60,
    #                      shape='strip_width', camn_to_process=[1, 3])

    # Rotate view 2 (to always have piston coming from right side)
    img_process._do_batch('rotate', rec_names_to_process=rec_names_to_process, from_fn_name='save_strobe', camn_to_process=[2], degrees=270)

    # Stitch all views together and save in cam_save_paths/_Stitched
    img_process._do_batch('stitch', rec_names_to_process=rec_names_to_process, from_fn_name='save_strobe')

    print('> All processes as been processed (total time elapsed: {0:.4f} s)'.format(time.time() - start))
    #exit()