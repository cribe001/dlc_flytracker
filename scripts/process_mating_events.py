from process.batch_processing import BatchProcessing
import numpy as np

img_process = BatchProcessing()

img_process.cam_paths = {1: 'F:\\Photron1', 2: 'Y:\\', 3: 'Z:\\'}
img_process.cam_save_paths = {1: 'F:\\Photron1\\_Process', 2: 'Y:\\_Process', 3: 'Z:\\_Process'}

img_process.init_paths_names()
rec_names_to_process = {}
for camn in range(1, img_process.number_cam + 1):
    rec_names_to_process[camn] = [s for s in img_process.all_folders_rec_cam[camn] if '20200316' in s]

# Compare observed mating event with recordings (delete others)
mating_events = np.genfromtxt('F:\\mating_events.csv', delimiter=';', skip_header=0, names=True)
rec_mating_names = ['cam{0}_'.format(int(camn)) + "{:06d}".format(int(mating_events['day'][i]))
                    + '_' + "{:06d}".format(int(mating_events['time'][i])) for i, camn in
                    enumerate(mating_events['camn'])]

rec_cnt_be_found = list(set(rec_mating_names) - set(img_process.all_folders_rec_cam[mating_events['camn'][0]]))
if len(rec_cnt_be_found) > 0:
    print('>>> recordings that couldnt be found:' + str(rec_cnt_be_found))
    exit()

rec_to_delete = (list(set(img_process.all_folders_rec_cam[mating_events['camn'][0]]) - set(rec_mating_names)))
img_process._do_batch('delete', rec_names_to_process=rec_names_to_process, to_del_names=rec_to_delete)

# Delete all without correspondings recording for camera 2 and 3
img_process.main_cam = 2
img_process._do_batch('delete', rec_names_to_process=rec_names_to_process, to_del_names=[])
img_process.main_cam = 3
img_process._do_batch('delete', rec_names_to_process=rec_names_to_process, to_del_names=[])