import os
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from PIL import Image

sides_to_plot = ['right', 'left']
body_parts = ['proboscis tip', 'head', 'torso', 'abdomen']
plot_body = True

nb_cam = 3
camn = 1
scorer = 'Antoine Cribellier'

colors = ['b', 'g', 'r'] #

dlc_path = '/home/user/Desktop/Antoine/_maDLC'
labeled_path = os.path.join(dlc_path, 'labeled-data')

all_folders = sorted(os.listdir(labeled_path), key=lambda x: x[5:20])
rec_names = [s for s in all_folders if 'cam{0}_'.format(camn) in s and not '_labeled' in s]

cpt_image, cpt_image_with_label = 0, 0
for rec_name in rec_names:
    all_file_rec = sorted(os.listdir(os.path.join(labeled_path, rec_name)), key=lambda x: x[5:20])
    csv_paths = [s for s in all_file_rec if '.csv' in s]

    if len(csv_paths) == 0: continue
    df = pd.read_csv(os.path.join(labeled_path, rec_name, csv_paths[0]), index_col=0, header=[0, 1, 2, 3])
    df.columns = df.columns.set_levels([scorer], level="scorer")

    label_names = df.columns.levels[2]
    image_paths = df.index.values

    csv_dict = {}
    for label in label_names:
        csv_dict[label] = [list(df[(scorer, 'individual1', label, 'x')]), list(df[(scorer, 'individual1', label, 'y')])]
        csv_dict[label] = np.transpose(csv_dict[label])

    save_path = os.path.join(labeled_path, rec_name + '_labeled')
    if not os.path.exists(save_path): os.makedirs(save_path)

    for i, image_path in enumerate(image_paths):
        image_name = image_path[image_path.index('img'):]
        found_real = False

        fig = plt.figure()
        ax = fig.add_subplot(111)
        plt.imshow(Image.open(os.path.join(dlc_path, image_path)))
        for label in label_names:
            if any([side in label for side in sides_to_plot]) or any([part in label for part in body_parts]):
                color = colors[int(label[-1])-1]
                ax.scatter(csv_dict[label][i][0], csv_dict[label][i][1], marker='.', color=color, s=10)

            if not np.isnan(csv_dict[label][i][0]): found_real = True

        ax.set_xlabel('x [m]')
        ax.set_ylabel('y [m]')
        # plt.show()

        if found_real:
            plt.savefig(os.path.join(save_path, image_name[:-4] + '_labeled.png'), bbox_inches='tight', pad_inches=0)
            plt.savefig(os.path.join(save_path, image_name[:-4] + '_labeled.pdf'), bbox_inches='tight', pad_inches=0,  format='pdf')

        plt.close()

        cpt_image += 1
        if found_real:
            cpt_image_with_label += 1
        else:
            print('WARN: {0}: didnt found any label for images {1}'.format(rec_name, image_name))

    print('>> {0} has been processed.'.format(rec_name))

print('> all the images have been generated')
print('> There is {0} labeled images (out of {1})'.format(cpt_image_with_label, cpt_image))