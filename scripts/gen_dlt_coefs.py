import sys, os
import numpy as np
from camera import calib

from camera.calib import read_dlt_coefs

corePath = os.path.join("/home", "user", "Desktop", "John", "_Python", "flitrak3d", "camera")
sys.path.append(corePath)

# calib_path = '/home/user/Desktop/John/test_data_escape/calib/'
calib_path = os.path.join(os.getcwd(), '..', 'calib')

img_size = (1024, 1024)
nb_cam = 3


xyz_name = 'xyz_calibration_device_big.csv'
# xy_name = '20200305_xypts.csv'
# dlt_name = '20200305_DLTcoefs-py.csv'
xy_name = '20200306_xypts.csv'
dlt_name = '20200306_DLTcoefs-py.csv'

# xyz_name = 'xyz_calibration_device_small.csv'
# xy_name = '20200401_1531_xypts.csv'
# dlt_name = '20200401_1531_DLTcoefs-py.csv'
# # xy_name = '20200615_xypts-rot.csv'
# # dlt_name = '20200615_DLTcoefs-py.csv'

pts_cams = [[512, 512], [512, 512], [512, 512]]
# pts_cams = [[712, 512], [712, 512], [712, 512]]
# pts_cams = [[712, 712], [712, 712], [712, 712]]

# pts_cams = [[726, 291], [983, 347], [701, 36]]  # 20200304_074905 at frame 1401
pts_cams = [[668, 847], [467, 357], [543, 841]]  # 20200307_082727 at frame 1 (manual tracking on imageJ)
pts_cams = [[590, 872], [480, 453], [531, 863]]  # 20200307_082727 at frame 401 (manual tracking on imageJ)
#pts_cams = [[936, 1011], [1024-365, 465], [544, 859]]  # 20200307_082727 at frame 101 + rotation cam2

#pts_cams = [[758, 610], [60, 272], [938, 626]]  # bead 8 from 20200306 (big)
#pts_cams = [[476.833, 367.5], [256.833, 580.833], [732.833, 380.833]]  # bead 8 from 20200401_1531 (small)


# # flip x coord
# for i, camn in enumerate(range(1, nb_cam + 1)):
#     pts_cams[i] = [img_size[0] - pts_cams[i][0], pts_cams[i][1]]

# # flip y coord
# for i, camn in enumerate(range(1, nb_cam + 1)):
#     pts_cams[i] = [pts_cams[i][0], img_size[1] - pts_cams[i][1]]

xy_path = os.path.join(calib_path, xy_name)
dlt_path = os.path.join(calib_path, dlt_name)

# # # Generate dlt coefficients
# # dlt_path = os.path.join(calib_path, 'test_DLTcoefs-py.csv')
xyz_path = os.path.join(calib_path, xyz_name)
#
# dlt_coefs, _ = calib.gen_dlt_coefs_from_paths(xyz_path, xy_path, points_to_remove=[1, 10, 16, 20], plot_error_analysis=False, img_height=img_size[0])  # big
# dlt_coefs, _ = calib.gen_dlt_coefs_from_paths(xyz_path, xy_path, points_to_remove=[1, 3, 4, 11, 13, 20], plot_error_analysis=False, img_height=img_size[0])  # small
dlt_coefs, _ = calib.gen_dlt_coefs_from_paths(xyz_path, xy_path, points_to_remove=[], plot_error_analysis=True, img_height=img_size[0])  # small
calib.save_dlt_coefs(dlt_coefs, dlt_path)  # TODO re-find points to remove with corrected function

# Load dlt_coefs
dlt_coefs = np.zeros((nb_cam, 11))
dlt_csv = np.genfromtxt(dlt_path, delimiter=',', skip_header=0, names=True)
dlt_csv.dtype.names = ['cam{0}'.format(camn) for camn in range(1, nb_cam + 1)]
for i, camn in enumerate(range(1, nb_cam + 1)):
    dlt_coefs[i] = np.append(np.array(dlt_csv['cam{0}'.format(camn)]), 1)

dlt_coefs = read_dlt_coefs(dlt_path)

# Reconstruct 3d and reproject to 2d
obj_coord = calib.recon3d(3, nb_cam, dlt_coefs, pts_cams)
repro_pts_cams = calib.find2d(nb_cam, dlt_coefs, np.array([obj_coord]))
mean_repro_err = np.sqrt(np.mean(np.sum((repro_pts_cams - pts_cams) ** 2, 1)))

print('> 3d reconstruction = {0}'.format(obj_coord))
print('> diff reprojection = \n{0}'.format(repro_pts_cams - pts_cams))
print('> mean reprojection error = {0}'.format(mean_repro_err))






