import os, time, yaml

from camera import calib
from process.batch_processing import BatchProcessing

from convert_xypts import *

start = time.time()

dlt_path = os.path.join(os.getcwd(), 'data/calib/20200305_DLTcoefs-py.csv')
xyz_path = os.path.join(os.getcwd(), 'data/calib/xyz_calibration_device_big.csv')
csv_wings_geo_path = os.path.join(os.getcwd(), '../acoluzzii_wing.csv')
dlc_cfg_path = '/home/user/Desktop/Antoine/_maDLC/config.yaml'

shuffle = 31
iteration = 70000
model_name = 'DLC_resnet50_EscapeKinematicsmei26shuffle{0}_{1}'.format(shuffle, iteration)

fit_method = '3d'  # '2d' or '3d'
opt_method = 'leastsq'  # 'powell' or 'leastsq' or 'least_squares'
body_param_names = ['yaw_a', 'pitch_a', 'roll_a', 'x_com', 'y_com', 'z_com']
wing_param_names = ['stroke_a', 'deviation_a', 'rotation_a']

img_size = (1024, 1024)

img_process = BatchProcessing()
img_process.cam_paths = {1: '/media/user/MosquitoEscape_Photron1/Photron1',
                         2: '/media/user/MosquitoEscape_Photron2/Photron2',
                         3: '/media/user/MosquitoEscape_Photron3/Photron3'}
img_process.cam_save_paths = {1: '/media/user/MosquitoEscape_Photron1/Photron1/_Process',
                              2: '/media/user/MosquitoEscape_Photron2/Photron2/_Process',
                              3: '/media/user/MosquitoEscape_Photron3/Photron3/_Process'}

# Fill dict with name of recordings to process (all recordings from given dates)
dates_to_process = ['20200207', '20200209', '20200211', '20200212', '20200213', '20200216', '20200217',
                    '20200224', '20200225', '20200229', '20200301', '20200302', '20200303', '20200304', '20200305']
dates_calib = ['20200208', '20200208', '20200208', '20200208', '20200215', '20200215', '20200215',
               '20200219', '20200221', '20200228', '20200228', '20200305', '20200305', '20200305', '20200305']

# Generate dlt coefficients
for date_calib in np.unique(dates_calib):
    print('> Generating calibration for {0}...'.format(date_calib))
    xy_path = os.path.join(os.getcwd(), 'data/calib/' + date_calib + '_xypts.csv')
    # xy_path2 = os.path.join(os.getcwd(), 'data/calib/' + date_calib + '_xypts2.csv')
    #
    # convert_xypts_from_matlab(xy_path, xy_path2, 3, img_size)

    # xy_rot_path = os.path.join(os.getcwd(), 'data/calib/' + date_calib + '_xypts-rot.csv')
    # rotate_xypts(xy_path, xy_rot_path, 3, [2], 90, img_size)

#     dlt_path = os.path.join(os.getcwd(), 'data/calib/' + date_calib + '_DLTcoefs-py.csv')
#     dlt_coefs, _ = calib.gen_dlt_coefs_from_paths(xyz_path, xy_path2, points_to_remove=[1, 10, 16, 20], plot_error_analysis=False, img_height=img_size[0])
    dlt_coefs, _ = calib.gen_dlt_coefs_from_paths(xyz_path, xy_path, points_to_remove=[], plot_error_analysis=True, img_height=img_size[0])
    calib.save_dlt_coefs(dlt_coefs, dlt_path) # TODO re-find points to remove with corrected function + check if coord from matlab (is so use img_height=img_size[0])=True)

img_process.show_plot = False
img_process.update_leading_zero = False
img_process.init_paths_names()

for i, date in enumerate(dates_to_process):
    # Update dlt coefficients
    dlt_path = os.path.join(os.getcwd(), 'data/calib/' + dates_calib[i] + '_DLTcoefs-py.csv')
    img_process.load_dlt_coefs(dlt_path)

    processes_dict = yaml.load(open('../processes.yaml'), Loader=yaml.SafeLoader)
    for processes_num in list(processes_dict.keys())[1:]:
        if 'fn' in processes_dict[processes_num].keys() and processes_dict[processes_num]['fn'] in ['recon3d']:
            processes_dict[processes_num]['kwargs']['dlt_path'] = dlt_path

    with open(os.path.join('processes_updated.yaml'), 'w') as yaml_file:
        yaml.safe_dump(processes_dict, yaml_file, encoding='utf-8', allow_unicode=True)

    # Select dates to process
    rec_names_to_process = {}
    for camn in range(1, img_process.nb_cam + 1):
        rec_names_to_process[camn] = [s for s in img_process.all_folders_rec_cam[camn] if date in s]

    # # Keep only rec_names that haven't been processed yet
    # non_processed_rec_names = {1: [], 2: [], 3: []}
    # for camn in range(1, img_process.nb_cam + 1):
    #     for i, rec_name in enumerate(rec_names_to_process[camn]):
    #         if not os.path.exists(os.path.join(img_process.cam_save_paths[camn], '_Avi', rec_name)):
    #             non_processed_rec_names[camn].append(rec_names_to_process[camn][i])
    #
    # rec_names_to_process = non_processed_rec_names

    # # Do cropping, rotating, stitching, etc at once and save as .avi (save much time and space)
    # img_process.do_batch('multi_processes', rec_names_to_process=rec_names_to_process, yaml_path='processes_updated.yaml', delete_previous=True)

    # # Do 2d tracking (blob detection) on images in cam_save_paths/_Sample
    # img_process.do_batch('track2d', rec_names_to_process=rec_names_to_process, from_fn_name='sample')
    #
    # # Do 3d reconstruction of tracks
    # img_process.do_batch('recon3d', rec_names_to_process=rec_names_to_process, from_fn_name='sample', dlt_path=dlt_path)

    # # Track features using DeepLabCut
    # img_process.do_batch('analyse_dlc', rec_names_to_process=rec_names_to_process, from_fn_name='save_avi', cfg_path=dlc_cfg_path,
    #                     shuffle=shuffle, trainingsetindex=0, batch_size=5, save_avi=False, model_name=model_name, delete_previous=True)

    # # Load (+ filtering and unscrambling) 2d coords from DLC + Reverse processes (unstitch, rotate back, uncrop) + Reconstruct 3d coord
    # img_process.do_batch('load_dlc', rec_names_to_process=rec_names_to_process, from_fn_name='analyse_dlc', model_name=model_name,
    #                       tracker_method='skeleton', dlt_path=dlt_path)

    # Optimize fit of skeleton to find body and wings anglJues
    img_process.do_multiprocessing = True
    img_process.threshold_likelihood = 0.85
    img_process._do_batch('fit_skeleton', rec_names_to_process=rec_names_to_process, from_fn_name='load_dlc', model_name=model_name,
                          csv_path=csv_wings_geo_path, body_param_names=body_param_names, wing_param_names=wing_param_names,
                          animal_name="fly", fit_method=fit_method, opt_method=opt_method, dlt_path=dlt_path)

    print('> All processes as been processed (total time elapsed: {0:.4f} s)'.format(time.time() - start))
