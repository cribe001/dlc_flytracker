import os
import numpy as np
import pandas as pd
import deeplabcut as dlc
import pickle, glob, copy

# shuffle = 29
# trainingsetindex = 0
# individual = 0

nb_cam = 3
camn = 1
video_format = 'avi'

convert_to_h5 = True
convert_to_csv = True

cfg_path = '/home/user/Desktop/Antoine/_maDLC/config.yaml'
# avi_path = '/media/user/MosquitoEscape_Photron1/Photron1/_Process/_Avi'
# dlc_path = '/media/user/MosquitoEscape_Photron1/Photron1/_Process/_DLC'
#
# rec_names = ['cam1_20200303_030117']
# all_folders = sorted(os.listdir(avi_path), key=lambda x: x[5:20])
# rec_names = [s for s in all_folders if 'cam{0}_'.format(camn) in s]

# to analyse all _full.pickle in one folder
avi_path = '/home/user/Desktop/Antoine/_maDLC'
rec_names = ['videos']

init_tracklets_path = 'init_tracklet_sk.pickle'

with open(init_tracklets_path, "rb") as file:
    init_tracklets_dict = pickle.load(file)

for rec_name in rec_names:
    # Convert _full.pickle to fake _sk.pickle (keeping raw detection)

    #save_path = os.path.join(dlc_path, rec_name)
    save_path = avi_path

    if not os.path.exists(save_path): os.makedirs(save_path)
    full_pickle_paths = glob.glob(os.path.join(save_path, rec_name, '*_full.pickle'))

    for full_pickle_path in full_pickle_paths:
        tracklet_pickle_path = full_pickle_path[:-len('_full.pickle')] + '_sk.pickle'
    
        with open(full_pickle_path, "rb") as file:
            full_dict = pickle.load(file)
    
        header = full_dict.pop("metadata")
        label_full_names = header["all_joints_names"]

        # Fill tracklet dict with all not filtered detection (use the detection with highest likelihood)
        tracklets_dict = copy.deepcopy(init_tracklets_dict)
        label_tracklet_names = [s[1] for s in tracklets_dict['header']]
        frames = list(tracklets_dict.keys())[:-1]

        # for key in list(tracklets_dict.keys()):
        #     if key in [0, 'header']: continue
        #     tracklets_dict[0].update(tracklets_dict.pop(key))

        for i, frame in enumerate(frames):
            #frame_name = 'frame{0:04d}'.format(frame)
            frame_name = label_tracklet_names[i]

            for label_full_in, label_name in enumerate(label_full_names):
                label_tracklet_in = label_tracklet_names.index(label_name)
                if len(full_dict[frame_name]['confidence'][label_full_in]) == 0: continue
                best_in = np.argmax(full_dict[frame_name]['confidence'][label_full_in])

                # tracklets_dict[0][frame_name][label_tracklet_in] = full_dict[frame_name]['coordinates'][0][label_full_in][best_in][0]
                # tracklets_dict[0][frame_name][label_tracklet_in +1] = full_dict[frame_name]['coordinates'][0][label_full_in][best_in][1]
                # tracklets_dict[0][frame_name][label_tracklet_in +2] = full_dict[frame_name]['confidence'][label_full_in][best_in]

                tracklets_dict[frame][frame_name][label_tracklet_in] = full_dict[frame_name]['coordinates'][0][label_full_in][best_in][0]
                tracklets_dict[frame][frame_name][label_tracklet_in + 1] = full_dict[frame_name]['coordinates'][0][label_full_in][best_in][1]
                tracklets_dict[frame][frame_name][label_tracklet_in + 2] = full_dict[frame_name]['confidence'][label_full_in][best_in]

        with open(tracklet_pickle_path, "wb") as f:
            pickle.dump(tracklets_dict, f, pickle.HIGHEST_PROTOCOL)

        print('>> {0} has been saved.'.format(tracklet_pickle_path))

        if convert_to_h5:  # Convert .pickle with tracklets to .h5
            dlc.convert_raw_tracks_to_h5(cfg_path, tracklet_pickle_path, min_tracklet_len=0)

    if convert_to_csv:  # Convert .pickle with tracklets to .csv
        dlc.analyze_videos_converth5_to_csv(os.path.join(avi_path, rec_name), videotype=video_format)



