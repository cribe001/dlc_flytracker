import os, time, yaml
from process.batch_processing import BatchProcessing
import numpy as np

selection_csv = 'selection_escapes.csv'
selection_csv = 'selection_escapes_very_good.csv'
selection_csv = 'selection_escapes_very_good-troubleshooting.csv'
# selection_csv = 'selection_bad_tracking.csv'

dlt_path = os.path.join(os.getcwd(), 'data/calib/20200305_DLTcoefs-py.csv')
csv_wings_geo_path = os.path.join(os.getcwd(), '../acoluzzii_wing.csv')
dlc_cfg_path = '/home/user/Desktop/Antoine/_maDLC/config.yaml'


img_process = BatchProcessing()
img_process.cam_paths = {1: '/media/user/MosquitoEscape_Photron1/Photron1',
                         2: '/media/user/MosquitoEscape_Photron2/Photron2',
                         3: '/media/user/MosquitoEscape_Photron3/Photron3'}
img_process.cam_save_paths = {1: '/media/user/MosquitoEscape_Photron1/Photron1/_Process',
                              2: '/media/user/MosquitoEscape_Photron2/Photron2/_Process',
                              3: '/media/user/MosquitoEscape_Photron3/Photron3/_Process'}

# Fill dict with name of recordings to process (all recordings from given dates)
dates_to_process = ['20200207', '20200209', '20200211', '20200212', '20200213', '20200216', '20200217',
                    '20200224', '20200225', '20200229', '20200301', '20200302', '20200303', '20200304', '20200305']
dates_calib = ['20200208', '20200208', '20200208', '20200208', '20200215', '20200215', '20200215',
               '20200219', '20200221', '20200228', '20200228', '20200305', '20200305', '20200305', '20200305']

shuffle = 31
iteration = 70000
model_name = 'DLC_resnet50_EscapeKinematicsmei26shuffle{0}_{1}'.format(shuffle, iteration)

fit_method = '3d'  # '2d' or '3d'
opt_method = 'leastsq'  # 'powell' or 'leastsq' or 'least_squares'
body_param_names = ['yaw_a', 'pitch_a', 'roll_a', 'x_com', 'y_com', 'z_com']
wing_param_names = ['stroke_a', 'deviation_a', 'rotation_a']
#wing_param_names = ['stroke_a', 'deviation_a', 'rotation_a', 'x_hinge', 'y_hinge', 'z_hinge']

img_process.update_leading_zero = False
img_process.init_paths_names()

# # Fill dict with name of recordings to process
# # > either all recordings from a given date
# date_to_process = '20200303'
# rec_names_to_process = {}
# for camn in range(1, img_process.nb_cam + 1):
#     rec_names_to_process[camn] = [s for s in img_process.all_folders_rec_cam[camn] if date_to_process in s]

# > or a given set of recordings (in selection_escapes.csv'
example_escapes = np.genfromtxt(selection_csv, delimiter=',', skip_header=0, names=True)
example_escapes_names = ['cam{0}_'.format(int(camn)) + "{:06d}".format(int(example_escapes['day'][i])) + '_' +
                        "{:06d}".format(int(example_escapes['time'][i])) for i, camn in enumerate(example_escapes['camn'])]

# # Compare selected escapes events (in selection_escapes.csv) with all recordings on HDDs
# rec_cnt_be_found = list(set(example_escapes_names) - set(img_process.all_folders_rec_cam[example_escapes['camn'][0]]))
# if len(rec_cnt_be_found) > 0:
#     print('>>> recordings that couldnt be found:' + str(rec_cnt_be_found))
#     exit()

rec_names_to_process_all = img_process.gen_rec_names_list(example_escapes_names)

#rec_names_to_process_all = {1: ['cam1_20200303_030117'], 2: ['cam2_20200303_030120'], 3: ['cam3_20200303_030120']}
#rec_names_to_process_all = {1: ['cam1_20200303_041412'], 2: ['cam2_20200303_041415'], 3: ['cam3_20200303_041415']}
#rec_names_to_process_all = {1: ['cam1_20200303_043202'], 2: ['cam2_20200303_043204'], 3: ['cam3_20200303_043204']}
rec_names_to_process_all = {1: ['cam1_20200305_025327'], 2: ['cam2_20200305_025333'], 3: ['cam3_20200305_025333']}
img_process.show_plot = False

for i, date in enumerate(dates_to_process):
    start = time.time()

    # Update dlt coefficients
    dlt_path = os.path.join(os.getcwd(), 'data/calib/' + dates_calib[i] + '_DLTcoefs-py.csv')
    img_process.load_dlt_coefs(dlt_path)

    processes_dict = yaml.load(open('../processes.yaml'), Loader=yaml.SafeLoader)
    for processes_num in list(processes_dict.keys())[1:]:
        if 'fn' in processes_dict[processes_num].keys() and processes_dict[processes_num]['fn'] in ['recon3d']:
            processes_dict[processes_num]['kwargs']['dlt_path'] = dlt_path

    with open(os.path.join('processes_updated.yaml'), 'w') as yaml_file:
        yaml.safe_dump(processes_dict, yaml_file, encoding='utf-8', allow_unicode=True)

    # Select dates to process
    rec_names_to_process = {}
    for camn in range(1, img_process.nb_cam + 1):
        rec_names_to_process[camn] = [s for s in img_process.all_folders_rec_cam[camn] if date in s]
        rec_names_to_process[camn] = [s for s in rec_names_to_process[camn] if s in rec_names_to_process_all[camn]]

    if len(rec_names_to_process[camn]) == 0: continue

    # # Do cropping, rotating, stitching, etc at once and save as .avi (save much time and space)
    # img_process.do_batch('multi_processes', rec_names_to_process=rec_names_to_process, yaml_path='config.yaml', delete_previous=True)

    # # Convert images to 8 bits and reduce frame_rate (save in cam_save_paths/_Sample)
    # img_process.do_batch('sample', rec_names_to_process=rec_names_to_process, step_frame=50, delete_previous=True)

    # # Do 2d tracking (blob detection) on images in cam_save_paths/_Sample
    # img_process.do_batch('track2d', rec_names_to_process=rec_names_to_process, from_fn_name='sample')

    # # Do 3d reconstruction of tracks
    # img_process.do_batch('recon3d', rec_names_to_process=rec_names_to_process, from_fn_name='sample', dlt_path=dlt_path)

    # # Crop all frames and save in cam_save_paths/_Cropped
    # img_process.do_batch('crop', rec_names_to_process=rec_names_to_process, from_fn_name='sample', height_crop=200, width_crop=200, delete_previous=True)
    #
    # # Rotate view 2 (to always have piston coming from right side)
    # img_process.do_batch('rotate', rec_names_to_process=rec_names_to_process, from_fn_name='crop', camn_to_process=[2], degrees=270)
    #
    # # Upsizing of the images (increase resolution)
    # #img_process.do_batch('resize', rec_names_to_process=rec_names_to_process, resize_ratio=2, from_fn_name='crop', delete_previous=True)
    #
    # # Enhance images (increase sharpness and contrast)
    # # img_process.do_batch('enhance_sharpness', factor=2.0, rec_names_to_process=rec_names_to_process, from_fn_name='resize', delete_previous=True)
    # #img_process.do_batch('enhance_contrast', factor=1.5, rec_names_to_process=rec_names_to_process, from_fn_name='enhance_sharpness')
    #
    # # Stitch all views together and save in cam_save_paths/_Stitched
    # img_process.do_batch('stitch', rec_names_to_process=rec_names_to_process, from_fn_name='crop', delete_previous=True)

    # # Save recordings in .avi
    # img_process.do_batch('save_avi', rec_names_to_process=rec_names_to_process, from_fn_name='stitch', lossless=False, delete_previous=True)

    # # Track features using DeepLabCut
    # img_process.do_batch('analyse_dlc', rec_names_to_process=rec_names_to_process, from_fn_name='save_avi', cfg_path=dlc_cfg_path,
    #                      shuffle=shuffle, trainingsetindex=0, batch_size=5, save_avi=False, model_name=model_name,
    #                      delete_previous=True)
    #
    # # Load (+ filtering and unscrambling) 2d coords from DLC + Reverse processes (unstitch, rotate back, uncrop) + Reconstruct 3d coord
    # img_process.do_batch('load_dlc', rec_names_to_process=rec_names_to_process, from_fn_name='analyse_dlc', model_name=model_name,
    #                      tracker_method='skeleton', dlt_path=dlt_path)

    # Optimize fit of skeleton to find body and wings angles
    img_process.do_multiprocessing = True
    img_process.threshold_likelihood = 0.8
    img_process._do_batch('fit_skeleton', rec_names_to_process=rec_names_to_process, from_fn_name='load_dlc', model_name=model_name,
                          csv_path=csv_wings_geo_path, body_param_names=body_param_names, wing_param_names=wing_param_names,
                          animal_name="fly", fit_method=fit_method, opt_method=opt_method, dlt_path=dlt_path,
                          angles_to_correct=['stroke_a', 'deviation_a'])

    # Save .png of skeleton fit on raw images
    img_process._do_batch('plot_skeleton', rec_names_to_process=rec_names_to_process, from_fn_name='analyse_dlc', model_name=model_name,
                          csv_path=csv_wings_geo_path, dlt_path=dlt_path, save_avi=True)

    print('> All processes as been processed (total time elapsed: {0:.4f} s)'.format(time.time() - start))


## Tricks:
## - check match of legs knowing that the distances (width) between point in two views should be cst
## - try to match each legs on one views with all legs on the other, use the best match (lowest error)
## - get best 2 views match out of the 3 views if match all 3 views give poor results
## - lengths of the wings (spam and chord), of hinge to hinge, of the abdomen to head-torso and of proboscis tip to head-torso are cst (can use the mode and filter out values that are too diiferent)
## -
## - filter/smooth using kalman filter (both way?)

# Find out if the legs are touching the piston