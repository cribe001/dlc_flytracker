import os, yaml
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt

from skeleton_fitter.modules.limbs.insect_wings_flat_geo import build_skeleton3d as wings_geo_build_skeleton3d
from skeleton_fitter.utils import length_from_vector


def update_wings_init_skeleton3d(init_yaml_path: str, csv_path: str):
    """
        Update the initial coordinates of the wings 3d skeleton (in  init_yaml_path) using the 2d coordinates in csv_path

    Args:
        init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
        csv_path: Path of a csvs file that contain 2d coordinates of a fly wing
    """

    with open(init_yaml_path, 'r') as file:
        init_dict = yaml.safe_load(file)

    wing_params = init_dict['init_params']['limbs']['wing']

    labels = ['hinge', 'leading_edge_q1', 'leading_edge_q2', 'leading_edge_q3', 'tip', 'trailing_edge_q3',
              'trailing_edge_q2', 'trailing_edge_q1']

    wing_csv = pd.read_csv(csv_path, names=['y', 'x'])
    span = np.max(wing_csv['y'])

    # Initialise and get right order
    wing_skeleton3d = {}
    for label in labels:
        wing_skeleton3d[label] = np.array([0.0, 0.0, 0.0])

    # Initialise wings skeleton with 8 points around circle in xy plan (hinge at [0, 0, 0], tip at [+/- span, 0, 0])
    nb_parts = 3
    signe = 1  # - for right wing, + for left wing
    wing_skeleton3d['tip'][1] = span * signe

    for n in range(1, nb_parts + 1):
        y_part = span * n / (nb_parts + 1)
        y_transposed = wing_csv['y'] - y_part

        # find largest negative and smallest positive y for positive x (leading) or negative x (trailing)
        in_nearest_leading = \
            [np.where(np.where(wing_csv['x'] > 0, y_transposed, np.inf) < 0, y_transposed, -np.inf).argmax(),
             np.where(np.where(wing_csv['x'] > 0, y_transposed, -np.inf) > 0, y_transposed, np.inf).argmin()]
        in_nearest_trailing = \
            [np.where(np.where(wing_csv['x'] < 0, y_transposed, np.inf) < 0, y_transposed, -np.inf).argmax(),
             np.where(np.where(wing_csv['x'] < 0, y_transposed, -np.inf) > 0, y_transposed, np.inf).argmin()]

        x_leading = np.interp(y_part, wing_csv['y'][in_nearest_leading], wing_csv['x'][in_nearest_leading])
        x_trailing = np.interp(y_part, wing_csv['y'][in_nearest_trailing], wing_csv['x'][in_nearest_trailing])

        wing_skeleton3d['leading_edge_q' + str(n)] = np.array([x_leading, signe * y_part, 0])
        wing_skeleton3d['trailing_edge_q' + str(n)] = np.array([x_trailing, signe * y_part, 0])

        span_v = wing_skeleton3d['tip'] - wing_skeleton3d['hinge']
        chord_v = wing_skeleton3d['leading_edge_q2'] - wing_skeleton3d['trailing_edge_q2']
        wing_params['aspect_ratio'] = float(np.round(length_from_vector(span_v), 9) / np.round(length_from_vector(chord_v), 9))
        wing_params['chord'] = float(wing_params['span']/wing_params['aspect_ratio'])
        # print(aspect_ratio)

    # shape_x = np.array(wing_csv['x']).shape
    # index_prev_labels = np.ones(shape_x) * np.nan
    # for i, label in enumerate(labels):
    #     contour3d = np.array([np.array(wing_csv['x']), np.array(wing_csv['y']), np.zeros(shape_x)]).T
    #     distances = np.sqrt(np.sum((np.tile(wing_skeleton3d['left'][label].T, (shape_x[0], 1)) - contour3d) ** 2, axis=1))
    # 
    #     in_min_2distances = np.sort(np.argsort(distances)[0:2])
    #     if in_min_2distances[0] == 0: in_min_2distances = np.flip(in_min_2distances)  # for hinge
    #     index_prev_labels[in_min_2distances[1]:] = i

    for label in wing_skeleton3d.keys():
        wing_skeleton3d[label] = [float(wing_skeleton3d[label][0]),
                                   float(wing_skeleton3d[label][1]),
                                   float(wing_skeleton3d[label][2])]  # Convert numpy arrays to lists of floats

    init_dict['init_skeleton3d']['limbs']['wing'] = wing_skeleton3d

    init_yaml_path = init_yaml_path[:-5] + '-updated.yaml'
    with open(init_yaml_path, 'w+') as file:
        yaml.safe_dump(init_dict, file, encoding='utf-8', allow_unicode=True)


def update_geo_wings_init_skeleton3d(init_yaml_path: str, csv_path: str):
    """
        Update the initial coordinates of the wings 3d skeleton (in  init_yaml_path) using the 2d coordinates in csv_path

    Args:
        init_yaml_path: Path of yaml file that contains initial kinematic parameters, bounds for these parameters
                as well as 3d skeleton coordinates for a given animal (e.g. mosquito)
        csv_path: Path of a csvs file that contain 2d coordinates of a fly wing
    """

    with open(init_yaml_path, 'r') as file:
        init_dict = yaml.safe_load(file)

    wing_params = init_dict['init_params']['limbs']['wing']

    labels = ['hinge', 'leading_edge_q1', 'leading_edge_q2', 'leading_edge_q3', 'tip', 'trailing_edge_q3',
              'trailing_edge_q2', 'trailing_edge_q1']  # to get the right indices in wing_geometry3d['index_prev_labels']

    wing_csv = pd.read_csv(csv_path, names=['y', 'x'])
    span = np.max(wing_csv['y'])

    # Initialise and get right order
    wing_skeleton3d = {}
    for label in labels:
        wing_skeleton3d[label] = np.array([0.0, 0.0, 0.0])

    # Initialise wings skeleton with 8 points around circle in xy plan (hinge at [0, 0, 0], tip at [+/- span, 0, 0])

    nb_parts = 3
    signe = 1  # - for right wing, + for left wing
    wing_skeleton3d['tip'][1] = span * signe

    for n in range(1, nb_parts + 1):
        y_part = span * n / (nb_parts + 1)
        y_transposed = wing_csv['y'] - y_part

        # find largest negative and smallest positive y for positive x (leading) or negative x (trailing)
        in_nearest_leading = \
            [np.where(np.where(wing_csv['x'] > 0, y_transposed, np.inf) < 0, y_transposed, -np.inf).argmax(),
             np.where(np.where(wing_csv['x'] > 0, y_transposed, -np.inf) > 0, y_transposed, np.inf).argmin()]
        in_nearest_trailing = \
            [np.where(np.where(wing_csv['x'] < 0, y_transposed, np.inf) < 0, y_transposed, -np.inf).argmax(),
             np.where(np.where(wing_csv['x'] < 0, y_transposed, -np.inf) > 0, y_transposed, np.inf).argmin()]

        x_leading = np.interp(y_part, wing_csv['y'][in_nearest_leading], wing_csv['x'][in_nearest_leading])
        x_trailing = np.interp(y_part, wing_csv['y'][in_nearest_trailing], wing_csv['x'][in_nearest_trailing])

        wing_skeleton3d['leading_edge_q' + str(n)] = np.array([x_leading, signe * y_part, 0])
        wing_skeleton3d['trailing_edge_q' + str(n)] = np.array([x_trailing, signe * y_part, 0])

    span_v = wing_skeleton3d['tip'] - wing_skeleton3d['hinge']
    chord_v = wing_skeleton3d['leading_edge_q2'] - wing_skeleton3d['trailing_edge_q2']
    wing_params['aspect_ratio'] = float(np.round(length_from_vector(span_v), 9) / np.round(length_from_vector(chord_v), 9))
    wing_params['chord'] = float(wing_params['span']/wing_params['aspect_ratio'])

    shape_x = np.array(wing_csv['x']).shape
    nb_pts_per_label = len(wing_csv['x'])/len(labels)
    assert nb_pts_per_label % 1 == 0  # check that nb_pts_per_label is an integer

    nb_pts_before = int(np.floor(nb_pts_per_label/2))
    nb_pts_after = int(np.ceil(nb_pts_per_label/2))

    index_prev_labels = np.ones(shape_x) * np.nan
    for ind_label, label in enumerate(labels):
        contour3d = np.array([np.array(wing_csv['x']), np.array(wing_csv['y']), np.zeros(shape_x)]).T
        distances = np.sqrt(np.sum((np.tile(wing_skeleton3d[label].T, (shape_x[0], 1)) - contour3d) ** 2, axis=1))

        in_min_2distances = np.sort(np.argsort(distances)[0:2])
        if 0 in in_min_2distances:  # for hinge
            in_min_2distances = np.flip(in_min_2distances)
            index_prev_labels[in_min_2distances[1]:] = ind_label
        else:
            index_prev_labels[in_min_2distances[1] - nb_pts_before:-nb_pts_after] = ind_label

    wing_geometry3d = {}
    for ind_label, label in enumerate(labels):
        num_occurrence = np.count_nonzero(index_prev_labels == ind_label)
        min_i = -int(np.ceil(num_occurrence/2))
        max_i = int(np.floor(num_occurrence/2))

        for i in range(min_i+1, max_i+1):
            if i == 0: new_label = label  # At the original position of the label
            else: new_label = label + '_{0}'.format(i)  # around (negative number are before, positive are after)

            ind_new_label = np.where(index_prev_labels == ind_label)[0][i]
            wing_geometry3d[new_label] = np.array([np.array(wing_csv['x'][ind_new_label]),
                                                   np.array(wing_csv['y'][ind_new_label]), 0.0])

    wing_geometry3d = wings_geo_build_skeleton3d(wing_geometry3d, wing_params)

    # Save wing skeleton (only 8 labels)
    for label in wing_skeleton3d.keys():
        wing_skeleton3d[label] = [float(wing_skeleton3d[label][0]),
                                  float(wing_skeleton3d[label][1]),
                                  float(wing_skeleton3d[label][2])]  # Convert numpy arrays to lists of floats

    init_dict['init_skeleton3d']['limbs']['wing'] = wing_skeleton3d
    init_yaml_path = init_yaml_path[:-5] + '-updated.yaml'
    with open(init_yaml_path, 'w+') as file:
        yaml.safe_dump(init_dict, file, encoding='utf-8', allow_unicode=True)

    # Save wing geometry
    for label in wing_geometry3d.keys():
        wing_geometry3d[label] = [float(wing_geometry3d[label][0]),
                                  float(wing_geometry3d[label][1]),
                                  float(wing_geometry3d[label][2])]  # Convert numpy arrays to lists of floats

    init_dict['init_skeleton3d']['limbs']['wing'] = wing_geometry3d
    init_yaml_path = init_yaml_path[:-5] + '-updated_geo.yaml'
    with open(init_yaml_path, 'w+') as file:
        yaml.safe_dump(init_dict, file, encoding='utf-8', allow_unicode=True)


def main():
    # Mosquitoes
    # csv_path = os.path.join(os.getcwd(), '../data/mosquito_escapes/acoluzzii_wing.csv')
    #
    # init_yaml_path = os.path.join(os.getcwd(), '../data/mosquito_escapes/initial_parameters/mosquito.yaml')
    # update_wings_init_skeleton3d(init_yaml_path, csv_path)
    #
    # init_yaml_path = os.path.join(os.getcwd(), '../data/mosquito_escapes/initial_parameters/mosquito_hybrid.yaml')
    # update_wings_init_skeleton3d(init_yaml_path, csv_path)
    #
    # init_yaml_path = os.path.join(os.getcwd(), '../data/mosquito_escapes/initial_parameters/mosquito_geo.yaml')
    # update_geo_wings_init_skeleton3d(init_yaml_path, csv_path)
    #
    # # Plot the wing in 2d
    # wing_csv = pd.read_csv(csv_path, names=['x', 'y'])
    #
    # fig = plt.figure()
    # ax = fig.add_subplot(111)
    # ax.scatter(wing_csv['x'], wing_csv['y'])
    # ax.set_xlabel('x [mm]')
    # ax.set_ylabel('y [mm]')
    # ax.axis('equal')
    # plt.grid()
    # plt.show()

    # ------------------------------------------------------------------------------------------------------------------
    # Bumblebees
    csv_path = os.path.join(os.getcwd(), '../data/bumblebee/bumblebee_wing_xy.csv')

    # old_csv_path = os.path.join(os.getcwd(), '../data/bumblebee/bumblebee_wing_xy-non_aligned.csv')
    # wing_csv = pd.read_csv(old_csv_path, names=['x', 'y'])
    #
    # wing_csv['x'] = wing_csv['x'] - 9.89
    # wing_csv['y'] = wing_csv['y'] - 6.63
    #
    # x_ori, y_ori = [0, 0]  # Coordinates of the point to rotate the wing around
    # angle_deg = 152.5
    # angle_rad = np.deg2rad(angle_deg)
    # for i in range(0, len(wing_csv['x'])):
    #     x_new = x_ori + np.cos(angle_rad) * (wing_csv['x'][i] - x_ori) - np.sin(angle_rad) * (wing_csv['y'][i] - y_ori)
    #     y_new = y_ori + np.sin(angle_rad) * (wing_csv['x'][i] - x_ori) + np.cos(angle_rad) * (wing_csv['y'][i] - y_ori)
    #
    #     wing_csv['x'][i], wing_csv['y'][i] = x_new, y_new
    #
    # wing_csv = wing_csv.drop(list(range(1704, 1706)))  # Drop last two points to have a # of points that can be divided by # of labels (=8)
    # wing_csv['x'][0], wing_csv['y'][0] = 0, 0  # To make sure the first point is at the hinge
    # wing_csv['x'] = wing_csv['x'] - np.min(wing_csv['x'])
    #
    # wing_csv.to_csv(csv_path, header=False, index=False)
    #
    # # Plot the wing in 2d
    # fig2 = plt.figure()
    # ax2 = fig2.add_subplot(111)
    # ax2.scatter(wing_csv['x'], wing_csv['y'])
    # ax2.set_xlabel('x [mm]')
    # ax2.set_ylabel('y [mm]')
    # ax2.axis('equal')
    # plt.grid()
    # plt.show()

    init_yaml_path = os.path.join(os.getcwd(), '../data/bumblebee/initial_parameters/bumblebee_fly_hybrid.yaml')
    update_geo_wings_init_skeleton3d(init_yaml_path, csv_path)


if __name__ == "__main__":
    main()

