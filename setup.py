from setuptools import setup

setup(
    name='flitrak3d',
    version='0.0.1',
    packages=[''],
    url='https://github.com/AntoineCribel/dlc_flytracker',
    license='',
    author='Antoine Cribellier',
    author_email='antoine.cribellier@wur.nl',
    description='A fly tracker based on DeepLabCut'
)
