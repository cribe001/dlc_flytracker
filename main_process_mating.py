import os, time

from camera import calib
from process.batch_processing import BatchProcessing
from point_tracker.tracker2d import BlobDetectorSettings
from point_tracker.reconstructor3d import Reconstructor3DSettings

start = time.time()

dlc_cfg_path = '/home/user/Desktop/Antoine/_DLC/config.yaml'

xyz_path = os.path.join(os.getcwd(), 'data/mosquito_escapes/calib/to_delete/xyz_calibration_device_big.csv')
xy_path = os.path.join(os.getcwd(), 'data/mosquito_escapes/calib/to_delete/20200311_xypts.csv')
dlt_path = os.path.join(os.getcwd(), 'data/mosquito_escapes/calib/to_delete/20200311_DLTcoefs-py.csv')
points_to_remove = [16, 20]

# xyz_path = os.path.join(os.getcwd(), 'data/mosquito_escapes/calib/to_delete/xyz_calibration_device_small.csv')
# xy_path = os.path.join(os.getcwd(), 'data/mosquito_escapes/calib/to_delete/20200401_1531_xypts.csv')
# dlt_path = os.path.join(os.getcwd(), 'data/mosquito_escapes/calib/to_delete/20200401_1531_DLTcoefs-py.csv')
# points_to_remove = [1, 3, 4, 11, 20]

# Generate dlt coefficients
# dlt_coefs, _ = calib.gen_dlt_coefs_from_paths(xyz_path, xy_path,
#                                               points_to_remove=points_to_remove, plot_error_analysis=True)
# calib.save_dlt_coefs(dlt_coefs, dlt_path)

recording_paths = ['/media/user/8TB_Mosquito_Mating_Landing_BU/_MatingKinematics/Photron1',
                   '/media/user/8TB_Mosquito_Mating_Landing_BU/_MatingKinematics/Photron2',
                   '/media/user/8TB_Mosquito_Mating_Landing_BU/_MatingKinematics/Photron3']

save_path = '/media/user/8TB_Mosquito_Mating_Landing_BU/_MatingKinematics/_Process'
framerate = 12500

update_leading_zero = False
max_diff_date_s = 90
show_plot = False

# ------------------------------------------------------------------------------------------------------------------
# Initialization of the BatchProcessing class
img_process = BatchProcessing.from_directory_by_dates(recording_paths, framerate, save_path, threshold_s=90)
rec_names_to_process = 'cam1_20200316_165330'
rec_names_to_process = 'all'

# # Fill dict with name of recordings to process
# # > either all recordings from a given date
# date_to_process = '20200303'
# rec_names_to_process = {}
# for camn in range(1, img_process.nb_cam + 1):
#     rec_names_to_process[camn] = [s for s in img_process.all_folders_rec_cam[camn] if date_to_process in s]

# ------------------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------------------
# Will prepare recordings to be used by Deeplabcut (do each step separately)

# # ------------------------------------------------------------------------------------------------------------------
# # Do manual 2d tracking and then 3d reconstruction of tracks
# recon_sets = Reconstructor3DSettings(threshold_mean_repro_err=15)
# img_process.manual_tracking(dlt_path, reconstructor_settings=recon_sets, delete_previous=False,
#                             rec_names_to_process=rec_names_to_process, show_plot=show_plot)

# ------------------------------------------------------------------------------------------------------------------
# Crop all frames and save in save_path + '/_Cropped'
img_process.dynamic_crop_images(200, 200, from_tracks=True, show_plot=False,
                                rec_names_to_process=rec_names_to_process, delete_previous=True)

# ------------------------------------------------------------------------------------------------------------------
# Enhance image contrast from path '/_Cropped'
img_process.auto_contrast_images(rec_names_to_process=rec_names_to_process, from_fn_name='crop', delete_previous=True)

# ------------------------------------------------------------------------------------------------------------------
# Stitch all views together (for each frame) and save in save_path + '/_Stitched'
img_process.stitch_images(rec_names_to_process=rec_names_to_process, from_fn_name='autocontrast', delete_previous=True)

# ------------------------------------------------------------------------------------------------------------------
# Save each recording in an .avi in save_path + '/_Avi'
img_process.save_avi(rec_names_to_process=rec_names_to_process, from_fn_name='stitch', delete_previous=True)

print('> All processes as been processed (total time elapsed: {0:.4f} s)'.format(time.time() - start))